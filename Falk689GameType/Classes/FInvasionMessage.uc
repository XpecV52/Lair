class FInvasionMessage extends KFInvasionMessage;

static function string GetNameOf(Class<Monster> OClass)
{
	local string S;

	S = OClass.Default.MenuName;

	if(S == "")
		S = string(OClass.Name);

	if (class<FalkMonsterBase>(OClass) != none && class<FalkMonsterBase>(OClass).Default.FIsBoss)
		Return "The"@S;

	else if (ShouldUseAn(S))
		Return "an"@S;

	else Return "a"@S;
}

defaultproperties
{
}
