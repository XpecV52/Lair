class Falk689GameType extends Falk689GameTypeBase;

var   SpecialSquad                     FMetalClotClass;            // metal clot class
var   bool                             FMetalClotSpawned;          // just spawn it once
var   bool                             FResetSong;                 // should we reset default song?
var   int                              FMetalClotWaveCheck;        // have we tried spawning it already this wave?
var   int                              ActiveWeaponPickups;        // Max active weapon pickups
var   int                              ActiveAmmoPickups;          // Max active ammo pickups
var   int                              CurrentWeaponPickups;       // Currently active weapon pickups
var   int                              CurrentAmmoPickups;         // Currently active ammo pickups
var   int                              AmmoSpawnTimer;             // int timer to check ammo spawns
var   int                              WaveSpawnedAmmo;            // how many ammo we spawned this wave
var   int                              FAlivePlayersCheck;         // this way we can check if someone died since last alive check
var   int                              WaveSpawnedWeapons;         // how many weapons we spawned this wave
var   int                              fLobbyTimeout;              // new lobby timeout since I'm lazy af
var   byte                             fLockedWeapons;             // how many achievement locked weapons we have
var   globalconfig int                 FMetalClotSpawnChance;      // random metal clot spawn chance
var   globalconfig int                 FMetalClotInitWave;         // when to start trying to spawn the metal clot
var   globalconfig int                 FMetalClotEndWave;          // when to stop trying to spawn the matl clot
var   globalconfig array<String>       Maps;                       // used to switch to a random map on start
var   bool                             fShouldCleanUp;             // should we clean up zed time list?
var   bool                             FSlayedZeds;                // already automatically slayed zeds
var   bool                             FWipeSongStarted;           // has the wipe song already started?
var   globalconfig float               AmmoRespawnTimeOT;          // how much time we take to recheck ammo spawns (one to three players)
var   globalconfig float               AmmoRespawnTimeFS;          // how much time we take to recheck ammo spawns (four to six players)
var   globalconfig float               AmmoRespawnTimeSN;          // how much time we take to recheck ammo spawns (seven to nine players)
var   globalconfig float               AmmoRespawnTimeMX;          // how much time we take to recheck ammo spawns (more than nine players)
var   globalconfig float               MaxAmmoPerWaveOT;           // Max total ammo to spawn per wave one to three players
var   globalconfig float               MaxAmmoPerWaveFS;           // Max total ammo to spawn per wave four to six players
var   globalconfig float               MaxAmmoPerWaveSN;           // Max total ammo to spawn per wave seven to nine players
var   globalconfig float               MaxAmmoPerWaveMX;           // Max total ammo to spawn per wave ten to max players
var   globalconfig float               MaxAmmoLastWaveM;           // Max total ammo multiplier for the last wave
var   globalconfig float               MaxWeaponsPerWaveOT;        // Max total weapons pickup per wave
var   globalconfig float               MaxWeaponsPerWaveFS;        // Max total weapons pickup per wave
var   globalconfig float               MaxWeaponsPerWaveSN;        // Max total weapons pickup per wave
var   globalconfig float               MaxWeaponsPerWaveMX;        // Max total weapons pickup per wave
var   globalconfig int                 FSlayNumber;                // Maximum zeds to slay with the timer
var   globalconfig float               FSlayTimer;                 // seconds before slaying zeds automatically
var   globalconfig bool                FAllowOnePerkChange;        // allow one perk change per wave, to be used with bNoPerkChanges set to True
var   float                            FDelayedSStalkerCheck;      // time in seconds for the next delayed stalker skills check
var   float                            FSStalkerCheckDelay;        // used to configure and reset FDelayedSStalkerCheck
var   float                            FCurSlayTimer;              // Current slay timer
var   float                            fCurCD;                     // used to calc late activation on zed time skills
var   float                            fCurAS;                     // used to trigger timed ammo respawn
var   float                            AmmoSpawnMulti;             // multiplier for max active ammo pickups
var   float                            WeaponSpawnMulti;           // multiplier for max active weapon pickups 
var   float                            FWaveSpawnPeriod;           // how many seconds to wait before spawning the next squad
var   float                            FWaveSpawnPeriodSeven;      // how many seconds to wait before spawning the next squad with seven or more players
var   float                            FWaveSpawnPeriodTen;        // how many seconds to wait before spawning the next squad with ten or more players
var   float                            FDebugTick;                 // used to print debug periodically
var   string                           LastStandingSong;           // play this when a wild Andewyl appears
var   string                           WipeSong;                   // play this when his mouse disagrees with his existence 
var   string                           CurrentWaveSong;            // stored current wave song

// weapons spawns multipliers
var globalconfig float BBWSpawnsMultiplier;
var globalconfig float HOEWSpawnsMultiplier;
var globalconfig float SuicidalWSpawnsMultiplier;
var globalconfig float HardWSpawnsMultiplier;
var globalconfig float NormalWSpawnsMultiplier;

// ammo spawns multipliers
var globalconfig float BBASpawnsMultiplier;
var globalconfig float HOEASpawnsMultiplier;
var globalconfig float SuicidalASpawnsMultiplier;
var globalconfig float HardASpawnsMultiplier;
var globalconfig float NormalASpawnsMultiplier;

// needed for my random implementation of ammo spawn
struct FPickupIndex
{
   var int  val;  // random number
   var byte stop; // four state variable (0 = ok, 1 = to be stopped, 2 = stopped, 3 = ignored)
};

// game start
event InitGame(string Options, out string Error)
{
   local KFLevelRules    KFLRit;
   local ShopVolume      SH;
   local ZombieVolume    ZZ;
   local byte            fN;
   local string          InOpt;
   local string          MapName;

   Super.InitGame(Options, Error);

   default.BroadcastClass = class'Falk689BroadcastHandler';
   BroadcastClass         = class'Falk689BroadcastHandler';
   BroadcastHandler       = spawn(BroadcastClass);

   MapName                = GetCurrentMapName(Level);

   GIPropsExtras[0]       = Default.GIPropsExtras[0];

   // switching to a random map
   if (MapName == "KFL-Maprandomizer")
   {
      fN = Rand(Maps.Length);
      Level.ServerTravel(Maps[fN], false);
   }

   MaxPlayers = Clamp(GetIntOption(Options, "MaxPlayers", MaxPlayers ), 0, 12);
   default.MaxPlayers = Clamp(default.MaxPlayers, 0, 12);

   foreach DynamicActors(class'KFLevelRules',KFLRit)
   {
      if(KFLRules==none)
         KFLRules = KFLRit;
      else Warn("MULTIPLE KFLEVELRULES FOUND!!!!!");
   }

   foreach AllActors(class'ShopVolume',SH)
   {
      if(!SH.bObjectiveModeOnly || bUsingObjectiveMode)
      {
         ShopList[ShopList.Length] = SH;
      }
   }

   foreach DynamicActors(class'ZombieVolume',ZZ)
   {
      if(!ZZ.bObjectiveModeOnly || bUsingObjectiveMode)
      {
         ZedSpawnList[ZedSpawnList.Length] = ZZ;
      }
   }

   //provide default rules if mapper did not need custom one
   if(KFLRules==none)
      KFLRules = spawn(class'KFLevelRules');

   InOpt = ParseOption(Options, "UseBots");
   if (InOpt != "")
      bNoBots = bool(InOpt);

   MonsterCollection = Class'KFMod.KFMonstersCollection';

   bCustomGameLength   = true;
   UpdateGameLength();

   LoadUpMonsterList();

}

// replace a squad with the metal clot
function Falk689AddMetalClot()
{
   local Class<KFMonster> MC;
   local array< class<KFMonster> > TempSquads;
   local int j;

   //log("Loading up metal clot...");

   MC = Class<KFMonster>(DynamicLoadObject(FMetalClotClass.ZedClass[0],Class'Class'));

   if(MC == None)
   {
      warn("Couldn't DLO Metal Clot!!!");
   }

   else
   {
      for(j=0; j<FMetalClotClass.NumZeds[0]; j++)
      {
         //log("SpecialSquad!!! Wave "$WaveNum$" Monster "$j$" = "$MC);
         TempSquads[TempSquads.Length] = MC;
      }
   }

   FMetalClotSpawned = true;
   NextSpawnSquad = TempSquads;
}

// Respawn ammo and weapons (called by timer and game start)
function FalkRespawnPickups()
{
   local int i, FAlive, MaxAmmoPerWave, MaxWeaponsPerWave;
   local int j, FCheck;
   local float FRandom, AmmoSpawnChance, WeaponSpawnChance;
   local array<FPickupIndex> Idxs;

   // Setting spawn chance 
   if (GameDifficulty >= 8.0) // Bloodbath
   {
      AmmoSpawnChance   = Default.BBASpawnsMultiplier;
      WeaponSpawnChance = Default.BBWSpawnsMultiplier;
   }

   else if (GameDifficulty >= 7.0) // Hell on Earth
   {
      AmmoSpawnChance   = Default.HOEASpawnsMultiplier;
      WeaponSpawnChance = Default.HOEWSpawnsMultiplier;
   }

   else if (GameDifficulty >= 5.0) // Suicidal
   {
      AmmoSpawnChance   = Default.SuicidalASpawnsMultiplier;
      WeaponSpawnChance = Default.SuicidalWSpawnsMultiplier;
   }

   else if (GameDifficulty >= 4.0) // Hard
   {
      AmmoSpawnChance   = Default.HardASpawnsMultiplier;
      WeaponSpawnChance = Default.HardWSpawnsMultiplier;
   }

   else
   {
      AmmoSpawnChance   = Default.NormalASpawnsMultiplier;
      WeaponSpawnChance = Default.NormalWSpawnsMultiplier;
   }

   FAlive = GetAlivePlayers();

   if (FAlive >= 10) 
   {
      MaxAmmoPerWave    = AmmoPickups.Length   * MaxAmmoPerWaveMX;
      MaxWeaponsPerWave = WeaponPickups.Length * MaxWeaponsPerWaveMX;
   }

   else if (FAlive >= 7)
   {
      MaxAmmoPerWave    = AmmoPickups.Length   * MaxAmmoPerWaveSN;
      MaxWeaponsPerWave = WeaponPickups.Length * MaxWeaponsPerWaveSN;
   }

   else if (FAlive >= 4)
   {
      MaxAmmoPerWave    = AmmoPickups.Length   * MaxAmmoPerWaveFS;
      MaxWeaponsPerWave = WeaponPickups.Length * MaxWeaponsPerWaveFS;
   }

   else
   {
      MaxAmmoPerWave    = AmmoPickups.Length   * MaxAmmoPerWaveOT;
      MaxWeaponsPerWave = WeaponPickups.Length * MaxWeaponsPerWaveOT;
   }

   // increase max ammo count for last wave
   if (FCurrentWave >= FinalWave)
      MaxAmmoPerWave *= MaxAmmoLastWaveM;

   // Ramdomly spawn weapons pickups
   Idxs   = GetRandomIndexes(WeaponPickups.Length);
   FCheck = Idxs.Length;

   While (FCheck > 0 && CurrentWeaponPickups < ActiveWeaponPickups && (bWaveBossInProgress || WaveSpawnedWeapons < MaxWeaponsPerWave))
   {
      for (j = 0; j < Idxs.Length && CurrentWeaponPickups < ActiveWeaponPickups && (bWaveBossInProgress || WaveSpawnedWeapons < MaxWeaponsPerWave); j++)
      {
         Idxs[j].stop = FalkShouldStopThisIdx(Idxs, j, i, WeaponPickups.Length);

         if (Idxs[j].stop == 2)
         {
            FCheck--;
            //log("Stop value: "@Idx[j]+i@" - FCheck: "@Fcheck);
         }

         else if (Idxs[j].stop < 2 && !WeaponPickups[Idxs[j].val + i].bIsEnabledNow)
         {
            FRandom = FRand();

            //warn("Checking Number: "@Idxs[j].val + i);

            if (FRandom <= WeaponSpawnChance)
            {
               WeaponPickups[Idxs[j].val + i].EnableMe();
               CurrentWeaponPickups++;
               WaveSpawnedWeapons++;
               //log("Weapon Spawned: "@WaveSpawnedWeapons);
               //log("Max: "@MaxWeaponsPerWave);
            }
         }

         if (Idxs[j].stop == 1) // stop this before it gets ugly
         {
            Idxs[j].stop = 3;
            Fcheck--;
         }
      }

      i++;
   }

   // Randomly spawn ammo pickups
   Idxs   = GetRandomIndexes(AmmoPickups.Length);
   FCheck = Idxs.Length;
   i      = 0;

   While (FCheck > 0 && CurrentAmmoPickups < ActiveAmmoPickups && WaveSpawnedAmmo < MaxAmmoPerWave)
   {
      for (j = 0; j < Idxs.Length && CurrentAmmoPickups < ActiveAmmoPickups && (bWaveBossInProgress || WaveSpawnedAmmo < MaxAmmoPerWave); j++)
      {
         Idxs[j].stop = FalkShouldStopThisIdx(Idxs, j, i, AmmoPickups.Length);

         if (Idxs[j].stop == 2)
         {
            FCheck--;
            //log("Stop value: "@Idxs[j].val+i@" --- FCheck: "@Fcheck);
         }

         else if (Idxs[j].stop < 2 && AmmoPickups[Idxs[j].val + i].bSleeping)
         {

            FRandom = FRand();

            //log("Checking Number: "@Idxs[j].val + i);

            if (FRandom <= AmmoSpawnChance)
            {
               AmmoPickups[Idxs[j].val + i].GotoState('Pickup');
               CurrentAmmoPickups++;
               WaveSpawnedAmmo++;
               //log("Ammo: "@WaveSpawnedAmmo);
               //log("Max: "@MaxAmmoPerWave);
            } 
         }

         //log("Stop State: "@Idxs[j].stop);

         if (Idxs[j].stop == 1) // stop this before it gets ugly
         {
            Idxs[j].stop = 3;
            Fcheck--;
            //log("Stop value 2: "@Idxs[j].val+i@" --- FCheck: "@Fcheck);
         }

      }

      i++;
   }
}

// returns an array of random numbers based on a pickup list length
function array<FPickupIndex> GetRandomIndexes(int FLen)
{
   local int i, Random;
   local array<FPickupIndex> Idxs;

   for (i = 0; i < Max(1, FLen / 5); i++) // add at least one random number
   {
      Idxs.insert(i, 1);
      Random = RandRange(1, FLen-2);

      Idxs[i].val = Random;
      //log(Idxs[i].val);
   } 

   Idxs.insert(i, 1); // this should append a zero to the array

   return Idxs;
}

// returns the state of the index if reached the end of the array or the value of another entry - 1
// or we just have double values in our random array
function int FalkShouldStopThisIdx(array<FPickupIndex> Idxs, int FIdx, int add, int len)
{
   local int i, result;

   // this one was already stopped
   if (Idxs[FIdx].stop == 2)
      return 3;

   if (Idxs[FIdx].stop > 0)
      return Idxs[FIdx].stop;

   //log("len: "@len);
   //log("val+add: "@Idxs[FIdx].val + add);
   // we just reached the end of the array
   if (Idxs[FIdx].val + add == len - 1)
   {
      //log("END STATE 1");
      result = 1;

      // still check and remove doubles at start 
      if (add > 0)
         return 1;
   }

   // safety, just avoid weird shit to happen
   else if (Idxs[FIdx].val + add > len - 1)
   {
      return 2;
   }

   for (i = 0; i < Idxs.Length; i++)
   {
      if (i != FIdx)
      { 
         // double number, stop this now
         if (add == 0 && Idxs[i].stop < 2 && Idxs[FIdx].val == Idxs[i].val)
         {
            //Idxs[FIdx].stop = 2;
            return 2;
         }

         // probably the last time we'll need this one
         if (Idxs[FIdx].val + add == Idxs[i].val - 1)
         {
            //log("STATE 1");
            result = 1;
            //Idxs[FIdx].stop = 1;

            // no need to check for doubles more than once
            if (add > 0)
               return 1;
         }
      }
   }

   return result;
}

// metal clot check
function bool AddSquad()
{
   local int numspawned;
   local int ZombiesAtOnceLeft;
   local int TotalZombiesValue;
   local int r;

   // alive player number has changed, repeat max monsters check
   if (FAlivePlayers != FAlivePlayersCheck)
   {
      FAlivePlayersCheck = FAlivePlayers;
      MaxZombiesOnce     = GetMaxZombiesOnce(FAlivePlayers);
      MaxMonsters        = Clamp(TotalMaxMonsters, 5, MaxZombiesOnce);
   }

   if(LastZVol==none || NextSpawnSquad.length==0)
   {
      // Throw in the special squad if the time is right
      if (Level.NetMode == NM_DedicatedServer && GameDifficulty < 8.0 && !FMetalClotSpawned && FMetalClotWaveCheck != WaveNum && WaveNum >= FMetalClotInitWave && WaveNum <= FMetalClotEndWave)
      {
         r = Rand(99);
         FMetalClotWaveCheck = WaveNum;
         log("Metal Clot Check: "@r);

         if (r <= FMetalClotSpawnChance)
            Falk689AddMetalClot();

      }

      else
      {
         //log("build next squad");
         BuildNextSquad();
      }

      LastZVol = FindSpawningVolume();

      if( LastZVol!=None )
         LastSpawningVolume = LastZVol;
   }

   if(LastZVol == None)
   {
      NextSpawnSquad.length = 0;
      return false;
   }

   // How many zombies can we have left to spawn at once
   ZombiesAtOnceLeft = MaxMonsters - NumMonsters;

   //Log("Spawn on"@LastZVol.Name);
   if(LastZVol.SpawnInHere(NextSpawnSquad,,numspawned,TotalMaxMonsters,ZombiesAtOnceLeft,TotalZombiesValue))
   {
      NumMonsters  += numspawned; //NextSpawnSquad.Length;
      WaveMonsters += numspawned; //NextSpawnSquad.Length;

      /*if(bDebugMoney)
      {
         if (GameDifficulty >= 7.0) // Hell on Earth
            TotalZombiesValue *= 0.5;

         else if ( GameDifficulty >= 5.0 ) // Suicidal
            TotalZombiesValue *= 0.6;

         else if ( GameDifficulty >= 4.0 ) // Hard
            TotalZombiesValue *= 0.75;

         TotalPossibleWaveMoney += TotalZombiesValue;
         TotalPossibleMatchMoney += TotalZombiesValue;
      }*/

      NextSpawnSquad.Remove(0, numspawned);

      return true;
   }

   else
   {
      TryToSpawnInAnotherVolume();
      return false;
   }
} 


// fixed weird shit on ammopickup
function AmmoPickedUp(KFAmmoPickup PickedUp)
{
   CurrentAmmoPickups--;

   PickedUp.GotoState('Sleeping', 'Begin');
}


// fixed weird shit on weaponpickup
function WeaponPickedUp(KFRandomItemSpawn PickedUp)
{
   if (PickedUp == none) // can this really happen?
      return;

   PickedUp.DisableMe();
   CurrentWeaponPickups--;
}

// force zed time
exec function FalkForceZedTime()
{
   DramaticEvent(1.0, 8.0);
}

// force fake zed time
exec function FalkForceFakeZedTime()
{
   DramaticEvent(1.0, 2.0);
}

// force fake short zed time
exec function FalkForceShortZedTime()
{
   DramaticEvent(1.0, 0.3);
}

// try to force and end wave, for debug 
exec function FalkForceEndWave()
{
   KillZeds();
   MaxMonsters      = 0;
   TotalMaxMonsters = 0;
}


// added zed time skills triggers
function DramaticEvent(float BaseZedTimePossibility, optional float DesiredZedTimeDuration)
{
   local float RandChance;
   local float TimeSinceLastEvent;
   local Controller C;
   local class<FVeterancyTypes> Vet;

   TimeSinceLastEvent = Level.TimeSeconds - LastZedTimeEvent;

   // Don't go in slomo if we were just IN slomo
   if (TimeSinceLastEvent < 10.0 && (BaseZedTimePossibility != 1.0 || (fFakeZedTime && (DesiredZedTimeDuration == 0 || DesiredZedTimeDuration >= 1.0))))
   {
      //warn("Zed Time Return:"@fFakeZedTime@"Desired Time:"@DesiredZedTimeDuration);
      return;
   }

   // If we're in zed time, don't go in fake zed time state
   if (bZEDTimeActive && DesiredZedTimeDuration > 0 && DesiredZedTimeDuration < ZEDTimeDuration)
   {
      //warn("Zed Time Return Return 2");
      return;
   }

   if (TimeSinceLastEvent > 60)
      BaseZedTimePossibility *= 4.0;

   else if (TimeSinceLastEvent > 30)
      BaseZedTimePossibility *= 2.0;

   RandChance = FRand();

   if (RandChance <= BaseZedTimePossibility)
   {
      /*if (bZEDTimeActive)
        warn("Zed Time Extended:"@Level.TimeSeconds);

      else
        warn("Zed Time Start:"@Level.TimeSeconds);*/

      bZEDTimeActive  = true;
      bSpeedingBackUp = false;

      //warn("Desired Duration:"@DesiredZedTimeDuration);

      // fake zed time start
      if (BaseZedTimePossibility >= 1.0 && DesiredZedTimeDuration > 0 && DesiredZedTimeDuration < ZEDTimeDuration)
      {
         //warn("FAKE ZED TIME START");
         fFakeZedTime = True;
      }

      LastZedTimeEvent = Level.TimeSeconds;

      if (DesiredZedTimeDuration != 0.0)
         CurrentZEDTimeDuration = DesiredZedTimeDuration;

      else
         CurrentZEDTimeDuration = ZEDTimeDuration;

      SetGameSpeed(ZedTimeSlomoScale);

      // don't play zed time sound for short ones
      if (fFakeZedTime && CurrentZEDTimeDuration <= 0.8)
      {
         //warn("No Sound");
         fFakeNoSound = True;
      }

      for (C = Level.ControllerList; C != none; C = C.NextController)
      { 
         if (FPCServ(C)!= none)
         {
            FPCServ(C).FalkClientEnterZedTime(fFakeNoSound);

            // fake zed time shouldn't trigger skills
            if (!fFakeZedTime)
            {
               // Falk689 zed time start stuff
               if (C.Pawn != None && C.Pawn.Health > 0 && !C.PlayerReplicationInfo.bOnlySpectator &&
                     KFPlayerReplicationInfo(C.PlayerReplicationInfo).ClientVeteranSkill != none)
               {
                  Vet = class<FVeterancyTypes>(KFPlayerReplicationInfo(C.PlayerReplicationInfo).ClientVeteranSkill);

                  if (Vet != None)
                  {
                     // Zed time reset
                     if (PlayerController(C) != none)
                        ZedTimeIndex(PlayerController(C), 3); // this means zed time start

                     Vet.Static.StartZedTime(KFPlayerReplicationInfo(C.PlayerReplicationInfo));
                     fCurCD = 0.0;
                  }
               }
            }
         }

         if (C.PlayerReplicationInfo != none && KFSteamStatsAndAchievements(C.PlayerReplicationInfo.SteamStatsAndAchievements) != none)
            KFSteamStatsAndAchievements(C.PlayerReplicationInfo.SteamStatsAndAchievements).AddZedTime(ZEDTimeDuration);
      }

      // same as above, we shouldn't need to cleanup anything if it was a fake zed time
      if (!fFakeZedTime)
         fShouldCleanUp = True;
   }
}


// zed time skills end
event Tick(float DeltaTime)
{
   local float TrueTimeFactor;
   local Controller C;
   local class<FVeterancyTypes> Vet;
   local int FAlive;
   local FHumanPawn FHP;

   // decrease the free farming timer
   if (Level.NetMode == NM_DedicatedServer && Role == ROLE_Authority && bWaveInProgress && fLastKilledZedTimer > 0)
      fLastKilledZedTimer -= DeltaTime;

   // Delayed recheck for stalker shared skill after a disconnect
   if (Level.NetMode == NM_DedicatedServer && FDelayedSStalkerCheck > 0)
   {
      FDelayedSStalkerCheck -= DeltaTime;

      if (FDelayedSStalkerCheck <= 0)
      {
         //warn("DELAYED STUFF");
         fSStalkerSkills = GetStalkerViewSharers(); // update how many have the shared stalker view skill

         // if anybody has the skill, tell the pawn they have to see stalkers during zed time
         if (fSStalkerSkills > 0)
         {
            foreach DynamicActors(class'FHumanPawn', FHP)
            {
               //warn("DELAYED ON");
               FHP.fSStalkerInTeam = True;
            }
         }

         // else just tell them to suck a dick
         else
         {
            foreach DynamicActors(class'FHumanPawn', FHP)
            {
               //warn("DELAYED OFF");
               FHP.fSStalkerInTeam = False;
            }
         }
      }
   }

   Super.Tick(DeltaTime);

   if(bZEDTimeActive)
   {
      TrueTimeFactor = 1.1/Level.TimeDilation;
      CurrentZEDTimeDuration -= DeltaTime * TrueTimeFactor;

      if(CurrentZEDTimeDuration < (ZEDTimeDuration*0.166) && CurrentZEDTimeDuration > 0)
      {
         if(!bSpeedingBackUp)
         {
            bSpeedingBackUp = true;

            for(C=Level.ControllerList;C!=None;C=C.NextController)
            {
               if (FPCServ(C)!= none)
                  FPCServ(C).FalkClientExitZedTime(fFakeNoSound);
            }
         }

         SetGameSpeed(Lerp( (CurrentZEDTimeDuration/(ZEDTimeDuration*0.166)), 1.0, 0.2 ));
      }


      if(CurrentZEDTimeDuration <= 0)
      {
         bZEDTimeActive        = false;
         bSpeedingBackUp       = false;
         SetGameSpeed(1.0);
         ZedTimeExtensionsUsed = 0;
         fFakeNoSound          = False;
         fCurCD                = 0;

         // fake zed time exit, don't tell anything to the perks since they may or may not trigger stuff there
         if (fFakeZedTime)
            fFakeZedTime = False;

         // normal zed time with end skill triggers
         else
         {
            for(C=Level.ControllerList;C!=None;C=C.NextController)
            {
               if (KFPlayerController(C)!= none)
               {
                  // Falk689 zed time end stuff
                  if (KFPlayerReplicationInfo(C.PlayerReplicationInfo).ClientVeteranSkill != none)
                  {
                     Vet = class<FVeterancyTypes>(KFPlayerReplicationInfo(C.PlayerReplicationInfo).ClientVeteranSkill);

                     if (Vet != None)
                        Vet.Static.EndZedTime(KFPlayerReplicationInfo(C.PlayerReplicationInfo));
                  }
               }
            }
         }
      }
   }

   else if (fShouldCleanUp)
   {
      //log("Zed Time End: "@Level.TimeSeconds);
      fShouldCleanUp = False;
      fZedTimeEnd    = Level.TimeSeconds;
      ZedTimeEndReset(); // set up reset for our zed time skill list
   }

   if (NumMonsters > 0)
   {
      // Ammo respawn stuff
      if (fCurAS < 1)
         fCurAS += DeltaTime;

      else if (CurrentAmmoPickups <= ActiveAmmoPickups)
      {
         fCurAS = 0;
         AmmoSpawnTimer++;

         FAlive = GetAlivePlayers();

         // decrease ammo spawn timer based on alive players
         if ((AmmoSpawnTimer >= AmmoRespawnTimeMX && FAlive >= 10) ||
             (AmmoSpawnTimer >= AmmoRespawnTimeSN && FAlive >= 7)  ||
             (AmmoSpawnTimer >= AmmoRespawnTimeFS && FAlive >= 4)  ||
             (AmmoSpawnTimer >= AmmoRespawnTimeOT))
         {
            AmmoSpawnTimer = 0;
            //log("RESPAWN AMMO");
            FalkRespawnPickups();
         }
      }
   }
}

// fixed ammo pickups and not sure what else
State MatchInProgress
{
   // Now my kids tend to die if you forget them in your car under the boiling sun, just like irl
   function Tick(float Delta)
   {
      local Controller C;
      local array<FalkMonster> FC;
      local int i;

      Global.Tick(Delta);

      if ((Level.NetMode == NM_DedicatedServer || Level.NetMode == NM_ListenServer) && !FSlayedZeds && FCurrentWave < FinalWave && NumMonsters <= FSlayNumber)
      {
         if (FCurSlayTimer < FSlayTimer)
            FCurSlayTimer += Delta;

         else if (!FSlayedZeds)
         {
            FSlayedZeds = True;

            for (C=Level.ControllerList; C!=None; C=C.NextController)
            {
               if (C != none && C.Pawn                 != none &&
                   FalkMonster(C.Pawn)                 != none &&
                   FalkZombieBoss(C.Pawn)              == none &&
                   FalkZombieFleshpound(C.Pawn)        == none &&
                   FalkZombieScrake(C.Pawn)            == none &&
                   FZombieMetalClot_STANDARD(C.Pawn)   == none)
               {
                  FC[FC.Length] = FalkMonster(C.Pawn);
               }
            }

            for (i=0; i<FC.Length; i++)
            {
               if (FC[i] == none)
                  continue;

               FC[i].FSeppuku();
            }
         }

         else
            FCurSlayTimer = 0;
      }
   }

   function bool UpdateMonsterCount() // To avoid invasion errors.
   {
      local Controller C;
      local int i,j;

      For(C=Level.ControllerList; C!=None; C=C.NextController)
      {
         if(C.Pawn != None && C.Pawn.Health > 0)
         {
            if(Monster(C.Pawn) != None)
               i++;
            else j++;
         }
      }

      NumMonsters = i;
      Return (j>0);
   }

   function bool BootShopPlayers()
   {
      local int i,j;
      local bool bRes;

      j = ShopList.Length;
      for( i=0; i<j; i++ )
      {
         if( ShopList[i].BootPlayers() )
            bRes = True;
      }
      Return bRes;
   }

   function SelectShop()
   {
      local array<ShopVolume> TempShopList;
      local int i;
      local int SelectedShop;

      // Can't select a shop if there aren't any
      if ( ShopList.Length < 1 )
         return;

      for ( i = 0; i < ShopList.Length; i++ )
      {
         if ( ShopList[i].bAlwaysClosed )
            continue;

         TempShopList[TempShopList.Length] = ShopList[i];
      }

      SelectedShop = Rand(TempShopList.Length);

      if ( TempShopList[SelectedShop] != KFGameReplicationInfo(GameReplicationInfo).CurrentShop )
      {
         KFGameReplicationInfo(GameReplicationInfo).CurrentShop = TempShopList[SelectedShop];
      }
      else if ( SelectedShop + 1 < TempShopList.Length )
      {
         KFGameReplicationInfo(GameReplicationInfo).CurrentShop = TempShopList[SelectedShop + 1];
      }
      else
      {
         KFGameReplicationInfo(GameReplicationInfo).CurrentShop = TempShopList[0];
      }
   }

   function OpenShops()
   {
      local int i;
      local Controller C;
      local ServerStStats ST;
      local ClientPerkRepLink CL;
	   local LinkedReplicationInfo L;

      bTradingDoorsOpen            = True;

      for( i=0; i<ShopList.Length; i++ )
      {
         if( ShopList[i].bAlwaysClosed )
            continue;
         if( ShopList[i].bAlwaysEnabled )
         {
            ShopList[i].OpenShop();
         }
      }

      if ( KFGameReplicationInfo(GameReplicationInfo).CurrentShop == none )
      {
         SelectShop();
      }

      KFGameReplicationInfo(GameReplicationInfo).CurrentShop.OpenShop();

      // Tell all players to start showing the path to the trader
      for (C=Level.ControllerList; C!=None; C=C.NextController)
      {
         // testing stuff, enable perk swap on listen server
         if (FAllowOnePerkChange && C.PlayerReplicationInfo != None)
         {
            CL = ClientPerkRepLink(C.PlayerReplicationInfo.CustomReplicationInfo);

            // fix stuff on dedicated server
            if (CL == None)
            {
               for(L=C.PlayerReplicationInfo.CustomReplicationInfo; L!=None; L=L.NextReplicationInfo)
               {
                  CL = ClientPerkRepLink(L);

                  if (CL != None)
                     break;
               }
            }

            if (CL != None)
            {
               ST = ServerStStats(CL.StatObject);

               if (ST != None)
                  ServerPerksMut(ST.MutatorOwner).bNoPerkChanges = False;
            }
         }

         if( C.Pawn!=None && C.Pawn.Health>0 )
         {
            // Disable pawn collision during trader time
            C.Pawn.bBlockActors = false;

            if( KFPlayerController(C) !=None )
            {
               KFPlayerController(C).SetShowPathToTrader(true);

               // Have Trader tell players that the Shop's Open
               if (WaveNum < FinalWave)
                  KFPlayerController(C).ClientLocationalVoiceMessage(C.PlayerReplicationInfo, none, 'TRADER', 2);

               else
                  KFPlayerController(C).ClientLocationalVoiceMessage(C.PlayerReplicationInfo, none, 'TRADER', 3);

               //Hints
               KFPlayerController(C).CheckForHint(31);
               HintTime_1 = Level.TimeSeconds + 11;
            }
         }
      }
   }

   function CloseShops()
   {
      local int i;
      local Controller C;
      local Pickup Pickup;
      local Projectile fProj;
      local ServerStStats ST;
      local ClientPerkRepLink CL;
	   local LinkedReplicationInfo L;

      bTradingDoorsOpen            = False;

      // disallow perk change in dedicated server
      if (Level.NetMode == NM_DedicatedServer && Role == ROLE_Authority)
      {
         FalkDisablePerkSelect();
         //log("Veterancy selection disabled");
      }

      for( i=0; i<ShopList.Length; i++ )
      {
         if( ShopList[i].bCurrentlyOpen )
            ShopList[i].CloseShop();
      }

      SelectShop();

      //log("Wave: "@WaveNum);
      //log("Final Wave: "@FinalWave);

      if (WaveNum == FinalWave)
      {
         foreach AllActors(class'Pickup', Pickup)
         {
            if (Pickup.bDropped)
               Pickup.Destroy();
         }

         foreach AllActors(class'Projectile', fProj)
         {
            if (CrossbowArrowFalk(fProj) != none)
               fProj.Destroy();
         }
      }

      // Tell all players to stop showing the path to the trader
      for ( C = Level.ControllerList; C != none; C = C.NextController )
      {
         if (WaveNum >= FinalWave && FPCServ(C) != None)
            FPCServ(C).SetTakeDamage(false);

         // testing stuff, disable perk swap
         if (FAllowOnePerkChange && C.PlayerReplicationInfo != None)
         {
            CL = ClientPerkRepLink(C.PlayerReplicationInfo.CustomReplicationInfo);

            // fix stuff on dedicated server
            if (CL == None)
            {
               for(L=C.PlayerReplicationInfo.CustomReplicationInfo; L!=None; L=L.NextReplicationInfo)
               {
                  CL = ClientPerkRepLink(L);

                  if (CL != None)
                     break;
               }
            }

            if (CL != None)
            {
               ST = ServerStStats(CL.StatObject);

               if (ST != None)
                  ServerPerksMut(ST.MutatorOwner).bNoPerkChanges = True;
            }
         }

         if ( C.Pawn != none && C.Pawn.Health > 0 )
         {
            // Restore pawn collision during trader time
            C.Pawn.bBlockActors = C.Pawn.default.bBlockActors;

            if ( KFPlayerController(C) != none )
            {
               KFPlayerController(C).SetShowPathToTrader(false);
               KFPlayerController(C).ClientForceCollectGarbage();

               if ( WaveNum < FinalWave - 1 )
               {
                  // Have Trader tell players that the Shop's Closed
                  KFPlayerController(C).ClientLocationalVoiceMessage(C.PlayerReplicationInfo, none, 'TRADER', 6);
               }
            }
         }
      }
   }

   function Timer()
   {
      local Controller C;
      local bool bOneMessage;
      local Bot B;

      Global.Timer();

      if ( Level.TimeSeconds > HintTime_1 && bTradingDoorsOpen && bShowHint_2 )
      {
         for ( C = Level.ControllerList; C != None; C = C.NextController )
         {
            if( C.Pawn != none && C.Pawn.Health > 0 )
            {
               KFPlayerController(C).CheckForHint(32);
               HintTime_2 = Level.TimeSeconds + 11;
            }
         }

         bShowHint_2 = false;
      }

      if ( Level.TimeSeconds > HintTime_2 && bTradingDoorsOpen && bShowHint_3 )
      {
         for ( C = Level.ControllerList; C != None; C = C.NextController )
         {
            if( C.Pawn != None && C.Pawn.Health > 0 )
            {
               KFPlayerController(C).CheckForHint(33);
            }
         }

         bShowHint_3 = false;
      }

      if ( !bFinalStartup )
      {
         bFinalStartup = true;
         PlayStartupMessage();
      }
      if ( NeedPlayers() && AddBot() && (RemainingBots > 0) )
         RemainingBots--;
      ElapsedTime++;
      GameReplicationInfo.ElapsedTime = ElapsedTime;
      if( !UpdateMonsterCount() )
      {
         EndGame(None,"TimeLimit");
         Return;
      }

      if( bUpdateViewTargs )
         UpdateViews();

      if (!bNoBots && !bBotsAdded)
      {
         if(KFGameReplicationInfo(GameReplicationInfo) != none)

            if((NumPlayers + NumBots) < MaxPlayers && KFGameReplicationInfo(GameReplicationInfo).PendingBots > 0 )
            {
               AddBots(1);
               KFGameReplicationInfo(GameReplicationInfo).PendingBots --;
            }

         if (KFGameReplicationInfo(GameReplicationInfo).PendingBots == 0)
         {
            bBotsAdded = true;
            return;
         }
      }

      if (bWaveBossInProgress)
      {
         // Close Trader doors
         if( bTradingDoorsOpen )
         {
            CloseShops();
            TraderProblemLevel = 0;
         }
         if( TraderProblemLevel<4 )
         {
            if( BootShopPlayers() )
               TraderProblemLevel = 0;
            else TraderProblemLevel++;
         }
         if( !bHasSetViewYet && TotalMaxMonsters<=0 && NumMonsters>0 )
         {
            bHasSetViewYet = True;
            for ( C = Level.ControllerList; C != None; C = C.NextController )
               if ( C.Pawn!=None && KFMonster(C.Pawn)!=None && KFMonster(C.Pawn).MakeGrandEntry() )
               {
                  ViewingBoss = KFMonster(C.Pawn);
                  Break;
               }
            if( ViewingBoss!=None )
            {
               ViewingBoss.bAlwaysRelevant = True;
               for ( C = Level.ControllerList; C != None; C = C.NextController )
               {
                  if (PlayerController(C) != None)
                  {
                     PlayerController(C).SetViewTarget(ViewingBoss);
                     PlayerController(C).ClientSetViewTarget(ViewingBoss);
                     PlayerController(C).bBehindView = True;
                     PlayerController(C).ClientSetBehindView(True);
                     PlayerController(C).ClientSetMusic(BossBattleSong, MTRAN_Instant);

                     if (FPCServ(C) != None)
                        FPCServ(C).SetTakeDamage(false);

                  }

                  if (C.PlayerReplicationInfo != None && bRespawnOnBoss)
                  {
                     C.PlayerReplicationInfo.bOutOfLives = false;
                     C.PlayerReplicationInfo.NumLives = 0;

                     if ((C.Pawn == None) && !C.PlayerReplicationInfo.bOnlySpectator && PlayerController(C)!=None)
                        C.GotoState('PlayerWaiting');
                  }
               }
            }
         }

         else if( ViewingBoss!=None && !ViewingBoss.bShotAnim )
         {
            ViewingBoss = None;
            for ( C = Level.ControllerList; C != None; C = C.NextController )
               if( PlayerController(C)!=None )
               {
                  if( C.Pawn==None && !C.PlayerReplicationInfo.bOnlySpectator && bRespawnOnBoss )
                     C.ServerReStartPlayer();
                  if( C.Pawn!=None )
                  {
                     PlayerController(C).SetViewTarget(C.Pawn);
                     PlayerController(C).ClientSetViewTarget(C.Pawn);
                  }
                  else
                  {
                     PlayerController(C).SetViewTarget(C);
                     PlayerController(C).ClientSetViewTarget(C);
                  }
                  PlayerController(C).bBehindView = False;
                  PlayerController(C).ClientSetBehindView(False);

                  if (FPCServ(C) != None)
                     FPCServ(C).SetTakeDamage(true);
               }
         }

         if (TotalMaxMonsters <= 0 || (Level.TimeSeconds>WaveEndTime))
         {
            // if everyone's spawned and they're all dead
            if (NumMonsters <= 0)
               DoWaveEnd();
         }
         else AddBoss();
      }

      else if (bWaveInProgress)
      {
         WaveTimeElapsed += 1.0;

         // Close Trader doors
         if (bTradingDoorsOpen)
         {
            CloseShops();
            TraderProblemLevel = 0;
         }
         if( TraderProblemLevel<4 )
         {
            if( BootShopPlayers() )
               TraderProblemLevel = 0;
            else TraderProblemLevel++;
         }
         if(!MusicPlaying)
            StartGameMusic(True);

         if (TotalMaxMonsters <= 0)
         {
            if (NumMonsters <= 5)
            {
               for ( C = Level.ControllerList; C != None; C = C.NextController )
                  if ( KFMonsterController(C)!=None && KFMonsterController(C).CanKillMeYet() )
                  {
                     C.Pawn.KilledBy(C.Pawn);
                     Break;
                  }
            }

            // if everyone's spawned and they're all dead
            if (NumMonsters <= 0)
            {
               DoWaveEnd();
            }
         } // all monsters spawned

         else if ( (Level.TimeSeconds > NextMonsterTime) && (NumMonsters+NextSpawnSquad.Length <= MaxMonsters) )
         {
            WaveEndTime = Level.TimeSeconds+160;
            if( !bDisableZedSpawning )
            {
               AddSquad(); // Comment this out to prevent zed spawning
            }

            if(nextSpawnSquad.length>0)
            {
               NextMonsterTime = Level.TimeSeconds + 0.2;
            }
            else
            {
               NextMonsterTime = Level.TimeSeconds + CalcNextSquadSpawnTime();
            }
         }
      }

      else if (NumMonsters <= 0)
      {
         if (WaveNum == FinalWave && !bUseEndGameBoss)
         {
            if( bDebugMoney )
            {
               log("$$$$$$$$$$$$$$$$ Final TotalPossibleMatchMoney = "$TotalPossibleMatchMoney,'Debug');
            }

            EndGame(None,"TimeLimit");
            return;
         }

         else if(WaveNum == (FinalWave + 1) && bUseEndGameBoss)
         {
            if( bDebugMoney )
            {
               log("$$$$$$$$$$$$$$$$ Final TotalPossibleMatchMoney = "$TotalPossibleMatchMoney,'Debug');
            }

            EndGame(None,"TimeLimit");
            return;
         }

         WaveCountDown--;

         if (!CalmMusicPlaying)
         {
            InitMapWaveCfg();
            StartGameMusic(False);
         }

         // Open Trader doors
         if ( WaveNum != InitialWave && !bTradingDoorsOpen )
         {
            OpenShops();
         }

         // Select a shop if one isn't open
         if ( KFGameReplicationInfo(GameReplicationInfo).CurrentShop == none )
         {
            SelectShop();
         }

         KFGameReplicationInfo(GameReplicationInfo).TimeToNextWave = WaveCountDown;
         if (WaveCountDown == 30)
         {
            for ( C = Level.ControllerList; C != None; C = C.NextController )
            {
               if ( KFPlayerController(C) != None )
               {
                  // Have Trader tell players that they've got 30 seconds
                  KFPlayerController(C).ClientLocationalVoiceMessage(C.PlayerReplicationInfo, none, 'TRADER', 4);
               }
            }
         }

         else if ( WaveCountDown == 10 )
         {
            for ( C = Level.ControllerList; C != None; C = C.NextController )
            {
               if ( KFPlayerController(C) != None )
               {
                  // Have Trader tell players that they've got 10 seconds
                  KFPlayerController(C).ClientLocationalVoiceMessage(C.PlayerReplicationInfo, none, 'TRADER', 5);
               }
            }
         }

         else if ( WaveCountDown == 5 )
         {
            KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonstersOn=false;
            InvasionGameReplicationInfo(GameReplicationInfo).WaveNumber = WaveNum;
         }

         else if ( WaveCountDown <= 0 )
         {
            bWaveInProgress = true;
            KFGameReplicationInfo(GameReplicationInfo).bWaveInProgress = true;

            if( WaveNum == FinalWave && bUseEndGameBoss )
            {
               StartWaveBoss();
            }

            else
            {
               SetupWave();

               for ( C = Level.ControllerList; C != none; C = C.NextController )
               {
                  if ( PlayerController(C) != none )
                  {
                     PlayerController(C).LastPlaySpeech = 0;

                     if ( KFPlayerController(C) != none )
                     {
                        KFPlayerController(C).bHasHeardTraderWelcomeMessage = false;
                     }
                  }

                  if ( Bot(C) != none )
                  {
                     B = Bot(C);
                     InvasionBot(B).bDamagedMessage = false;
                     B.bInitLifeMessage = false;

                     if ( !bOneMessage && (FRand() < 0.65) )
                     {
                        bOneMessage = true;

                        if ( (B.Squad.SquadLeader != None) && B.Squad.CloseToLeader(C.Pawn) )
                        {
                           B.SendMessage(B.Squad.SquadLeader.PlayerReplicationInfo, 'OTHER', B.GetMessageIndex('INPOSITION'), 20, 'TEAM');
                           B.bInitLifeMessage = false;
                        }
                     }
                  }
               }
            }
         }
      }
   }

   // Use a sine wave to somewhat randomly increase/decrease the frequency (and
   // also the intensity) of zombie squad spawning. This will give "peaks and valleys"
   // to the intensity of the zombie attacks
   function float CalcNextSquadSpawnTime()
   {
      local float NextSpawnTime;
      local float SineMod;

      SineMod = 1.0 - Abs(sin(WaveTimeElapsed * SineWaveFreq));

      //NextSpawnTime = KFLRules.WaveSpawnPeriod;
      NextSpawnTime = FWaveSpawnPeriod;

      if (FAlivePlayers >= 10)
         NextSpawnTime = FWaveSpawnPeriodTen;

      if (FAlivePlayers >= 7)
         NextSpawnTime = FWaveSpawnPeriodSeven;

      NextSpawnTime += SineMod * (NextSpawnTime * 2);

      return NextSpawnTime;
   }

   function DoWaveEnd()
   {
      local Controller C;
      //local KFDoorMover KFDM;
      local PlayerController Survivor;
      local int SurvivorCount;
      local byte x;
      local FPCServ FPC;

      if (fChangeCurWave)
      {
         fChangeCurWave = false;
         FCurrentWave++;
         warn("FCurrentWave update:"@FCurrentWave);

         // allow perk change in dedicated server
         if (Level.NetMode == NM_DedicatedServer && Role == ROLE_Authority)
         {
            FalkEnablePerkSelect();
            //log("Veterancy selection enabled");
         }
      }

      // reset the free farming timer on wave end
      if (Level.NetMode == NM_DedicatedServer && Role == ROLE_Authority)
      {
         if (fLastKilledZedTimer <= 0)
            ResetNoZedVars();

         fLastKilledZedTimer = Default.fLastKilledZedTimer;
      }

      // stop last standing song
      if (FResetSong)
      {
         MapSongHandler.CombatSong                         = CurrentWaveSong;
         MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = CurrentWaveSong; 
         FResetSong                                        = False;
      }

      // Only reset this at the end of wave 0. That way the sine wave that scales
      // the intensity up/down will be somewhat random per wave
      if( WaveNum < 1 )
         WaveTimeElapsed = 0;

      if (!rewardFlag)
         RewardSurvivingPlayers();

      if( bDebugMoney )
      {
         log("$$$$$$$$$$$$$$$$ Wave "$WaveNum$" TotalPossibleWaveMoney = "$TotalPossibleWaveMoney,'Debug');
         log("$$$$$$$$$$$$$$$$ TotalPossibleMatchMoney = "$TotalPossibleMatchMoney,'Debug');
         TotalPossibleWaveMoney=0;
      }

      // Clear Trader Message status
      bDidTraderMovingMessage = false;
      bDidMoveTowardTraderMessage = false;

      bWaveInProgress = false;
      bWaveBossInProgress = false;
      bNotifiedLastManStanding = false;
      KFGameReplicationInfo(GameReplicationInfo).bWaveInProgress = false;

      WaveCountDown = Max(TimeBetweenWaves,1);
      KFGameReplicationInfo(GameReplicationInfo).TimeToNextWave = WaveCountDown;
      WaveNum++;

      if (WaveNum < FinalWave + 1)
      {
         for (C = Level.ControllerList; C != none; C = C.NextController)
         {
            FPC = FPCServ(C);

            /*if (C.PlayerReplicationInfo != none && FPC != none)
               warn(C.PlayerReplicationInfo.PlayerName@FPC.fReadyToPlay);*/

            if (C.PlayerReplicationInfo != none && FPC != none && FPC.fReadyToPlay)
            {
               C.PlayerReplicationInfo.bOutOfLives = false;
               C.PlayerReplicationInfo.NumLives = 0;

               if ( KFPlayerController(C) != none )
               {
                  // Unlock ach weapons on listen server
                  if (Level.NetMode == NM_ListenServer)
                  {
                     for (x=0; x<=fLockedWeapons; x++)
                        SetUnlocked(PlayerController(C), x, True);
                  }

                  if ( KFPlayerReplicationInfo(C.PlayerReplicationInfo) != none )
                  {
                     KFPlayerController(C).bChangedVeterancyThisWave = false;

                     if ( KFPlayerReplicationInfo(C.PlayerReplicationInfo).ClientVeteranSkill != KFPlayerController(C).SelectedVeterancy )
                     {
                        KFPlayerController(C).SendSelectedVeterancyToServer();
                     }
                  }
               }

               if ( C.Pawn != none )
               {
                  if ( PlayerController(C) != none )
                  {
                     Survivor = PlayerController(C);
                     SurvivorCount++;
                  }
               }
               else if ( !C.PlayerReplicationInfo.bOnlySpectator )
               {
                  //C.PlayerReplicationInfo.Score = int(C.PlayerReplicationInfo.Score);

                  if(PlayerController(C) != none)
                  {
                     PlayerController(C).GotoState('PlayerWaiting');
                     PlayerController(C).SetViewTarget(C);
                     PlayerController(C).ClientSetBehindView(false);
                     PlayerController(C).bBehindView = False;
                     PlayerController(C).ClientSetViewTarget(C.Pawn);
                  }

                  C.ServerReStartPlayer();
               }

               if ( KFPlayerController(C) != none )
               {
                  if ( KFSteamStatsAndAchievements(PlayerController(C).SteamStatsAndAchievements) != none )
                  {
                     KFSteamStatsAndAchievements(PlayerController(C).SteamStatsAndAchievements).WaveEnded();
                  }
               }
            }
         }

         bUpdateViewTargs = True;
         FAlivePlayers = GetAlivePlayers(); // set alive players count on wave end

         //respawn doors
         //foreach DynamicActors(class'KFDoorMover', KFDM)
         //   KFDM.RespawnDoor();
      }
   }

   function InitMapWaveCfg()
   {
      local int i,l;
      local KFRandomSpawn RS;

      l = ZedSpawnList.Length;
      for( i=0; i<l; i++ )
         ZedSpawnList[i].NotifyNewWave(WaveNum);
      foreach DynamicActors(Class'KFRandomSpawn',RS)
         RS.NotifyNewWave(WaveNum,FinalWave-1);
   }

   function StartWaveBoss()
   {
      local int i,l;

      fBossWaveStarted = True;
      FAlivePlayers    = GetAlivePlayers(); // set alive players count on wave start
      AmmoSpawnTimer   = 0; // take some time to spawn more ammo

      l = ZedSpawnList.Length;

      for( i=0; i<l; i++ )
         ZedSpawnList[i].Reset();
      bHasSetViewYet = False;
      WaveEndTime = Level.TimeSeconds+60;
      NextSpawnSquad.Length = 1;

      if( KFGameLength != GL_Custom )
      {
         NextSpawnSquad[0] = Class<KFMonster>(DynamicLoadObject(MonsterCollection.default.EndGameBossClass,Class'Class'));
         NextspawnSquad[0].static.PreCacheAssets(Level);
      }
      else
      {
         NextSpawnSquad[0] = Class<KFMonster>(DynamicLoadObject(EndGameBossClass,Class'Class'));
         NextspawnSquad[0].static.PreCacheAssets(Level);
      }

      if( NextSpawnSquad[0]==None )
         NextSpawnSquad[0] = Class<KFMonster>(FallbackMonster);

      KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonsters = 1;
      TotalMaxMonsters = 1;
      bWaveBossInProgress = True;
   }

   function UpdateViews() // To fix camera stuck on ur spec target
   {
      local Controller C;

      bUpdateViewTargs = False;
      for ( C = Level.ControllerList; C != None; C = C.NextController )
      {
         if ( PlayerController(C) != None && C.Pawn!=None )
            PlayerController(C).ClientSetViewTarget(C.Pawn);
      }
   }

   // Setup the random ammo pickups
   function SetupPickups()
   {
      // Never spawn more than x% ammo and weapons
      ActiveWeaponPickups = WeaponPickups.Length * WeaponSpawnMulti;
      ActiveAmmoPickups   = AmmoPickups.Length   * AmmoSpawnMulti;
   }

   function BeginState()
   {
      Super.BeginState();

      WaveNum = InitialWave;
      InvasionGameReplicationInfo(GameReplicationInfo).WaveNumber = WaveNum;

      // Two seconds initial countdown
      WaveCountDown = 2;

      SetupPickups();
   }

   function EndState()
   {
      local Controller C;

      Super.EndState();

      // Tell all players to stop showing the path to the trader
      For( C=Level.ControllerList; C!=None; C=C.NextController )
      {
         if( C.Pawn!=None && C.Pawn.Health>0 )
         {
            // Restore pawn collision during trader time
            C.Pawn.bBlockActors = C.Pawn.default.bBlockActors;

            if( KFPlayerController(C) !=None )
            {
               KFPlayerController(C).SetShowPathToTrader(false);
            }
         }
      }
   }
}

// decrease alive players count if someone was killed
function Killed(Controller Killer, Controller Killed, Pawn KilledPawn, class<DamageType> damageType)
{
   local Controller C;
   local string MapName;
   local KFSteamStatsAndAchievements StatsAndAchievements;
   local FHumanPawn FHP;
   local KFPlayerReplicationInfo KFPRI;
   local class<KFVeterancyTypes> KFVet;

   // a player died, check if we should disable shared zed time skills
   if (PlayerController(Killed) != none)
   {
      //warn("PLAYER KILLED");

      DecStalkerViewSharers(PlayerController(Killed));

      // tell the pawns they have to stop seeing stalkers if nobody with the skill is alive
      if (fSStalkerSkills <= 0)
      {
         //warn("INSTANT OFF");

         foreach DynamicActors(class'FHumanPawn', FHP)
         {
            FHP.fSStalkerInTeam = False;
         }
      }
   }

   if (PlayerController(Killer) != none)
   {
      if (KFMonster(KilledPawn) != None && Killed != Killer)
      {
         // restart the farming timer
         if (Level.NetMode == NM_DedicatedServer && Role == ROLE_Authority)
         {
            if (fLastKilledZedTimer <= 0)
               ResetNoZedVars();

            //warn("RESTART:"@Level.TimeSeconds);
            fLastKilledZedTimer = Default.fLastKilledZedTimer;
         }

         // zed time extension stuff
         if (bZEDTimeActive)
         {
            KFPRI = KFPlayerReplicationInfo(Killer.PlayerReplicationInfo);

            if (KFPRI != none)
            {
               KFVet = KFPRI.ClientVeteranSkill;

               if (KFVet != none && HasZedTimeExtension(PlayerController(Killer), KFPRI, KFVet))
               {
                  //warn("Zed Time Extension");
                  // Force Zed Time extension for every kill as long as the Player's Perk has Extensions left
                  DramaticEvent(1.0);
                  //ZedTimeExtensionsUsed++;
               }
            }
         }


         StatsAndAchievements = KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements);

         if (StatsAndAchievements != none)
         {
            if( Killer.Pawn.Physics == PHYS_FALLING && damageType == class'DamTypeKrissM')
            {
               StatsAndAchievements.AddAirborneZedKill();
            }

            MapName = GetCurrentMapName(Level);

            StatsAndAchievements.AddKill(KFMonster(KilledPawn).bLaserSightedEBRM14Headshotted, class<DamTypeMelee>(damageType) != none, bZEDTimeActive, class<DamTypeM4AssaultRifle>(damageType) != none || class<DamTypeM4203AssaultRifle>(damageType) != none, class<DamTypeBenelli>(damageType) != none, class<DamTypeMagnum44Pistol>(damageType) != none, class<DamTypeMK23Pistol>(damageType) != none, class<DamTypeFNFALAssaultRifle>(damageType) != none, class<DamTypeBullpup>(damageType) != none, MapName);

            if ( Level.NetMode != NM_StandAlone && Level.Game.NumPlayers > 1 && KilledPawn.AnimAction == 'ZombieFeed' )
            {
               StatsAndAchievements.AddFeedingKill();
            }

            if (MapName ~= "KF-HillbillyHorror")
            {
               if ( class<DamTypeTrenchgun>(damageType) != none )
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Set200ZedOnFireOnHillbilly);
               }
            }

            if ( KilledPawn.IsA('KFMonster') )
            {
               if (KFMonster(KilledPawn) != none && KFMonster(KilledPawn).ZappedBy != none && KFMonster(KilledPawn).ZappedBy.Controller != none && KFMonster(KilledPawn).bZapped && KFPlayerController(KFMonster(KilledPawn).ZappedBy.Controller) != none)
               {
                  KFSteamStatsAndAchievements(KFPlayerController(KFMonster(KilledPawn).ZappedBy.Controller).SteamStatsAndAchievements).AddZedKilledWhileZapped();
               }

               if ( Killer.Pawn != none && KFWeapon(Killer.Pawn.Weapon) != none && KFWeapon(Killer.Pawn.Weapon).Tier3WeaponGiver != none &&
                     KFSteamStatsAndAchievements(KFWeapon(Killer.Pawn.Weapon).Tier3WeaponGiver.SteamStatsAndAchievements) != none )
               {
                  KFSteamStatsAndAchievements(KFWeapon(Killer.Pawn.Weapon).Tier3WeaponGiver.SteamStatsAndAchievements).AddDroppedTier3Weapon();
                  KFWeapon(Killer.Pawn.Weapon).Tier3WeaponGiver = none;
               }

               if ( class<DamTypeSPGrenadeImpact>(damageType) != none || class<DamTypeM79GrenadeImpact>(damageType) != none )
               {
                  StatsAndAchievements.CheckAndSetAchievementComplete( StatsAndAchievements.KFACHIEVEMENT_KillZedWithImpactSPG );
               }

               if ( bZEDTimeActive )
               {
                  if ( class<DamTypeSPThompson>(damageType) != none || class<DamTypeBullpup>(damageType) != none )
                  {
                     StatsAndAchievements.AddZedTimeKill();
                  }
               }

               if ( class<DamTypeBurned>(damageType) != none || class<DamTypeFlamethrower>(damageType) != none ||
                     class<DamTypeBlowerThrower>(damageType) != none )
               {
                  StatsAndAchievements.AddMonsterKillsWithBileOrFlame( KilledPawn.Class );
               }
            }

            if ( KilledPawn.IsA('FalkZombieCrawler') )
            {
               if ( class<DamTypeCrossbow>(damageType) != none )
               {
                  StatsAndAchievements.KilledCrawlerWithCrossbow();
               }
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddCrawlerKillWithKSG();
               }

               if (class<DamTypeThompson>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill15HillbillyCrawlersThomOrMKB);
               }

               if (class<DamTypeMKb42AssaultRifle>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill15HillbillyCrawlersThomOrMKB);
               }

               if ( KilledPawn.Physics == PHYS_Falling && class<DamTypeM79Grenade>(damageType) != none )
               {
                  StatsAndAchievements.AddCrawlerKilledInMidair();
               }

               if ( class<DamTypeBullpup>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).KilledCrawlerWithBullpup();
               }

               StatsAndAchievements.AddXMasCrawlerKill();
            }
            else if ( KilledPawn.IsA('FalkZombieBloat') )
            {
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddBloatKillWithKSG();
               }

               StatsAndAchievements.AddBloatKill(class<DamTypeBullpup>(damageType) != none);
            }
            else if ( KilledPawn.IsA('FalkZombieSiren') )
            {
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddSirenKillWithKSG();
               }

               StatsAndAchievements.AddSirenKill(class<DamTypeLawRocketImpact>(damageType) != none);
            }
            else if ( KilledPawn.IsA('FalkZombieStalker') )
            {
               if( class<DamTypeWinchester>(damageType) != none )
               {
                  StatsAndAchievements.AddStalkerKillWithLAR();
               }
               else if ( class<DamTypeFrag>(damageType) != none )
               {
                  StatsAndAchievements.AddStalkerKillWithExplosives();
               }
               else if ( class<DamTypeMelee>(damageType) != none )
               {
                  // 25% chance saying something about killing Stalker("Kissy, kissy, darlin!" or "Give us a kiss!")
                  if ( !bDidKillStalkerMeleeMessage && FRand() < 0.25 )
                  {
                     PlayerController(Killer).Speech('AUTO', 19, "");
                     bDidKillStalkerMeleeMessage = true;
                  }
               }
               else if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddStalkerKillWithKSG();
               }
               else if ( class<DamTypeNailGun>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill4StalkersNailgun);
               }

               if ( KFMonster(KilledPawn).bBackstabbed )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).AddStalkerBackstab();
               }

               StatsAndAchievements.AddXMasStalkerKill();
            }
            else if ( KilledPawn.IsA('FalkZombieHusk') )
            {
               if ( class<DamTypeBurned>(damageType) != none || class<DamTypeFlamethrower>(damageType) != none )
               {
                  StatsAndAchievements.KilledHusk(KFMonster(KilledPawn).bDamagedAPlayer);
               }
               else if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddHuskKillWithKSG();
               }
               else if ( class<DamTypeDeagle>(damageType) != none ||
                     class<DamTypeMagnum44Pistol>(damageType) != none ||
                     class<DamTypeDualies>(damageType) != none ||
                     class<DamTypeFlareProjectileImpact>(damageType) != none ||
                     class<DamTypeMK23Pistol>(damageType) != none ||
                     class<DamTypeMagnum44Pistol>(damageType) != none )
               {
                  StatsAndAchievements.KilledHuskWithPistol();
               }
               else if( class<DamTypeHuskGun>(damageType) != none ||
                     class<DamTypeHuskGunProjectileImpact>(damageType) != none )
               {
                  StatsAndAchievements.KilledXMasHuskWithHuskCannon();
               }
               else if ( class<DamTypeLAW>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).KilledHuskWithLAW();
               }
            }
            else if ( KilledPawn.IsA('FalkZombieScrake') )
            {
               KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).AddScrakeKill(MapName);

               if ( class<DamTypeLAW>(damageType) != none || class<DamTypeSealSquealExplosion>(damageType) != none)
               {
                  StatsAndAchievements.AddScrakeAndFPOneShotKill(true, false);
               }
               else if ( class<DamTypeM203Grenade>(damageType) != none)
               {
                  StatsAndAchievements.AddM203NadeScrakeKill();
               }
               else if ( class<DamTypeChainsaw>(damageType) != none )
               {
                  StatsAndAchievements.AddChainsawScrakeKill();
               }
               else if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddScrakeKillWithKSG();
               }
               else if( class<DamTypeFlareRevolver>(damageType )!= none ||
                     class<DamTypeHuskGun>(damageType) != none ||
                     class<DamTypeHuskGunProjectileImpact>(damageType) != none ||
                     class<DamTypeBurned>(damageType) != none ||
                     class<DamTypeHuskGunProjectileImpact>(damageType) != none ||
                     class<DamTypeFlameNade>(damageType) != none ||
                     class<DamTypeFlamethrower>(damageType) != none ||
                     class<DamTypeFlameNade>(damageType) != none ||
                     class<DamTypeFlareProjectileImpact>(damageType) != none ||
                     class<DamTypeTrenchgun>(damageType) != none )
               {
                  StatsAndAchievements.ScrakeKilledByFire();
               }
               else if(class<DamTypeClaymoreSword>(damageType) != none)
               {
                  StatsAndAchievements.AddXMasClaymoreScrakeKill();
               }
               else if ( class<DamTypeCrossbow>(damageType) != none || class<DamTypeCrossbowHeadShot>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).KilledScrakeWithCrossbow();
               }
            }
            else if ( KilledPawn.IsA('FalkZombieFleshpound') )
            {
               StatsAndAchievements.KilledFleshpound(class<DamTypeMelee>(damageType) != none, class<DamTypeAA12Shotgun>(damageType) != none, class<DamTypeKnife>(damageType) != none, class<DamTypeClaymoreSword>(damageType) != none);

               if ( class<DamTypeLAW>(damageType) != none  || class<DamTypeSealSquealExplosion>(damageType) != none)
               {
                  StatsAndAchievements.AddScrakeAndFPOneShotKill(false, true);
               }
               else if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddFleshPoundKillWithKSG();
               }
               else if ( class<DamTypeDwarfAxe>(damageType) != none )
               {
                  if ( KFMonster(KilledPawn).bBackstabbed )
                  {
                     StatsAndAchievements.AddFleshpoundAxeKill();
                  }
               }
               else if (   class<DamTypeDualies>(damageType) != none ||
                     class<DamTypeDeagle>(damageType) != none ||
                     class<DamTypeDualDeagle>(damageType) != none ||
                     class<DamTypeMK23Pistol>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).KilledFleshpoundWithPistol();
               }

            }
            else if ( KilledPawn.IsA('FalkZombieBoss') )
            {
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddBossKillWithKSG();
               }

               for ( C = Level.ControllerList; C != none; C = C.NextController )
               {
                  if ( PlayerController(C) != none && KFSteamStatsAndAchievements(PlayerController(C).SteamStatsAndAchievements) != none )
                  {
                     KFSteamStatsAndAchievements(PlayerController(C).SteamStatsAndAchievements).KilledPatriarch(KFMonster(KilledPawn).bHealed, class<DamTypeLAW>(damageType) != none, GameDifficulty >= 5.0, KFMonster(KilledPawn).bOnlyDamagedByCrossbow, class<DamTypeClaymoreSword>(damageType) != none, MapName);
                  }
               }
            }
            else if ( KilledPawn.IsA('FalkZombieClot') )
            {
               StatsAndAchievements.AddClotKill();
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddClotKillWithKSG();
               }
               else if ( class<DamTypeWinchester>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).AddClotKillWithLAR();
               }
               else if ( class<DamTypeBurned>(damageType) != none || class<DamTypeFlamethrower>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).ClotKilledByFire();
               }
            }
            else if ( KilledPawn.IsA('FalkZombieGorefast') )
            {
               if ( class<DamTypeKSGShotgun>(damageType) != none )
               {
                  StatsAndAchievements.AddGoreFastKillWithKSG();
               }
               else if ( class<DamTypeMelee>(damageType) != none )
               {
                  KFSteamStatsAndAchievements(PlayerController(Killer).SteamStatsAndAchievements).MeleedGorefast();
               }
               else if ( class<DamTypeTrenchgun>(damageType) != none ||
                     class<DamTypeFlareRevolver>(damageType) != none ||
                     class<DamTypeFlareProjectileImpact>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Set3HillbillyGorefastsOnFire);
               }

               if( KFMonster(KilledPawn).bBackstabbed )
               {
                  StatsAndAchievements.AddGorefastBackstab();
               }
            }

            if ( class<KFWeaponDamageType>(damageType) != none )
            {
               class<KFWeaponDamageType>(damageType).Static.AwardKill(StatsAndAchievements,KFPlayerController(Killer),KFMonster(KilledPawn));

               if ( class<DamTypePipeBomb>(damageType) != none )
               {
                  if ( KFPlayerReplicationInfo(Killer.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Killer.PlayerReplicationInfo).ClientVeteranSkill == class'KFVetDemolitions' )
                  {
                     StatsAndAchievements.AddDemolitionsPipebombKill();
                  }
               }
               else if ( class<DamTypeBurned>(damageType) != none )
               {
                  // 1% chance of the Killer saying something about burning the enemy to death
                  if ( FRand() < 0.01 && Level.TimeSeconds - LastBurnedEnemyMessageTime > BurnedEnemyMessageDelay )
                  {
                     PlayerController(Killer).Speech('AUTO', 20, "");
                     LastBurnedEnemyMessageTime = Level.TimeSeconds;
                  }
               }
               else if ( class<DamTypeSCARMK17AssaultRifle>(damageType) != none )
               {
                  StatsAndAchievements.AddSCARKill();
               }
               else if ( class<DamTypeM7A3M>(damageType) != none && KFMonster(KilledPawn).bDamagedAPlayer )
               {
                  StatsAndAchievements.OnKilledZedInjuredPlayerWithM7A3();
               }
               else if ( class<DamTypeMKb42AssaultRifle>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill6ZedWithoutReloadingMKB42);
               }
               else if ( class<DamTypeAxe>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill5HillbillyZedsIn10SecsSythOrAxe);
               }
               else if ( class<DamTypeScythe>(damageType) != none)
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill5HillbillyZedsIn10SecsSythOrAxe);
               }
               else if ( class<DamTypeM32Grenade>(damageType) != none ||
                     class<DamTypeSeekerRocketImpact>(damageType) != none ||
                     class<DamTypeSeekerSixRocket>(damageType) != none )
               {
                  StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill10ZedsSeekerOrM32In5Secs);
               }
            }
            StatsAndAchievements.AddKillPoints(StatsAndAchievements.KFACHIEVEMENT_Kill1000HillbillyZeds);
         }
      }
   }

   // the pat killed a player, queue a taunt animation
   else if (KFHumanPawn(KilledPawn) != none && FalkMonsterBase(Killer.Pawn) != none && FalkMonsterBase(Killer.Pawn).FIsBoss && class<DamTypeBossRadialFalk>(DamageType) == none)
      FalkMonsterBase(Killer.Pawn).FKillTauntStart(GetAlivePlayers()); 

   if ( (MonsterController(Killed) != None) || (Monster(KilledPawn) != None) )
   {
      ZombiesKilled++;
      KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonsters = Max(TotalMaxMonsters + NumMonsters - 1,0);
      if ( !bDidTraderMovingMessage )
      {
         if ( PlayerController(Killer) != none && float(ZombiesKilled) / float(ZombiesKilled + TotalMaxMonsters + NumMonsters - 1) >= 0.20 )
         {
            if ( WaveNum < FinalWave - 1 || (WaveNum < FinalWave && bUseEndGameBoss) )
            {
               // Have Trader tell players that the Shop's Moving
               PlayerController(Killer).ServerSpeech('TRADER', 0, "");
            }

            bDidTraderMovingMessage = true;
         }
      }
      else if ( !bDidMoveTowardTraderMessage )
      {
         if ( PlayerController(Killer) != none && float(ZombiesKilled) / float(ZombiesKilled + TotalMaxMonsters + NumMonsters - 1) >= 0.80 )
         {
            if ( WaveNum < FinalWave - 1 || (WaveNum < FinalWave && bUseEndGameBoss) )
            {
               if ( Level.NetMode != NM_Standalone || Killer.Pawn == none || KFGameReplicationInfo(GameReplicationInfo).CurrentShop == none ||
                     VSizeSquared(Killer.Pawn.Location - KFGameReplicationInfo(GameReplicationInfo).CurrentShop.Location) > 2250000 ) // 30 meters
               {
                  // Have Trader tell players that the Shop's Almost Open
                  PlayerController(Killer).Speech('TRADER', 1, "");
               }
            }

            bDidMoveTowardTraderMessage = true;
         }
      }
   }

   Super(Invasion).Killed(Killer,Killed,KilledPawn,DamageType);
}


// return torch battery drain mod
function int GetTorchBatteryDrain(KFWeapon fWeapon)
{
   if (Slayer_AUGFalk(fWeapon) != none || Spas12Falk(fWeapon) != none)
      return 5;

   return 10;
}


// return how many dudes haven't falked up yet
function int GetAlivePlayers()
{
   local Controller C;
   local int AliveCount;
   local FHumanPawn FP;

   //log("GetAlivePlayers");

   if (MaxLives > 0)
   {
      for (C=Level.ControllerList; C!=None; C=C.NextController)
      {
         if (C.PlayerReplicationInfo != None && C.bIsPlayer && !C.PlayerReplicationInfo.bOnlySpectator && C.Pawn != none)
         {
            FP = FHumanPawn(C.Pawn);

            if (FP != none && !FP.bPendingDelete && FP.fHealth > 0)
               AliveCount++; 
         }
      }
   }

   return AliveCount;
}

// tweaked zed numbers
function SetupWave()
{
   local int i,j;
   local float NewMaxMonsters;
   local float DifficultyMod; //, NumPlayersMod;
   local int UsedNumPlayers;
   local FHumanPawn FHP;

   AmmoSpawnTimer     = 0; // take some time to spawn more ammo
   WaveSpawnedAmmo    = 0; 
   WaveSpawnedWeapons = 0;
   TraderProblemLevel = 0;
   rewardFlag         = false;
   fChangeCurWave     = true;
   ZombiesKilled      = 0;
   WaveMonsters       = 0;
   WaveNumClasses     = 0;
   FSlayedZeds        = False;
   FCurSlayTimer      = 0;
   NewMaxMonsters     = Waves[WaveNum].WaveMaxMonsters;
   FAlivePlayers      = GetAlivePlayers();               // set alive players count on wave start
   FAlivePlayersCheck = FAlivePlayers;                   // set the check at wave start
   fSStalkerSkills    = GetStalkerViewSharers();         // update how many have the shared stalker view skill

   // if anybody has the skill, tell the pawn they have to see stalkers during zed time
   if (fSStalkerSkills > 0)
   {
      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         FHP.fSStalkerInTeam = True;
      }
   }

   // else just tell them to suck a dick
   else
   {
      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         FHP.fSStalkerInTeam = False;
      }
   }

   FalkSetLastPlayedWave();

   //log("Wave start alive players: "@FAlivePlayers);

   // scale number of zombies by difficulty

   if (GameDifficulty >= 8.0)      // Bloodbath
      DifficultyMod = 1.8;

   else if (GameDifficulty >= 7.0) // Hell on Earth
      DifficultyMod = 1.6;

   else if (GameDifficulty >= 5.0) // Suicidal
      DifficultyMod = 1.4;

   else if (GameDifficulty >= 4.0) // Hard
      DifficultyMod = 1.2;

   else                            // Normal
      DifficultyMod = 1.0;

   UsedNumPlayers = NumPlayers + NumBots;

   TotalMaxMonsters = NewMaxMonsters * DifficultyMod * FAlivePlayers; // scale based only on alive players

   MaxZombiesOnce = GetMaxZombiesOnce(FAlivePlayers);
   MaxMonsters    = Clamp(TotalMaxMonsters, 5, MaxZombiesOnce);
   //log("MaxMonsters: "@MaxMonsters);

   KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonsters   = TotalMaxMonsters;
   KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonstersOn = true;
   WaveEndTime                                                         = Level.TimeSeconds + Waves[WaveNum].WaveDuration;
   AdjustedDifficulty                                                  = GameDifficulty + Waves[WaveNum].WaveDifficulty;

   j = ZedSpawnList.Length;

   for(i=0; i<j; i++)
      ZedSpawnList[i].Reset();

   j = 1;
   SquadsToUse.Length = 0;

   for (i=0; i<InitSquads.Length; i++)
   {
      if ((j & Waves[WaveNum].WaveMask) != 0)
      {
         SquadsToUse.Insert(0,1);
         SquadsToUse[0] = i;
      }

      j *= 2;
   }

   // Save this for use elsewhere
   InitialSquadsToUseSize = SquadsToUse.Length;
   bUsedSpecialSquad      = false;
   SpecialListCounter     = 1;

   //Now build the first squad to use
   BuildNextSquad();
}

// return how many zombies we should spawn at once
function int GetMaxZombiesOnce(int fPlayers)
{
   return 28 + (fPlayers - 1) * 2;
}


// less valuable zeds
function ScoreKill(Controller Killer, Controller Other)
{
   local PlayerReplicationInfo OtherPRI;
   local float KillScore;
   local Controller C;
   local FMonsterController FMC;

   OtherPRI = Other.PlayerReplicationInfo;

   if (OtherPRI != None)
   {
      OtherPRI.NumLives++;
      OtherPRI.Score = Max(0, OtherPRI.Score);
      OtherPRI.bOutOfLives = true;

      if (Level.NetMode == NM_DedicatedServer && PlayerController(Other) != none)
      {
         //log("MaxMonsters: "@MaxMonsters);
         //log("NumMonsters: "@NumMonsters);
         //log("Dosh: "@OtherPRI.Score);

         FalkSetDosh(PlayerController(Other), Max(0, OtherPRI.Score), 2);
      }

      if (Killer != None && Killer.PlayerReplicationInfo != None && Killer.bIsPlayer)
         BroadcastLocalizedMessage(class'FInvasionMessage',1,OtherPRI,Killer.PlayerReplicationInfo);

      else if (Killer == None || Monster(Killer.Pawn) == None)
         BroadcastLocalizedMessage(class'FInvasionMessage',1,OtherPRI);

      else
         BroadcastLocalizedMessage(class'FInvasionMessage',1,OtherPRI,,Killer.Pawn.Class);

      CheckScore(None);
   }

   if (GameRulesModifiers != None && Killer != None && Killer.Pawn != none)
      GameRulesModifiers.ScoreKill(Killer, Other);

   // try to get the killer back
   if (Killer == None)
   {
      FMC = FMonsterController(Other);

      if (FMC != none)
         Killer = FMC.fLastDamagedByController;
   }

   if (Killer == None || !Killer.bIsPlayer || Killer == Other || MonsterController(Killer) != None)
      return;

   if (LastKilledMonsterClass == None)
      KillScore = 6;

   else
      KillScore = LastKilledMonsterClass.Default.ScoringValue;

   if (Killer.PlayerReplicationInfo != none)
   {
      // Scale killscore by difficulty
      if (GameDifficulty >= 8.0) // bloodbath
         KillScore *= 0.3;

      else if (GameDifficulty >= 7.0) // hell on earth
         KillScore *= 0.4;

      else if (GameDifficulty >= 5.0) // suicidal
         KillScore *= 0.5;

      else if (GameDifficulty >= 4.0) // hard
         KillScore *= 0.6;

      else                            // normal
         KillScore *= 0.8;

      KillScore = Max(1, int(KillScore));
      Killer.PlayerReplicationInfo.Kills++;

      ScoreKillAssists(KillScore, Other, Killer);

      Killer.PlayerReplicationInfo.NetUpdateTime = Level.TimeSeconds - 1;
   }

   if (Killer.PlayerReplicationInfo != none && Killer.PlayerReplicationInfo.Score < 0)
      Killer.PlayerReplicationInfo.Score = 0;


   // Marco's Kill Messages
   if (Class'HUDKillingFloor'.Default.MessageHealthLimit <= Other.Pawn.Default.Health ||
         Class'HUDKillingFloor'.Default.MessageMassLimit   <= Other.Pawn.Default.Mass)
   {
      for (C=Level.ControllerList; C!=None; C=C.nextController)
      {
         if (C.bIsPlayer && xPlayer(C) != None)
            xPlayer(C).ReceiveLocalizedMessage(Class'KillsMessage',1,Killer.PlayerReplicationInfo,,Other.Pawn.Class);
      }
   }

   else if (xPlayer(Killer) != None)
      xPlayer(Killer).ReceiveLocalizedMessage(Class'KillsMessage',,,,Other.Pawn.Class);
}


// Save FalkDosh on assist
function ScoreKillAssists(float Score, Controller Victim, Controller Killer)
{
   local int i;
   local float GrossDamage, ScoreMultiplier, KillScore;
   local KFMonsterController fVictim;
   local KFPlayerReplicationInfo KFPRI;
   local PlayerController PC;

   fVictim = KFMonsterController(Victim);

   if (fVictim == none)
      return;

   if (fVictim.KillAssistants.Length > 1)
   {
      for (i=0; i<fVictim.KillAssistants.Length; i++)
         GrossDamage += fVictim.KillAssistants[i].Damage;

      ScoreMultiplier = Score / GrossDamage;

      for (i=0; i<fVictim.KillAssistants.Length; i++)
      {
         PC = PlayerController(fVictim.KillAssistants[i].PC);

         if (PC != none && PC.PlayerReplicationInfo != none)
         {
            KillScore = ScoreMultiplier * fVictim.KillAssistants[i].Damage;
            PC.PlayerReplicationInfo.Score += KillScore;
            FalkSetDosh(PC, KillScore, 4);

            KFPRI = KFPlayerReplicationInfo(PC.PlayerReplicationInfo);

            if (KFPRI != none)
            {
               if (PC != Killer)
                  KFPRI.KillAssists++;

               KFPRI.ThreeSecondScore += KillScore;
            }
         }
      }
   }

   else if (Killer.PlayerReplicationInfo != none)
   {
      Killer.PlayerReplicationInfo.Score += Score;

      if (PlayerController(Killer) != none)
         FalkSetDosh(PlayerController(Killer), Score, 4);
   }
}


// simpler reward system, you survive? you get a fixed amount of dosh
function bool RewardSurvivingPlayers()
{
   local Controller C;
   //log("RewardSurvivingPlayers");

   for (C = Level.ControllerList; C != none; C = C.NextController)
   {
      if (C.Pawn != none && C.PlayerReplicationInfo != none)
      {
         if (FCurrentWave >= FinalWave)
         {
            if (Level.Game.GameDifficulty >= 8.0)       // BB
               C.PlayerReplicationInfo.Score        += FLastWaveBonusBB;

            else if (Level.Game.GameDifficulty >= 7.0)       // HOE
               C.PlayerReplicationInfo.Score        += FLastWaveBonusHOE;

            else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
               C.PlayerReplicationInfo.Score        += FLastWaveBonusSUI;

            else if (Level.Game.GameDifficulty >= 4.0)  // hard
               C.PlayerReplicationInfo.Score        += FLastWaveBonusHRD;

            else                                        // normal
               C.PlayerReplicationInfo.Score        += FLastWaveBonusNRM;
         }

         else
         {

            if (Level.Game.GameDifficulty >= 8.0)       // BB
               C.PlayerReplicationInfo.Score        += FWaveBonusBB;

            else if (Level.Game.GameDifficulty >= 7.0)   // HOE
               C.PlayerReplicationInfo.Score        += FWaveBonusHOE;

            else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
               C.PlayerReplicationInfo.Score        += FWaveBonusSUI;

            else if (Level.Game.GameDifficulty >= 4.0)  // hard
               C.PlayerReplicationInfo.Score        += FWaveBonusHRD;

            else                                        // normal
               C.PlayerReplicationInfo.Score        += FWaveBonusNRM;
         }

         C.PlayerReplicationInfo.NetUpdateTime = Level.TimeSeconds - 1;

         if (Level.NetMode == NM_DedicatedServer && PlayerController(C) != none && !C.PlayerReplicationInfo.bOnlySpectator) // storing dosh amounts to reset if this player reconnects
         {
            //log("Giving reward to: "@PlayerController(C).GetPlayerIDHash());
            FalkSetDosh(PlayerController(C), C.PlayerReplicationInfo.Score);
         }
      }

   }

   return true;
}


// A player connected, we add no cash here since we can't get his SteamID
event PlayerController Login(string Portal, string Options, out string Error)
{
   local PlayerController NewPlayer;
   local Controller C;

   NewPlayer = Super.Login(Portal, Options, Error);

   if (NewPlayer.PlayerReplicationInfo.bOnlySpectator && NumSpectators > MaxSpectators)
   {
      Error = GameMessageClass.Default.MaxedOutMessage;
      NewPlayer.Destroy();
      return None;
   }

   else if (!NewPlayer.PlayerReplicationInfo.bOnlySpectator && NumPlayers > MaxPlayers && !NewPlayer.PlayerReplicationInfo.bAdmin)
   {
      Error = GameMessageClass.Default.MaxedOutMessage;
      NewPlayer.Destroy();
      return None;
   }

   for (C=Level.ControllerList; C!=None; C=C.NextController)
   {
      if ((C.PlayerReplicationInfo != None) && C.PlayerReplicationInfo.bOutOfLives && !C.PlayerReplicationInfo.bOnlySpectator && !GameReplicationInfo.bMatchHasBegun)
      {
         NewPlayer.PlayerReplicationInfo.bOutOfLives = true;
         NewPlayer.PlayerReplicationInfo.NumLives = 1;
         Break;
      }
   }

   NewPlayer.SetGRI(GameReplicationInfo);

   if (bDelayedStart && WaveNum < FinalWave + 1)
   {
      NewPlayer.GotoState('PlayerWaiting');
      return NewPlayer;
   }

   return NewPlayer;
}

// don't spawn after patriarch
event PostLogin(PlayerController NewPlayer)
{
   local int i;

   NewPlayer.SetGRI(GameReplicationInfo);
   NewPlayer.PlayerReplicationInfo.PlayerID = CurrentID++;

   Super(Invasion).PostLogin(NewPlayer);

   if (UnrealPlayer(NewPlayer) != None)
      UnrealPlayer(NewPlayer).ClientReceiveLoginMenu(LoginMenuClass, bAlwaysShowLoginMenu);

   if (NewPlayer.PlayerReplicationInfo.Team != None)
      GameEvent("TeamChange",""$NewPlayer.PlayerReplicationInfo.Team.TeamIndex,NewPlayer.PlayerReplicationInfo);

   if (NewPlayer != None)
   {
      if (Level.NetMode == NM_ListenServer)
      {
         if (Level.GetLocalPlayerController() == NewPlayer)
            NewPlayer.InitializeVoiceChat();
      }
   }

   if (KFPlayerController(NewPlayer) != none)
   {
      for (i = 0; i < InstancedWeaponClasses.Length; i++)
         KFPlayerController(NewPlayer).ClientWeaponSpawned(InstancedWeaponClasses[i], none);
   }

   if (NewPlayer.PlayerReplicationInfo.bOnlySpectator) // must not be a spectator
      KFPlayerController(NewPlayer).JoinedAsSpectatorOnly();

   else if (WaveNum < FinalWave + 1)
      NewPlayer.GotoState('PlayerWaiting');

   if (KFPlayerController(NewPlayer) != None)
      StartInitGameMusic(KFPlayerController(NewPlayer));

   if (bCustomGameLength && NewPlayer.SteamStatsAndAchievements != none)
      NewPlayer.SteamStatsAndAchievements.bUsedCheats = true;
}

function StartGameMusic(bool bCombat)
{
   local Controller C;
   local string S;

   if(MapSongHandler == None)
      Return;

   if (bCombat)
   {
      if (bWaveBossInProgress)
      {
         MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = BossBattleSong;
         S = BossBattleSong;
      }

      else if (MapSongHandler.WaveBasedSongs.Length<=WaveNum || MapSongHandler.WaveBasedSongs[WaveNum].CombatSong=="")
         S = MapSongHandler.CombatSong;

      else
         S = MapSongHandler.WaveBasedSongs[WaveNum].CombatSong;

      MusicPlaying = True;
      CalmMusicPlaying = False;
   }

   else
   {
      if (bWaveBossInProgress)
      {
         MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = BossBattleSong;
         S = BossBattleSong;
      }

      if (MapSongHandler.WaveBasedSongs.Length<=WaveNum || MapSongHandler.WaveBasedSongs[WaveNum].CalmSong=="")
         S = MapSongHandler.Song;

      else
         S = MapSongHandler.WaveBasedSongs[WaveNum].CalmSong;

      CalmMusicPlaying = True;
      MusicPlaying = False;
   }

   for (C=Level.ControllerList;C!=None;C=C.NextController)
   {
      if (KFPlayerController(C)!= none)
         KFPlayerController(C).NetPlayMusic(S, MapSongHandler.FadeInTime,MapSongHandler.FadeOutTime);
   }
}

// Play boss song for new players
function StartInitGameMusic(KFPlayerController Other)
{
   local string S;

   if(MapSongHandler == None)
      Return;

   if (MusicPlaying)
   {
      if (bWaveBossInProgress)
      {
         MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = BossBattleSong;
         S = BossBattleSong;
      }

      else if (MapSongHandler.WaveBasedSongs.Length <= WaveNum || MapSongHandler.WaveBasedSongs[WaveNum].CombatSong == "")
         S = MapSongHandler.CombatSong;

      else
         S = MapSongHandler.WaveBasedSongs[WaveNum].CombatSong;
   }

   else if (CalmMusicPlaying)
   {
      if (bWaveBossInProgress)
      {
         MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = BossBattleSong;
         S = BossBattleSong;
      }

      else if (MapSongHandler.WaveBasedSongs.Length <= WaveNum || MapSongHandler.WaveBasedSongs[WaveNum].CalmSong == "")
         S = MapSongHandler.Song;

      else
         S = MapSongHandler.WaveBasedSongs[WaveNum].CalmSong;
   }

   if (S != "")
      Other.NetPlayMusic(S,0.5,0);
}

// someone disconnected, let's store his dosh
function Logout(Controller Exiting)
{
   //local FHumanPawn FHP;

   FDelayedSStalkerCheck = Default.FSStalkerCheckDelay;

   if (Level.NetMode == NM_DedicatedServer && PlayerController(Exiting) != none)
   {
      /*if (fShouldStoreDoshOnDisconnect(PlayerController(Exiting)))
      { 
         warn("Stored Dosh: "@PlayerController(Exiting).PlayerReplicationInfo.Score);*/
         FalkSetDosh(PlayerController(Exiting), PlayerController(Exiting).PlayerReplicationInfo.Score, 1);
      //}

      fSetSpawnedThisWave(PlayerController(Exiting));
   }

   super.Logout(Exiting);
}


// let's handle this like it was a disconnect
function bool BecomeSpectator(PlayerController P)
{
   local FHumanPawn FHP;

   if (P.PlayerReplicationInfo==None || P.PlayerReplicationInfo.bOnlySpectator)
      return False;

   if (Level.NetMode == NM_DedicatedServer && P.Pawn != none)
   {
      // do this on spectator since the pawn death isn't working in this case for some reason
      //warn("SPECTATOR");
      DecStalkerViewSharers(P);

      // tell the pawns they have to stop seeing stalkers if nobody with the skill is alive
      if (fSStalkerSkills <= 0)
      {
         foreach DynamicActors(class'FHumanPawn', FHP)
         {
            FHP.fSStalkerInTeam = False;
         }
      }

      //if (fShouldStoreDoshOnDisconnect(P))
      FalkSetDosh(P, P.PlayerReplicationInfo.Score, 1);

      if (!bWaveInProgress)
         fSetSpawnedThisWave(P);
   }

   return Super.BecomeSpectator(P);
}


// don't reset cash on become player
function bool AllowBecomeActivePlayer(PlayerController P)
{
   if (P.PlayerReplicationInfo == None || !P.PlayerReplicationInfo.bOnlySpectator)
      return false;   

   if (!GameReplicationInfo.bMatchHasBegun || (NumPlayers >= MaxPlayers) || P.IsInState('GameEnded') || P.IsInState('RoundEnded'))
   {
      P.ReceiveLocalizedMessage(GameMessageClass, 13);
      return false;
   }

   if ((Level.NetMode == NM_Standalone) && (NumBots > InitialBots))
   {
      RemainingBots--;
      bPlayerBecameActive = true;
   }

   bForceRespawn = true;

   return true;
}


// removed weird shit
function int ReduceDamage(int Damage, pawn injured, pawn instigatedBy, vector HitLocation, out vector Momentum, class<DamageType> DamageType)
{
   local KFPlayerController PC;
   local KFPlayerReplicationInfo KFPRI;

   if (KFPawn(Injured) != none)
   {
      KFPRI = KFPlayerReplicationInfo(Injured.PlayerReplicationInfo);

      if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
         Damage = KFPRI.ClientVeteranSkill.Static.ReduceDamage(KFPRI, KFPawn(Injured), instigatedBy, Damage, DamageType);
   }

   // no damage from welders, wtf
   if (DamageType == class'DamTypeWelder')
      return 0;

   // This stuff cuts thru all the B.S, porcamadonna
   if (class<DamTypeBloatVomitFalk>(DamageType) != none || DamageType == class'SirenScreamDamage')
      return Damage;

   if (instigatedBy == None)
      return Super(xTeamGame).ReduceDamage(Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);

   if (Monster(Injured) != None)
   {
      if (instigatedBy != None)
      {
         PC = KFPlayerController(instigatedBy.Controller);

         if (Class<KFWeaponDamageType>(damageType) != none && PC != none)
            Class<KFWeaponDamageType>(damageType).Static.AwardDamage(KFSteamStatsAndAchievements(PC.SteamStatsAndAchievements), Clamp(Damage, 1, Injured.Health));
      }

      return super(UnrealMPGameInfo).ReduceDamage(Damage, injured, InstigatedBy, HitLocation, Momentum, DamageType);
   }

   // Not sure if this is real or fixing a bug with the damage log or whatever
   if (class<KFWeaponDamageType>(DamageType) != None && class<KFWeaponDamageType>(DamageType).default.bIsExplosive)
      Damage *= 1.25;

   if (injured != instigatedBy && MonsterController(InstigatedBy.Controller) == None)
      return 0;

   Damage = super(UnrealMPGameInfo).ReduceDamage(Damage, injured, InstigatedBy, HitLocation, Momentum, DamageType);

   if (instigatedBy == None)
      return Damage;

   return (Damage * instigatedBy.DamageScaling);
}


// Play last standing and wipe song
function bool CheckMaxLives(PlayerReplicationInfo Scorer)
{
   local Controller C;
   local PlayerController Living;
   local byte AliveCount;

   if (MaxLives > 0)
   {
      for (C=Level.ControllerList; C!=None; C=C.NextController)
      {
         if ((C.PlayerReplicationInfo != None) && C.bIsPlayer && !C.PlayerReplicationInfo.bOutOfLives && !C.PlayerReplicationInfo.bOnlySpectator)
         {
            AliveCount++;

            if(Living == None)
               Living = PlayerController(C);
         }
      }

      // play the wipe song
      if (AliveCount == 0)
      {
         if (FCurrentWave < FinalWave && /*WaveNum < FinalWave &&*/ bWaveInProgress && !FWipeSongStarted)
         {
            FWipeSongStarted                                  = true;
            MapSongHandler.CombatSong                         = WipeSong;
            MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = WipeSong;

            for(C=Level.ControllerList; C!=None; C=C.NextController)
            {
               if (KFPlayerController(C)!= none)
                  KFPlayerController(C).NetPlayMusic(WipeSong, MapSongHandler.FadeInTime,MapSongHandler.FadeOutTime);
            }
         }

         EndGame(Scorer,"LastMan");
         return true;
      }

      else if (FAlivePlayers > 1 && !bNotifiedLastManStanding && AliveCount==1 && Living != None)
      {
         bNotifiedLastManStanding = true;
         Living.ReceiveLocalizedMessage(Class'FLastManStandingMsg');

         if (!bWaveBossInProgress && !CalmMusicPlaying) // play last standing song
         {
            FResetSong                                        = True;
            CurrentWaveSong                                   = MapSongHandler.WaveBasedSongs[WaveNum].CombatSong;
            MapSongHandler.CombatSong                         = LastStandingSong;
            MapSongHandler.WaveBasedSongs[WaveNum].CombatSong = LastStandingSong;

            for(C=Level.ControllerList; C!=None; C=C.NextController)
            {
               if (KFPlayerController(C)!= none)
                  KFPlayerController(C).NetPlayMusic(LastStandingSong, MapSongHandler.FadeInTime,MapSongHandler.FadeOutTime);
            }
         }
      }
   }

   return false;
}


// not sure what's this but imma override the shit out of it
function bool KillZed(Controller c)
{
    return false;
}

// set achievement unlock state on the controller
function SetUnlocked(PlayerController PC, byte idx, bool uStatus)
{
   local int i;
   local KFPlayerReplicationInfo KFPRI;
   local FPCServ FPC;

   if (PC != none)
   {
      KFPRI = KFPlayerReplicationInfo(PC.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         FPC = FPCServ(PC);

         i = ZedTimeIndex(PC);

         //log("Setting ach "@idx-2@" to "@uStatus);

         uStatus = True; // debug stuff

         switch (idx)
         {
            case 0:
               if (FPC != none)
                  FPC.medicAchievement    = uStatus;
               break;

            case 1:
               if (FPC != none)
                  FPC.supportAchievement  = uStatus;
               break;

            case 2:
               if (FPC != none)
                  FPC.sharpAchievement    = uStatus;
               break;

            case 3:
               if (FPC != none)
                  FPC.commandoAchievement = uStatus;
               break;

            case 4:
               if (FPC != none)
                  FPC.zerkAchievement     = uStatus;
               break;

            case 5:
               if (FPC != none)
                  FPC.firebugAchievement  = uStatus;
               break;

            case 6:
               if (FPC != none)
                  FPC.demoAchievement     = uStatus;
               break;

            case 7:
               if (FPC != none)
                  FPC.artiAchievement     = uStatus;
               break;
         }
      }
   }
}

// return configurable var result
function bool FalkAllowOnePerkChange()
{
   return Default.FAllowOnePerkChange;
}

// spawn a bit more zeds if there are more players
function AddBossBuddySquad()
{
    local int numspawned;
    local int TotalZombiesValue;
    local int i;
    local int TempMaxMonsters;
    local int TotalSpawned;
    local int TotalZeds;
    local int SpawnDiff;

    // Scale the number of helpers by the number of players
    if (NumPlayers == 1)
        TotalZeds = 8;

    else if (NumPlayers <= 5)
        TotalZeds = 10;

    else if (NumPlayers <= 11)
        TotalZeds = 14;

    else
        TotalZeds = 16;

    for (i = 0; i<10; i++)
    {
        if (TotalSpawned >= TotalZeds)
        {
            FinalSquadNum++;
            return;
        }

        numspawned = 0;

        // Set up the squad for spawning
        NextSpawnSquad.length = 0;
        AddSpecialPatriarchSquad();

        LastZVol = FindSpawningVolume();

        if (LastZVol != None)
            LastSpawningVolume = LastZVol;

        if (LastZVol == None)
        {
            LastZVol = FindSpawningVolume();

            if (LastZVol != None)
                LastSpawningVolume = LastZVol;

            if (LastZVol == none)
                warn("Error!!! Couldn't find a place for the Patriarch squad after 2 tries!!!");
        }

        // See if we've reached the limit
        if ((NextSpawnSquad.Length + TotalSpawned) > TotalZeds)
        {
            SpawnDiff = (NextSpawnSquad.Length + TotalSpawned) - TotalZeds;

            if (NextSpawnSquad.Length > SpawnDiff)
                NextSpawnSquad.Remove(0, SpawnDiff);

            else
            {
                FinalSquadNum++;
                return;
            }

            if (NextSpawnSquad.Length == 0)
            {
                FinalSquadNum++;
                return;
            }
        }

        // Spawn the squad
        TempMaxMonsters = 999;

        if (LastZVol.SpawnInHere(NextSpawnSquad,,numspawned,TempMaxMonsters,999,TotalZombiesValue))
        {
            NumMonsters += numspawned;
            WaveMonsters+= numspawned;
            TotalSpawned += numspawned;

            NextSpawnSquad.Remove(0, numspawned);
        }
    }

    FinalSquadNum++;
}

// Uppercase The, huge updates
function string GetNameOf( Pawn Other )
{
    local string S;

    if(Other == None)
        Return "Someone";

    if (Other.PlayerReplicationInfo != None)
        Return Other.PlayerReplicationInfo.PlayerName;

    S = Other.MenuName;

    if (S == "")
    {
        Other.MenuName = string(Other.Class.Name);
        S = Other.MenuName;
    }

    if (FalkMonsterBase(Other) != None && FalkMonsterBase(Other).FIsBoss)
        Return "The"@S;

    else if (Class'FInvasionMessage'.Static.ShouldUseAn(S))
        Return "an"@S;

    else Return "a"@S;
}

// removed no late joiners message since it's broken anyway
event PreLogin( string Options, string Address, string PlayerID, out string Error, out string FailCode )
{
    Super(Invasion).PreLogin(Options, Address, PlayerID, Error, FailCode);
}

// a flare was spawned, update the counter
function FlareSpawned(PlayerController PC)
{
   local FHumanPawn FHP;

   if (Role == ROLE_Authority)
   {
      fCurrentFlares++;

      //warn("ADD fCurrentFlares"@fCurrentFlares@"- Time:"@Level.TimeSeconds);

      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         FHP.fTotalFlares = fCurrentFlares;
      }

      if (PC.Pawn != none)
      {
         FHP = FHumanPawn(PC.Pawn);

         if (FHP != none)
         {
            FHP.fPlayerFlares++;
         }
      }
   }
}

// a flare was destroyed, update the counter
function FlareDestroyed(PlayerController PC)
{
   local FHumanPawn FHP;

   if (Role == ROLE_Authority)
   {
      fCurrentFlares--;

      //warn("REM fCurrentFlares"@fCurrentFlares@"- Time:"@Level.TimeSeconds);

      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         FHP.fTotalFlares = fCurrentFlares;
      }

      if (PC.Pawn != none)
      {
         FHP = FHumanPawn(PC.Pawn);

         if (FHP != none)
         {
            FHP.fPlayerFlares--;
         }
      }
   }
}

// used to update the unlimited stalker visibility from classes located before our veterancy types
function ExtStalkerViewShareUpdate(PlayerController PC, class<KFVeterancyTypes> KFVet)
{
   local class<FVeterancyTypes> FVet;
   local FHumanPawn FHP;

   FVet = class<FVeterancyTypes>(KFVet);

   if (FVet != none)
   {
      //warn("EXTERNAL STALKER UPDATE");
      StalkerViewShareUpdate(PC, FVet.Static.ShareStalkerVisibility(KFPlayerReplicationInfo(PC.PlayerReplicationInfo)));
   }

   fSStalkerSkills = GetStalkerViewSharers(); // update how many have the shared stalker view skill

   // if anybody has the skill, tell the pawn they have to see stalkers during zed time
   if (fSStalkerSkills > 0)
   {
      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         //warn("EXTERNAL ON");
         FHP.fSStalkerInTeam = True;
      }
   }

   // else just tell them to suck a dick
   else
   {
      foreach DynamicActors(class'FHumanPawn', FHP)
      {
         //warn("EXTERNAL OFF");
         FHP.fSStalkerInTeam = False;
      }
   }
}

// remove the lobby timer to autostart the game even if the players aren't ready
auto State PendingMatch
{
    function RestartPlayer( Controller aPlayer )
    {
        if ( CountDown <= 0 )
            RestartPlayer(aPlayer);
    }

    function Timer()
    {
        local Controller P;
        local bool bReady;
        local int PlayerCount, ReadyCount;

        Global.Timer();

        if ( Level.NetMode == NM_StandAlone && NumSpectators > 0 ) // Spectating only.
        {
            StartMatch();
            PlayStartupMessage();
            return;
        }

        // first check if there are enough net players, and enough time has elapsed to give people
        // a chance to join
        if ( NumPlayers == 0 )
            bWaitForNetPlayers = true;

        if ( bWaitForNetPlayers && Level.NetMode != NM_Standalone )
        {
            if ( NumPlayers >= MinNetPlayers )
                ElapsedTime++;
            else
                ElapsedTime = 0;

            if ( NumPlayers == MaxPlayers || ElapsedTime > NetWait )
                bWaitForNetPlayers = false;
        }

        if ( Level.NetMode != NM_Standalone && (bWaitForNetPlayers || (bTournament && NumPlayers < MaxPlayers)) )
        {
            PlayStartupMessage();
            return;
        }

        // check if players are ready
        bReady = true;
        StartupStage = 1;

        for (P = Level.ControllerList; P != None; P = P.NextController)
        {
            if (P.IsA('PlayerController') && P.PlayerReplicationInfo != none && P.bIsPlayer && P.PlayerReplicationInfo.Team != none && P.PlayerReplicationInfo.bWaitingPlayer && !P.PlayerReplicationInfo.bOnlySpectator)
            {
                PlayerCount++;

                if (!P.PlayerReplicationInfo.bReadyToPlay)
                    bReady = false;

                else
                    ReadyCount++;
            }
        }

        if (PlayerCount > 0 && bReady && !bReviewingJumpspots)
            StartMatch();

        PlayStartupMessage();

        /*if (NumPlayers > 2)
            ElapsedTime++;*/

        if (PlayerCount > 1 && ReadyCount >= PlayerCount - 1 && fLobbyTimeout > 0)
        {
            if (fLobbyTimeout <= 1)
            {
                for (P = Level.ControllerList; P != None; P = P.NextController)
                {
                    if ( P.IsA('PlayerController') && P.PlayerReplicationInfo != none )
                        P.PlayerReplicationInfo.bReadyToPlay = True;
                }

                fLobbyTimeout = 0;
            }

            else
                fLobbyTimeout--;

            KFGameReplicationInfo(GameReplicationInfo).LobbyTimeout = fLobbyTimeout;
        }

        else
        {
           fLobbyTimeout = Default.fLobbyTimeout;
           KFGameReplicationInfo(GameReplicationInfo).LobbyTimeout = -1;
        }
    }

    function BeginState()
    {
        bWaitingToStartMatch = true;
        StartupStage = 0;

        if (fLobbyTimeout <= 0)
            LobbyTimeCounter = 10;
        else
            LobbyTimeCounter = fLobbyTimeout;

        NetWait = Max(NetWait,0);
    }

    function EndState()
    {
        KFGameReplicationInfo(GameReplicationInfo).LobbyTimeout = -1;
    }

Begin:
    if ( bQuickStart )
    {
        StartMatch();
    }
}


// Kill the other zeds instead of blocking them on game end
function DoBossDeath()
{
   bZEDTimeActive         =  true;
   bSpeedingBackUp        = false;
   LastZedTimeEvent       = Level.TimeSeconds;
   CurrentZEDTimeDuration = ZEDTimeDuration * 2;
   SetGameSpeed(ZedTimeSlomoScale);

   FalkForceEndWave();
}


// return how many share the unlimited shared zed time stalker skill
function int GetStalkerViewSharers()
{
   local int i, result;
   local FHumanPawn FHP;

   while (i < fZedUsed.length)
   {
      if (fZedUsed[i].PC != none && fZedUsed[i].PC.Pawn != none && fZedUsed[i].fShareStalkers)
      {
         FHP = FHumanPawn(fZedUsed[i].PC.Pawn);

         if (FHP != none && FHP.fHealth > 0 && FHP.Health > 0)
            result++;
      }

      i++;
   }

   //warn("Shared Skill:"@result);

   return result;
}

defaultproperties
{
   BBWSpawnsMultiplier=0.35
   BBASpawnsMultiplier=0.35
   HOEWSpawnsMultiplier=0.35
   HOEASpawnsMultiplier=0.35
   SuicidalWSpawnsMultiplier=0.35
   SuicidalASpawnsMultiplier=0.35
   HardWSpawnsMultiplier=0.35
   HardASpawnsMultiplier=0.35
   NormalWSpawnsMultiplier=0.35
   NormalASpawnsMultiplier=0.35
   FMetalClotclass=(ZedClass=("Falk689ZedsFix.FZombieMetalClot_STANDARD"),NumZeds=(1))
   FMetalClotWaveCheck=-1
   AmmoRespawnTimeOT=25.0
   AmmoRespawnTimeFS=22.5
   AmmoRespawnTimeSN=20.0
   AmmoRespawnTimeMX=17.5
   AmmoSpawnMulti=0.5
   WeaponSpawnMulti=0.5
   MaxAmmoPerWaveOT=1.0
   MaxAmmoPerWaveFS=1.5
   MaxAmmoPerWaveSN=2.0
   MaxAmmoPerWaveMX=2.5
   MaxAmmoLastWaveM=2.0
   MaxWeaponsPerWaveOT=0.5
   MaxWeaponsPerWaveFS=1.0
   MaxWeaponsPerWaveSN=1.5
   MaxWeaponsPerWaveMX=2.0
   FWaveSpawnPeriod=0.0          // set all of them to 0 to make it more hardcore cause im a cunt with attention deficit issues that can't wait a couple seconds for zombies to spawn..... oh wait that's Snake
   FWaveSpawnPeriodSeven=0.0     
   FWaveSpawnPeriodTen=0.0      
   FMetalClotInitWave=1
   FMetalClotEndWave=3
   FMetalClotSpawnChance=4
   FAllowOnePerkChange=True
   FSStalkerCheckDelay=0.5
   FDebugTick=1.0
   fLockedWeapons=7
   fLobbyTimeout=60
   LoginMenuClass="Falk689ServerPerks.FInvasionLoginMenu"
   HUDType="Falk689ServerPerks.HUDFalk"
   ScoreBoardType="Falk689ServerPerks.FScoreBoard"
   FallbackMonsterClass="Falk689ZedsFix.FZombieStalker_STANDARD"
   EndGameBossClass="Falk689ZedsFix.FZombieBoss_STANDARD"
   BossBattleSong="BMIMF"
   LastStandingSong="Lastonealive"
   WipeSong="WipeSong"
   GameName="DO NOT SELECT ME"
   GameMessageClass=Class'FGameMessages'
   Description="Base structure to make 'Lair Single Player' work. Do not select this dud, but the single player game type."
   KFHints(0)="The brute tends to defend his head, making it more difficult for sharpshooters to kill him."
   KFHints(1)="A syringe heals you for 30 HP when you're alone, while it heals for 20 HP when two or more players are present."
   KFHints(2)="The fleshpound halves any damage he receives, but takes 50% more damage against explosives."
   KFHints(3)="Specimens build up incremental resistances to ice and electricity the more they get afflicted with such status ailments."
   KFHints(4)="If you happen to spot a very rare specimen during the early waves, don't take him lightly."
   KFHints(5)="The scrake takes 25% more damage against fire, but beware of his wrathful charge if you set him aflame."
   KFHints(6)="Crawlers and stalkers may spawn in uncommon places and will usually attack in group."
   KFHints(7)="Husks can hit you directly with their fire missiles."
   KFHints(8)="The more you weigh, the slower you move. Just like in real life."
   KFHints(9)="Reach level 6 with a perk to unlock its unique zed-time skill."
   KFHints(10)="Bloats vomit in a constant stream, making it possible for you to partially dodge their attacks if you move away fast enough."
   KFHints(11)="Check the online Lair Handbook in the guides section if you want detailed information about weapons, grenades, zeds, difficulties."
   KFHints(12)="Pay attention to weapons descriptions at the trader, you might acquire useful information regarding them."
   KFHints(13)="Remember those days when you could kill the Patriarch before he healed? They're gone, unless you're carrying nukes."
   KFHints(14)="Every perk has a dedicated grenade with its own purpose."
   KFHints(15)="Never go for the heads of specimens when tossing hand grenades, unless you're using throwing knives."
   KFHints(16)="There's a cheap worn kevlar available under the tools menu at the trader, ask your teammates to repair it in order to save dosh."
   ScreenShotName="LairTextures_T.Thumbnails.DONOTSELECTME"
   BroadcastClass=class'Falk689BroadcastHandler'
   GIPropsExtras(0)="1.000000;Beginner;2.000000;Normal;4.000000;Hard;5.000000;Suicidal;7.000000;Hell on Earth;8.000000;Bloodbath"
}
