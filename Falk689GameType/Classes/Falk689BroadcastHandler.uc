class Falk689BroadcastHandler extends BroadcastHandler;

// kind of don't do weird shit and let us spam
function Broadcast( Actor Sender, coerce string Msg, optional name Type )
{
	local Controller C;
	local PlayerController P;
	local PlayerReplicationInfo PRI;

	// don't limit spam, just return empty messages
	if (Len(Msg) < 1 || Msg == " " || Msg == "  " || Msg == "   ")
		return;

	if ( Pawn(Sender) != None )
		PRI = Pawn(Sender).PlayerReplicationInfo;
	else if ( Controller(Sender) != None )
		PRI = Controller(Sender).PlayerReplicationInfo;

	if ( bPartitionSpectators && !Level.Game.bGameEnded && (PRI != None) && !PRI.bAdmin && (PRI.bOnlySpectator || PRI.bOutOfLives) )
	{
		For ( C=Level.ControllerList; C!=None; C=C.NextController )
		{
			P = PlayerController(C);
			if ( (P != None) && (P.PlayerReplicationInfo.bOnlySpectator || P.PlayerReplicationInfo.bOutOfLives) )
				BroadcastText(PRI, P, Msg, Type);
		}
	}
	else
	{
		For ( C=Level.ControllerList; C!=None; C=C.NextController )
		{
			P = PlayerController(C);
			if ( P != None )
				BroadcastText(PRI, P, Msg, Type);
		}
	}
}

defaultproperties
{
}
