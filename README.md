# LAIR TOTAL GAME BALANCE

## INTRODUCTION
Total game balance for Killing Floor that features the following:

* overhaul of all original perks
* overhaul of all original weapons
* overhaul of all original specimens
* all available difficulties
* all available DLC weapons
* all zeds upscaled in 4K
* perks up to level 20
* unique grenades
* unique zed-time skills
* intense boss fight
* possibility to skip trading time
* real weapons names
* unlockable weapons
* achievements
* 1 new perk
* 1 new specimen
* 1 new difficulty
* 49 new weapons
* 46 custom maps with custom music
* 12 available player slots per server

Lair aims to be the place where the most refined and competitive gameplay takes place, everything has been done keeping that final result in mind.
There are no major visual reworks except for the upscaling of zeds, most of the changes are under the hood and related to fixing bugs, exploits, broken mechanics, and so on.
It's still Killing Floor as you know it, just much more refined, cooperative, balanced, competitive, and fun.

## HOW TO INSTALL
You can either decide to install the full package, or directly download the files from the server into your "Cache" folder.
However, Lair comes with a single player mode that you wouldn't be able to run without having the full package installed.
Also, music files can't be directly downloaded from the server.
We can't ensure that directly downloading everything from the server will work if you have other custom content installed.
In order to install all necessary files, scroll down where the official direct download is, and click it.
If, for any reason, it doesn't load, just refresh the page a few seconds later.
When the download is over, extract all the folders inside the package into your "...Steam\steamapps\common\KillingFloor" folder, and overwrite everything.
Make sure you've enabled an option to see file extensions on your OS, while this is not mandatory, it will help avoid unnecessary misunderstandings during the next step.
There will be a couple files respectively called "ServerPerksFalk.ini" and "ServerPerksFalk (Linux).ini" in the "System" folder, make sure you delete the first and rename the second one to "ServerPerksFalk.ini" if you're playing the game on Linux, otherwise just ignore or delete the latter.
We've included a "KillingFloor.ini.bak" in case you'll need the vanilla file back, which can be done by simply removing the ".bak" extension to it after moving our custom "KillingFloor.ini" file elsewhere.

## HOW TO PLAY SINGLE PLAYER MODE
Single player mode is very useful to get some first generic impressions.
In order to play it, follow these next steps:

* click on "Host Game"
* go to the "Mutators" tab
* make sure to remove all active mutators, if there are any
* find all seven mutators which names start with "Lair" and add them to the "Active Mutators" list
* go to the "Gametype" tab
* click on "Lair Single Player"
* select the difficulty you wish, although this specific game mode is balanced for "Hard" difficulty
* pick a map you want to try out and you're good to go

From now on, you'll just need to click on "Host Game" and select "Lair Single Player" in order to play, since all the mutators need to be activated only once.
If you wish to host a vanilla game after playing with this gametype, you'll need to restart the game and remove "Lair" mutators.
Please note that "Bloodbath" difficulty is not available in single player mode.

## HOW TO PLAY ONLINE
Join our official servers by finding them in the "Internet Games" tab or by directly connecting to them using the "Open IP" option.
If you decide to look for them via query, make sure all search filters are unchecked and the difficulty is set to "Any Difficulty" before refreshing the online servers list.
Remember to add them to your favorites so that the next time you'll be able to instantly find them.
Lair EU #1: 93.42.98.64:7707 (Rome, Italy)
Lair EU #2: 93.42.98.64:7709 (Rome, Italy)
Lair EU #3: 93.42.98.64:7711 (Rome, Italy)
Lair USA #1: 199.231.161.114:7707 (Los Angeles, California)
Lair USA #2: 206.217.202.80:7707 (Dallas, Texas)
Lair USA #3: 107.182.226.184:7707 (New York City, New York)

## RULES
Lair is supposed to be an extremely cooperative and competitive environment, please refrain from disrupting the flow of the game.
Abusive language and behaviour will not be tolerated.
Teaming up with other players in order to kick someone from the server without a solid reason is strictly forbidden.
Cheating will find you banned indefinetely.
Moderators have no obligation to warn players before taking action.

## USEFUL LINKS
* [Official direct download](http://falk83.altervista.org/lairpack)
* [Official Steam workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=2775547996)
* [Official group](https://steamcommunity.com/groups/LairTGB)
* [Official handbook](https://steamcommunity.com/sharedfiles/filedetails/?id=1604764184)

## FEEDBACK
You're highly encouraged to leave feedback.
In order to do so, please post it [here](https://steamcommunity.com/groups/LairTGB/discussions/0/2265817017327116073).

## TECHNICALITIES
Power, range, and speed values of each weapon have been calculated based on comparing weapons of the same perk.
If you happen to witness the server changing to a different map from the one that was voted, it's because a crash occurred and the server was automatically brought back up.
In case you experience any sort of lag, such as your ammunition count on the HUD being updated with a delay, open the console, type "Netspeed 64000", and proceed to confirm the given command.
Whenever you need to quit, to make sure your progress gets correctly saved, properly disconnect from the server instead of abruptly closing the game.
If you happen to see your achievements progress not updating in real time, be assured it's only an extremely rare visual bug that will be fixed by reconnecting to the server or changing map.
Make sure your connection type is set to "Cable/ADSL" to prevent possible bugs from happening, such as being unable to see perks in the corresponding tab.

## TROUBLESHOOTING
Should you encounter any unknown issue, please post it [here](https://steamcommunity.com/groups/LairTGB/discussions/0/2266943550299751202).

## TOURNAMENTS
Better get some training, since we're planning to spark some competition in the future.
To keep yourself informed about this topic, subscribe to [this](https://steamcommunity.com/groups/LairTGB/discussions/0/2266943550316791064) discussion.

## CREDITS
* [Credits page](https://gitlab.com/Falk689/Lair/-/blob/master/CREDITS.md)

## DONATIONS
While the EU servers are hosted by ourselves, the USA servers are being rented from a third party service.
This means that they cost us money on a monthly basis.
If you ever consider helping us out, be assured that anything you send our way will be used for the upkeep of said servers.

* Paypal: [Andewyl](https://www.paypal.me/andewyl)
