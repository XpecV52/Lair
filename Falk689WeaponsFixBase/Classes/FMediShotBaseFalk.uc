class FMediShotBaseFalk extends KFMod.MP7MMedicGun
   abstract;

Const MaxAmmoCount=5000;

var bool    fShouldReload;        // used to force reload on all weapons
var bool    fPawnAmmoRestore;     // we restored ammo from the pawn
var bool    fPickupAmmoRestore;   // we restored ammo from the pickup
var float   fSyringeFixTime;      // how much time to fix syringe ammo from the pawn
var int     fHealingAmmoFalk;     // lazy fix but this should work
var int     fStoredAmmo;          // stored ammo from the pickup
var byte    FHUDGreyThreshold;  // visual stuff, make the HUD healing ammo number pink at this threshold instead of the default one

var byte    fTry;                 // How many times we tried to spawn a pickup
var byte    fMaxTry;              // How many times we should try to spawn a pickup
var vector  fCZRetryLoc;          // Z Correction we apply every retry
var vector  fCXRetryLoc;          // X Correction we apply every retry
var vector  fCYRetryLoc;          // Y Correction we apply every retry
var vector  fACZRetryLoc;         // computed Z correction
var vector  fACXRetryLoc;         // computed X correction
var vector  fACYRetryLoc;         // computed Y correction

replication
{
	reliable if (Role == ROLE_Authority)
		fHealingAmmoFalk, fStoredAmmo;
}

// using fHealingAmmoFalk
simulated function float ChargeBar()
{
	return FClamp(float(fHealingAmmoFalk)/float(Default.fHealingAmmoFalk),0,1);
}

// using fHealingAmmoFalk
simulated function MaxOutAmmo()
{
	if ( bNoAmmoInstances )
	{
		if ( AmmoClass[0] != None )
			AmmoCharge[0] = MaxAmmo(0);
		return;
	}
	if ( Ammo[0] != None )
		Ammo[0].AmmoAmount = Ammo[0].MaxAmmo;

	fHealingAmmoFalk = Default.fHealingAmmoFalk;
}

// not sure what this was but I feel like I won't miss it
simulated function SuperMaxOutAmmo()
{
   MaxOutAmmo();
}

// Start medic darts ammo istant reload
simulated function  StartForceReload()
{
   fShouldReload = True;
}

// Istantly reload medic darts ammo
simulated function bool ForceHealingReload()
{
   fHealingAmmoFalk = Default.fHealingAmmoFalk;

   return False; //(fHealingAmmoFalk < Default.fHealingAmmoFalk); // why was this a thing? - future falk
}

// using fHealingAmmoFalk
simulated function bool HasAmmo()
{
   if (fHealingAmmoFalk > 0)
      return true;

	if (bNoAmmoInstances)
    	return ((AmmoClass[0] != none && FireMode[0] != none && AmmoCharge[0] >= FireMode[0].AmmoPerFire));

   return (Ammo[0] != none && FireMode[0] != none && Ammo[0].AmmoAmount >= FireMode[0].AmmoPerFire);
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// using fHealingAmmoFalk;
simulated function int AmmoAmount(int mode)
{
   if (Mode == 1)
      return fHealingAmmoFalk;

   else
      return super.AmmoAmount(mode);
}

// using fHealingAmmoFalk
simulated function bool AmmoMaxed(int mode)
{
   if (Mode == 1)
      return fHealingAmmoFalk>=Default.fHealingAmmoFalk;

	else
	   return super.AmmoMaxed(mode);
}

// again, use a falk var
simulated function float AmmoStatus(optional int Mode)
{
    if (Mode == 1)
	   return float(fHealingAmmoFalk)/float(Default.fHealingAmmoFalk);

	else
	   return super.AmmoStatus(Mode);
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// using fHealingAmmoFalk
function bool AddAmmo(int AmmoToAdd, int Mode)
{
   if (Mode == 1)
   {
      if (fHealingAmmoFalk < Default.fHealingAmmoFalk)
      {
         fHealingAmmoFalk += AmmoToAdd;

         if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
            fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
      return true;
   }

   else
      return super.AddAmmo(AmmoToAdd,Mode);
}


// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}


defaultproperties
{
   MagCapacity=20
   ReloadRate=3.166
   fHealingAmmoFalk=5000
   HealAmmoCharge=5000
   AmmoRegenRate=0.200000 // smg fast recharge rate standard	
   HealBoostAmount=20
   fSyringeFixTime=0.5
   fStoredAmmo=-1
   fShouldReload=False
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   FHUDGreyThreshold=50
}
