class DamTypeSpitfireFalk extends DamTypeSW76Falk;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
	KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   bDealBurningDamage=True
   bCheckForHeadShots=False
}
