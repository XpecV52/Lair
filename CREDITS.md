# Lair Total Game Balance - Credits

* [Andewyl](https://steamcommunity.com/id/FearOfABlankPlanet): project management, gameplay balance, maps edits
* [Falk689](https://steamcommunity.com/id/Falk689): code, models, textures, animations
* [Marco](https://steamcommunity.com/profiles/76561197975509070): server perks mutator that made the entire project possible
* [Hemi](https://steamcommunity.com/id/hemicrania) & [Braindead](https://steamcommunity.com/profiles/76561197989157704) & [Benjamin](https://steamcommunity.com/id/BenjaminGoose): creation of the brute
* [scaryghost](https://github.com/scaryghost): server achievements mutator
* [IJC Development](https://steamcommunity.com/id/ijcdevelopment): concept of freeze mechanics and what's left of their original code
* [Electro 69](https://steamcommunity.com/profiles/76561198048284656): helping out fixing some of the weirdest bugs we've had to deal with over the years
* [Lorex](https://steamcommunity.com/id/Lorex634): helping out with the creation of the handbook
