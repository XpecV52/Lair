class ZEDGunPickupFalk extends KFMod.ZEDGunPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ZEDGunFalk'
   cost=500
   ItemName="Zed Eradication Device"
   ItemShortName="ZED"
   PickupMessage="You got a Zed Eradication Device"
   CorrespondingPerkIndex=8
   BuyClipSize=100
   AmmoCost=50
   PowerValue=80
   SpeedValue=40
   RangeValue=100
   Weight=8.000000
}
