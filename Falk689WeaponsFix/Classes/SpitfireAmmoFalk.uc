class SpitfireAmmoFalk extends FlameAmmo;

defaultproperties
{
   InitialAmount=200
   MaxAmmo=400
   AmmoPickupAmount=80      // 20% standard for % weapons
   PickupClass=Class'SpitfireAmmoPickupFalk'
}
