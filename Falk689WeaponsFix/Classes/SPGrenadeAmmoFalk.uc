class SPGrenadeAmmoFalk extends KFMod.SPGrenadeAmmo;

defaultproperties
{
    MaxAmmo=24
    InitialAmount=12
    AmmoPickupAmount=3
    PickupClass=Class'SPGrenadeAmmoPickupFalk'
    ItemName="Orca bombs"
}