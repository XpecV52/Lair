class M7A3MMedicGunFalk extends MP7MMedicGunFalk;

var localized   string  ReloadMessage;
var localized   string  EmptyMessage;

var() Material ScopeGreen;
var() Material ScopeRed;
var() ScriptedTexture MyScriptedTexture;

var string MyMessage;
var int MyHealth;
var   Font MyFont;
var   Font MyFont2;
var   Font SmallMyFont;
var  color MyFontColor;
var  color MyFontColor2;
var int OldValue;


static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
	default.ScopeGreen = Material(DynamicLoadObject("KF_Weapons5_Scopes_Trip_T.M7A3.Scope_Finall", class'Material', true));
	default.ScopeRed = Material(DynamicLoadObject("KF_Weapons5_Scopes_Trip_T.M7A3.ScopeRed_Shader", class'Material', true));
	default.MyScriptedTexture = ScriptedTexture(DynamicLoadObject("KF_Weapons5_Scopes_Trip_T.M7A3_Ammo_Script.AmmoNumber", class'ScriptedTexture', true));
	default.MyFont = Font(DynamicLoadObject("IJCFonts.DigitalBig", class'Font', true));
	default.MyFont2 = Font(DynamicLoadObject("IJCFonts.DigitalBig", class'Font', true));
	default.SmallMyFont = Font(DynamicLoadObject("IJCFonts.DigitalMed", class'Font', true));

	if ( M7A3MMedicGun(Inv) != none )
	{
		M7A3MMedicGun(Inv).ScopeGreen = default.ScopeGreen;
		M7A3MMedicGun(Inv).ScopeRed = default.ScopeRed;
		M7A3MMedicGun(Inv).MyScriptedTexture = default.MyScriptedTexture;
		M7A3MMedicGun(Inv).MyFont = default.MyFont;
		M7A3MMedicGun(Inv).MyFont2 = default.MyFont2;
		M7A3MMedicGun(Inv).SmallMyFont = default.SmallMyFont;
	}

	super(KFMedicGun).PreloadAssets(Inv, bSkipRefCount);
}

static function bool UnloadAssets()
{
	if ( super(KFMedicGun).UnloadAssets() )
	{
    	default.ScopeGreen = none;
    	default.ScopeRed = none;
    	default.MyScriptedTexture = none;
    	default.MyFont = none;
    	default.MyFont2 = none;
    	default.SmallMyFont = none;
		return true;
	}

	return false;
}


// The server lets the client know they successfully healed someone
simulated function ClientSuccessfulHeal(String HealedName)
{
    if( PlayerController(Instigator.Controller) != none )
    {
        PlayerController(Instigator.controller).ClientMessage(SuccessfulHealMessage$HealedName, 'CriticalEvent');
    }
}

simulated function int MaxAmmo(int mode)
{
    if( Mode == 1 )
    {
	   return Default.fHealingAmmoFalk;
	}
	else
	{
	   return super.MaxAmmo(mode);
	}
}

simulated function FillToInitialAmmo()
{
	if ( bNoAmmoInstances )
	{
		if ( AmmoClass[0] != None )
			AmmoCharge[0] = Max(AmmoCharge[0], AmmoClass[0].Default.InitialAmount);
        fHealingAmmoFalk = Default.fHealingAmmoFalk;

		return;
	}

	if ( Ammo[0] != None )
        Ammo[0].AmmoAmount = Ammo[0].AmmoAmount;

    fHealingAmmoFalk = Default.fHealingAmmoFalk;

}

simulated function int AmmoAmount(int mode)
{
    if( Mode == 1 )
    {
	   return fHealingAmmoFalk;
	   MyHealth = ChargeBar() * 100;
	}
	else
	{
	   return super.AmmoAmount(mode);
	}
}

simulated function bool AmmoMaxed(int mode)
{
    if( Mode == 1 )
    {
	   return fHealingAmmoFalk>=Default.fHealingAmmoFalk;
	}
	else
	{
	   return super.AmmoMaxed(mode);
	}
}

simulated function float AmmoStatus(optional int Mode) // returns float value for ammo amount
{
    if( Mode == 1 )
    {
	   return float(fHealingAmmoFalk)/float(Default.fHealingAmmoFalk);
	}
	else
	{
	   return super.AmmoStatus(Mode);
	}
}

// override to activate infinite medic ammo on zed time
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if(Mode == 1)
   {
      // if we're medic and in zed time we get infinite medic darts
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

		if (KFPRI                    != none &&
          KFPRI.ClientVeteranSkill != none)
      {

         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.InfiniteMedicDarts(KFPRI)) 
            return True;
      }

      if(Load > fHealingAmmoFalk)
         return False;

      fHealingAmmoFalk -= Load;
      return True;
   }

   else
      return super.ConsumeAmmo(Mode, load, bAmountNeededIsMax);
}

function bool AddAmmo(int AmmoToAdd, int Mode)
{
    if( Mode == 1 )
    {
    	if( fHealingAmmoFalk<Default.fHealingAmmoFalk )
    	{
    		fHealingAmmoFalk+=AmmoToAdd;
    		if( fHealingAmmoFalk>Default.fHealingAmmoFalk)
    		{
    			fHealingAmmoFalk = Default.fHealingAmmoFalk;
    		}
    	}
        return true;
    }
    else
    {
        return super.AddAmmo(AmmoToAdd,Mode);
    }
}

simulated function bool HasAmmo()
{
    if( fHealingAmmoFalk > 0 )
    {
        return true;
    }

	if ( bNoAmmoInstances )
	{
    	return ( (AmmoClass[0] != none && FireMode[0] != none && AmmoCharge[0] >= FireMode[0].AmmoPerFire) );
	}
    return (Ammo[0] != none && FireMode[0] != none && Ammo[0].AmmoAmount >= FireMode[0].AmmoPerFire);
}

simulated function CheckOutOfAmmo()
{
    return;
}


simulated final function SetTextColor( byte R, byte G, byte B )
{
	MyFontColor.R = R;
	MyFontColor.G = G;
	MyFontColor.B = B;
	MyFontColor.A = 255;
}

simulated final function SetTextColor2( byte R, byte G, byte B )
{
	MyFontColor2.R = R;
	MyFontColor2.G = G;
	MyFontColor2.B = B;
	MyFontColor2.A = 255;
 }

simulated function RenderOverlays( Canvas Canvas )
{
   // fixes for weird bugs
   if (MyScriptedTexture == None)
   {
      MyScriptedTexture = ScriptedTexture(DynamicLoadObject("KF_Weapons5_Scopes_Trip_T.M7A3_Ammo_Script.AmmoNumber", class'ScriptedTexture', true));
      return;
   }

   if (MyFont == None)
   {
	   MyFont = Font(DynamicLoadObject("IJCFonts.DigitalBig", class'Font', true));
      return;
   }

   if (MyFont2 == None)
   {
	   MyFont2 = Font(DynamicLoadObject("IJCFonts.DigitalBig", class'Font', true));
      return;
   }

   if (SmallMyFont == None)
   {
      SmallMyFont = Font(DynamicLoadObject("IJCFonts.DigitalMed", class'Font', true));
      return;
   }

   // mostly vanilla code
   if (fHealingAmmoFalk >= 2500)
   {
      MyHealth = ChargeBar() * 100;
      SetTextColor2(76,148,177);
      ++MyScriptedTexture.Revision;
   }

   else
   {
      MyHealth = ChargeBar() * 100;
      SetTextColor2(218,18,18);
      ++MyScriptedTexture.Revision;
   }

   if (AmmoAmount(0) <= 0)
   {
      if( OldValue!=-5 )
      {
         OldValue = -5;
         Skins[2] = ScopeRed;
         MyFont = SmallMyFont;
         SetTextColor(218,18,18);
         MyMessage = EmptyMessage;
         ++MyScriptedTexture.Revision;
      }
   }

   else if (bIsReloading)
   {
      if (OldValue != -4)
      {
         OldValue = -4;
         MyFont = SmallMyFont;
         SetTextColor(32,187,112);
         MyMessage = ReloadMessage;
         ++MyScriptedTexture.Revision;
      }
   }

   else if (OldValue!=(MagAmmoRemaining + 1))
   {
      OldValue = MagAmmoRemaining + 1;
      Skins[2] = ScopeGreen;
      MyFont = Default.MyFont;

      if ((MagAmmoRemaining ) <= (MagCapacity/2))
         SetTextColor(32,187,112);
      if ((MagAmmoRemaining ) <= (MagCapacity/3))
      {
         SetTextColor(218,18,18);
         Skins[2] = ScopeRed;
      }
      if ((MagAmmoRemaining ) >= (MagCapacity/2))
         SetTextColor(76,148,177);
      MyMessage = String(MagAmmoRemaining);

      ++MyScriptedTexture.Revision;
   }

   MyScriptedTexture.Client = Self;
   Super(KFMedicGun).RenderOverlays(Canvas);
   MyScriptedTexture.Client = None;
}

simulated function RenderTexture(ScriptedTexture Tex)
{
	local int w, h;

	// Ammo
	Tex.TextSize( MyMessage, MyFont, w, h );
	Tex.DrawText( ( Tex.USize / 2 ) - ( w / 2 ), ( Tex.VSize / 2 ) - ( h / 1.2 ),MyMessage, MyFont, MyFontColor );
	// Health
	Tex.TextSize( MyHealth, MyFont2, w, h );
	Tex.DrawText( ( Tex.USize / 2 ) - ( w / 2 ), ( Tex.VSize / 2 ) - 8 ,MyHealth, MyFont2, MyFontColor2 );
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (M7A3MPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         M7A3MPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   ReloadMessage="REL"
   EmptyMessage="Empty"
   MyFontColor=(B=177,G=148,R=76,A=255)
   MyFontColor2=(B=177,G=148,R=76,A=255)
   HealBoostAmount=20
   SuccessfulHealMessage="You healed "
   AmmoRegenRate=0.300000 // standard slow recharge rate for non-smg
   MagCapacity=20
   ReloadRate=2.9000
   ReloadAnim="Reload"
   ReloadAnimRate=1.000000
   WeaponReloadAnim="Reload_M7A3"
   Weight=5.000000
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=55.000000
   bModeZeroCanDryFire=True
   SleeveNum=3
   TraderInfoTexture=Texture'KillingFloor2HUD.Trader_Weapon_Icons.Trader_M7A3'
   bIsTier3Weapon=True
   MeshRef="KF_Wep_M7A3.M7A3"
   SkinRefs(0)="KF_Weapons5_Trip_T.Weapons.M7A3_cmb"
   SkinRefs(1)="KF_Weapons5_Scopes_Trip_T.M7A3_Ammo_Script.AmmoShader"
   SkinRefs(2)="KF_Weapons5_Scopes_Trip_T.M7A3.Scope_Finall"
   SelectSoundRef="KF_M7A3Snd.M7A3_Select"
   HudImageRef="KillingFloor2HUD.WeaponSelect.M7A3_unselected"
   SelectedHudImageRef="KillingFloor2HUD.WeaponSelect.M7A3"
   PlayerIronSightFOV=65.000000
   ZoomedDisplayFOV=45.000000
   FireModeClass(0)=Class'M7A3MFireFalk'
   FireModeClass(1)=Class'M7A3MAltFireFalk'
   PutDownAnim="PutDown"
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.550000
   CurrentRating=0.550000
   bShowChargingBar=True
   Description="An advanced Horzine prototype assault rifle, modified to fire healing darts. Higher stopping power than an AAR525, but slow fire rate and slow medical darts recharge."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=55.000000
   Priority=140
   InventoryGroup=3
   GroupOffset=13
   PickupClass=Class'M7A3MPickupFalk'
   PlayerViewOffset=(X=20.000000,Y=15.000000,Z=-5.000000)
   BobDamping=6.000000
   AttachmentClass=Class'KFMod.M7A3MAttachment'
   IconCoords=(X1=245,Y1=39,X2=329,Y2=79)
   ItemName="Medical M7A3 Assault Rifle"
   TransientSoundVolume=1.250000
}
