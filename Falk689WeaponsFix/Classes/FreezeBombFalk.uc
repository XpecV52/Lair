class FreezeBombFalk extends FlameNadeFalk;

#exec OBJ LOAD FILE=IJC_Project_Santa_A.ukx
#exec OBJ LOAD FILE=LairSounds_S.uax
#exec OBJ LOAD FILE=LairTextures_T.utx

// override superclass postbeginplay
simulated function PostBeginPlay()
{
   Super(FlameNade).PostBeginPlay();
}

// just kaboom
simulated singular function HitWall(vector HitNormal, actor Wall)
{

   // hopefully shatter glasses more reliably
   if (KFPawn(Wall) == none && KFPawn(Wall.Base) == none && KFDoorMover(Wall) == none && KFDoorMover(Wall.Base) == none)
      Wall.TakeDamage(100, Instigator, Location, HitNormal, class'DamTypeFragImpactFalk');

   // Don't let it hit this player, or blow up on another player
   if (bHasExploded || Wall == none || Wall == Instigator || Wall.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Wall) != none)
      return;

   Explode(Location, HitNormal);
}

// Touch only if we haven't exploded yet and don't hit broken zeds heads
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local FalkMonsterBase P;
   local float fHeadMulti;
   local vector X;

   // hopefully shatter glasses more reliably
   if (KFPawn(Other) == none && KFPawn(Other.Base) == none && KFDoorMover(Other) == none && KFDoorMover(Other.Base) == none)
      Other.TakeDamage(100, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');

   // Don't let it hit this player, or blow up on another player
   if (bHasExploded || Other == none || Other == Instigator || Other.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   X = Vector(Rotation);

   if (!bHasExploded)
   {
      P = FalkMonsterBase(Other);

      if (P != none)
      {
         fHeadMulti = P.FalkGetNadeHeadScale();

         if (fHeadMulti <= 0 || !P.IsHeadShot(HitLocation, X, fHeadMulti))
            Explode(HitLocation,Normal(HitLocation-Other.Location));
      }
   }
}

// Explode  
simulated function Explode(vector HitLocation, vector HitNormal)
{
   bHasExploded = True;
   BlowUp(HitLocation);

   PlaySound(sound'LairSounds_S.Grenades.FreezeGrenadeExplosion',,100.5*TransientSoundVolume);
   PlaySound(sound'LairSounds_S.Grenades.FreezeGrenadeExplosion',,100.5*TransientSoundVolume);
   PlaySound(sound'LairSounds_S.Grenades.FreezeGrenadeExplosion',,100.5*TransientSoundVolume);

   Spawn(Class'FreezeExplosionFalk',,, HitLocation, rotator(vect(0,0,1)));

   Destroy();
}


defaultproperties
{
   Speed=850.0
   ExplodeTimer=10.000000
   Damage=20.000000
   MyDamageType=class'DamTypeFreezeBomb'
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.ArtilleristGrenade'
}


