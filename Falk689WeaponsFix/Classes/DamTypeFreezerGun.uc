class DamTypeFreezerGun extends KFWeaponDamageType;

var float Damage;

defaultproperties
{
   Damage=5.000000
   //WeaponClass=Class'FreezerGun'
   DeathString="%o frozen by %k."
   FemaleSuicide="%o froze till death."
   MaleSuicide="%o froze till death."
   bArmorStops=False
   DeathOverlayMaterial=Texture'IJC_Project_Santa_A.Overlay.IceOverlay'
   DeathOverlayTime=5.000000
   bCheckForHeadShots=False
   bLocationalHit=False
}
