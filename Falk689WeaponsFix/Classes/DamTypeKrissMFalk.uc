class DamTypeKrissMFalk extends KFMod.KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
    WeaponClass=Class'KrissMMedicGunFalk'
    DeathString="%k killed %o (Vector-M)"
    bRagdollBullet=True
    KDamageImpulse=5500.000000
    KDeathVel=175.000000
    KDeathUpKick=15.000000
}