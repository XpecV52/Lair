class AshotFalk extends AshotBaseFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var bool              fWaitingToReload;     // are we waiting for an automatic reload?

var float             fReloadTimer;         // time between checks to trigger an automatic reload
var float             fCurReloadTimer;      // current time between checks to trigger an automatic reload
var float             fBringUpTimer;        // time required for the weapon to be ready after the bringup

replication
{
   reliable if(Role == ROLE_Authority)
      fWaitingToReload;
}

// pending reload function
simulated function WeaponTick(float dt)
{
   Super.WeaponTick(dt);

   // reload related stuff
   if (!fWeaponReady && ClientGrenadeState != GN_TempDown)
   {
      if (fCurReloadTimer < fBringUpTimer)
         fCurReloadTimer += dt;

      else
      {
         fCurReloadTimer = 0;
         fWeaponReady    = True;   
      }
   }

   else if (fWaitingToReload || (AmmoAmount(0) > 0 && MagAmmoRemaining == 0))
   {

      if(fCurReloadTimer < fReloadTimer)
         fCurReloadTimer += dt;

      else if (AmmoAmount(0) > 0)
      {
         fCurReloadTimer = 0;

         if (AllowReload())
         {
            ReloadMeNow();
            fWaitingToReload = False;
         }
      }

      else
         fCurReloadTimer = 0;
   }
}

// set weapon in non ready state
simulated function BringUp(optional Weapon PrevWeapon)
{
   fWeaponReady    = False;
   fCurReloadTimer = 0;
   Super.BringUp(PrevWeapon);
}

// set weapon to unready state on putdown
simulated function bool PutDown()
{
   if (Super.PutDown())
   {
      fCurReloadTimer = 0;
      fWeaponReady    = False;

      return True;
   }

   return False;
}

// set pending reload
simulated function SetPendingReload()
{
   fWaitingToReload = true;
}

// don't add an ammo randomly after fire
function ServerStopFire(byte Mode)
{
  super(BaseKFWeapon).ServerStopFire(Mode);
}

// don't block throw or swap on no ammo and block while waiting for reload or on bringup/putdown
simulated function bool CanThrow()
{
   if (!fWeaponReady || ClientState == WS_BringUp || ClientState == WS_PutDown)
   {
      //warn(1@"-Time:"@Level.TimeSeconds);
      return False;
   }

   if (AmmoAmount(0) > 0 && MagAmmoRemaining <= 0)
   {
      //warn(2@"-Time:"@Level.TimeSeconds);
      return False;
   }

   if (ClientGrenadeState == GN_TempDown)
   {
      //warn(3@"-Time:"@Level.TimeSeconds);
      return False;
   }

   return Super.CanThrow();
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// force reload on fire end even our user is dumb and keeps the mouse button pressed
simulated function bool AllowReload()
{
	UpdateMagCapacity(Instigator.PlayerReplicationInfo);

	if(bIsReloading || MagAmmoRemaining >= MagCapacity      ||
		ClientState == WS_BringUp                            ||
		AmmoAmount(0) <= MagAmmoRemaining                    ||
		(FireMode[0].NextFireTime - Level.TimeSeconds) > 0.1)
   {
		return false;
   }

	return true;
}

// don't interrupt the reload
simulated function bool InterruptReload()
{
   return False;
}

// nope
simulated function ServerInterruptReload()
{
}

// same as above, just keep reloading
simulated function ClientInterruptReload()
{
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{ 
   bHoldToReload=False
   FireModeClass(0)=Class'AshotFireFalk'
   FireModeClass(1)=Class'KFMod.NoFire'
   ReloadRate=1.860000
   ReloadAnim="Reload"
   WeaponReloadAnim="Reload_SingleFlare"
   ReloadAnimRate=1.8
   PickupClass=Class'AshotPickupFalk'
   Priority=40
   MagCapacity=1
   Weight=2.000000
   InventoryGroup=2
   Skins(0)=Combiner'LairTextures_T.CustomReskins.CyclopeCombiner'
   Skins(1)=Combiner'LairTextures_T.CustomReskins.CyclopeShellCombiner'
   ItemName="Cyclope Pistol"
   Description="A light-weight shotgun pistol. Stronger than an AA12, but must be reloaded after each shot. Penetration efficiency slightly better than the average."
   BringUpTime=0.360000
   PutDownTime=0.420000
   Mesh=SkeletalMesh'LairAnimations_A.CyclopeMesh'
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   ForceZoomOutOnFireTime=0.400000
   HudImage=Texture'Ashot.A_unselected'
   SelectedHudImage=Texture'Ashot.A_selected'
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=65.000000
   SleeveNum=2
   TraderInfoTexture=Texture'Ashot.A_trader'
   bIsTier2Weapon=True
   PlayerIronSightFOV=70.000000
   ZoomedDisplayFOV=45.000000
   PutDownAnim="PutDown"
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.650000
   CurrentRating=0.650000
   DisplayFOV=65.000000
   GroupOffset=11
   PlayerViewOffset=(X=20.000000,Y=25.000000,Z=-10.000000)
   //PlayerViewOffset=(X=18.000000,Y=20.000000,Z=-6.000000)
   BobDamping=6.000000
   AttachmentClass=Class'Ashot.AshotAttachment'
   IconCoords=(X1=253,Y1=146,X2=333,Y2=181)
   fReloadTimer=0.1
   fBringUpTimer=0.9
}
