class MP7MMedicGunFalk extends MP7MMedicGunBaseFalk;

Const MaxAmmoCount=5000;

var bool     fShouldReload;          // used to force reload on all weapons
var bool     fPawnAmmoRestore;       // we restored ammo from the pawn
var bool     fPickupAmmoRestore;     // we restored ammo from the pickup
var float    fSyringeFixTime;        // how much time to fix syringe ammo from the pawn
var int      fHealingAmmoFalk;       // lazy fix but this should work
var int      fStoredAmmo;            // stored ammo from the pickup

var byte     fTry;                 // How many times we tried to spawn a pickup
var byte     fMaxTry;              // How many times we should try to spawn a pickup
var vector   fCZRetryLoc;          // Z Correction we apply every retry
var vector   fCXRetryLoc;          // X Correction we apply every retry
var vector   fCYRetryLoc;          // Y Correction we apply every retry
var vector   fACZRetryLoc;         // computed Z correction
var vector   fACXRetryLoc;         // computed X correction
var vector   fACYRetryLoc;         // computed Y correction

var float    FBringUpTime;         // last time bringup was called
var float    FBringUpSafety;       // safety to prevent reload on server before the weapon is ready


replication
{
	reliable if (Role == ROLE_Authority)
		fHealingAmmoFalk, fStoredAmmo;
}

// using fHealingAmmoFalk
simulated function float ChargeBar()
{
	return FClamp(float(fHealingAmmoFalk)/float(Default.fHealingAmmoFalk),0,1);
}

// using fHealingAmmoFalk
simulated function MaxOutAmmo()
{
	if ( bNoAmmoInstances )
	{
		if ( AmmoClass[0] != None )
			AmmoCharge[0] = MaxAmmo(0);
		return;
	}
	if ( Ammo[0] != None )
		Ammo[0].AmmoAmount = Ammo[0].MaxAmmo;

	fHealingAmmoFalk = Default.fHealingAmmoFalk;
}

// not sure what this was but I feel like I won't miss it
simulated function SuperMaxOutAmmo()
{
   MaxOutAmmo();
}

// Start medic darts ammo istant reload
simulated function  StartForceReload()
{
   fShouldReload = True;
}

// Istantly reload medic darts ammo
simulated function bool ForceHealingReload()
{
   fHealingAmmoFalk = Default.fHealingAmmoFalk;

   return False; //(fHealingAmmoFalk < Default.fHealingAmmoFalk); // why was this a thing? - future falk
}

// override to activate infinite medic ammo on zed time
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Mode == 1)
   {
      // if we're medic and in zed time we get infinite medic darts
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

		if (KFPRI                    != none &&
          KFPRI.ClientVeteranSkill != none)
      {

         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.InfiniteMedicDarts(KFPRI)) 
            return True;
      }

      if(Load > fHealingAmmoFalk)
         return False;

      fHealingAmmoFalk -= Load;
      return True;
   }

   else
      return super.ConsumeAmmo(Mode, load, bAmountNeededIsMax);
}

// using fHealingAmmoFalk
simulated function bool HasAmmo()
{
   if (fHealingAmmoFalk > 0)
      return true;

	if (bNoAmmoInstances)
    	return ((AmmoClass[0] != none && FireMode[0] != none && AmmoCharge[0] >= FireMode[0].AmmoPerFire));

   return (Ammo[0] != none && FireMode[0] != none && Ammo[0].AmmoAmount >= FireMode[0].AmmoPerFire);
}

// Fixed stuff and added a second force reload
simulated function Tick(float dt)
{
	if (Level.NetMode != NM_Client)
	{
      // attempting to restore stored ammo from the pawn
      if (!fPawnAmmoRestore)
      {
         if (fStoredAmmo == -1 && FHumanPawn(Instigator) != none)
         {
            if (FHumanPawn(Instigator).fStoredSyringeAmmo > -1 && FHumanPawn(Instigator).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
            {
               fStoredAmmo = FHumanPawn(Instigator).fStoredSyringeAmmo;
               //warn("Tick recovering from pawn: "@fStoredAmmo);
               fPawnAmmoRestore = True;
            }

            else
            {
               //warn("F2");
               fPawnAmmoRestore = True;
               
               if (!fPickupAmmoRestore)
               {
                  fPickupAmmoRestore = True;
                  fHealingAmmoFalk   = 0;
               }
            }
         }

         else
         {
            //warn("F1");
            fPawnAmmoRestore = True;
         }
      }

      // setting ammo back on pickup, final step
      if (fStoredAmmo > -1)
      {
         fHealingAmmoFalk = fStoredAmmo;
         fStoredAmmo      = -1;
      }

      if (fShouldReload)
         fShouldReload = ForceHealingReload();

      else if (fHealingAmmoFalk < Default.fHealingAmmoFalk && RegenTimer < Level.TimeSeconds)
      {
         RegenTimer = Level.TimeSeconds + AmmoRegenRate;

		   if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
			   fHealingAmmoFalk += 100 * KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetSyringeChargeRate(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

		   else
			   fHealingAmmoFalk += 100;

		   if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
			   fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
	}
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// using fHealingAmmoFalk;
simulated function int AmmoAmount(int mode)
{
   if (Mode == 1)
      return fHealingAmmoFalk;

   else
      return super.AmmoAmount(mode);
}

// using fHealingAmmoFalk
simulated function bool AmmoMaxed(int mode)
{
   if (Mode == 1)
      return fHealingAmmoFalk>=Default.fHealingAmmoFalk;

	else
	   return super.AmmoMaxed(mode);
}

// again, use a falk var
simulated function float AmmoStatus(optional int Mode)
{
    if (Mode == 1)
	   return float(fHealingAmmoFalk)/float(Default.fHealingAmmoFalk);

	else
	   return super.AmmoStatus(Mode);
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// using fHealingAmmoFalk
function bool AddAmmo(int AmmoToAdd, int Mode)
{
   if (Mode == 1)
   {
      if (fHealingAmmoFalk < Default.fHealingAmmoFalk)
      {
         fHealingAmmoFalk += AmmoToAdd;

         if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
            fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
      return true;
   }

   else
      return super.AddAmmo(AmmoToAdd,Mode);
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (MP7MPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         MP7MPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// restore ammo from the pawn if they're fresh enough
function PickupFunction(Pawn Other)
{
   Super.PickupFunction(Other);

   if (fStoredAmmo == -1 && FHumanPawn(Other) != none &&
       FHumanPawn(Other).fStoredSyringeAmmo > -1      &&
       FHumanPawn(Other).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
   {
      fStoredAmmo = FHumanPawn(Other).fStoredSyringeAmmo;
      //warn("Restoring ammo from the pawn: "@fStoredAmmo);
      FHumanPawn(Other).fStoredSyringeAmmo = -1;
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   //warn("Setting:"@Level.TimeSeconds);

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   MagCapacity=20
   ReloadRate=3.166
   fHealingAmmoFalk=5000
   HealAmmoCharge=5000
   AmmoRegenRate=0.200000 // smg fast recharge rate standard	
   HealBoostAmount=20
   fSyringeFixTime=0.5
   fStoredAmmo=-1
   FireModeClass[0]=Class'MP7MFireFalk'
   FireModeClass[1]=Class'MP7MAltFireFalk'
   PickupClass=Class'MP7MPickupFalk'
   Description="The MP7 is a submachine gun chambered for the 4.6x30mm cartridge. It features a high fire rate, but does poor damage and has a small clip. This version has been modified to fire healing darts."
   ItemName="Medical Heckler & Koch MP7 SMG"
   fShouldReload=False
   Priority=40
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
