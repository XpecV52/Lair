class AAR525SBurstFireFalk extends FlameBurstFireFalk;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<AAR525STendrilFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local FlameTendrilFalk FB;

   FB = FlameTendrilFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'AAR525SAmmoFalk'
   ProjectileClass=Class'AAR525STendrilFalk'
   FireRate=0.07000
   ProjSpawnOffset=(X=40.000000,Y=10.000000,Z=-15.000000)
}