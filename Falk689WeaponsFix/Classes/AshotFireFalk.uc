class AshotFireFalk extends M79Fire;

var byte BID;

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   if (KFPawn(Instigator).SecondaryItem != none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (Level.TimeSeconds - LastClickTime>FireRate)
      LastClickTime = Level.TimeSeconds;

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
      return false;

   return super(WeaponFire).AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

// set weapon to pending reload state
event ModeDoFire()
{
   if (!AllowFire())
      return;

   super.ModeDoFire();

   if (M79GrenadeLauncherFalk(Weapon) != None && Weapon.Role == ROLE_Authority)
      M79GrenadeLauncherFalk(Weapon).SetPendingReload();
}


defaultproperties
{ 
   ProjectileClass=Class'AshotBulletFalk'
   AmmoClass=Class'AshotAmmoFalk'
   Spread=1000.00000
   ProjPerFire=5
   bAttachSmokeEmitter=True
   TransientSoundVolume=2.0
   TransientSoundRadius=500.000000
   FireSoundRef="Ashot.Ashot_fire_mono"
   StereoFireSoundRef="Ashot.Ashot_fire_mono"
   NoAmmoSound="Ashot.Ashot_DryFire"
   FireRate=0.410000
   FireAnimRate=1.80000
   BotRefireRate=1.800000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stKar'
   aimerror=1.000000
   maxVerticalRecoilAngle=500
   maxHorizontalRecoilAngle=100
   FireAimedAnim="Fire_Iron"
   ShakeOffsetMag=(X=6.0,Y=2.0,Z=10.0)
   ShakeOffsetRate=(X=1000.0,Y=1000.0,Z=1000.0)
   ShakeOffsetTime=3.0
   ShakeRotMag=(X=50.0,Y=50.0,Z=400.0)
   ShakeRotRate=(X=12500.0,Y=12500.0,Z=12500.0)
   ShakeRotTime=5.0
   bWaitForRelease=true
   bRandomPitchFireSound=false
}
