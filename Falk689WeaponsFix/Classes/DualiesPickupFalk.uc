class DualiesPickupFalk extends NineMMPlusDualPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=150
   InventoryType=Class'DualiesFalk'
   Weight=2.000000
   ItemName="Dual Beretta 92FS M9A1s"
   ItemShortName="Dual M9A1s"
   PickupMessage="You found another Beretta 92FS M9A1"
   BuyClipSize=30
   AmmoCost=15
   PowerValue=9
   SpeedValue=60
   RangeValue=100
}
