class SPThompsonAmmoFalk extends KFMod.SPThompsonAmmo;

defaultproperties
{
    AmmoPickupAmount=50
    MaxAmmo=300
    InitialAmount=150
    PickupClass=Class'SPThompsonAmmoPickupFalk'
    ItemName=".45 ACP rounds"
}