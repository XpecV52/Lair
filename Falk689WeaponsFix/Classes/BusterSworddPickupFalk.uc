class BusterSworddPickupFalk extends BusterSwordd.BusterSworddPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=10.000000
   cost=2800
   PowerValue=67
   SpeedValue=50
   RangeValue=45
   InventoryType=Class'BusterSworddFalk'
   ItemName="Buster Sword"
   ItemShortName="Buster"
   PickupMessage="You got a Buster Sword"
   PickupSound=Sound'KF_ClaymoreSnd.foley.WEP_Claymore_Foley_Pickup'
   DrawScale=2.050000
}
