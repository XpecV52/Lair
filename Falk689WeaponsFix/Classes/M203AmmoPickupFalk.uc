class M203AmmoPickupFalk extends KFMod.M203AmmoPickup;

defaultproperties
{
    AmmoAmount=2
    InventoryType=Class'M203AmmoFalk'
    PickupMessage="You found some 40x46mm grenades"
}