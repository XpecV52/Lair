class M249PickupFalk extends M249Mut.M249Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=12.000000
   cost=2000
   AmmoCost=100
   BuyClipSize=100
   PowerValue=54
   SpeedValue=50
   RangeValue=100
   ItemName="M249 Squad Automatic Weapon"
   ItemShortName="M249"
   CorrespondingPerkIndex=7
   InventoryType=Class'M249Falk'
   PickupMessage="You got an M249 Squad Automatic Weapon"
   DrawScale=1.25
}