class LocalFNTPSPickupFalk extends FN_TPS.FN_TPSPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=1600
   BuyClipSize=8
   AmmoCost=16
   InventoryType=Class'LocalFNTPSFalk'
   ItemName="FN Tactical Police Shotgun"
   ItemShortName="TPS"
   PickupMessage="You got an FN Tactical Police Shotgun"
   PowerValue=52
   SpeedValue=30
   RangeValue=100
}
