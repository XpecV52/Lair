class M4A1IronBeastSAPickupFalk extends M4A1IronBeastSAMut.M4A1IronBeastSAPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=7.000000
   cost=3000
   AmmoCost=15
   BuyClipSize=30
   PowerValue=15
   SpeedValue=70
   RangeValue=100
   ItemName="Dragon's Breath M4A1 Iron Beast"
   ItemShortName="M4A1 IB"
   AmmoItemName="5.56mm rounds"
   InventoryType=Class'M4A1IronBeastSAAssaultRifleFalk'
   PickupMessage="You got a Dragon's Breath M4A1 Iron Beast"
   CorrespondingPerkIndex=5
   DrawScale=1.35
}