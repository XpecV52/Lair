class MedicBrowningFireFalk extends KFFire;

var() 	name 			EmptyFiringAnim;
var()		name			EmptyFireAimedAnim;

var byte  BID;                // bullet ID to prevent multiple hits on a single zed

function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other;
   local array<int>	     HitPoints;
   local array<Actor>     IgnoreActors;
   local Actor            DamageActor;
   local Actor            OldDamageActor;
   local int              i;
   local int              bHP;
   local byte             LoopCount;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X + 
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   While (LoopCount <= 10)
   {
      //log("DoTrace Loop");
      LoopCount++;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if(Other == None)
         break;

      else if (Other == Instigator || Other.Base == Instigator)
      {
         //log("Instigator");
         IgnoreActors[IgnoreActors.Length] = Other;
         Other.SetCollision(false);
         Start = HitLocation;
         continue;
      }

      if(ExtendedZCollision(Other) != None && Other.Owner != None)
         DamageActor = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if(KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            //log("KFMonster");

            if (DamageActor != OldDamageActor) // don't hit this zed twice
            {
               OldDamageActor = DamageActor;
               bHP            = KFMonster(DamageActor).Health;

               //log("old hp: "@bHP);

               if (bHP > 0)
               {
                  DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

                  //log("new hp: "@KFMonster(DamageActor).Health);

                  if (KFMonster(DamageActor).Health != bHP) // this zed was hit, break the loop
                     break;

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = Other;
                     Other.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
               {
                  //log("current hp = 0");
                  break;
               }
            }

            else
            {
               IgnoreActors[IgnoreActors.Length] = Other;
               Other.SetCollision(false);
               //log("ignore");
            }
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

         Start = HitLocation;
      }

      else if (HitScanBlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         //log("HitScanBlockingVolume");
         break;
      }
   }

   // Turn the collision back on for any actors we turned it off
   if (IgnoreActors.Length > 0)
   {
      for (i=0; i<IgnoreActors.Length; i++)
      {
         if(IgnoreActors[i] != None)
            IgnoreActors[i].SetCollision(true);
      }
   }
}

function PlayFiring()
{
    local float RandPitch;

	if ( Weapon.Mesh != None )
	{
		if ( FireCount > 0 )
		{
			if( KFWeap.bAimingRifle )
			{
                if ( Weapon.HasAnim(FireLoopAimedAnim) )
    			{
    				Weapon.PlayAnim(FireLoopAimedAnim, FireLoopAnimRate, 0.0);
    			}
    			else if( Weapon.HasAnim(FireAimedAnim) )
    			{
    				if( Weapon.HasAnim(FireAimedAnim) )
					{
						if (KFWeapon(Weapon).MagAmmoRemaining>0)
						{
						Weapon.PlayAnim(FireAimedAnim, FireAnimRate, TweenTime);
						}
						else
						{
						Weapon.PlayAnim(EmptyFireAimedAnim, FireAnimRate, TweenTime);
						}
					}
    			}
    			else
    			{
                    if (KFWeapon(Weapon).MagAmmoRemaining>0)
					{
                    Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
					}
					else
					{
                    Weapon.PlayAnim(EmptyFiringAnim, FireAnimRate, TweenTime);
					}
    			}
			}
			else
			{
                if ( Weapon.HasAnim(FireLoopAnim) )
    			{
    				Weapon.PlayAnim(FireLoopAnim, FireLoopAnimRate, 0.0);
    			}
    			else
    			{
    				Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
    			}
			}
		}
		else
		{
            if( KFWeap.bAimingRifle )
			{
                if( Weapon.HasAnim(FireAimedAnim) )
    			{
					if (KFWeapon(Weapon).MagAmmoRemaining>0)
					{
                    Weapon.PlayAnim(FireAimedAnim, FireAnimRate, TweenTime);
					}
					else
					{
                    Weapon.PlayAnim(EmptyFireAimedAnim, FireAnimRate, TweenTime);
					}
    			}
    			else
    			{
                    if (KFWeapon(Weapon).MagAmmoRemaining>0)
					{
                    Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
					}
					else
					{
                    Weapon.PlayAnim(EmptyFiringAnim, FireAnimRate, TweenTime);
					}
    			}
			}
			else
			{
                if (KFWeapon(Weapon).MagAmmoRemaining>0)
					{
                    Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
					}
					else
					{
                    Weapon.PlayAnim(EmptyFiringAnim, FireAnimRate, TweenTime);
					}
			}
		}
	}


	if( Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() &&
	   Weapon.Instigator.IsFirstPerson() && StereoFireSound != none )
	{
        if( bRandomPitchFireSound )
        {
            RandPitch = FRand() * RandomPitchAdjustAmt;

            if( FRand() < 0.5 )
            {
                RandPitch *= -1.0;
            }
        }

        Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
    }
    else
    {
        if( bRandomPitchFireSound )
        {
            RandPitch = FRand() * RandomPitchAdjustAmt;

            if( FRand() < 0.5 )
            {
                RandPitch *= -1.0;
            }
        }

        Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
    }
    ClientPlayForceFeedback(FireForce);

    FireCount++;
}


defaultproperties
{
   FireAimedAnim="Fire_Iron"
	EmptyFiringAnim="Fire_Empty"
   EmptyFireAimedAnim="Fire_Iron_Empty"
   RecoilRate=0.070000
   maxVerticalRecoilAngle=1200
   maxHorizontalRecoilAngle=200
   ShellEjectClass=Class'ROEffects.KFShellEjectHandCannon'
   ShellEjectBoneName="slidetop"
   DamageType=Class'DamTypeMedicBrowningFalk'
   DamageMin=105
   DamageMax=105
   Momentum=20000.000000
   bPawnRapidFireAnim=True
   bWaitForRelease=True
   bAttachSmokeEmitter=True
   TransientSoundVolume=1.800000
   FireLoopAnim=
   FireEndAnim=
   TweenTime=0.025000
   FireSoundRef="KF_MK23Snd.MK23_Fire_M"
   StereoFireSoundRef="KF_MK23Snd.MK23_Fire_S"
   NoAmmoSoundRef="KF_HandcannonSnd.50AE_DryFire"
   FireRate=0.280000
   AmmoClass=Class'MedicBrowningAmmoFalk'
   AmmoPerFire=1
   ShakeRotMag=(X=75.000000,Y=75.000000,Z=400.000000)
   ShakeRotRate=(X=12500.000000,Y=12500.000000,Z=10000.000000)
   ShakeRotTime=3.500000
   ShakeOffsetMag=(X=6.000000,Y=1.000000,Z=8.000000)
   ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
   ShakeOffsetTime=2.500000
   BotRefireRate=0.650000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stKar'
   aimerror=40.000000
   Spread=0.010000
   SpreadStyle=SS_Random
}