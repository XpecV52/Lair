class Express870PickupFalk extends Express870Shotgun.Express870Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=2250
   Weight=8.000000
   InventoryType=Class'Express870Falk'
   ItemName="Express 870 Shotgun"
   ItemShortName="Express"
   PickupMessage="You got an Express 870 Shotgun"
   BuyClipSize=8
   AmmoCost=16
   PowerValue=55
   SpeedValue=30
   RangeValue=100
}
