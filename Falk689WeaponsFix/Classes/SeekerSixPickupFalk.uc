class SeekerSixPickupFalk extends KFMod.SeekerSixPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'SeekerSixRocketLauncherFalk'
   ItemName="Seeker Six Minirocket Launcher"
   ItemShortName="Seeker Six"
   PickupMessage="You got a Seeker Six Minirocket Launcher"
   cost=2300
   Weight=6.000000
   BuyClipSize=6
   AmmoCost=18
   PowerValue=27
   SpeedValue=80
   RangeValue=100
   DrawScale=1.100000
}
