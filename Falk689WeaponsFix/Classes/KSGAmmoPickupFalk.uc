class KSGAmmoPickupFalk extends KFMod.KSGAmmoPickup;

defaultproperties
{
    AmmoAmount=12
    InventoryType=Class'KSGAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}
