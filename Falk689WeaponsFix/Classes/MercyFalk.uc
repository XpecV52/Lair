class MercyFalk extends Mercy.Mercy;

#exec OBJ LOAD FILE=LairAnimations_A.ukx

var float fAmmoTick;
var float fCurAmmoTick;
var bool BBurstmode;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

// Use alt fire to switch fire modes
simulated function AltFire(float F)
{
   if (MercyFireFalk(FireMode[0]) != none && MercyFireFalk(FireMode[0]).fShouldBurst && MercyFireFalk(FireMode[0]).bBursting)
      return;

   DoToggle();
}

// Toggle burst/auto fire
simulated function DoToggle()
{
   local PlayerController Player;

   if (IsFiring())
      return;

   Player = Level.GetLocalPlayerController();

   if (Player != None && MercyFireFalk(FireMode[0]) != none)
   {
      MercyFireFalk(FireMode[0]).fShouldBurst = !MercyFireFalk(FireMode[0]).fShouldBurst;

      if (MercyFireFalk(FireMode[0]).fShouldBurst)
         Player.ReceiveLocalizedMessage(class'MercySwitchMessage',0);

      else
         Player.ReceiveLocalizedMessage(class'MercySwitchMessage',1); 

      ServerChangeFireMode(MercyFireFalk(FireMode[0]).fShouldBurst);
   }

   PlayOwnedSound(ToggleSound,SLOT_None,2.0,,,,false);
   
}

exec function SwitchModes()
{
   DoToggle();
}

// Set the new fire mode on the server
function ServerChangeFireMode(bool bNewWaitForRelease)
{
   if (MercyFireFalk(FireMode[0]) != none)
      MercyFireFalk(FireMode[0]).fShouldBurst = bNewWaitForRelease;
}

// testing autoreload on ammo pickup
simulated function WeaponTick(float dt)
{
   local FHumanPawn FHP;

   Super.WeaponTick(dt);

   // autoreload code
   if (Role == ROLE_Authority)
   {
      fCurAmmoTick += dt;

      if (fCurAmmoTick >= fAmmoTick)
      {
         fCurAmmoTick = 0;
         FHP          = FHumanPawn(Instigator);

         if (FHP != none && FHP.fMercyReload > 0)
         {
            FHP.fMercyReload--;
            MagAmmoRemaining    = AmmoAmount(0);
         }
      }
   }
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if(Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled())
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow && HasAmmo())
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   MagCapacity=50
   WeaponReloadAnim=
   Weight=2.000000
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=70.000000
   bModeZeroCanDryFire=True
   TraderInfoTexture=Texture'Mercy_T.Shop'
   Mesh=SkeletalMesh'LairAnimations_A.MercyMesh'
   HudImage=Texture'Mercy_T.Shop'
   SelectedHudImage=Texture'Mercy_T.Selected'
   ZoomedDisplayFOV=50.000000
   FireModeClass(0)=Class'MercyFireFalk'
   FireModeClass(1)=Class'KFMod.NoFire'
   PutDownAnim="PutDown"
   SelectSound=Sound'Mercy_SND.sniper_worldreload'
   AIRating=0.450000
   CurrentRating=0.450000
   bShowChargingBar=True
   Description="Strife's revolver from Darksiders. Vicious stopping power, but very scarce ammo efficiency. Can switch between burst and semi-automatic fire modes. Each barrel fires a bullet in burst fire mode, resulting in a total of four shots fired."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=70.000000
   Priority=55
   InventoryGroup=2
   GroupOffset=3
   PutDownTime=0.33000
   BringUpTime=0.38000
   PickupClass=Class'MercyPickupFalk'
   PlayerViewOffset=(X=5.000000,Y=20.000000,Z=-10.000000)
   BobDamping=6.000000
   AttachmentClass=Class'Mercy.MercyAttachment'
   IconCoords=(X1=250,Y1=110,X2=330,Y2=145)
   ItemName="Mercy Revolver"
   bUseDynamicLights=True
   Skins(0)=Combiner'Mercy_T.1'
   AmbientGlow=15
   TransientSoundVolume=1.000000
   fAmmoTick=0.2
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
