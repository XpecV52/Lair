class Dual44MagnumPickupFalk extends KFMod.Dual44MagnumPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	Weight=4.000000
	cost=900
	AmmoCost=24
	BuyClipSize=12
	PowerValue=28
    SpeedValue=84
    RangeValue=100
	ItemName="Dual Smith & Wesson 29s"
	ItemShortName="SW29s"
	AmmoItemName=".44 rounds"
	InventoryType=Class'Dual44MagnumFalk'
	PickupMessage="You found another Smith & Wesson 29"
}