class SW76PickupFalk extends SW76Wep.SW76Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   AmmoCost=16
   cost=650
   BuyClipSize=32
   PowerValue=28
   SpeedValue=85
   RangeValue=100
   Weight=3.000000
   ItemName="Smith & Wesson 76 SMG"
   ItemShortName="SW76"
   AmmoItemName="7.62x51mm rounds"
   CorrespondingPerkIndex=7
   InventoryType=Class'SW76Falk'
   PickupMessage="You got a Smith & Wesson 76 SMG"
}
