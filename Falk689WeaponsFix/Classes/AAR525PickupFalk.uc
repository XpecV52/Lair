class AAR525PickupFalk extends AAR525PickupBaseFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.AlienRifleCommandoPickup'
   cost=4000
   InventoryType=Class'AAR525Falk'
   Weight=7.000000
   ItemName="Alien Assault Rifle 525"
   PickupMessage="You got an Alien Assault Rifle 525"
   PowerValue=69
   SpeedValue=35
   RangeValue=100
   BuyClipSize=25
   AmmoCost=13
}