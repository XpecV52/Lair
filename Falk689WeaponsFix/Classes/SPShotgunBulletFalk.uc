class SPShotgunBulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=29.000000
   MaxPenetrations=2
   PenDamageReduction=0.750000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeSPShotgunFalk'
}