class TrenchgunAmmoFalk extends KFMod.TrenchgunAmmo;

#EXEC OBJ LOAD FILE=KillingFloorHUD.utx

defaultproperties
{
   MaxAmmo=48
   InitialAmount=18
   AmmoPickupAmount=6
}