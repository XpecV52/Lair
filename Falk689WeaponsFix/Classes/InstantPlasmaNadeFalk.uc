class InstantPlasmaNadeFalk extends PlasmaNadeFalk;

// Don't load anything, just stay put
static function PreloadAssets()
{
   return;
}

// Do nothing here
function PostBeginPlay()
{
}

// Go Kaboom
function PostNetBeginPlay()
{
   SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}

defaultproperties
{
   ExplodeTimer=0.010000
   DrawScale=0.000000
   DrawType=DT_None
   ZapAmount=689.000000
}
