class MTS255PickupFalk extends MTS255.MTS255Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=6.000000
   InventoryType=Class'MTS255Falk'
   cost=950
   BuyClipSize=5
   AmmoCost=10
   ItemName="MTs255 Semi-Automatic Shotgun"
   ItemShortName="MTs255"
   PickupMessage="You got an MTs255 Semi-Automatic Shotgun"
   PowerValue=42
   SpeedValue=70
   RangeValue=100
}