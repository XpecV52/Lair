class BullpupAmmoPickupFalk extends KFMod.BullpupAmmoPickup;

defaultproperties
{
    AmmoAmount=30
    InventoryType=Class'BullpupAmmoFalk'
    PickupMessage="You found some 5.56x45mm NATO rounds"
}