class ZEDGunProjectileFalk extends KFMod.ZEDGunProjectile;

var Actor      FLastDamaged;   // last damaged zed, used to attempt to fix multiple shots on the same zed
var byte       BulletID;       // bullet ID, passed at takedamage to prevent multiple shots
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
   reliable if(Role == ROLE_Authority)
      BulletID, FLastDamaged, fCInstigator;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// use BulletID to apply damage to zeds
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn HitPawn;

   // Don't let it hit this player, or blow up on another player
   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged)
      return;

   // Don't collide with bullet whip attachments
   if (KFBulletWhipAttachment(Other) != none)
      return;

   // Don't allow hits on poeple
   if (KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   FLastDamaged = Other;

   if (Instigator != none)
      OrigLoc = Instigator.Location;

   X = Vector(Rotation);

   if (Role == ROLE_Authority)
   {
      if (ROBulletWhipAttachment(Other) != none)
      {
         if (!Other.Base.bDeleteMe)
         {
            Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

            if (Other == none || HitPoints.Length == 0)
               return;

            HitPawn = KFPawn(Other);

            if (Role == ROLE_Authority)
            {
               if (HitPawn != none)
               {
                  if (!HitPawn.bDeleteMe)
                  {
                     HitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                     HitPawn.ProcessLocationalDamage(Damage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), MyDamageType,HitPoints);
                  }
               }
            }
         }
      }

      else
      {
         if (Pawn(Other) != none && Pawn(Other).IsHeadShot(HitLocation, X, 1.0))
         {
            Other.SetDelayedDamageInstigatorController(fCInstigator);
            Pawn(Other).TakeDamage(Damage * HeadShotDamageMult, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
         }

         else if (Other != none)
         {
            Other.SetDelayedDamageInstigatorController(fCInstigator);
            Other.TakeDamage(Damage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
         }
      }
   }

   if (!bDud && Other != none)
      Explode(HitLocation,Normal(HitLocation-Other.Location));
}

// How about NOPE!?
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

defaultproperties
{
   Speed=1700.000000
   MaxSpeed=1700.000000
   Damage=80.000000
   HeadShotDamageMult=1.000000
   MyDamageType=Class'DamTypeZEDGunFalk'
   ExplosionDecal=Class'BurnMarkMediumFalk'
}
