class FNTPSFireFalk extends FN_TPS.FN_TPSFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((FNTPSFalk(Weapon)      != None && FNTPSFalk(Weapon).fLoadingLastBullet) ||
       (LocalFNTPSFalk(Weapon) != None && LocalFNTPSFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 210)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'FNTPSAmmoFalk'
   ProjectileClass=Class'FNTPSBulletFalk'
   FireRate=0.965
   ProjPerFire=7
   TransientSoundVolume=2.0
   TransientSoundRadius=500.000000
   FireSound=Sound'KF_PumpSGSnd.SG_Fire'
   StereoFireSoundRef="KF_PumpSGSnd.SG_FireST"
   NoAmmoSound=Sound'KF_PumpSGSnd.SG_DryFire'
}
