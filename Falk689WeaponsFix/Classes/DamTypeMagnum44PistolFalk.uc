class DamTypeMagnum44PistolFalk extends KFMod.DamTypeMagnum44Pistol;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'Magnum44PistolFalk'
   HeadShotDamageMult=1.100000
}