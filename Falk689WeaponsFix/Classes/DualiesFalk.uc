class DualiesFalk extends NineMMPlusDual;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

simulated function PostBeginPlay()
{
   Super(KFWeapon).PostBeginPlay();
}

function bool HandlePickupQuery( pickup Item )
{
   if (Item.InventoryType==Class'SingleFalk')
   {
      if (LastHasGunMsgTime < Level.TimeSeconds && PlayerController(Instigator.Controller) != none)
      {
         LastHasGunMsgTime = Level.TimeSeconds+0.5;
         PlayerController(Instigator.Controller).ReceiveLocalizedMessage(Class'KFMainMessages',1);
      }

      return True;
   }

   return Super(KFWeapon).HandlePickupQuery(Item);
}


function GiveTo(pawn Other, optional Pickup Pickup)
{
	local Inventory I;
	local int OldAmmo;
	local bool bNoPickup;

	MagAmmoRemaining = 0;

	for (I=Other.Inventory; I!=None; I=I.Inventory)
	{
		if (SingleFalk(I) != none)
		{
			if (WeaponPickup(Pickup)!= none)
			{
				WeaponPickup(Pickup).AmmoAmount[0] += Weapon(I).AmmoAmount(0);
			}

			else
			{
				OldAmmo = Weapon(I).AmmoAmount(0);
				bNoPickup = true;
			}

			MagAmmoRemaining = SingleFalk(I).MagAmmoRemaining;

			I.Destroyed();
			I.Destroy();

			break;
		}
	}

	if (KFWeaponPickup(Pickup) != None && Pickup.bDropped)
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + KFWeaponPickup(Pickup).MagAmmoRemaining, 0, MagCapacity);
	}

	else
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + Class'SingleFalk'.Default.MagCapacity, 0, MagCapacity);
	}

	Super(Weapon).GiveTo(Other, Pickup);

	if (bNoPickup)
	{
      Ammo[0].AmmoAmount /= 2;
		AddAmmo(OldAmmo, 0);
		Clamp(Ammo[0].AmmoAmount, 0, MaxAmmo(0));
	}
}

simulated function bool PutDown()
{
   if (Instigator.PendingWeapon.class == class'SingleFalk')
      bIsReloading = false;

   return super.PutDown();
}

// removed laser
simulated function WeaponTick(float dt)
{
	if (MagAmmoRemaining >= 2 || bIsReloading)
	{
		IdleAnim = default.IdleAnim;
		IdleAimAnim = default.IdleAimAnim;
		SelectAnim = default.SelectAnim;
		PutDownAnim = default.PutDownAnim;
		ReloadAnim = default.ReloadAnim;
		ModeSwitchAnim = default.ModeSwitchAnim;
	}

	else if (MagAmmoRemaining == 1)
	{
		if ((DualiesFireFalk(FireMode[0]).FireAimedAnim == 'BFireRight_Iron') || (DualiesFireFalk(FireMode[0]).FireAnim == 'BFireRight'))
		{
			ReloadAnim = 'LReload';
			IdleAnim = 'LIdle';
			IdleAimAnim = 'LIdle_Iron';
			SelectAnim = 'LSelect';
			PutDownAnim = 'LPutDown';
			ModeSwitchAnim = 'LLightOn';	
		}
		else
		{
			ReloadAnim = 'RReload';
			IdleAnim = 'RIdle';
			IdleAimAnim = 'RIdle_Iron';
			SelectAnim = 'RSelect';
			PutDownAnim = 'RPutDown';
			ModeSwitchAnim = 'RLightOn';	
		}
	}

	else if (MagAmmoRemaining == 0)
	{
		ReloadAnim = 'Reload';
		IdleAnim = 'BIdle';
		IdleAimAnim = 'BIdle_Iron';
		SelectAnim = 'BSelect';
		PutDownAnim = 'BPutDown';
		ModeSwitchAnim = 'BLightOn';	
	}

	super(KFWeapon).WeaponTick(dt);
}

// remove laser
simulated function ToggleLaser()
{
}

// remove laser
simulated function TurnOffLaser(optional bool bPutDown)
{
}

// remove laser
simulated function TurnOnLaser()
{
}

// remove laser
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

	Super(KFWeapon).BringUp(PrevWeapon);	
}

// vanilla alt fire
simulated function AltFire(float F)
{
   Super(KFWeapon).AltFire(F);
}

// nope
function ServerSetLaserActive(bool bNewWaitForRelease)
{
}

// remove laser
simulated event RenderOverlays( Canvas Canvas )
{
	local int m;

	if (Instigator == None)
		return;

	if (Instigator.Controller != None)
		Hand = Instigator.Controller.Handedness;

	if ((Hand < -1.0) || (Hand > 1.0))
		return;

	// draw muzzleflashes/smoke for all fire modes so idle state won't
	// cause emitters to just disappear
	for (m = 0; m < NUM_FIRE_MODES; m++)
	{
		if (FireMode[m] != None)
		{
			FireMode[m].DrawMuzzleFlash(Canvas);
		}
	}

	SetLocation( Instigator.Location + Instigator.CalcDrawOffset(self) );
	SetRotation( Instigator.GetViewRotation() + ZoomRotInterp);
	bDrawingFirstPerson = true;
	Canvas.DrawActor(self, false, false, DisplayFOV);
	bDrawingFirstPerson = false;
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local Inventory            I;
   local vector               Direction;
   local int      AmmoThrown, OtherAmmo;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(Class'SinglePickupFalk',,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(Class'SinglePickupFalk',,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      AmmoThrown = AmmoAmount(0);

      if( Instigator.Health > 0 )
      {
         OtherAmmo = AmmoThrown / 2;
         AmmoThrown -= OtherAmmo;
         I = Spawn(Class'SingleFalk');
         I.GiveTo(Instigator);
         Weapon(I).Ammo[0].AmmoAmount = OtherAmmo;
         SingleFalk(I).MagAmmoRemaining = MagAmmoRemaining/2;
         MagAmmoRemaining = Max(MagAmmoRemaining-SingleFalk(I).MagAmmoRemaining,0);
      }

      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      WeaponPickup(Pickup).AmmoAmount[0] = AmmoThrown;

      if( KFWeaponPickup(Pickup)!=None )
         KFWeaponPickup(Pickup).MagAmmoRemaining = MagAmmoRemaining;

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroyed();
      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   DemoReplacement=Class'SingleFalk'
   PickupClass=class'DualiesPickupFalk'
   FireModeClass(0)=Class'DualiesFireFalk'
   FireModeClass(1)=Class'KFMod.SingleALTFire'
   ReloadAnim="BReload"
   Priority=10
   HudImage=Texture'LairTextures_T.CustomReskins.DualiesUnselected'
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.DualiesSelected'
   ItemName="Dual Beretta 92FS M9A1s"
   Weight=2.000000
   Description="Pair of 9mm pistols. Higher chances to avoid finding your magazines empty in the middle of a brawl with specimens."
   Mesh=SkeletalMesh'9mmPlus_R.Dual_1P'
   DrawScale=0.900000
   Skins(1)=Combiner'9mmPlus_R.Frame_cmb'
   Skins(2)=Combiner'9mmPlus_R.Slide_cmb'
   Skins(3)=Combiner'9mmPlus_R.Slide_cmb'
   Skins(4)=Combiner'9mmPlus_R.Slide_cmb'
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
