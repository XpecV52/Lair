class AK12LLIPickupFalk extends AK12LLIMut.AK12LLIPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=6.000000
   cost=2000
   InventoryType=Class'AK12LLIAssaultRifleFalk'
   BuyClipSize=35
   AmmoCost=18
   ItemName="Silenced AK12 Assault Rifle"
   ItemShortName="Silenced AK12"
   PickupMessage="You got a Silenced AK12 Assault Rifle"
   PowerValue=37
   SpeedValue=45
   RangeValue=100
}