class AngelicPickupFalk extends Angelic.AngelicPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=11.000000
   cost=3300
   PowerValue=79
   SpeedValue=40
   RangeValue=60
   ItemName="Angelic Greatsword"
   ItemShortName="Angelic"
   InventoryType=Class'AngelicFalk'
   PickupMessage="You got the Angelic Greatsword"
   StaticMesh=StaticMesh'Angelic_SM.Angelic'
   DrawScale=1.20000
}
