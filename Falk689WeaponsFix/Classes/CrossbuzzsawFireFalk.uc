class CrossbuzzsawFireFalk extends KFMod.CrossbuzzsawFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<CrossbuzzsawBladeFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local CrossbuzzsawBladeFalk FB;

   FB = CrossbuzzsawBladeFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
    ProjPerFire=1
    FireRate=2.30000
    FireAnimRate=1.000000
    AmmoClass=Class'CrossbuzzsawAmmoFalk'
    ProjectileClass=Class'CrossbuzzsawBladeFalk'
    RecoilRate=0.100000
    EffectiveRange=7500.000000
}
