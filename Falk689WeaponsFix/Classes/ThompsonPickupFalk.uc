class ThompsonPickupFalk extends KFMod.ThompsonPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ThompsonSMGFalk'
   ItemName="Thompson M1A1 SMG"
   ItemShortName="M1A1"
   PickupMessage="You got a Thompson M1A1 SMG"
   cost=1200
   Weight=5.000000
   BuyClipSize=30
   AmmoCost=15
   PowerValue=40
   SpeedValue=65
   RangeValue=100
}