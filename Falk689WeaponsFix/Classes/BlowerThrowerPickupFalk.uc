class BlowerThrowerPickupFalk extends KFMod.BlowerThrowerPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'BlowerThrowerFalk'
   ItemName="Bloat Bile Launcher"
   ItemShortName="Bile Launcher"
   PickupMessage="You got a Bloat Bile Launcher"
   PowerValue=24
   SpeedValue=62
   RangeValue=50
   BuyClipSize=50
   AmmoCost=25
   cost=1250
}