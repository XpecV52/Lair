class ATMineSoundFix extends M79ClusterFalk;

// Don't load anything, just stay put
static function PreloadAssets()
{
   return;
}

// Don't take any damage, we don't need it
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   return;
}

// Just start, explode and destroy 
simulated function PostBeginPlay()
{ 
   SetTimer(0.05, false);
}

// Delayed explosion
function Timer()
{
   Explode(Location, vect(0,0,1));
}

// Overridden prevent collisions
simulated function HitWall(vector HitNormal, actor Wall)
{
   return;
}

// don't Bounce on hitwall
simulated function BounceHitWall(vector HitNormal, actor Wall)
{
   return;
}

// don't kaboom on wall 
simulated function KaboomHitWall(vector HitNormal, actor Wall)
{
   return;
}

// Don't touch me
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{ 
   return;
}

// Play a sound and destroy
simulated function Explode(vector HitLocation, vector HitNormal)
{
   if (bHasExploded)
      return;

   bHasExploded = True;

   //log("kaboom");
   PlaySound(ExplosionSound,,2.0);
   Destroy();
}

defaultproperties
{
   ExplosionSound=Sound'KF_GrenadeSnd.Nade_Explode_1'
   DrawScale=0.000000
   DrawType=DT_None
}
