class M4203PickupFalk extends M4203Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'M4203AssaultRifleFalk'
   PickupMessage="You got a Colt M4+M203 Carbine"
   ItemName="Colt M4+M203 Carbine"
   ItemShortName="M4+M203"
   BuyClipSize=30
   AmmoCost=15
   Weight=6.000000
   cost=2600
   PowerValue=60
   SpeedValue=10
   RangeValue=100
}
