class FlameTendrilFalk extends FlameTendril;

var Actor      FLastDamaged;
var int        BulletID;
var float      FDamage;
var Controller fCInstigator;
var bool       fAlreadyHit;

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, FLastDamaged, fCInstigator;
}

// Secondary point blank fix, this way we just hurt pawns, not walls
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   HurtWall = None;

   //log("HitWall");
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
      return;

   if (Wall.bStatic && Wall.bWorldGeometry)
   {
      Explode(Location + ExploWallOut * HitNormal, HitNormal);

      if (Trail != None)
      {
         Trail.mRegen=False;
         Trail.SetPhysics(PHYS_None);
      }

      if (FlameTrail != none)
	   {
		   FlameTrail.Kill();
		   FlameTrail.SetPhysics(PHYS_None);
	   }

      Destroy();
   }
}

// attempt to fix point blank
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local int bHp;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   if (fAlreadyHit)
   {
      Destroy();
      return;
   }

   fAlreadyHit  = True;
   FLastDamaged = Other;

   X = Vector(Rotation);

   if (Pawn(Other) != none)
   {
      bHP = Pawn(Other).Health;
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Pawn(Other).TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      // don't destroy damage if we haven't damaged the zed
      if (bHP > 0 && Pawn(Other).Health < bHP)
         Destroy();
   }

   // this may be needed since shotguns basically fire bugs
   else
   {
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
      Destroy();
   }
}

// modified to pass BulletID to the zed
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
	local actor Victims;
	local float damageScale, dist;
	local vector dirs;
	local int NumKilled;
	local KFMonster KFMonsterVictim;
	local Pawn P;
	local KFPawn KFP;
	local array<Pawn> CheckedPawns;
	local int i;
	local bool bAlreadyChecked;

	if (bHurtEntry)
		return;

	bHurtEntry = true;

	foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
	{
		// don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
		if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
		 && ExtendedZCollision(Victims)==None )
		{
			dirs = Victims.Location - HitLocation;
			dist = FMax(1,VSize(dirs));
			dirs = dirs/dist;
			damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);
			if ( Victims == LastTouched )
				LastTouched = None;

			P = Pawn(Victims);

			if( P != none )
			{
		        for (i = 0; i < CheckedPawns.Length; i++)
				{
		        	if (CheckedPawns[i] == P)
					{
						bAlreadyChecked = true;
						break;
					}
				}

				if( bAlreadyChecked )
				{
					bAlreadyChecked = false;
					P = none;
					continue;
				}

				KFMonsterVictim = KFMonster(Victims);

				if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
				{
					KFMonsterVictim = none;
				}

				KFP = KFPawn(Victims);

				if( KFMonsterVictim != none )
				{
					damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
				}
				else if( KFP != none )
				{
				    damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
				}

				CheckedPawns[CheckedPawns.Length] = P;

				if ( damageScale <= 0)
				{
					P = none;
					continue;
				}
				else
				{
					//Victims = P;
					P = none;
				}
			}


			Victims.TakeDamage
			(
				damageScale * DamageAmount,
				Instigator,
				Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
				(damageScale * Momentum * dirs),
				DamageType,
            BulletID
			);

			if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
				Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

			if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
			{
				NumKilled++;
			}
		}
	}
	if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
	{
		Victims = LastTouched;
		LastTouched = None;
		dirs = Victims.Location - HitLocation;
		dist = FMax(1,VSize(dirs));
		dirs = dirs/dist;
		damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
      Victims.SetDelayedDamageInstigatorController(fCInstigator);
		Victims.TakeDamage
		(
			damageScale * DamageAmount,
			Instigator,
			Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
			(damageScale * Momentum * dirs),
			DamageType,
         BulletID
		);
		if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
			Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
	}

	bHurtEntry = false;
}

defaultproperties
{
   FDamage=20.000000
   MyDamageType=Class'DamTypeFlameThrowerFalk'
   Speed=2400.000000           // this is what actually influences the real range of the gun
   MaxSpeed=2400.000000        // this is what actually influences the real range of the gun
   ExplosionDecal=Class'BurnMarkLargeFalk'
}
