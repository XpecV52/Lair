class KatanaPickupFalk extends KFMod.KatanaPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

DefaultProperties
{
    Weight=3.000000
    cost=900
    PowerValue=39
    SpeedValue=70
    RangeValue=38
    ItemName="Katana"
	ItemShortName="Katana"
    InventoryType=Class'KatanaFalk'
    PickupMessage="You got a Katana"
}
