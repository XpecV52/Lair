class SPThompsonPickupFalk extends KFMod.SPThompsonPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'SPThompsonSMGFalk'
   ItemName="Dr T's Lead Delivery System"
   ItemShortName="LDS"
   cost=1500
   BuyClipSize=50
   AmmoCost=25
   PickupMessage="You got a Dr T's Lead Delivery System"
   PowerValue=40
   SpeedValue=65
   RangeValue=100
   Weight=5.000000
}