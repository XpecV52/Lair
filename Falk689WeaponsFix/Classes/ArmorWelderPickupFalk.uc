class ArmorWelderPickupFalk extends WelderPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ArmorWelderFalk'
   ItemName="Welder"
   PickupMessage="You got a Welder"
   cost=10
   PowerValue=0
   SpeedValue=0
   RangeValue=0
   ItemShortName="Welder"
   CorrespondingPerkIndex=8
}
