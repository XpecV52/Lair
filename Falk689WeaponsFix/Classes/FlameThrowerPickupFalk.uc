class FlameThrowerPickupFalk extends KFMod.FlameThrowerPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'FlameThrowerFalk'
   cost=1750
   PickupMessage="You got a Flamethrower"
   ItemShortName="Flamer"
   ItemName="Flamethrower"
   BuyClipSize=100
   AmmoCost=50
   PowerValue=10
   SpeedValue=80
   RangeValue=60
   Weight=8.000000
}