class M203AmmoFalk extends KFMod.M203Ammo;

defaultproperties
{
    MaxAmmo=12
    InitialAmount=6
    AmmoPickupAmount=2
    PickupClass=Class'M203AmmoPickupFalk'
    ItemName="40x46mm grenades"
}