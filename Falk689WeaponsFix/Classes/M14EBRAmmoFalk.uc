class M14EBRAmmoFalk extends KFMod.M14EBRAmmo;

defaultproperties
{
	MaxAmmo=140
	InitialAmount=60
	AmmoPickupAmount=20
	PickupClass=Class'M14EBRAmmoPickupFalk'
	ItemName="7.62x51mm NATO rounds"
}