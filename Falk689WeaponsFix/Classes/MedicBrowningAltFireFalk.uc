class MedicBrowningAltFireFalk extends MP7MAltFireFalk;

defaultproperties
{
   ProjectileClass=Class'MedicBrowningHealingProjectileFalk'
   AmmoPerFire=1750 // this consumes 35 charges
   FireRate=0.75 // 50% slower than standard
   FireAnimRate=0.5
}
