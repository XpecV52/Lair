class ChainsawPickupFalk extends KFMod.ChainsawPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=7.000000
   InventoryType=Class'ChainsawFalk'
   cost=1150
   ItemName="Chainsaw"
   ItemShortName="Chainsaw"
   PickupMessage="You got a Chainsaw"
   PowerValue=51
   SpeedValue=35
   RangeValue=15
}
