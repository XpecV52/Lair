class FlareRevolverFireFalk extends KFMod.FlareRevolverFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<FlareRevolverProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local FlareRevolverProjectileFalk FB;

   FB = FlareRevolverProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'FlareRevolverAmmoFalk'
   ProjectileClass=Class'FlareRevolverProjectileFalk'
   FireRate=0.40000
}
