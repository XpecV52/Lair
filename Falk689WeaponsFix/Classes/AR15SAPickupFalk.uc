class AR15SAPickupFalk extends AR15SAMut.AR15SAPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=4.000000
   cost=1800
   AmmoCost=15
   BuyClipSize=30
   PowerValue=63
   SpeedValue=55
   RangeValue=100
   ItemName="Armalite AR15 Service Rifle"
   ItemShortName="AR15"
   AmmoItemName="5.56x45mm NATO rounds"
   InventoryType=Class'AR15SAFalk'
   PickupMessage="You got an Armalite AR15 Service Rifle"
}