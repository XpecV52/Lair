class MediShotHealingProjectileFalk extends MP7MHealingProjectileFalk;

function AddDamagedHealStats( int MedicReward )
{
	local KFSteamStatsAndAchievements KFSteamStats;

	if ( Instigator == none || Instigator.PlayerReplicationInfo == none )
	{
		return;
	}

	KFSteamStats = KFSteamStatsAndAchievements( Instigator.PlayerReplicationInfo.SteamStatsAndAchievements );

	if ( KFSteamStats != none )
	{
	 	KFSteamStats.AddDamageHealed(MedicReward, true);
	}
}

defaultproperties
{
   HealBoostAmount=20
}