class M79AmmoPickupFalk extends KFMod.M79AmmoPickup;

defaultproperties
{
    AmmoAmount=3
    InventoryType=Class'M79AmmoFalk'
    PickupMessage="You found some 40x46mm grenades"
}