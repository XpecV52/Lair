class DamTypeHuntingRifleFalk extends BDHuntingRifleFinal.DamTypeHuntingRifle;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=2.250000  // heavy sharpshooter weaponry standard
   WeaponClass=Class'HuntingRifleFalk'
}