class LocalLAWFalk extends KFMod.LAW;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=KF_Weapons_Trip_T.utx

var bool  fWeaponReady;
var bool  fAiming;
var bool  fStartAiming;

var float fReloadTimer;
var float fCurReloadTimer;
var float fBringUpTimer;
var float fCurAimTimer;
var float fAimTimer;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction


replication
{
   reliable if(Role == ROLE_Authority)
      fAiming, fWeaponReady;
}

// just don't
simulated function DoAutoSwitch(){}

// pending reload function
simulated function WeaponTick(float dt)
{
   Super.WeaponTick(dt);

   if (Role == ROLE_Authority)
   {
      // reload related stuff
      if (!fWeaponReady)
      {
         if (fCurReloadTimer < fBringUpTimer)
            fCurReloadTimer += dt;

         else
         {
            fCurReloadTimer = 0;
            fWeaponReady    = True;   
         }
      }

      else if (AmmoAmount(0) > 0 && MagAmmoRemaining == 0)
      {

         if(fCurReloadTimer < fReloadTimer)
            fCurReloadTimer += dt;

         else if (AmmoAmount(0) > 0)
         {
            fCurReloadTimer = 0;

            if (AllowReload())
               ReloadMeNow();
         }

         else
            fCurReloadTimer = 0;
      }
   }
   // aim related stuff
   if (fStartAiming)
   {
      if (fCurAimTimer < fAimTimer)
         fCurAimTimer += dt;

      else
      {
         fCurAimTimer = 0;
         fStartAiming = False;
         fAiming      = True;
      }
   }
}

simulated function BringUp(optional Weapon PrevWeapon)
{
   local KFPlayerController Player;

   fWeaponReady = False;

   Super.BringUp(PrevWeapon);

   Player = KFPlayerController(Instigator.Controller);

   if (Player != none && ClientGrenadeState != GN_BringUp)
   {
      Player.WeaponPulloutRemark(23); // LAW
   }
}

simulated function AddReloadedAmmo()
{
   Super(KFWeapon).AddReloadedAmmo();
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// don't block throw or swap on no ammo and block while waiting for reload
simulated function bool CanThrow()
{
   if (!fWeaponReady || ClientState == WS_BringUp || ClientState == WS_PutDown)
      return False;

   if (AmmoAmount(0) <= 0)
      return True;

   if (MagAmmoRemaining <= 0)
      return False;

   return True;
}


// trying to fix some weird shit happeing
function ServerStopFire(byte Mode)
{
  fAiming = False;
  super(BaseKFWeapon).ServerStopFire(Mode);
  ServerRequestAutoReload(); 
}

// set fAiming to true on zoom in finish
simulated event OnZoomInFinished()
{
   Super.OnZoomInFinished();
   fStartAiming = True;
}

// set fAiming to false on zoom out
simulated function ZoomOut(bool bAnimateTransition)
{
   fAiming = False;
   Super.ZoomOut(bAnimateTransition);
}

// don't aim unless the weapon is ready
simulated exec function ToggleIronSights()
{
   if (!fWeaponReady && !fAiming)
      return;

   super.ToggleIronSights();
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   Weight=13.00000
   FireModeClass[0]=Class'LAWFireFalk'
   FireModeClass[1]=Class'KFMod.NoFire'
   PickupClass=Class'LocalLAWPickupFalk'
   ItemName="Light Antiarmor Weapon 80"
   PutDownTime=0.830000
   BringUpTime=0.6
   Mesh=SkeletalMesh'LairAnimations_A.LAWMesh'
   Skins(0)=Combiner'KF_Weapons_Trip_T.Supers.law_cmb'
   SkinRefs(0)="KF_Weapons_Trip_T.Supers.law_cmb"
   SkinRefs(1)="KF_Weapons_Trip_T.Supers.law_reddot_shdr"
   SkinRefs(2)="KF_Weapons_Trip_T.Supers.rocket_cmb"
   MeshRef=""
   SelectedHudImageRef="KillingFloorHUD.WeaponSelect.LAW"
   SelectedHudImage=Texture'KillingFloorHUD.WeaponSelect.LAW'
   HudImageRef="KillingFloorHUD.WeaponSelect.LAW_unselected"
   HudImage=Texture'KillingFloorHUD.WeaponSelect.LAW_unselected'
   Description="The LAW80 is a man-portable, disposable weapon suited for the neutralization of tanks. Very powerful damage output and good range, but rockets are extremely expensive. Beware of the slow reload. Demomen obtain grenades by killing heavy zeds with the rocket explosion."
   ReloadRate=4.35
   ReloadAnim="Reload"
   ReloadAnimRate=1.0
   bHoldToReload=False
   MagCapacity=1
   fReloadTimer=0.1
   fBringUpTimer=0.7
   fAimTimer=0.5
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
