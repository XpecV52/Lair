class BoomStickFalk extends BoomStickBaseFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool  fWaitingToReload;

var float fReloadTimer;
var float fCurReloadTimer;
var float fBringUpTimer;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

replication
{
   reliable if(Role == ROLE_Authority)
      fWaitingToReload;
}

// pending reload function
simulated function WeaponTick(float dt)
{
   Super.WeaponTick(dt);

   // reload related stuff
   if (!fWeaponReady && ClientGrenadeState != GN_TempDown)
   {
      if (fCurReloadTimer < fBringUpTimer)
         fCurReloadTimer += dt;

      else
      {
         fCurReloadTimer = 0;
         fWeaponReady    = True;   
      }
   }

   else if (fWaitingToReload || (AmmoAmount(0) > 0 && MagAmmoRemaining == 0))
   {

      if(fCurReloadTimer < fReloadTimer)
         fCurReloadTimer += dt;

      else if (AmmoAmount(0) > 0)
      {
         fCurReloadTimer = 0;

         if (AllowReload())
         {
            ReloadMeNow();
            fWaitingToReload = False;
         }
      }

      else
         fCurReloadTimer = 0;
   }
}

// set pending reload
simulated function SetPendingReload()
{
   fWaitingToReload = true;
}

// set weapon to unready state on putdown
simulated function bool PutDown()
{
   if (Super.PutDown())
   {
      fCurReloadTimer = 0;
      fWeaponReady    = False;

      return True;
   }

   return False;
}

// don't add an ammo randomly after fire
function ServerStopFire(byte Mode)
{
  super(BaseKFWeapon).ServerStopFire(Mode);
}

simulated function BringUp(optional Weapon PrevWeapon)
{
   local KFPlayerController Player;

   fWeaponReady = False;

   Super.BringUp(PrevWeapon);

   Player = KFPlayerController(Instigator.Controller);

   if (Player != none && ClientGrenadeState != GN_BringUp)
   {
      Player.WeaponPulloutRemark(21); // BOOMSTICK
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// don't block throw or swap on no ammo and block while waiting for reload or on bringup/putdown
simulated function bool CanThrow()
{
   if (!fWeaponReady || ClientState == WS_BringUp || ClientState == WS_PutDown)
   {
      //warn(1);
      return False;
   }

   if (AmmoAmount(0) > 0 && MagAmmoRemaining <= 0)
   {
      //warn(2);
      return False;
   }

   if (ClientGrenadeState == GN_TempDown)
   {
      //warn(3);
      return False;
   }

   return Super.CanThrow();
}

// Overriden to support the special single firing or dual firing of the shotty
simulated function bool ConsumeAmmo(int Mode, float Load, optional bool bAmountNeededIsMax)
{
	if(super(Weapon).ConsumeAmmo(0, Load, bAmountNeededIsMax))
	{
        MagAmmoRemaining -= Load;

        NetUpdateTime = Level.TimeSeconds - 1;
        return true;
	}

	return false;
}

// only reload when empty and take into account the alt fire
simulated function bool AllowReload()
{
   if (MagAmmoRemaining > 0)
      return False;

	UpdateMagCapacity(Instigator.PlayerReplicationInfo);

	if (KFInvasionBot(Instigator.Controller) != none && !bIsReloading &&
		MagAmmoRemaining < MagCapacity && AmmoAmount(0) > MagAmmoRemaining)
   {
		return true;
   }

	if (KFFriendlyAI(Instigator.Controller) != none && !bIsReloading &&
		MagAmmoRemaining < MagCapacity && AmmoAmount(0) > MagAmmoRemaining)
   {
		return true;
   }

   // force reload on fire end even our user is dumb and keeps the mouse button pressed
	if(bIsReloading || MagAmmoRemaining >= MagCapacity      ||
		ClientState == WS_BringUp                            ||
		AmmoAmount(0) <= MagAmmoRemaining                    ||
		(FireMode[0].NextFireTime - Level.TimeSeconds) > 0.1 ||
		(FireMode[1].NextFireTime - Level.TimeSeconds) > 0.1)
   {
		return false;
   }

   return true;
}

// don't reload like a shotgun
simulated function AddReloadedAmmo()
{
   Super(KFWeapon).AddReloadedAmmo();
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   bHoldToReload=False
   PickupClass=Class'BoomStickPickupFalk'
   FireModeClass(0)=Class'BoomStickFireFalk'
   FireModeClass(1)=Class'BoomStickAltFireFalk'
   Weight=9.000000
   ItemName="Stoeger Coach Gun"
   Description="A heavy hunting shotgun that can quickly take down even the largest zed. Insanely powerful secondary fire, ten pellets are contained in each shell. Expensive ammo compared to other shotguns. Scarce ammo efficiency."
   ForceZoomOutOnFireTime=0.010000
   ForceZoomOutOnAltFireTime=0.010000
   MagCapacity=2
   ReloadRate=2.534
   ReloadAnim="Reload"
   ReloadAnimRate=1.1
   WeaponReloadAnim="Reload_HuntingShotgun"
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=55.000000
   TraderInfoTexture=Texture'KillingFloorHUD.Trader_Weapon_Images.Trader_Hunting_Shotgun'
   bIsTier2Weapon=True
   Mesh=SkeletalMesh'LairAnimations_A.BoomstickMesh'
   MeshRef="LairAnimations_A.BoomstickMesh"
   Skins(0)=Combiner'KF_Weapons_Trip_T.Shotguns.boomstick_cmb'
   SelectSoundRef="KF_DoubleSGSnd.2Barrel_Select"
   HudImage=Texture'LairTextures_T.CustomReskins.BoomstickUnselected'
   HudImageRef="LairTextures_T.CustomReskins.BoomstickUnselected"
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.BoomstickSelected'
   SelectedHudImageRef="LairTextures_T.CustomReskins.BoomstickSelected"
   PlayerIronSightFOV=70.000000
   ZoomedDisplayFOV=40.000000
   PutDownAnim="PutDown"
   AIRating=0.900000
   CurrentRating=0.900000
   bSniping=False
   DisplayFOV=55.000000
   Priority=80
   InventoryGroup=4
   GroupOffset=2
   PlayerViewOffset=(X=8.000000,Y=14.000000,Z=-8.000000)
   BobDamping=6.000000
   AttachmentClass=Class'KFMod.BoomStickAttachment'
   bUseDynamicLights=True
   TransientSoundVolume=1.000000
   fReloadTimer=0.1
   fBringUpTimer=0.7
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
