class SyringeFireFalk extends SyringeFire;

const SnekID = "76561197973670335";    // snek ID
//const SnekID = "76561198043543449";  // testing ID

// block with AllowFire()
event ModeDoFire()
{
   local FHumanPawn FP;

	if (!AllowFire())
		return;

   FP = FHumanPawn(Instigator);

   if (FP != none)
   {
      FP.fSyringeMain      = True;
      FP.fCanSwitchSyringe = True;
   }

   Super.ModeDoFire(); // We don't consume the ammo just yet.	
}

function bool AllowFire()
{
   if ((NextFireTime - FireRate * 0.06) < Level.TimeSeconds + PreFireTime)
	   return Weapon.AmmoAmount(ThisModeNum) >= AmmoPerFire;

   return False;
}

function Timer()
{
   local KFPlayerReplicationInfo PRI;
   local int MedicReward;
   local float fMedicReward;
   local FHumanPawn Healed;
   local float HealSum; // for modifying based on perks
   local FPCServ FPC;
   local bool fShouldReward;
   local Falk689GameTypeBase Flk;

   Healed        = FHumanPawn(CachedHealee);
   CachedHealee  = none;
   fShouldReward = True;

   if (Healed != none && Healed.Health > 0 && Healed != Instigator)
   {
      Weapon.ConsumeAmmo(ThisModeNum, AmmoPerFire);

      MedicReward = SyringeFalk(Weapon).GetHealBoostAmount();

      if ( KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none )
      {
         MedicReward *= KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetHealPotency(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));
      }

      HealSum = MedicReward;

      if ( (Healed.Health + Healed.FHealthToGive + MedicReward) > Healed.HealthMax )
      {
         MedicReward = Healed.HealthMax - (Healed.Health + Healed.FHealthToGive);

         if ( MedicReward < 0 )
         {
            MedicReward = 0;
         }
      }

      Healed.GiveHealth(HealSum, Healed.HealthMax);

      FPC = FPCServ(Instigator.Controller);

      if (FPC != None)
      {
         // medic reward block stuff
         Flk = Falk689GameTypeBase(Level.Game);

         if (Flk != none)
            fShouldReward = Flk.ShouldRewardMedic(PlayerController(Instigator.Controller));

         // snek stuff
         if (FPC.GetPlayerIDHash() == SnekID)
         {
            FPC.snekBlock = True;

            if (Weapon.Role == ROLE_Authority && SyringeFalk(Weapon) != none)
               SyringeFalk(Weapon).PraiseSnek(); 
         }
      }


      PlayerController(Instigator.Controller).Speech('AUTO', 5, "");
      LastHealMessageTime = Level.TimeSeconds;

      PRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (PRI != None && fShouldReward)
      {
         if (MedicReward > 0 && KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements) != none)
            KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements).AddDamageHealed(MedicReward);

         fMedicReward = float(MedicReward) * 0.3; // 20hp healed = £ 6

         // difficulty scaling
         if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
            fMedicReward *= 0.34;

         else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
            fMedicReward *= 0.5;

         else if (Level.Game.GameDifficulty >= 5.0 ) // Suicidal
            fMedicReward *= 0.67;

         else if (Level.Game.GameDifficulty >= 4.0 ) // Hard
            fMedicReward *= 0.84;

         MedicReward = int(fMedicReward);

         PRI.ReceiveRewardForHealing(MedicReward, Healed);

         if ( KFHumanPawn(Instigator) != none )
         {
            KFHumanPawn(Instigator).AlphaAmount = 255;
         }
      }
   }
}

defaultproperties
{
   AmmoPerFire=2500
   FireRate=2.250000
   FireAnimRate=1.3
   FireLoopAnimRate=1.3
   PreFireTime=0
}
