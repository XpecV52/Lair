class BlowerThrowerFireFalk extends KFMod.BlowerThrowerFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<BlowerThrowerProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local BlowerThrowerProjectileFalk FB;

   FB = BlowerThrowerProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}


defaultproperties
{
   AmmoClass=Class'BlowerThrowerAmmoFalk'
   ProjectileClass=Class'BlowerThrowerProjectileFalk'
   FireRate=0.07
}