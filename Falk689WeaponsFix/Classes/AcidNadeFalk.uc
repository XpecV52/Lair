class AcidNadeFalk extends NadeFalk;

#exec OBJ LOAD FILE=LairTextures_T.utx

// Override initial cluster check 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super(Nade).PostBeginPlay();
}

// Initial timer setting
function PostNetBeginPlay()
{
	SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}

// Explode but check if we spawned all clusters before destroy
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   bHasExploded = True;
   BlowUp(HitLocation);

   //PlaySound(ExplodeSounds[rand(ExplodeSounds.length)],,2.0);
   PlaySound(sound'KF_EnemiesFinalSnd.Bloat_DeathPop', SLOT_Misc, 2.0, true, 525);

   Spawn(Class'LowGoreBileExplosion',,, HitLocation, rotator(vect(0,0,1)));
   Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));

   if (Role != ROLE_Authority || !fShouldKaboom || fSClusters >= fNClusters)
   {
      fShouldCheck = False;
      Destroy();
   }
}

// Overridden to prevent Chuck Norris from blowing up our nade
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// Every cluster should have the same ID 
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local byte i;
   local Projectile P;
   local vector fPLocation;
   local vector SpawnCorrection;

   SpawnCorrection = vect(0,0,0);

   /*if (bHasExploded)
      return;*/

   if (Role == ROLE_Authority && fShouldCheck)
   {
      // Check again if we should spawn explosive shrapnels

      if (Instigator != none)
      {
         if (!fShouldKaboom)
         {
            KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

            if (KFPRI != none)
            {
               Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

               if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
                  fShouldKaboom = True;
            }
         }

         if (fShouldKaboom)
         {
            // attempt to fix ground cluster spawn
            fPLocation = Instigator.Location + fCZRetryLoc;

            //log(Location.Z);
            //log(fPLocation.Z);

            if (Location.Z <= fPLocation.Z) // if the nade if under the player, correct spawn position
               SpawnCorrection = fCSpawnLoc;
         }
      }

      // Kaboom
      if (fShouldKaboom)
      {
         for(i=0; i<fNClusters; i++)
         {
            class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + 1;

            P = Spawn(fCClass,,, Location + SpawnCorrection, RotRand(True));

            if (P != none) 
            {
               fSClusters++;
               P.Instigator = Instigator;
               P.Damage     = Damage;

               if (M79ClusterFalk(P) != none) // pass siren immunity state to the cluster
               {
                  M79ClusterFalk(P).fCInstigator         = fCInstigator;
                  M79ClusterFalk(P).default.fSirenImmune = fSirenImmune;
               }
            }
         }

         //log("Initial spawn");
         //log(fSClusters);
      }
   }

   FalkExplode(HitLocation, HitNormal);
}


// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local Projectile P;
   local byte fAttempt;

   Super(Nade).Tick(DeltaTime);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      fLifeTime += DeltaTime;

      if (!bHasExploded && !fShouldKaboom && fLifeTime >= fCLastCheck + fCheckSecs && Instigator != none)
      {
         KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

         fCLastCheck = fLifeTime;

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
               fShouldKaboom = True;
         }
      }

      if (fShouldKaboom && bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + 1;

               if (fSuccessSpawn)
               {
                  P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                  if (P == None)
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(fCClass,,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 
                  P.Damage      = Damage;

                  fSClusters++;

                  fTry          = 0;

                  if (M79ClusterFalk(P) != none) // pass siren immunity state to the cluster
                  {
                     M79ClusterFalk(P).fCInstigator = fCInstigator;
                     M79ClusterFalk(P).default.fSirenImmune = fSirenImmune;
                  }
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}


// don't stick on the cross, hopefully 
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   if ((Pawn(Wall) != None) || (GameObjective(Wall) != None))
   {
      Explode(Location, HitNormal);
      return;
   }

   if (!fTimerSet)
   {
      SetTimer(ExplodeTimer * 0.8, false);
      fTimerSet = true;
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -2.0;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 10000;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 10000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
         EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

defaultproperties
{
   Speed=850.0
   fCClass=class'AcidClusterFalk'
   fNClusters=20
   fMaxTry=10
   fShouldKaboom=True
   fShouldCheck=True
   fCheckSecs=0.500000
   Damage=180.000000
   DamageRadius=300.000000
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.CommandoGrenade'
   ExplodeSounds=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   ExplosionDecal=Class'KFMod.VomitDecal'
   MomentumTransfer=0.000000
	MyDamageType=Class'DamTypeAcidNadeFalk'
}
