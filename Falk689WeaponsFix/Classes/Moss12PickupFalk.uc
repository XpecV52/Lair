class Moss12PickupFalk extends Moss12Mut.Moss12Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'Moss12Falk'
   ItemName="Moss12 Shotgun"
   ItemShortName="Moss12"
   PickupMessage="You got a Moss12 Shotgun"
   cost=700
   BuyClipSize=6
   AmmoCost=12
   PowerValue=46
   SpeedValue=30
   RangeValue=100
}
