class Spas12PickupFalk extends Spas12.SpasPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=1400
   Weight=6.000000
   InventoryType=Class'Spas12Falk'
   ItemName="Franchi SPAS12 Shotgun"
   ItemShortName="SPAS12"
   PickupMessage="You got a Franchi SPAS12 Shotgun"
   BuyClipSize=8
   AmmoCost=16
   PowerValue=63
   SpeedValue=30
   RangeValue=100
}
