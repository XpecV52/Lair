class WinchesterAmmoPickupFalk extends KFMod.WinchesterAmmoPickup;

defaultproperties
{
    AmmoAmount=8
    InventoryType=Class'WinchesterAmmoFalk'
    PickupMessage="You found .44 rounds"
}
