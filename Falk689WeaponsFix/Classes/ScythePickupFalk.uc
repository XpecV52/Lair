class ScythePickupFalk extends KFMod.ScythePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ScytheFalk'
   ItemName="Reaper Scythe"
   ItemShortName="Scythe"
   PickupMessage="You got a Reaper Scythe"
   cost=1600
   Weight=6.000000
   PowerValue=69
   SpeedValue=35
   RangeValue=42
}
