class SpitfirePickupFalk extends Spitfire.SpitfirePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=7.000000
   cost=900
   AmmoCost=40
   BuyClipSize=80
   PowerValue=7
   SpeedValue=80
   RangeValue=60
   InventoryType=Class'SpitfireFalk'
   ItemName="Spitfire Flamethrower"
   ItemShortName="Spitfire"
   PickupMessage="You got a Spitfire Flamethrower"
}