class M60PickupFalk extends M60Wep.M60Pickup;

function Destroyed()
{
   if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
   {
      if (KFGameType(Level.Game) != none)
         KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
   }

   super(WeaponPickup).Destroyed();
}

// use FStoredAmmo instead of MagAmmoRemaining to have proper ammo amount on drop/pickup
function InitDroppedPickupFor(Inventory Inv)
{
   local KFWeapon W;
   local Inventory InvIt;
   local byte bSaveAmmo[2];
   local int m;

   W = KFWeapon(Inv);

   if (W != None)
   {
      //Check if any other weapon is using the same ammo
      for(InvIt = W.Owner.Inventory; InvIt!=none; InvIt=InvIt.Inventory)
      {
         if(Weapon(InvIt)!=none && InvIt!=W)
         {
            for(m=0; m < 2; m++)
            {
               if(Weapon(InvIt).AmmoClass[m] == W.AmmoClass[m])
                  bSaveAmmo[m] = 1;
            }
         }
      }

      if (bSaveAmmo[0] == 0)
      {
         if (M60Falk(W) != none)
            MagAmmoRemaining = M60Falk(W).FStoredAmmo;

         else
            MagAmmoRemaining = W.MagAmmoRemaining;

         AmmoAmount[0] = W.AmmoAmount(0);
      }

      if (bSaveAmmo[1] == 0)
         AmmoAmount[1] = W.AmmoAmount(1);

      SellValue = W.SellValue;
   }
   SetPhysics(PHYS_Falling);
   GotoState('FallingPickup');
   Inventory = Inv;
   bAlwaysRelevant = false;
   bOnlyReplicateHidden = false;
   bUpdateSimulatedPosition = true;
   bDropped = true;
   bIgnoreEncroachers = false; // handles case of dropping stuff on lifts etc
   NetUpdateFrequency = 8;
   bNoRespawn = true;

   if ( KFWeapon(Inventory) != none )
   {
      if ( KFWeapon(Inventory).bIsTier2Weapon )
      {
         if ( !KFWeapon(Inventory).bPreviouslyDropped && PlayerController(Pawn(Inventory.Owner).Controller) != none )
         {
            KFWeapon(Inventory).bPreviouslyDropped = true;
            KFSteamStatsAndAchievements(PlayerController(Pawn(Inventory.Owner).Controller).SteamStatsAndAchievements).AddDroppedTier2Weapon();
         }
      }
      else
      {
         bPreviouslyDropped = KFWeapon(Inventory).bPreviouslyDropped;
         DroppedBy = PlayerController(Pawn(W.Owner).Controller);
      }
   }
}

defaultproperties
{
   Weight=14.000000
   cost=3000
   AmmoCost=150
   BuyClipSize=150
   PowerValue=69
   SpeedValue=50
   RangeValue=100
   ItemName="M60 Machine Gun"
   ItemShortName="M60"
   AmmoItemName="5.56mm ammo"
   CorrespondingPerkIndex=7
   InventoryType=Class'M60Falk'
   PickupMessage="You got an M60 Machine Gun"
   DrawScale=1.50000
}
