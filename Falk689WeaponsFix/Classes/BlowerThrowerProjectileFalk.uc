class BlowerThrowerProjectileFalk extends KFMod.BlowerBileProjectile;

var Actor      FLastDamaged;   // last damaged zed, used to attempt to fix multiple shots on the same zed
var byte       BulletID;       // bullet ID, passed at takedamage to prevent multiple shots
var float      FDamage;        // damage amount, prevents weird vanilla stuff to happen
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var bool       fAlreadyHit;    // also don't hit anybody else

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, FLastDamaged, fCInstigator;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
	local actor Victims;
	local float damageScale, dist;
	local vector dir;

	if ( bHurtEntry )
		return;

	bHurtEntry = true;
	foreach VisibleCollidingActors(class 'Actor', Victims, DamageRadius, HitLocation)
	{
      // removed self damage, first attempt
      if (Victims == Instigator)
         continue;

		// don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
		if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo') )
		{
			dir = Victims.Location - HitLocation;
			dist = FMax(1,VSize(dir));
			dir = dir/dist;
			damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
			Victims.SetDelayedDamageInstigatorController(fCInstigator);
			if ( Victims == LastTouched )
				LastTouched = None;
			Victims.TakeDamage
			(
				damageScale * DamageAmount,
				Instigator,
				Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir,
				(damageScale * Momentum * dir),
				DamageType,
            BulletID
			);
			if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
				Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

		}
	}
	if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
	{
		Victims = LastTouched;
		LastTouched = None;
		dir = Victims.Location - HitLocation;
		dist = FMax(1,VSize(dir));
		dir = dir/dist;
		damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
		Victims.SetDelayedDamageInstigatorController(fCInstigator);
		Victims.TakeDamage
		(
			damageScale * DamageAmount,
			Instigator,
			Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dir,
			(damageScale * Momentum * dir),
			DamageType,
         BulletID
		);
		if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
			Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
	}

	bHurtEntry = false;
}

// attempt to fix point blank
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local int bHp;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   if (fAlreadyHit)
   {
      Destroy();
      return;
   }

   fAlreadyHit  = True;
   FLastDamaged = Other;

   X = Vector(Rotation);

   if (Pawn(Other) != none)
   {
      bHP = Pawn(Other).Health;
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Pawn(Other).TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      // don't destroy damage if we haven't damaged the zed
      if (bHP > 0 && Pawn(Other).Health < bHP)
         Destroy();
   }

   // this may be needed since shotguns basically fire bugs
   else
   {
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
      Destroy();
   }
}

defaultproperties
{
    MaxPenetrations=1
    PenDamageReduction=0
    HeadShotDamageMult=1.000000
    Speed=1000.0
    MaxSpeed=1000.0
    Damage=48
    FDamage=48
    DamageRadius=120.000000
    MyDamageType=class'DamTypeBlowerThrowerFalk'
}
