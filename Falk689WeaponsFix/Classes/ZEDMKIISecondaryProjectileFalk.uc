class ZEDMKIISecondaryProjectileFalk extends ZEDMKIISecondaryProjectile;

// removed zed time on hit
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;

   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   ClearStayingDebugLines();

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         if ( Instigator == None || Instigator.Controller == None )
            Victims.SetDelayedDamageInstigatorController( InstigatorController );
         if ( Victims == LastTouched )
            LastTouched = None;

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if( KFMonsterVictim == none )
            {
               P = none;
               continue;
            }
            else
            {
               // Zap zeds only
               if( Role == ROLE_Authority )
               {
                  KFMonsterVictim.SetZapped(ZapAmount, Instigator);
                  NumKilled++;
               }
               //DrawStayingDebugLine( Location, P.Location,255, 255, 0);
            }

            P = none;
         }
      }
   }

   bHurtEntry = false;
}

// Make them stop!
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

defaultproperties
{
   ExplosionEmitter=Class'ZEDMKIISecondaryProjectileExplosionFalk'
   ExplosionDecal=Class'BurnMarkMediumFalk'
   ZapAmount=10.00000
   DamageRadius=250.000000
}
