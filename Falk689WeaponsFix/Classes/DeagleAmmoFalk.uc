class DeagleAmmoFalk extends KFMod.DeagleAmmo;

#EXEC OBJ LOAD FILE=InterfaceContent.utx

defaultproperties
{
   MaxAmmo=98
   InitialAmount=42
   AmmoPickupAmount=14
   PickupClass=Class'DeagleAmmoPickupFalk'
   ItemName=".50 AE rounds"
}