class HKG36CSAPickupFalk extends HKG36CSA.HKG36CSAPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=3.000000
   cost=900
   InventoryType=Class'HKG36CSAAssaultRifleFalk'
   BuyClipSize=30
   AmmoCost=15
   ItemName="Heckler & Koch G36C Assault Rifle"
   ItemShortName="G36C"
   PickupMessage="You got a Heckler & Koch G36C Assault Rifle"
   PowerValue=33
   SpeedValue=35
   RangeValue=100
}