class LocalKS23SAPickupFalk extends KS23SAPickupBaseFalk;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=6.000000
   cost=2750
   BuyClipSize=4
   AmmoCost=8
   PowerValue=96
   SpeedValue=25
   RangeValue=100
   ItemName="KS23 Special Carbine"
   ItemShortName="KS23"
   AmmoItemName="KS23 shells"
   InventoryType=Class'LocalKS23SAShotgunFalk'
   PickupMessage="You got a KS23 Special Carbine"
}
