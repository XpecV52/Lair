class FlaresPickupFalk extends Flares.FlarePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=50
   AmmoCost=50
   BuyClipSize=5
   InventoryType=Class'FlaresFalk'
   ItemName="Emergency Flares"
   ItemShortName="Flares"
   PickupMessage="You got some Emergency Flares"
   CorrespondingPerkIndex=8
   PowerValue=0
   SpeedValue=0
   RangeValue=0
}