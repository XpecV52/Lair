class AK12SAPickupFalk extends AK12SAgent.AK12SAPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=4.000000
   InventoryType=Class'AK12SAAssaultRifleFalk'
   cost=800
   BuyClipSize=35
   AmmoCost=18
   ItemName="AK12 Assault Rifle"
   ItemShortName="AK12"
   PickupMessage="You got an AK12 Assault Rifle"
   PowerValue=31
   SpeedValue=45
   RangeValue=100
}
