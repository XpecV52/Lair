class SPGrenadePickupFalk extends KFMod.SPGrenadePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'SPGrenadeLauncherFalk'
   Cost=1550
   BuyClipSize=1
   AmmoCost=5
   ItemName="Orca Bomb Propeller"
   ItemShortName="Orca Bomber"
   PickupMessage="You got an Orca Bomb Propeller"
   PowerValue=64
   SpeedValue=8
   RangeValue=85
   Weight=4.000000
   DrawScale=1.15
}
