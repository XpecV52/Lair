class ClaymoreSwordPickupFalk extends KFMod.ClaymoreSwordPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

DefaultProperties
{
    Weight=6.000000
    cost=1850
    PowerValue=63
    SpeedValue=40
    RangeValue=45
    ItemName="Claymore Sword"
	ItemShortName="Claymore"
    InventoryType=Class'ClaymoreSwordFalk'
    PickupMessage="You got a Claymore Sword"
}