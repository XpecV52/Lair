class StingerAmmoFalk extends KFAmmunition;

#EXEC OBJ LOAD FILE=KillingFloorHUD.utx

defaultproperties
{
    AmmoPickupAmount=40         // 10% for stinger only
    MaxAmmo=400
    InitialAmount=200
    PickupClass=Class'StingerAmmoPickupFalk'
    IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
    IconCoords=(X1=336,Y1=82,X2=382,Y2=125)
    ItemName="Stinger bullets"
}