class DamTypeM99SniperRifleFalk extends KFMod.DamTypeM99SniperRifle;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
    HeadShotDamageMult=2.750000 // this is to remain the highest multiplier, M99 exclusive
	WeaponClass=Class'M99SniperRifleFalk'
}