class MercyAmmoFalk extends Mercy.MercyAmmo;

#EXEC OBJ LOAD FILE=InterfaceContent.utx

defaultproperties
{
   AmmoPickupAmount=10  // 20% for mercy only
   MaxAmmo=50
   InitialAmount=20
   PickupClass=Class'MercyAmmoPickupFalk'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=338,Y1=40,X2=393,Y2=79)
   ItemName="Mercy bullets"
}
