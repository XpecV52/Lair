class SCARMK17AmmoFalk extends KFMod.SCARMK17Ammo;

defaultproperties
{
    MaxAmmo=240
    InitialAmount=100
    AmmoPickupAmount=20
    PickupClass=Class'SCARMK17AmmoPickupFalk'
    ItemName="7.62x51mm NATO rounds"
}
