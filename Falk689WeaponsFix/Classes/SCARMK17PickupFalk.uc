class SCARMK17PickupFalk extends KFMod.SCARMK17Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	Weight=5.000000
	cost=3100
	AmmoCost=10
	BuyClipSize=20
	PowerValue=65
    SpeedValue=50
    RangeValue=100
	ItemName="FN SCAR-H Combat Rifle"
	ItemShortName="SCAR-H"
	AmmoItemName="7.62x51mm rounds"
	InventoryType=Class'SCARMK17AssaultRifleFalk'
	PickupMessage="You got an FN SCAR-H Combat Rifle"
}