class AA12ExplosiveBulletFalk extends KFMod.M79GrenadeProjectile;

var byte       BulletID;             // used to prevent multi hit on a single zed
var Controller fCInstigator;         // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var Emitter    fSmokeTrail;          // visual stuff
var float      fSoundVolume;         // falk sound volume

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, fCInstigator;
}

simulated function Explode(vector HitLocation, vector HitNormal)
{
    local Controller C;
    local PlayerController  LocalPlayer;

    bHasExploded = True;

    // Don't explode if this is a dud
    if (bDud)
    {
        Velocity = vect(0,0,0);
        LifeSpan=1.0;
        SetPhysics(PHYS_Falling);
    }

    PlaySound(ExplosionSound,,fSoundVolume);

    if (EffectIsRelevant(Location,false))
    {
        Spawn(class'KaboomFalk',,,HitLocation + HitNormal*20,rotator(HitNormal));
        Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));
    }

    BlowUp(HitLocation);
    Destroy();

    // Shake nearby players screens
    LocalPlayer = Level.GetLocalPlayerController();

    if ((LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < DamageRadius))
        LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

    for (C=Level.ControllerList; C!=None; C=C.NextController)
        if ((PlayerController(C) != None) && (C != LocalPlayer)
            && (VSize(Location - PlayerController(C).ViewTarget.Location) < DamageRadius))
            C.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);
}


simulated function Destroyed()
{
	if (fSmokeTrail != none)
	{
       fSmokeTrail.Kill();
       fSmokeTrail.SetPhysics(PHYS_None);
	}

    Super.Destroyed();
}

static function PreloadAssets()
{
}

// Post death kill fix 
/*simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}*/


// post death kill fix and new, lighter, trail
simulated function PostBeginPlay()
{
   BCInverse = 1 / BallisticCoefficient;
   OrigLoc   = Location;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   if( Level.NetMode!=NM_DedicatedServer && (Level.NetMode!=NM_Client || Physics == PHYS_Projectile) )
   {
      if (!PhysicsVolume.bWaterVolume)
         fSmokeTrail = Spawn(Class'KFMod.SealSquealFuseEmitter', self);
   }

   if(!bDud)
   {
      Dir = vector(Rotation);
      Velocity = speed * Dir;
   }

   if (PhysicsVolume.bWaterVolume)
   {
      bHitWater = True;
      Velocity=0.6*Velocity;
   }

   super(Projectile).PostBeginPlay();
}

// use BulletID to damage zeds
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;


   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);
         if ( Victims == LastTouched )
            LastTouched = None;

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            KFP = KFPawn(Victims);

            if( KFMonsterVictim != none )
            {
               damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }
            else if( KFP != none )
            {
               damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if ( damageScale <= 0)
            {
               P = none;
               continue;
            }
            else
            {
               //Victims = P;
               P = none;
            }
         }

         if(Victims == Instigator)
            damageScale *= 0.3;

         Victims.TakeDamage
            (
             damageScale * DamageAmount,
             Instigator,
             Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
             (damageScale * Momentum * dirs),
             DamageType,
             BulletID
            );
         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }
   if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
   {
      Victims = LastTouched;
      LastTouched = None;
      dirs = Victims.Location - HitLocation;
      dist = FMax(1,VSize(dirs));
      dirs = dirs/dist;
      damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
      Victims.SetDelayedDamageInstigatorController(fCInstigator);

      if(Victims == Instigator)
         damageScale *= 0.3;

      Victims.TakeDamage
         (
          damageScale * DamageAmount,
          Instigator,
          Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
          (damageScale * Momentum * dirs),
          DamageType,
          BulletID
         );
      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}


// Overridden to prevent clusters and stuff from blowing up our nade
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   if (damageType == class'SirenScreamDamage')
      Disintegrate(HitLocation, vect(0,0,1));
 
   else if ((!bDud && !bHasExploded) && (KFHumanPawn(InstigatedBy) == none && (damageType == class'DamTypeFrag' || damageType == class'DamTypeBurned')))
   {
      Explode(HitLocation, vect(0,0,0));
   }
}

// Touch only if we haven't exploded yet and fix point blank stuff
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   // Don't let it hit this player, or blow up on another player
   if (bHasExploded || Other == none || Other == Instigator || Other.Base == Instigator || AA12ExplosiveBulletFalk(Other) != None || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   // fixed shit happening with location and replication
   if(Instigator != none)
      OrigLoc = Instigator.Location;

   if(!bDud && ((VSizeSquared(Location - OrigLoc) < ArmDistSquared) || OrigLoc == vect(0,0,0)))
   {
      if(Role == ROLE_Authority)
      {
         AmbientSound = none;
         PlaySound(Sound'ProjectileSounds.PTRD_deflect04',,2.0);
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(ImpactDamage, Instigator, HitLocation, Normal(Velocity), MyDamageType, BulletID);
      }

      bDud     = True;
      Velocity = vect(0,0,0);
      LifeSpan = 1.0;
      SetPhysics(PHYS_Falling);
   }

   if(!bDud)
      Explode(HitLocation,Normal(HitLocation-Other.Location));
}

// trying to fix shit happening in the client with gore
simulated function BlowUp(vector HitLocation)
{
	HurtRadius(Damage,DamageRadius, ImpactDamageType, MomentumTransfer, HitLocation);

	if (Role == ROLE_Authority)
		MakeNoise(1.0);
}

// not sure if I need this but better safe than taco
simulated function DelayedHurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   HurtRadius(DamageAmount, DamageRadius, ImpactDamageType, Momentum, HitLocation);
}

defaultproperties
{
   ShakeRotMag=(X=210.000000,Y=210.000000,Z=210.000000)
   ShakeRotRate=(X=4375.000000,Y=4375.000000,Z=4375.000000)
   ShakeRotTime=3.000000
   ShakeOffsetMag=(X=2.500000,Y=5.000000,Z=2.500000)
   ShakeOffsetRate=(X=105.000000,Y=105.000000,Z=105.000000)
   ShakeOffsetTime=1.750000
   RotMag=(X=245.000000,Y=245.000000,Z=245.000000)
   RotRate=(X=4375.000000,Y=4375.000000,Z=4375.000000)
   RotTime=3.000000
   OffsetMag=(X=2.500000,Y=5.000000,Z=2.500000)
   OffsetRate=(X=105.000000,Y=105.000000,Z=105.000000)
   OffsetTime=1.750000
   ImpactDamage=50
   Damage=150.000000
   Speed=3500.000000
   MaxSpeed=3500.000000
   ArmDistSquared=70000.000000
   ExplosionSound=Sound'KF_GrenadeSnd.Nade_Explode_1'
   DisintegrateSound=Sound'Inf_Weapons.faust_explode_distant02'
   MyDamageType=Class'DamTypeGrenadeImpactFalk'
   ImpactDamageType=Class'KFMod.DamTypeM79Grenade'
   fSoundVolume=1.25
   ExplosionDecal=class'ScorchMarkFalk'
}
