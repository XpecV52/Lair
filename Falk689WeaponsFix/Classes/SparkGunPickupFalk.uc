class SparkGunPickupFalk extends PolarStar.SpurPickup;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local FHumanPawn FHP;
      local Inventory Copy;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (SparkGunFalk(Copy) != none && SparkGunFalk(Copy).fStoredAmmo == -1)
            {
               SparkGunFalk(Copy).fStoredAmmo        = fStoredAmmo;
               SparkGunFalk(Copy).fPickupAmmoRestore = True;
               SparkGunFalk(Copy).fPawnAmmoRestore   = True;
               //Warn("Recovering from pickup: "@fStoredAmmo); 
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if ( KFWeapon(Copy) != none )
         {
            // sell price scaling based on difficulty
            if (!bDropped)
            {
               //warn("VRG");

               FHP = FHumanPawn(Other);

               if (FHP != none)
               {
                  if (Level.Game.GameDifficulty >= 8.0)       // BB
                  {
                     SellValue = cost * FHP.fBBRPCostMulti;
                     //warn("BB");
                  }

                  else if (Level.Game.GameDifficulty >= 7.0)  // HOE
                  {
                     SellValue = cost * FHP.fHOERPCostMulti;
                     //warn("HOE");
                  }

                  else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
                  {
                     SellValue = cost * FHP.fSuicidalRPCostMulti;
                     //warn("SUI");
                  }

                  else if (Level.Game.GameDifficulty >= 4.0)  // hard
                  {
                     SellValue = cost * FHP.fHardRPCostMulti;
                     //warn("HRD");
                  }
               }
            }

            //warn("VAL:"@SellValue);

            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
                  Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

defaultproperties
{
	ItemName="Spark Aid Gun"
	ItemShortName="Spark"
	PickupMessage="You got a Spark Aid Gun"
	cost=400
   InventoryType=Class'SparkGunFalk'
   UV2Texture=FadeColor'PatchTex.Common.PickupOverlay'
	Weight=1.000000
	CorrespondingPerkIndex=8
	PowerValue=0
   SpeedValue=20
   RangeValue=100
	PickupSound=None
	DrawScale=1.0
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.SparkGunPickup'
   Skins(0)=Texture'LairTextures_T.CustomReskins.SparkGun'
}
