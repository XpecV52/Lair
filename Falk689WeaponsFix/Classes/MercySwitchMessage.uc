class MercySwitchMessage extends CriticalEventPlus;

var() localized string SwitchMessage[2];

static function string GetString (optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2, optional Object OptionalObject)
{
	if (Switch >= 0 && Switch < 2)
		return Default.SwitchMessage[Switch];
}

defaultproperties
{
   SwitchMessage(0)="Set to burst fire"
   SwitchMessage(1)="Set to semi-automatic"
   DrawColor=(B=0,G=0,R=220)
   FontSize=-2
}
