class ATMinePickupFalk extends ATMine.ATMinePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ATMineExplosiveFalk'
   ItemName="Antitank Mines"
   ItemShortName="Mines"
   PickupMessage="You got Antitank Mines"
   AmmoCost=450
   cost=450
   PowerValue=100
   SpeedValue=0
   RangeValue=15
}