class PlasmaNadeFalk extends KFMod.Nade;

#exec OBJ LOAD FILE=KF_GrenadeSnd.uax
#exec OBJ LOAD FILE=LairTextures_T.utx

var()   float ZapAmount; // Amount of zap to apply to zeds when this projectile explodes near them
var bool      fTimerSet; // safety to prevent setting the timer twice

replication
{
   reliable if (Role==ROLE_Authority)
      fTimerSet;
}

// Initial timer setting
function PostNetBeginPlay()
{
   SetTimer(ExplodeTimer, false);
   fTimerSet = true;
}

simulated function Explode(vector HitLocation, vector HitNormal)
{
   //local PlayerController  LocalPlayer;

   bHasExploded = True;
   BlowUp(HitLocation);

   // Plasma Effects..
   PlaySound(sound'KF_GrenadeSnd.FlameNade_Explode',,100.5*TransientSoundVolume);

   Spawn(Class'PlasmaNadeExplosionFalk',,, HitLocation, rotator(vect(0,0,1)));
   Destroy();
}


simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;

   if ( bHurtEntry )
      return;

   bHurtEntry = true;

   ClearStayingDebugLines();

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         if (Instigator == None || Instigator.Controller == None)
            Victims.SetDelayedDamageInstigatorController( InstigatorController );

         if (Victims == LastTouched)
            LastTouched = None;

         P = Pawn(Victims);

         if(P != none)
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if(bAlreadyChecked)
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if(KFMonsterVictim != none && KFMonsterVictim.Health <= 0)
            {
               KFMonsterVictim = none;
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if( KFMonsterVictim == none )
            {
               P = none;
               continue;
            }

            else if (Role == ROLE_Authority)
            {
               KFMonsterVictim.SetZapped(ZapAmount, Instigator);
               NumKilled++;
            }

            P = none;
         }
      }
   }

   bHurtEntry = false;
}

// Siren override
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   // hopefully shatter glasses more reliably
   if (KFPawn(Wall) == none && KFPawn(Wall.Base) == none && KFDoorMover(Wall) == none && KFDoorMover(Wall.Base) == none)
      Wall.TakeDamage(100, Instigator, Location, HitNormal, class'DamTypeFragImpactFalk');

   if ((Pawn(Wall) != None) || (GameObjective(Wall) != None))
   {
      Explode(Location, HitNormal);
      return;
   }

   if (!fTimerSet)
   {
      SetTimer(ExplodeTimer * 0.8, false);
      fTimerSet = true;
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -1.5;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 0;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 50000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
            EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

// Touch only if we haven't exploded yet and don't hit broken zeds heads
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local FalkMonsterBase P;
   local float fHeadMulti;
   local vector X;

   X = Vector(Rotation);

   // hopefully shatter glasses more reliably
   if (KFPawn(Other) == none && KFPawn(Other.Base) == none && KFDoorMover(Other) == none && KFDoorMover(Other.Base) == none)
      Other.TakeDamage(100, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');

   if (!bHasExploded)
   {
      P = FalkMonsterBase(Other);

      if (P != none)
      {
         fHeadMulti = P.FalkGetNadeHeadScale();

         if (fHeadMulti <= 0 || !P.IsHeadShot(HitLocation, X, fHeadMulti))
            Super.ProcessTouch(Other, HitLocation);
      }
   }
}


defaultproperties
{
   Speed=850.0
   ExplodeTimer=1.250000
   Damage=0.000000
   MyDamageType=Class'KFMod.DamTypeZEDGunMKII'
   ZapAmount=20.000000
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.SharpshooterGrenade'
}
