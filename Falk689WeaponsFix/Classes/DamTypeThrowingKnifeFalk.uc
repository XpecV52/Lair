class DamTypeThrowingKnifeFalk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
	KFStatsAndAchievements.AddMeleeDamage(Amount);
}

DefaultProperties
{
   WeaponClass=Class'ThrowingKnifeFalk'
   KDamageImpulse=2000.000000
   HeadShotDamageMult=1.100000
   DeathString="%o was beat down by %k"
   FemaleSuicide="%o beat herself down"
   MaleSuicide="%o beat himself down"
   bRagdollBullet=True
   bBulletHit=True
   PawnDamageEmitter=Class'ROEffects.ROBloodPuff'
   LowGoreDamageEmitter=Class'ROEffects.ROBloodPuffNoGore'
   LowDetailEmitter=Class'ROEffects.ROBloodPuffSmall'
   FlashFog=(X=600.000000)
   KDeathVel=100.000000
   KDeathUpKick=25.000000
   VehicleDamageScaling=0.600000
}
