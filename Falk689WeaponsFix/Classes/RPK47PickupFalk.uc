class RPK47PickupFalk extends RPK47.RPK47Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=10.000000
   cost=1500
   BuyClipSize=75
   AmmoCost=75
   InventoryType=Class'RPK47Falk'
   ItemName="RPK47 Machine Gun"
   ItemShortName="RPK47"
   PickupMessage="You got an RPK47 Machine Gun"
   CorrespondingPerkIndex=7
   EquipmentCategoryID=4
   PowerValue=61
   SpeedValue=50
   RangeValue=100
}