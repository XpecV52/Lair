class KSGPickupFalk extends KFMod.KSGPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	InventoryType=Class'KSGShotgunFalk'
	ItemName="Kel-Tec KSG"
	ItemShortName="KSG"
	PickupMessage="You got a Kel-Tec KSG"
	cost=2000
	BuyClipSize=12
	AmmoCost=24
	PowerValue=48
    SpeedValue=30
    RangeValue=100
}