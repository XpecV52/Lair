class MP7MAltFireFalk extends KFMod.MP7MAltFire;

// edited fire speed on zed time
function float GetFireSpeed()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

	if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
	{
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
         return Vet.Static.GetAltFireSpeedMod(KFPRI);
	}

	return 1;
}

// using my ammo var
simulated function bool AllowFire()
{
	if(KFWeapon(Weapon).bIsReloading)
		return false;
	if(KFPawn(Instigator).SecondaryItem!=none)
		return false;
	if(KFPawn(Instigator).bThrowingNade)
		return false;

	return MP7MMedicGunFalk(Weapon).fHealingAmmoFalk >= AmmoPerFire;
}

defaultproperties
{
   ProjectileClass=Class'MP7MHealingProjectileFalk'
   FireRate=0.50000
   AmmoPerFire=2500
}