class ShotgunBulletFalk extends KFMod.ShotgunBullet;

var Actor      FLastDamaged;   // last damaged zed, used to attempt to fix multiple shots on the same zed
var byte       BulletID;       // bullet ID, passed at takedamage to prevent multiple shots
var float      FDamage;        // damage amount, prevents weird vanilla stuff to happen
var byte       FPenetrations;  // how many zeds we penetrated... wink wink nudge nudge...
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, FPenetrations, FLastDamaged, fCInstigator;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// less shit here
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   HurtWall = None;

   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
      return;

   //log("Wall: "@Wall);

   if ((Wall.bStatic && Wall.bWorldGeometry) || 
       KFDoorMover(Wall)  != none            ||
       KFTraderDoor(Wall) != none)
   {
      Explode(Location + ExploWallOut * HitNormal, HitNormal);

      if (ImpactEffect != None && (Level.NetMode != NM_DedicatedServer))
         Spawn(ImpactEffect,,, Location, rotator(-HitNormal));

      if (Trail != None)
      {
         Trail.mRegen=False;
         Trail.SetPhysics(PHYS_None);
      }

      Destroy();
   }
}

// MultiHit and post-death kill fixes, and more, probably
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn KFHitPawn;
   local Pawn HitPawn;
   local int bHp;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   //log("Other: "@Other);

   FLastDamaged = Other;

   X = Vector(Rotation);

   if(ROBulletWhipAttachment(Other) != none)
   {
      if(!Other.Base.bDeleteMe)
      {
         Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

         if(Other == none || HitPoints.Length == 0)
            return;

         KFHitPawn = KFPawn(Other);

         if (Role == ROLE_Authority)
         {
            if (KFHitPawn != none)
            {
               if(!KFHitPawn.bDeleteMe)
               {
                  KFHitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                  KFHitPawn.ProcessLocationalDamage(FDamage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), MyDamageType,HitPoints);

                  FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

                  if (FPenetrations >= MaxPenetrations)
                     Destroy();

                  FPenetrations++;
               }
            }
         }
      }
   }

   else
   {
      HitPawn = Pawn(Other);

      if (HitPawn == none)
         HitPawn = Pawn(Other.Base);

      if (HitPawn != none)
      {
         bHP = HitPawn.Health;

         Other.SetDelayedDamageInstigatorController(fCInstigator);
         HitPawn.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         // don't scale damage if we haven't damaged the zed
         if (bHP > 0 && HitPawn.Health < bHP)
         {
            if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
               PenDamageReduction = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.static.GetShotgunPenetrationDamageMulti(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo),default.PenDamageReduction);

            else
               PenDamageReduction = default.PenDamageReduction;

            FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

            // if we've struck through more than the max number of foes, destroy.
            if (FPenetrations >= MaxPenetrations)
               Destroy();

            FPenetrations++;
         }
      }

      // this may be needed since shotguns basically fire bugs
      else
      {
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
            PenDamageReduction = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.static.GetShotgunPenetrationDamageMulti(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo),default.PenDamageReduction);

         else
            PenDamageReduction = default.PenDamageReduction;

         FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

         // if we've struck through more than the max number of foes, destroy.
         if (FPenetrations >= MaxPenetrations)
            Destroy();

         FPenetrations++;
      }
   }
}

defaultproperties
{
   FDamage=35.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MomentumTransfer=30000.00000
   MyDamageType=Class'DamTypeShotgunFalk'
}
