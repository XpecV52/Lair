class HK23EPickupFalk extends KFWeaponPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=10.000000
   cost=900
   AmmoCost=90
   BuyClipSize=90
   PowerValue=48
   SpeedValue=50
   RangeValue=100
   ItemName="HK23E Light Machine Gun"
   ItemShortName="HK23E"
   AmmoItemName="5.56mm NATO rounds"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   CorrespondingPerkIndex=7
   EquipmentCategoryID=4
   InventoryType=Class'HK23EFalk'
   PickupMessage="You got a HK23E Light Machine Gun"
   PickupSound=Sound'HK23ESA_A.HK23_SND.HK23Select'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'HK23ESA_A.HK23_Static.HK23PickupSA'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
}
