class MediShotFalk extends MediShotBaseFalk;

#exec OBJ LOAD FILE=LairTextures_T.utx

// server stuff
var float fRRCheckTick;                 // on the server we kind of need to check and adjust ReloadRate times to times, this is the interval
var float fRRCurCheckTick;              // used to check what time is it and tick the ReloadRate check

var float fReloadSpeedFix; // reload time speed fix
var float fDReloadRate;    // desired reload rate

var float FBringUpTime;    // last time bringup was called
var float FBringUpSafety;  // safety to prevent reload on server before the weapon is ready


replication
{
   reliable if(Role == ROLE_Authority)
      fDReloadRate;
}

// don't load hud icons
static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
	local int i;

	if ( !bSkipRefCount )
	{
		default.ReferenceCount++;
	}

	UpdateDefaultMesh(SkeletalMesh(DynamicLoadObject(default.MeshRef, class'SkeletalMesh')));
	default.SelectSound = sound(DynamicLoadObject(default.SelectSoundRef, class'sound'));

	for ( i = 0; i < default.SkinRefs.Length; i++ )
		default.Skins[i] = Material(DynamicLoadObject(default.SkinRefs[i], class'Material'));

	if ( KFWeapon(Inv) != none )
	{
		Inv.LinkMesh(default.Mesh);
		KFWeapon(Inv).SelectSound = default.SelectSound;

		for (i = 0; i < default.SkinRefs.Length; i++)
			Inv.Skins[i] = default.Skins[i];
	}
}

// don't randomly unload anything
static function bool UnloadAssets()
{
	default.ReferenceCount--;
	return default.ReferenceCount == 0;
}

// Fixed stuff and added a second force reload
simulated function Tick(float dt)
{
	if (Level.NetMode != NM_Client)
	{
      // attempting to restore stored ammo from the pawn
      if (!fPawnAmmoRestore)
      {
         if (fStoredAmmo == -1 && FHumanPawn(Instigator) != none)
         {
            if (FHumanPawn(Instigator).fStoredSyringeAmmo > -1 && FHumanPawn(Instigator).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
            {
               fStoredAmmo = FHumanPawn(Instigator).fStoredSyringeAmmo;
               //warn("Tick recovering from pawn: "@fStoredAmmo);
               fPawnAmmoRestore = True;
            }

            else
            {
               //warn("F2");
               fPawnAmmoRestore = True;
               
               if (!fPickupAmmoRestore)
               {
                  fPickupAmmoRestore = True;
                  fHealingAmmoFalk   = 0;
               }
            }
         }

         else
         {
            //warn("F1");
            fPawnAmmoRestore = True;
         }
      }

      // setting ammo back on pickup, final step
      if (fStoredAmmo > -1)
      {
         fHealingAmmoFalk = fStoredAmmo;
         fStoredAmmo      = -1;
      }

      if (fShouldReload)
         fShouldReload = ForceHealingReload();

      else if (fHealingAmmoFalk < Default.fHealingAmmoFalk && RegenTimer < Level.TimeSeconds)
      {
         RegenTimer = Level.TimeSeconds + AmmoRegenRate;

		   if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
			   fHealingAmmoFalk += 100 * KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetSyringeChargeRate(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo));

		   else
			   fHealingAmmoFalk += 100;

		   if (fHealingAmmoFalk > Default.fHealingAmmoFalk)
			   fHealingAmmoFalk = Default.fHealingAmmoFalk;
      }
	}
}

/*simulated function AddReloadedAmmo()
{
    if(AmmoAmount(0) > 0)
        ++MagAmmoRemaining;

    if( !bHoldToReload )
        ClientForceKFAmmoUpdate(MagAmmoRemaining,AmmoAmount(0));
}*/

// override to activate infinite medic ammo on zed time
simulated function bool ConsumeAmmo(int Mode, float load, optional bool bAmountNeededIsMax)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   if (Mode == 1)
   {
      // if we're medic and in zed time we get infinite medic darts
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

		if (KFPRI                    != none &&
          KFPRI.ClientVeteranSkill != none)
      {

         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none && Vet.Static.InfiniteMedicDarts(KFPRI)) 
            return True;
      }

      if(Load > fHealingAmmoFalk)
         return False;

      fHealingAmmoFalk -= Load;
      return True;
   }

   return super(MP7MMedicGun).ConsumeAmmo(Mode, load, bAmountNeededIsMax);
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (MediShotPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         MediShotPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// attempt to see the whole reload animation in server
simulated function WeaponTick(float dt)
{
   local float LastSeenSeconds;

   if (fShouldBreechReload)
   {
      if (fRRCurCheckTick < fRRCheckTick)
         fRRCurCheckTick += dt;

      else
      {
         fRRCurCheckTick = 0;

         if (fLoadingLastBullet)
            ReloadRate = fLastBulletRate;

         else
            ReloadRate = fDReloadRate;

      }
   }

   //else if (fDReloadRate != Default.ReloadRate)
      //fDReloadRate = Default.ReloadRate;

   // vanilla stuff
	if ( bHasAimingMode )
	{
        if( bForceLeaveIronsights )
        {
        	if( bAimingRifle )
        	{
                ZoomOut(true);

            	if( Role < ROLE_Authority)
        			ServerZoomOut(false);
            }

            bForceLeaveIronsights = false;
        }

        if( ForceZoomOutTime > 0 )
        {
            if( bAimingRifle )
            {
        	    if( Level.TimeSeconds - ForceZoomOutTime > 0 )
        	    {
                    ForceZoomOutTime = 0;

                	ZoomOut(true);

                	if( Role < ROLE_Authority)
            			ServerZoomOut(false);
        		}
    		}
    		else
    		{
                ForceZoomOutTime = 0;
    		}
    	}
	}

	if ( (Level.NetMode == NM_Client) || Instigator == None || KFFriendlyAI(Instigator.Controller) == none && Instigator.PlayerReplicationInfo == None)
	   return;

	UpdateMagCapacity(Instigator.PlayerReplicationInfo);

	if(!bIsReloading)
	{
		if(!Instigator.IsHumanControlled())
		{
			LastSeenSeconds = Level.TimeSeconds - Instigator.Controller.LastSeenTime;
			if(MagAmmoRemaining == 0 || ((LastSeenSeconds >= 5 || LastSeenSeconds > MagAmmoRemaining) && MagAmmoRemaining < MagCapacity))
				ReloadMeNow();
		}
	}

	else
	{
		if ((Level.TimeSeconds - ReloadTimer) >= ReloadRate)
		{
			if(AmmoAmount(0) <= MagCapacity && !bHoldToReload)
			{
				MagAmmoRemaining = AmmoAmount(0);
				ActuallyFinishReloading();
			}

			else
			{
				AddReloadedAmmo();

				if (bHoldToReload)
               NumLoadedThisReload++;

				if (MagAmmoRemaining < MagCapacity && MagAmmoRemaining < AmmoAmount(0) && bHoldToReload)
					ReloadTimer = Level.TimeSeconds;

				if (MagAmmoRemaining >= MagCapacity || MagAmmoRemaining >= AmmoAmount(0) || !bHoldToReload || bDoSingleReload)
					ActuallyFinishReloading();

				else if (Level.NetMode!=NM_Client)
					Instigator.SetAnimAction(WeaponReloadAnim);
			}
		}

		else if(bIsReloading && !bReloadEffectDone && Level.TimeSeconds - ReloadTimer >= ReloadRate / 2)
		{
			bReloadEffectDone = true;
			ClientReloadEffects();
		}
	}
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// attempt to see the whole fockin reload animation in server
exec function ReloadMeNow()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   fDReloadRate = Default.ReloadRate;

   Super.ReloadMeNow();
}

// attempt to see the whole fockin reload animation
simulated function ClientReload()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   Super.ClientReload();
}

// second attempt to see the whole fockin reload animation
simulated function AddReloadedAmmo()
{
   if (AmmoAmount(0) > 0)
      ++MagAmmoRemaining;

   fDReloadRate -= fReloadSpeedFix;
   ReloadRate    = fDReloadRate;

   // last bullet to load, don't interrupt or drop
   if (fShouldBreechReload && AmmoAmount(0) > MagCapacity - 1 && MagAmmoRemaining == MagCapacity - 1)
   {
      ReloadRate         = fLastBulletRate;
      bCanThrow          = False;
      fLoadingLastBullet = True;
   }

   // restore vanilla stuff
   else if (MagAmmoRemaining == MagCapacity)
   {
      fDReloadRate        = Default.ReloadRate;
      bCanThrow           = True;
      fShouldBreechReload = False;
      fLoadingLastBullet  = False;
   }
}

// third attempt to see the whole fockin reload
simulated function bool InterruptReload()
{
   if (fLoadingLastBullet)
      return False;

   if (Super.InterruptReload())
   {
      fDReloadRate        = Default.ReloadRate;
      fShouldBreechReload = False;
      bCanThrow           = True;

      return True;
   }

   return False;
}

// attempt to fix more and more weird shit happening with the reload
simulated function ClientInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fDReloadRate        = Default.ReloadRate;
   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ClientInterruptReload();
}

// more attempt to fix that reload on server
simulated function ServerInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fDReloadRate        = Default.ReloadRate;
   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ServerInterruptReload();
}

// restore ammo from the pawn if they're fresh enough
function PickupFunction(Pawn Other)
{
   Super(MP7MMedicGun).PickupFunction(Other);

   if (fStoredAmmo == -1 && FHumanPawn(Other) != none &&
       FHumanPawn(Other).fStoredSyringeAmmo > -1      &&
       FHumanPawn(Other).fStoredSyringeTime + fSyringeFixTime >= Level.TimeSeconds)
   {
      fStoredAmmo = FHumanPawn(Other).fStoredSyringeAmmo;
      //warn("Restoring ammo from the pawn: "@fStoredAmmo);
      FHumanPawn(Other).fStoredSyringeAmmo = -1;
   }
}

// permit throwing while reloading, blocking it during the last bullet
simulated function bool CanThrow()
{
   if (bIsReloading && !fLoadingLastBullet)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}


// just don't
simulated function DoAutoSwitch(){}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   HealBoostAmount=20
   AmmoRegenRate=0.300000 // standard slow recharge rate for non-smg
   MagCapacity=4
   bHoldToReload=True
   WeaponReloadAnim="Reload_Shotgun"
   HudImage=Texture'LairTextures_T.CustomReskins.MedishotUnselected'
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.MedishotSelected'
   HudImageRef=""
   SelectedHudImageRef=""
   Weight=4.000000
   SleeveNum=0
   TraderInfoTexture=Texture'WhiskyMediShot_T.Trader_Medishot'
   FireModeClass(0)=Class'MediShotFireFalk'
   FireModeClass(1)=Class'MediShotAltFireFalk'
   SelectSound=Sound'KF_MP7Snd.MP7_Select'
   Priority=180
   GroupOffset=4
   PickupClass=Class'MediShotPickupFalk'
   AttachmentClass=Class'MediShotAttachmentFalk'
   Description="The Serbu Super-Shorty is a compact and stockless pump-action AOW chambered in 12-gauge. This particular version is modified to fire healing darts. Features a strong damage output and a slightly more efficient penetration than a Benelli M3."
   ItemName="Medical Serbu Super-Shorty Shotgun"
   Mesh=SkeletalMesh'WhiskyMediShot_A.medishot_weapon'
   MeshRef="WhiskyMediShot_A.medishot_weapon"
   Skins(1)=Texture'WhiskyMediShot_T.v1texture'
   SkinRefs(0)="WhiskyMediShot_T.v1texture"
   SkinRefs(1)="WhiskyMediShot_T.v1texture"
   ReloadRate=0.67
   fDReloadRate=0.67
   fLastBulletRate=1.24
   FBringUpSafety=0.1
   fReloadSpeedFix=0.02
   fRRCheckTick=0.1
}
