class FragPickupFalk extends FragPickupBaseFalk;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=0.000000
   InventoryType=Class'FragFalk'
   AmmoCost=40
   BuyClipSize=1
   ItemName="Grenades"
   ItemShortName="Grenades"
   AmmoItemName="Grenades"
   AmmoMesh=StaticMesh'KillingFloorStatics.FragPickup'
   EquipmentCategoryID=4
   CorrespondingPerkIndex=8
   PickupMessage="You got some Frag Grenades"
   PickupSound=Sound'KF_GrenadeSnd.Nade_Pickup'
   DrawScale=0.000000
   DrawType=DT_None
   CollisionRadius=0.000000
   CollisionHeight=0.000000
   PowerValue=0
   SpeedValue=0
   RangeValue=0
}
