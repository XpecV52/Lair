class Caius_SwordPickupFalk extends Caius_SwordPickupBaseFalk;

#EXEC OBJ LOAD FILE=LairStaticMeshes_SM.usx

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   PowerValue=91
   SpeedValue=20
   RangeValue=80
   Weight=13.000000
   cost=4000
   InventoryType=Class'Caius_SwordFalk'
   ItemName="Legendary Caius's Greatsword"
   ItemShortName="Caius's GS"
   PickupMessage="You got the Caius's Legendary Greatsword"
   DrawScale=0.10000
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.CaiusPickup'
}
