class DamTypeSCARMK17AssaultRifleFalk extends KFMod.DamTypeSCARMK17AssaultRifle;

defaultproperties
{
    WeaponClass=Class'SCARMK17AssaultRifleFalk'
    DeathString="%k killed %o (SCAR-H)"
	HeadShotDamageMult=1.100000
}