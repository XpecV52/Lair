class DwarfAxePickupFalk extends KFMod.DwarfAxePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=2000
   Weight=7.000000
   InventoryType=Class'DwarfAxeFalk'
   ItemName="Dwarven Axe"
   ItemShortName="Dwarven Axe"
   PickupMessage="You got a Dwarven Axe"
   PowerValue=70
   SpeedValue=30
   RangeValue=30
}
