class SeekerSixAmmoFalk extends KFMod.SeekerSixAmmo;

defaultproperties
{
   MaxAmmo=96
   InitialAmount=42
   AmmoPickupAmount=12
   PickupClass=Class'SeekerSixAmmoPickupFalk'
   ItemName="Seeking rockets"
}