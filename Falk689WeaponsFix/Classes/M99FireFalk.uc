class M99FireFalk extends KFMod.KFFire;

var float PenDamageReduction; // multiplier to apply on damage when we hit a zed
var byte  MaxPenetrations;    // max amount of penetrations on zeds
var byte  BID;                // bullet ID to prevent multiple hits on a single zed

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   if (KFPawn(Instigator).SecondaryItem != none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
   {
      if (Level.TimeSeconds - LastClickTime > FireRate)
         LastClickTime = Level.TimeSeconds;

      return false;
   }

   return super(WeaponFire).AllowFire();
}

// multi-hit and invisible head fix
function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other;
   local array<int>	     HitPoints;
   local array<Actor>     IgnoreActors;
   local Actor            DamageActor;
   local Actor            OldDamageActor;
   local byte             HitCount;
   local byte             LoopCount;
   local float            HitDamage;
   local int              i;
   local int              bHP;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X + 
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   While (LoopCount < 10)
   {
      LoopCount++;
      //log("DoTrace Loop");
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if(Other == None)
         break;

      else if (Other == Instigator || Other.Base == Instigator)
      {
         //log("Instigator");
         IgnoreActors[IgnoreActors.Length] = Other;
         Other.SetCollision(false);
         Start = HitLocation;
         continue;
      }

      if(ExtendedZCollision(Other) != None && Other.Owner != None)
         DamageActor = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if(KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            //log("KFMonster");

            if (DamageActor != OldDamageActor) // don't hit this zed twice
            {
               OldDamageActor = DamageActor;
               bHP            = KFMonster(DamageActor).Health;

               //log("old hp: "@bHP);

               if (bHP > 0)
               {
                  if (HitCount == 0)
                     HitDamage = DamageMax;

                  DamageActor.TakeDamage(int(HitDamage), Instigator, HitLocation, Momentum*X, DamageType, BID);

                  //log("new hp: "@KFMonster(DamageActor).Health);

                  if (KFMonster(DamageActor).Health != bHP) // this zed was hit, increase hitcount or break the loop
                  {
                     if (HitCount >= MaxPenetrations)
                        break;

                     HitDamage *= PenDamageReduction;
                     HitCount++;
                  }

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = Other;
                     Other.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
               {
                  //log("current hp = 0");
                  break;
               }
            }

            else
            {
               IgnoreActors[IgnoreActors.Length] = Other;
               Other.SetCollision(false);
               //log("ignore");
            }
         }

         else
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

         Start = HitLocation;
      }

      else if (HitScanBlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         //log("HitScanBlockingVolume");
         break;
      }
   }

   // Turn the collision back on for any actors we turned it off
   if (IgnoreActors.Length > 0)
   {
      for (i=0; i<IgnoreActors.Length; i++)
      {
         if(IgnoreActors[i] != None)
            IgnoreActors[i].SetCollision(true);
      }
   }
}

// set weapon to pending reload state
event ModeDoFire()
{
   super.ModeDoFire();

   // server
   if (Weapon.Role == ROLE_Authority && AllowFire() && Weapon != None)
      M99SniperRifleFalk(Weapon).SetPendingReload();
}

// keep this gun in a way that a headshot by a lv20 sharp oneshots even a 12man hoe fp, currenly deals 2021 damage to one

defaultproperties
{
   RecoilRate=0.100000
   maxVerticalRecoilAngle=4000
   maxHorizontalRecoilAngle=90
   FireAimedAnim="Fire_Iron"
   FireAnim="Fire"
   bRandomPitchFireSound=False
   FireSoundRef="KF_M99Snd.M99_Fire_M"
   StereoFireSoundRef="KF_M99Snd.M99_Fire_S"
   NoAmmoSoundRef="KF_SCARSnd.SCAR_DryFire"
   bWaitForRelease=True
   TransientSoundVolume=1.800000
   FireForce="AssaultRifleFire"
   FireRate=0.34
   AmmoClass=Class'M99AmmoFalk'
   ShakeRotMag=(X=5.000000,Y=7.000000,Z=3.000000)
   ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
   ShakeOffsetMag=(X=5.000000,Y=5.000000,Z=5.000000)
   BotRefireRate=3.570000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stPTRD'
   aimerror=1.000000
   Spread=0.004000
   DamageType=Class'DamTypeM99SniperRifleFalk'
   DamageMin=620               
   DamageMax=620
   MaxPenetrations=2
   PenDamageReduction=0.9
   AmmoPerFire=1
}
