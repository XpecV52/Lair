class DamTypeCaius_SwordFalk extends Caius_Sword.DamTypeCaius_Sword;

defaultproperties
{
   WeaponClass=Class'Caius_SwordFalk'
   HeadShotDamageMult=1.100000
   KDeathVel=150.000000
   KDeathUpKick=28.000000
   KDamageImpulse=9000.000000
}