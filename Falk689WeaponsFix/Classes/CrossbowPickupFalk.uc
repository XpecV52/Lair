class CrossbowPickupFalk extends KFMod.CrossbowPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=3000
   AmmoCost=10
   Weight=7.000000
   InventoryType=Class'CrossbowFalk'
   ItemName="Compound Crossbow"
   ItemShortName="Crossbow"
   PickupMessage="You got a Compound Crossbow"
   BuyClipSize=1
   PowerValue=53
   SpeedValue=6
   RangeValue=100
}
