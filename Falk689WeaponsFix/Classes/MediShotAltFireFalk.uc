class MediShotAltFireFalk extends MP7MAltFireFalk;

// interrupt reload with medic darts but not when loading the last bullet
simulated function bool AllowFire()
{
	if (MediShotFalk(Weapon) != none && MediShotFalk(Weapon).fLoadingLastBullet)
		return false;

	if (LocalMediShotFalk(Weapon) != none && LocalMediShotFalk(Weapon).fLoadingLastBullet)
		return false;

	if(KFPawn(Instigator).SecondaryItem != none)
		return false;

	if(KFPawn(Instigator).bThrowingNade)
		return false;


	if (MediShotFalk(Weapon) != none)
	   return MediShotFalk(Weapon).fHealingAmmoFalk >= AmmoPerFire;

	else if (LocalMediShotFalk(Weapon) != none)
	   return LocalMediShotFalk(Weapon).fHealingAmmoFalk >= AmmoPerFire;

   return false;
}


defaultproperties
{
    FireAimedAnim="AltFire"
    FireAnim="AltFire"
    ProjectileClass=Class'MediShotHealingProjectileFalk'
}
