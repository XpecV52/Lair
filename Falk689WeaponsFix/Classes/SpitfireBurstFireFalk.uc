class SpitfireBurstFireFalk extends Spitfire.SpitfireBurstFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 240)
      BID = 1;

   class<SpitfireTendrilFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

simulated function bool AllowFire()
{
	if(KFWeapon(Weapon).bIsReloading)
		return false;
	if(KFWeapon(Weapon).MagAmmoRemaining < 1)
	{
		if(Level.TimeSeconds - LastClickTime > FireRate)
		{
			Weapon.PlayOwnedSound(NoAmmoSound, SLOT_Interact, TransientSoundVolume,,,, false);
			LastClickTime = Level.TimeSeconds;
			if(Weapon.HasAnim(EmptyAnim))
				weapon.PlayAnim(EmptyAnim, EmptyAnimRate, 0.0);
		}
		return false;
	}

	LastClickTime = Level.TimeSeconds;

	return Super(CrossbowFire).AllowFire();
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local SpitfireTendrilFalk FB;

   FB = SpitfireTendrilFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'SpitfireAmmoFalk'
   ProjectileClass=Class'SpitfireTendrilFalk'
   FireRate=0.070000
   maxHorizontalRecoilAngle=70
   EffectiveRange=1500.000000
}
