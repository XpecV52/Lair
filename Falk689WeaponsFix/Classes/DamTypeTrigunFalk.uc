class DamTypeTrigunFalk extends Trigun_Pistol.DamTrigun_Pistol;

defaultproperties
{
   WeaponClass=Class'TrigunFalk'
   bCheckForHeadShots=True
   KDamageImpulse=1000
   KDeathVel=125
   KDeathUpKick=25
   bIsExplosive=True
   HeadShotDamageMult=1.100000
}