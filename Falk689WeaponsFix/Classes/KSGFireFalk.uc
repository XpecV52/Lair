class KSGFireFalk extends KFMod.KSGFire;

var byte BID;

// Added zed time support skills
event ModeDoFire()
{
	local float Rec;
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

	if (!AllowFire())
		return;

	Spread = Default.Spread;

	Rec = GetFireSpeed();
	FireRate = default.FireRate/Rec;
	FireAnimRate = default.FireAnimRate*Rec;
	Rec = 1;

   // wut is dis? gonna comment da shit out of it - past tense Falk
	/*if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
		Spread *= KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.ModifyRecoilSpread(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self, Rec);*/

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

	// if wide spread is on give us some additional spread
   if( KSGShotgun(Weapon) != none && KSGShotgun(Weapon).bWideSpread )
	   Spread *= 2.00;

	if( !bFiringDoesntAffectMovement )
	{
		if (FireRate > 0.25)
		{
			Instigator.Velocity.x *= 0.1;
			Instigator.Velocity.y *= 0.1;
		}

		else
		{
			Instigator.Velocity.x *= 0.5;
			Instigator.Velocity.y *= 0.5;
		}
	}

	super(BaseProjectileFire).ModeDoFire();

    // client
    if (Instigator.IsLocallyControlled())
        HandleRecoil(Rec);
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 240)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   ProjectileClass=Class'KSGBulletFalk'
   AmmoClass=Class'KSGAmmoFalk'
}
