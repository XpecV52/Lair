class AK12SAFireFalk extends KFMod.BullpupFire;

var byte BID; // bullet ID, used to prevent multiple shots on the same zed

function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other;
   local array<int>	     HitPoints;
   local array<Actor>     IgnoreActors;
   local Actor            DamageActor;
   local Actor            OldDamageActor;
   local int              i;
   local int              bHP;
   local byte             LoopCount;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X + 
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   While (LoopCount <= 10)
   {
      //log("DoTrace Loop");
      LoopCount++;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if(Other == None)
         break;

      else if (Other == Instigator || Other.Base == Instigator)
      {
         //log("Instigator");
         IgnoreActors[IgnoreActors.Length] = Other;
         Other.SetCollision(false);
         Start = HitLocation;
         continue;
      }

      if(ExtendedZCollision(Other) != None && Other.Owner != None)
         DamageActor = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if(KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            //log("KFMonster");

            if (DamageActor != OldDamageActor) // don't hit this zed twice
            {
               OldDamageActor = DamageActor;
               bHP            = KFMonster(DamageActor).Health;

               //log("old hp: "@bHP);

               if (bHP > 0)
               {
                  DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

                  //log("new hp: "@KFMonster(DamageActor).Health);

                  if (KFMonster(DamageActor).Health != bHP) // this zed was hit, break the loop
                     break;

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = Other;
                     Other.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
               {
                  //log("current hp = 0");
                  break;
               }
            }

            else
               break;
         }

         else
         {
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
         }

         Start = HitLocation;
      }

      else if (BlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         //log("HitScanBlockingVolume");
         break;
      }
   }

   // Turn the collision back on for any actors we turned it off
   if (IgnoreActors.Length > 0)
   {
      for (i=0; i<IgnoreActors.Length; i++)
      {
         if(IgnoreActors[i] != None)
            IgnoreActors[i].SetCollision(true);
      }
   }
}


defaultproperties
{
   AmmoClass=Class'AK12SAAmmoFalk'
   DamageMin=31
   DamageMax=31
   DamageType=Class'DamTypeAK12SAFalk'
   FireRate=0.100000
   FireAimedAnim="Fire"
   RecoilRate=0.070000
   maxVerticalRecoilAngle=500
   maxHorizontalRecoilAngle=250
   ShellEjectClass=Class'ROEffects.KFShellEjectAK'
   ShellEjectBoneName="Shell_eject"
   bRandomPitchFireSound=False
   FireSoundRef="AK12SA_A.AK12_SND.AK12Fire"
   StereoFireSoundRef="AK12SA_A.AK12_SND.AK12Fire"
   NoAmmoSoundRef="KF_AK47Snd.AK47_DryFire"
   Momentum=8500.000000
   TransientSoundVolume=1.800000
   FireLoopAnim="Fire"
   TweenTime=0.025000
   FireForce="AssaultRifleFire"
   ShakeRotMag=(X=50.000000,Y=50.000000,Z=350.000000)
   ShakeRotRate=(X=5000.000000,Y=5000.000000,Z=5000.000000)
   ShakeRotTime=0.750000
   ShakeOffsetMag=(X=6.000000,Y=3.000000,Z=7.500000)
   ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
   ShakeOffsetTime=1.250000
   BotRefireRate=0.990000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stSTG'
   aimerror=42.000000
   Spread=0.015000
   SpreadStyle=SS_Random
}
