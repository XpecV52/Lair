class CrossbowAmmoFalk extends KFMod.CrossbowAmmo;

defaultproperties
{
    MaxAmmo=30
    InitialAmount=15
    AmmoPickupAmount=5
    PickupClass=Class'CrossbowAmmoPickupFalk'
    ItemName="Arrow"
}