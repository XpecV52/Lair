class DeaglePickupFalk extends KFMod.DeaglePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

function inventory SpawnCopy(pawn Other)
{
   local Inventory I;
   local class<Inventory> D;

   for ( I = Other.Inventory; I != none; I = I.Inventory )
   {
        // can't just cast to Deagle to check, because golden deagle is a deagle
        // but we don't want to take your golden deagle and give you dual
        // normal deagles
      if( I.Class == class'DeagleFalk' )
      {
         if( Inventory != none )
            Inventory.Destroy();

         D = Default.InventoryType;
         Default.InventoryType = Class'DualDeagleFalk';

         AmmoAmount[0] += DeagleFalk(I).AmmoAmount(0);
         MagAmmoRemaining += DeagleFalk(I).MagAmmoRemaining;

         I.Destroyed();
         I.Destroy();

         I = Super.SpawnCopy(Other);
         Default.InventoryType = D;
         return I;
      }
   }

   InventoryType = Default.InventoryType;
   Return Super.SpawnCopy(Other);
}

defaultproperties
{
	Weight=2.000000
	cost=350
	BuyClipSize=7
	PowerValue=21
    SpeedValue=24
    RangeValue=100
	AmmoCost=7
	ItemName="IMI Desert Eagle MK XIX"
	ItemShortName="Deagle"
	AmmoItemName=".300 JHP rounds"
	InventoryType=Class'DeagleFalk'
	PickupMessage="You got an IMI Desert Eagle MK XIX"
}
