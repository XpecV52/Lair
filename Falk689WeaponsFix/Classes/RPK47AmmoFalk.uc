class RPK47AmmoFalk extends RPK47.RPK47Ammo;

defaultproperties
{
   AmmoPickupAmount=19 // one quarter of magazine, heavy weapons standard
   MaxAmmo=300
   InitialAmount=150
   PickupClass=Class'RPK47AmmoPickupFalk'
}