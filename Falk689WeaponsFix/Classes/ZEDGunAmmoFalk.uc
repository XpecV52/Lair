class ZEDGunAmmoFalk extends KFMod.ZEDGunAmmo;

defaultproperties
{
    MaxAmmo=400
    InitialAmount=200
    AmmoPickupAmount=50
    PickupClass=Class'ZEDGunAmmoPickupFalk'
    ItemName="ZED power cells"
}
