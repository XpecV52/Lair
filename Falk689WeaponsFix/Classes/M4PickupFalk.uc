class M4PickupFalk extends KFMod.M4Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	Weight=6.000000
	cost=1050
	AmmoCost=15
	BuyClipSize=30
	PowerValue=35
	SpeedValue=70
	RangeValue=100
	ItemName="Colt M4 Carbine"
	ItemShortName="Colt M4"
	AmmoItemName="5.56mm rounds"
	InventoryType=Class'M4AssaultRifleFalk'
	PickupMessage="You got a Colt M4 Carbine"
}
