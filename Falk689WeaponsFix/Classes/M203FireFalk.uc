class M203FireFalk extends M79FireFalk;

//var   byte  BID;
var   bool  bVeryLastShotAnim;
var   float FireLastRate;
var() Name  FireLastAnim;

// attempt to set last shot anim
event ModeDoFire()
{
   if (!AllowFire())
      return;

   bVeryLastShotAnim = Weapon.AmmoAmount(1) <= AmmoPerFire;

   Super(M79Fire).ModeDoFire();
}

// alternative point blank fix
/*function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<M79GrenadeProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}*/

// early setting fCInsigator
/*function PostSpawnProjectile(Projectile P)
{
   local M79GrenadeProjectileFalk FG;

   FG = M79GrenadeProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}*/

function float MaxRange()
{
    return 5000;
}

// by now don't do shit
simulated function bool AllowFire()
{
   return super(M79Fire).AllowFire();
}

// last fire anim when out of ammo
function PlayFiring()
{
   local float RandPitch;

   if (Weapon.Mesh != None)
   {
      if (bVeryLastShotAnim)
         Weapon.PlayAnim(FireLastAnim, FireAnimRate, TweenTime);

      else
         Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
   }

   if (Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() &&
         Weapon.Instigator.IsFirstPerson() && StereoFireSound != none)
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if( FRand() < 0.5 )
         {
            RandPitch *= -1.0;
         }
      }

      Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   else
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if( FRand() < 0.5 )
         {
            RandPitch *= -1.0;
         }
      }

      Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   ClientPlayForceFeedback(FireForce);  // jdf

   FireCount++;
}


defaultproperties
{
   EffectiveRange=2500.000000
   maxVerticalRecoilAngle=200
   maxHorizontalRecoilAngle=50
   FireAimedAnim="Fire_Iron_Secondary"
   FireSoundRef="KF_M79Snd.M79_Fire"
   StereoFireSoundRef="KF_M79Snd.M79_FireST"
   NoAmmoSoundRef="KF_M79Snd.M79_DryFire"
   ProjPerFire=1
   ProjSpawnOffset=(X=50.000000,Y=10.000000)
   bWaitForRelease=True
   TransientSoundVolume=1.800000
   FireAnim="Fire_Secondary"
   FireForce="AssaultRifleFire"
   FireRate=3.690000
   AmmoClass=Class'M203AmmoFalk'
   ShakeRotMag=(X=3.000000,Y=4.000000,Z=2.000000)
   ShakeRotRate=(X=10000.000000,Y=10000.000000,Z=10000.000000)
   ShakeOffsetMag=(X=3.000000,Y=3.000000,Z=3.000000)
   ProjectileClass=Class'M203GrenadeProjectileFalk'
   BotRefireRate=1.800000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stNadeL'
   aimerror=42.000000
   Spread=0.015000
   FireLastRate=0.6
   FireLastAnim="FireLast_Secondary"
}
