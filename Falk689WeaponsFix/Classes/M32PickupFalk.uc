class M32PickupFalk extends KFMod.M32Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'M32GrenadeLauncherFalk'
   cost=3500
   ItemShortName="M32"
   PickupMessage="You got a Milkor M32 Grenade Launcher"
   ItemName="Milkor M32 Grenade Launcher"
   BuyClipSize=6
   AmmoCost=30
   PowerValue=60
   SpeedValue=80
   RangeValue=100
   Weight=8.000000
   DrawScale=1.35
}