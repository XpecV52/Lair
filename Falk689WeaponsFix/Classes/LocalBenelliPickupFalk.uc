class LocalBenelliPickupFalk extends KFMod.BenelliPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=3400
   BuyClipSize=6
   AmmoCost=12
   InventoryType=Class'LocalBenelliShotgunFalk'
   ItemName="Benelli M4 Super 90 Combat Shotgun"
   ItemShortName="Benelli M4"
   PickupMessage="You got a Benelli M4 Super 90 Combat Shotgun"
   PowerValue=49
   SpeedValue=80
   RangeValue=100
}
