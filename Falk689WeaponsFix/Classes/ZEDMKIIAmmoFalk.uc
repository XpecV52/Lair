class ZEDMKIIAmmoFalk extends KFAmmunition;

#EXEC OBJ LOAD FILE=KillingFloorHUD.utx

defaultproperties
{
   AmmoPickupAmount=30
   MaxAmmo=360
   InitialAmount=180
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=451,Y1=445,X2=510,Y2=500)
}
