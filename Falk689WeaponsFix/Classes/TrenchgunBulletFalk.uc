class TrenchgunBulletFalk extends KFMod.TrenchgunBullet;

var int        BulletID;       // MultiHit fix base variable
var float      FDamage;        // My damage is 689% cooler than yours
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var Actor      FLastDamaged;   // used to further prevent us from hitting twice
var bool       fAlreadyHit;    // also don't hit anybody else

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, fCInstigator;
}

// Called by the firemode to actually start the projectile
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super.PostBeginPlay();
}

// simplified hitwall, removed useless stuff
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   HurtWall = None;

   if ((Wall.bStatic && Wall.bWorldGeometry) || 
       KFDoorMover(Wall)  != none            ||
       KFTraderDoor(Wall) != none)
   {
      Explode(Location + ExploWallOut * HitNormal, HitNormal);

      if (ImpactEffect != None && (Level.NetMode != NM_DedicatedServer))
         Spawn(ImpactEffect,,, Location, rotator(-HitNormal));

      if (Trail != None)
      {
         Trail.mRegen=False;
         Trail.SetPhysics(PHYS_None);
      }

      Destroy();
   }
}

// point blank bullet fix and more
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn KFHitPawn;
   local Pawn HitPawn;
   local int bHp;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   if (fAlreadyHit)
   {
      Destroy();
      return;
   }

   fAlreadyHit  = True;
   FLastDamaged = Other;

   X = Vector(Rotation);

   if (ROBulletWhipAttachment(Other) != none)
   {
      if (!Other.Base.bDeleteMe)
      {
         Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

         if( Other == none || HitPoints.Length == 0 )
            return;

         KFHitPawn = KFPawn(Other);

         if (Role == ROLE_Authority)
         {
            if (KFHitPawn != none)
            {
               if (!KFHitPawn.bDeleteMe)
               {
                  KFHitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                  KFHitPawn.ProcessLocationalDamage(FDamage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), MyDamageType,HitPoints);

                  Destroy();
               }
            }
         }
      }
   }

   else
   {
      HitPawn = Pawn(Other);

      if (HitPawn == none)
         HitPawn = Pawn(Other.Base);

      if (HitPawn != none)
      {

         bHP = HitPawn.Health;
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         HitPawn.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         // don't scale damage if we haven't damaged the zed
         if (bHP > 0 && HitPawn.Health < bHP)
            Destroy();
      }

      // this may be needed since shotguns basically fire bugs
      else
      {
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

         Destroy();
      }
   }
}



// kind of don't explode unless u have a deep reason to
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

defaultproperties
{
   MaxPenetrations=0
   PenDamageReduction=0.000000
   HeadShotDamageMult=1.100000
   Speed=3000.000000
   MaxSpeed=3000.000000
   Damage=16.000000
   FDamage=16.000000
   MyDamageType=Class'DamTypeTrenchgunFalk'
   ExplosionDecal=Class'BurnMarkTinyFalk'
}
