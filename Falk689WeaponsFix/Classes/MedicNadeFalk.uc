class MedicNadeFalk extends KFMod.MedicNade;

#exec OBJ LOAD FILE=LairTextures_T.utx

var byte       BulletID;       // bullet ID, passed at takedamage to prevent multiple shots
var bool       FTimerSet;      // don't set timer twice
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
   reliable if(Role == ROLE_Authority)
      FTimerSet, fCInstigator;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo   KFPRI;
   local class<FVeterancyTypes>    Vet;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   if (Instigator != none)
   {
      if (Role == ROLE_Authority && fCInstigator == none && Instigator.Controller != None)
         fCInstigator = Instigator.Controller;

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
         {
            class<DamTypeMedicNadeFalk>(MyDamageType).Default.fDotMultipler = Vet.Static.MedicDotDamage(KFPRI);

            //warn("New Dot Multi:"@class<DamTypeMedicNadeFalk>(MyDamageType).Default.fDotMultipler);
         }
      }
   }

   Super.PostBeginPlay();
}

function PostNetBeginPlay()
{
}

function HealOrHurt(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   //local float damageScale;
   local vector dir;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local FHumanPawn FP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;
   // Healing
   local KFPlayerReplicationInfo PRI;
   local int MedicReward;
   local float fMedicReward;
   local float HealSum; // for modifying based on perks
   local int PlayersHealed;
   local bool fShouldReward;
   local Falk689GameTypeBase Flk;

   if (bHurtEntry)
      return;

   NextHealTime = Level.TimeSeconds + HealInterval;

   bHurtEntry = true;

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         if( (Instigator==None || Instigator.Health<=0) && KFPawn(Victims)!=None )
            Continue;

         //damageScale = 1.0;

         Victims.SetDelayedDamageInstigatorController(fCInstigator);

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            FP = FHumanPawn(Victims);

            CheckedPawns[CheckedPawns.Length] = P;

            P = none;
         }
         else
         {
            continue;
         }

         if(FP == none)
         {
            //log(Level.TimeSeconds@"Hurting "$Victims$" for "$(damageScale * DamageAmount)$" damage");

            if( Pawn(Victims) != none && Pawn(Victims).Health > 0 )
            {
               /*Victims.TakeDamage(damageScale * DamageAmount, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius)
                     * dir,(damageScale * Momentum * dir),DamageType);*/

               Victims.TakeDamage(Default.Damage, Instigator, Victims.Location, dir, DamageType, BulletID);

               if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
               {
                  NumKilled++;
               }
            }
         }
         else
         {
            if (Instigator != none && FP.Health > 0 && FP.Health < FP.HealthMax)
            {
               if (FP.bCanBeHealed)
               {
                  PlayersHealed += 1;

                  MedicReward = HealBoostAmount;

                  PRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                  if (PRI != none && PRI.ClientVeteranSkill != none)
                     MedicReward *= PRI.ClientVeteranSkill.Static.GetHealPotency(PRI);

                  HealSum = MedicReward;

                  if ((FP.Health + FP.fHealthToGive + MedicReward) > FP.HealthMax)
                  {
                     MedicReward = FP.HealthMax - (FP.Health + FP.fHealthToGive);

                     if ( MedicReward < 0 )
                        MedicReward = 0;
                  }

                  if (FP == Instigator)
                     MedicReward = 0;

                  //log(Level.TimeSeconds@"Healing "$FP$" for "$HealSum$" base healamount "$HealBoostAmount$" health");
                  FP.GiveHealth(HealSum, FP.HealthMax);

                  if (PRI != None)
                  {
                     if (MedicReward > 0)
                     {
                        if (KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements) != none)
                           KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements).AddDamageHealed(MedicReward, false, true);

                        if (PlayerController(Instigator.Controller) != none)
                        {
                           // medic reward block stuff
                           Flk = Falk689GameTypeBase(Level.Game);

                           if (Flk != none)
                              fShouldReward = Flk.ShouldRewardMedic(PlayerController(Instigator.Controller));
                        }

                        if (fShouldReward)
                        {
                           fMedicReward = float(MedicReward) * 0.3; // 20hp healed = £ 6

                           // difficulty scaling
                           if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
                              fMedicReward *= 0.34;

                           else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
                              fMedicReward *= 0.5;

                           else if (Level.Game.GameDifficulty >= 5.0 ) // Suicidal
                              fMedicReward *= 0.67;

                           else if (Level.Game.GameDifficulty >= 4.0 ) // Hard
                              fMedicReward *= 0.84;

                           MedicReward = int(fMedicReward);

                           PRI.ReceiveRewardForHealing(MedicReward, FP);
                        }
                     }

                     if (KFHumanPawn(Instigator) != none)
                        KFHumanPawn(Instigator).AlphaAmount = 255;

                     if (PlayerController(Instigator.Controller) != none)
                     {
                        PlayerController(Instigator.Controller).ClientMessage(SuccessfulHealMessage$FP.GetPlayerName(), 'CriticalEvent');
                     }
                  }
               }
            }
         }

         FP = none;
      }

      BulletID += 10;

      if (BulletID >= 240)
         BulletID = Default.BulletID;

      if (PlayersHealed >= MaxNumberOfPlayers)
      {
         if (PRI != none)
         {
            KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements).HealedTeamWithMedicGrenade();
         }
      }
   }

   bHurtEntry = false;
}

// Siren override
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// Touch only if we haven't exploded yet and don't hit broken zeds heads
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   local FalkMonsterBase P;
   local float fHeadMulti;
   local vector X;

   X = Vector(Rotation);

   // hopefully shatter glasses more reliably
   if (KFPawn(Other) == none && KFPawn(Other.Base) == none && KFDoorMover(Other) == none && KFDoorMover(Other.Base) == none)
      Other.TakeDamage(100, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');

   if (!bHasExploded)
   {
      P = FalkMonsterBase(Other);

      if (P != none)
      {
         fHeadMulti = P.FalkGetNadeHeadScale();

         if (fHeadMulti <= 0 || !P.IsHeadShot(HitLocation, X, fHeadMulti))
         {
            Super.ProcessTouch(Other, HitLocation);

            if (!FTimerSet)
            {
               FTimerSet = True;
               SetTimer(ExplodeTimer, false);
            }
         }
      }
   }
}

// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;
   local PlayerController PC;

   // hopefully shatter glasses more reliably
   if (KFPawn(Wall) == none && KFPawn(Wall.Base) == none && KFDoorMover(Wall) == none && KFDoorMover(Wall.Base) == none)
      Wall.TakeDamage(100, Instigator, Location, HitNormal, class'DamTypeFragImpactFalk');

   if (!FTimerSet)
   {
      FTimerSet = True;
      SetTimer(ExplodeTimer, false);
   }

   // Reflect off Wall w/damping
   VNorm = (Velocity dot HitNormal) * HitNormal;
   Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

   RandSpin(50000);
   DesiredRotation.Roll = 0;
   RotationRate.Roll = 0;
   Speed = VSize(Velocity);

   if (Speed < 20)
   {
      bBounce = False;
      PrePivot.Z = -1.5;
      SetPhysics(PHYS_None);
      DesiredRotation = Rotation;
      DesiredRotation.Roll = 0;
      DesiredRotation.Pitch = 0;
      SetRotation(DesiredRotation);

      if (Trail != None)
         Trail.mRegen = false; // stop the emitter from regenerating
   }

   else
   {
      if ((Level.NetMode != NM_DedicatedServer) && (Speed > 50))
         PlaySound(ImpactSound, SLOT_Misc);

      else
      {
         bFixedRotationDir = false;
         bRotateToDesired = true;
         DesiredRotation.Pitch = 0;
         RotationRate.Pitch = 50000;
      }

      if (!Level.bDropDetail && (Level.DetailMode != DM_Low) && (Level.TimeSeconds - LastSparkTime > 0.5) &&
            EffectIsRelevant(Location,false))
      {
         PC = Level.GetLocalPlayerController();

         if ((PC.ViewTarget != None) && VSize(PC.ViewTarget.Location - Location) < 6000)
            Spawn(HitEffectClass,,, Location, Rotator(HitNormal));

         LastSparkTime = Level.TimeSeconds;
      }
   }
}

// spawn my medic fart on explode
simulated function Explode(vector HitLocation, vector HitNormal)
{
   bHasExploded = True;
   BlowUp(HitLocation);

   PlaySound(ExplosionSound,,TransientSoundVolume);

   if (Role == ROLE_Authority)
   {
      bNeedToPlayEffects = true;
      AmbientSound=Sound'Inf_WeaponsTwo.smoke_loop';
   }

   if (EffectIsRelevant(Location,false))
      Spawn(Class'FNadeHealing',,, HitLocation, rotator(vect(0,0,1)));
}

defaultproperties
{
   Speed=850.0
      HealBoostAmount=10
      MaxHeals=7          // it's actually eight ticks like this
      HealInterval=1.000000
      ExplosionSound=SoundGroup'KF_GrenadeSnd.NadeBase.MedicNade_Explode'
      SuccessfulHealMessage="You healed "
      MaxNumberOfPlayers=12
      Damage=50.000000
      DamageRadius=175.000000
      MyDamageType=Class'DamTypeMedicNadeFalk'
      ExplosionDecal=None
      StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.FieldMedicGrenade'
      DrawScale=1.000000
      SoundVolume=150
      SoundRadius=100.000000
      TransientSoundVolume=2.000000
      TransientSoundRadius=200.000000
      ExplodeTimer=1.500000
      LifeSpan=9.0
}
