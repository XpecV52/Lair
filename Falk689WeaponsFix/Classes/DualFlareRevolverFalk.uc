class DualFlareRevolverFalk extends KFMod.DualFlareRevolver;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

function bool HandlePickupQuery( pickup Item )
{
   if ( Item.InventoryType==Class'FlareRevolverFalk' )
   {
      if( LastHasGunMsgTime < Level.TimeSeconds && PlayerController(Instigator.Controller) != none )
      {
         LastHasGunMsgTime = Level.TimeSeconds + 0.5;
         PlayerController(Instigator.Controller).ReceiveLocalizedMessage(Class'KFMainMessages', 1);
      }

      return True;
   }

   return Super.HandlePickupQuery(Item);
}

function GiveTo(pawn Other, optional Pickup Pickup)
{
	local Inventory I;
	local int OldAmmo;
	local bool bNoPickup;
   local FHumanPawn FHP;

	MagAmmoRemaining = 0;

	for (I=Other.Inventory; I!=None; I=I.Inventory)
	{
		if (FlareRevolverFalk(I) != none)
		{
			if (WeaponPickup(Pickup)!= none)
			{
				WeaponPickup(Pickup).AmmoAmount[0] += Weapon(I).AmmoAmount(0);
			}

			else
			{
				OldAmmo = Weapon(I).AmmoAmount(0);
				bNoPickup = true;
			}

			MagAmmoRemaining = FlareRevolverFalk(I).MagAmmoRemaining;

			I.Destroyed();
			I.Destroy();

			break;
		}
	}

   FHP = FHumanPawn(Other);

   if (FHP != none && !Pickup.bDropped)
   {
      //warn("READING FROM PAWN");

      if (FHP.fSingleMagOnPickup > 0)
      {
         MagAmmoRemaining       = FHP.fSingleMagOnPickup;
         //warn("RESTORING MAG:"@FHP.fSingleMagOnPickup);
         FHP.fSingleMagOnPickup = 0;
      }

   }

	if (KFWeaponPickup(Pickup) != None && Pickup.bDropped)
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + KFWeaponPickup(Pickup).MagAmmoRemaining, 0, MagCapacity);
	}

	else
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + Class'FlareRevolverFalk'.Default.MagCapacity, 0, MagCapacity);
	}

	Super(Weapon).GiveTo(Other, Pickup);

	if (bNoPickup)
	{
      Ammo[0].AmmoAmount /= 2;
		AddAmmo(OldAmmo, 0);
		Clamp(Ammo[0].AmmoAmount, 0, MaxAmmo(0));
	}
}

// attempt to fix shit happening with ammo when taking a single pickup from the ground while having one in the inventory
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local FHumanPawn FHP;
   local KFPlayerReplicationInfo KFPRI;

   FHP = FHumanPawn(Instigator);

   if (FHP != none)
      KFPRI = KFPlayerReplicationInfo(FHP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if (WP != none && WP.bThrown == true)
         {
            //warn("PICKUP AMMO "@m@": "@WP.AmmoAmount[m]);
            InitialAmount = WP.AmmoAmount[m];
         }

         else
         {
            // Other change - if not thrown, give the gun a full clip
            MagAmmoRemaining = MagCapacity;
         }

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }

         else
            addAmount = InitialAmount;
         

         //warn("addAmount:"@addAmount);

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         // and here is the modification for instanced ammo actors
         if (WP != none && WP.bThrown==true)
         {
            addAmount += WP.AmmoAmount[m];
            //warn("Pickup AddAmount:"@addAmount);
         }

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.

            else
            {
               addAmount = Ammo[m].InitialAmount * (float(MagCapacity) / float(default.MagCapacity));
               FHP.fSingleAmmoOnPickup = 0;
            }

            if (FHP != none && FHP.fSingleAmmoOnPickup > 0)
            {
               addAmount               = FHP.fSingleAmmoOnPickup;
               //warn("RESTORING AMMO:"@FHP.fSingleAmmoOnPickup);
               FHP.fSingleAmmoOnPickup = 0;
            }
         }

         // Don't double add ammo if primary and secondary fire modes share the same ammo class
         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         // AddAmmo caps at MaxAmmo, but veterancy might allow for more than max,
         // so take that into account
         if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// I don't even wanna know why this is here but let's keep it since it's unlikely to fuck up stuff - Falk689
simulated function bool PutDown()
{
   if (Instigator.PendingWeapon.class == class'FlareRevolverFalk')
      bIsReloading = false;

   return super.PutDown();
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

/*function DropFrom(vector StartLocation)
{
   local int m;
   local Pickup Pickup;
   local Inventory I;
   local int AmmoThrown, OtherAmmo;

   if( !bCanThrow )
      return;

   AmmoThrown = AmmoAmount(0);
   ClientWeaponThrown();

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

   if ( Instigator != None )
      DetachFromPawn(Instigator);

   if( Instigator.Health > 0 )
   {
      OtherAmmo = AmmoThrown / 2;
      AmmoThrown -= OtherAmmo;
      I = Spawn(Class'FlareRevolverFalk');
      I.GiveTo(Instigator);
      Weapon(I).Ammo[0].AmmoAmount = OtherAmmo;
      FlareRevolverFalk(I).MagAmmoRemaining = MagAmmoRemaining / 2;
      MagAmmoRemaining = Max(MagAmmoRemaining-FlareRevolverFalk(I).MagAmmoRemaining,0);
   }

   Pickup = Spawn(Class'FlareRevolverPickupFalk',,, StartLocation);

   if ( Pickup != None )
   {
      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);
      WeaponPickup(Pickup).AmmoAmount[0] = AmmoThrown;
      if( KFWeaponPickup(Pickup)!=None )
         KFWeaponPickup(Pickup).MagAmmoRemaining = MagAmmoRemaining;
      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;
   }

    Destroyed();
   Destroy();
}*/

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local Inventory            I;
   local vector               Direction;
   local int      AmmoThrown, OtherAmmo;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(Class'FlareRevolverPickupFalk',,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }


         Pickup = Spawn(Class'FlareRevolverPickupFalk',,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      AmmoThrown = AmmoAmount(0);

      if( Instigator.Health > 0 )
      {
         OtherAmmo = AmmoThrown / 2;
         AmmoThrown -= OtherAmmo;
         I = Spawn(Class'FlareRevolverFalk');
         I.GiveTo(Instigator);
         Weapon(I).Ammo[0].AmmoAmount = OtherAmmo;
         FlareRevolverFalk(I).MagAmmoRemaining = MagAmmoRemaining / 2;
         MagAmmoRemaining = Max(MagAmmoRemaining-FlareRevolverFalk(I).MagAmmoRemaining,0);
      }

      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      WeaponPickup(Pickup).AmmoAmount[0] = AmmoThrown;

      if( KFWeaponPickup(Pickup)!=None )
         KFWeaponPickup(Pickup).MagAmmoRemaining = MagAmmoRemaining;

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroyed();
      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}


defaultproperties
{
   PickupClass=Class'DualFlareRevolverPickupFalk'
   AmmoClass=Class'DualFlareRevolverAmmoFalk'
   FireModeClass[0]=Class'DualFlareRevolverFireFalk'
   AppID=0
   DemoReplacement=Class'FlareRevolverFalk'
   ItemName="Dual Flare Revolvers"
   Description="A pair of flare revolvers to increase your efficiency in terms of lethality. Don't forget about the expensive ammo."
   ReloadRate=4.90000
   Priority=70
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
