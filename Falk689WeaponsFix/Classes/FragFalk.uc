class FragFalk extends KFMod.Frag;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairAnimations_A.ukx

static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
}

defaultproperties
{
   Weight=0.000000
   ItemName=" "
   PickupClass=Class'FragPickupFalk'
   FireModeClass[0]=Class'FragFireFalk'
   Description="Grenades. They change depending on your perk."
   TraderInfoTexture=Texture'LairTextures_T.CustomReskins.GrenadesTrader'
   AttachmentClass=Class'FragAttachmentFalk'
   Mesh=SkeletalMesh'LairAnimations_A.FragMesh'
   Skins(0)=Texture'LairTextures_T.CustomReskins.RiotHands'
}
