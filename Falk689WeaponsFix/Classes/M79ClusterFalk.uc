class M79ClusterFalk extends SPGrenadeProjectile;

var   byte       BulletID;       // used to prevent multi hit on a single zed
var   byte       fBounces;       // how many bounces we have left before kaboom
var   bool       fShouldCollide; // Should we collide against zeds?
var   bool       fSiren;         // Were we killed by a siren?
var   bool       fSirenImmune;   // Is this projectile immune to sirens?
var   Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons

replication
{
   reliable if(Role == ROLE_Authority)
      fBounces, fShouldCollide, fCInstigator;
}

static function PreloadAssets()
{
}

// Only destroy on siren scream
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   if(!fSirenImmune && damageType == class'SirenScreamDamage')
   {
      fSiren = True;
      Disintegrate(HitLocation, vect(0,0,1));
   }
}

// Added fixes on bounce dynamics and stuff
simulated function PostBeginPlay()
{ 
   local rotator SmokeRotation;
   local vector Dir;

   super(Projectile).PostBeginPlay();

   BCInverse  = 1 / BallisticCoefficient;
   OrigLoc    = Location;
   Dir        = vector(Rotation);
   Velocity   = RandRange(Speed, MaxSpeed) * Dir;

   if (Level.NetMode != NM_DedicatedServer)
   {
      SmokeTrail = Spawn(class'SmokeTrailFalk',self);
      SmokeTrail.SetBase(self);
      SmokeRotation.Pitch = 32768;
      SmokeTrail.SetRelativeRotation(SmokeRotation);
   }

   if (!bKillMomentum)
   {
      Dir = vector(Rotation);
      Velocity = Speed * Dir;
      Velocity.Z += TossZ;
   }

   UpdateDefaultStaticMesh(default.StaticMesh);
   SetTimer(0.2, false);
}


// Destroy after siren scream, else do something useful
function Timer()
{
   if (fSiren)
      Destroy();

   else
   {
      fBounces = 0;
      fShouldCollide = True;
   }
}

// Overridden to bounce 
simulated function HitWall(vector HitNormal, actor Wall)
{
   if (fBounces > 0)
   {
      BounceHitWall(HitNormal, Wall);
      fBounces--;
   }

   else
      KaboomHitWall(HitNormal, Wall);

}

// Bounce on hitwall
simulated function BounceHitWall(vector HitNormal, actor Wall)
{
   local Vector VNorm;

   if( Instigator != none )
      OrigLoc = Instigator.Location;

   // Reflect off walls
   if (KFPawn(Wall) == None)
   {
      VNorm = (Velocity dot HitNormal) * HitNormal;
      Velocity = -VNorm * DampenFactor + (Velocity - VNorm) * DampenFactorParallel;

      Speed = VSize(Velocity);
   }
}

// Kaboom on hitwall
simulated function KaboomHitWall(vector HitNormal, actor Wall)
{
   if( Instigator != none )
      OrigLoc = Instigator.Location;

   super(Projectile).HitWall(HitNormal,Wall);
}


// Touch only if we haven't exploded yet and fix point blank stuff
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   // Don't let it hit this player, or blow up on another player
   if (!fShouldCollide || bHasExploded || Other == none || Other == Instigator || Other.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   // Don't allow hits on poeple on the same team
   if(KFHumanPawn(Other) != none && Instigator != none &&
      KFHumanPawn(Other).PlayerReplicationInfo.Team.TeamIndex == Instigator.PlayerReplicationInfo.Team.TeamIndex)
   {
      return;
   }

   // fixed shit happening with location and replication
   if(Instigator != none)
      OrigLoc = Instigator.Location;

   // no dud, just kaboom
   Explode(HitLocation,Normal(HitLocation-Other.Location));
}


// M79Grenade Explode()
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local Controller C;
   local PlayerController  LocalPlayer;
   local byte i;

   if (bHasExploded)
      return;

   bHasExploded = True;

   PlaySound(ExplosionSound,,2.0);

   if (Level.NetMode != NM_DedicatedServer && EffectIsRelevant(Location,false))
   {
      Spawn(ExplosionEmitterClass,,,HitLocation + HitNormal*20,rotator(HitNormal));
      Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));
   }

   BlowUp(HitLocation);
   Destroy();

   // Shake nearby players screens
   LocalPlayer = Level.GetLocalPlayerController();

   if ((LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < DamageRadius))
      LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

   // some weird shit with an infinite recursion in this happened, when in doubt... attempt to fix it?
   for ( C=Level.ControllerList; C!=None; C=C.NextController )
   {
      i++;

      if ( (PlayerController(C) != None) && (C != LocalPlayer)
            && (VSize(Location - PlayerController(C).ViewTarget.Location) < DamageRadius) )
         C.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

      if (i > 200)
      {
         warn("Return 1");
         return;
      }
   }
}

// pass BulletID to the zed
simulated function HurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
	local int NumKilled;
	local KFMonster KFMonsterVictim;
	local Pawn P;
	local KFPawn KFP;
	local array<Pawn> CheckedPawns;
	local int i;
	local bool bAlreadyChecked;
   local byte fX;
   local byte fM;


    if ( bHurtEntry )
        return;

    bHurtEntry = true;

    foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
    {
        fX++;

        if (fX > 200)
        {
           //warn("Return 2");
           return;
        }

        // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
        if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
		 && ExtendedZCollision(Victims)==None )
        {
            dirs = Victims.Location - HitLocation;
            dist = FMax(1,VSize(dirs));
            dirs = dirs/dist;
            damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
            Victims.SetDelayedDamageInstigatorController(fCInstigator);
            if ( Victims == LastTouched )
                LastTouched = None;

			P = Pawn(Victims);

			if( P != none )
			{
		        for (i = 0; i < CheckedPawns.Length; i++)
				{
               fM++;

               if (fM > 200)
               {
                  //warn("Break 3");
                  break; 
               }

		        	if (CheckedPawns[i] == P)
					{
						bAlreadyChecked = true;
						break;
					}
				}

				if( bAlreadyChecked )
				{
					bAlreadyChecked = false;
					P = none;
					continue;
				}

                KFMonsterVictim = KFMonster(Victims);

    			if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
    			{
                    KFMonsterVictim = none;
    			}

                KFP = KFPawn(Victims);

                if( KFMonsterVictim != none )
                {
                    damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
                }
                else if( KFP != none )
                {
				    damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
                }

				CheckedPawns[CheckedPawns.Length] = P;

				if ( damageScale <= 0)
				{
					P = none;
					continue;
				}
				else
				{
					//Victims = P;
					P = none;
				}
			}


            Victims.TakeDamage
            (
                damageScale * DamageAmount,
                Instigator,
                Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
                (damageScale * Momentum * dirs),
                DamageType,
                BulletID
            );
            if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
                Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

			if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
                NumKilled++;
            }
        }
    }
    if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
    {
        Victims = LastTouched;
        LastTouched = None;
        dirs = Victims.Location - HitLocation;
        dist = FMax(1,VSize(dirs));
        dirs = dirs/dist;
        damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
        Victims.SetDelayedDamageInstigatorController(fCInstigator);
        Victims.TakeDamage
        (
            damageScale * DamageAmount,
            Instigator,
            Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
            (damageScale * Momentum * dirs),
            DamageType,
            BulletID
        );
        if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
    }

    bHurtEntry = false;
}

defaultproperties
{
   fShouldCollide=False
   fBounces=1
   fSiren=False
   fSirenImmune=False
   ImpactDamage=24.000000
   Damage=84.000000
   Speed=320.000000
   MaxSpeed=320.000000
   MyDamageType=Class'DamTypeExplosiveBulletFalk'
   StaticMeshRef="kf_generic_sm.40mm_Warhead"
   StaticMesh=StaticMesh'kf_generic_sm.40mm_Warhead'
   ExplosionSound=Sound'KF_GrenadeSnd.Nade_Explode_1'
   DisintegrateSound=Sound'Inf_Weapons.faust_explode_distant02'
   bDynamicLight=False
   LightType=LT_None
   ExplosionDecal=class'ScorchMarkFalk'
   ExplosionEmitterClass=class'KaboomFalk'
}
