class AcidClusterFalk extends M79ClusterFalk;

var xEmitter Trail;
var Emitter  ExtraTrail;

// Acid trail 
simulated function PostBeginPlay()
{ 
   local vector Dir;

   super(Projectile).PostBeginPlay();

   BCInverse  = 1 / BallisticCoefficient;
   OrigLoc    = Location;
   Dir        = vector(Rotation);
   Velocity   = RandRange(Speed, MaxSpeed) * Dir;

   if (Level.NetMode != NM_DedicatedServer)
   {
      ExtraTrail = Spawn(class'BlowerThrowerSecondaryEmitter',self);
		Trail = Spawn(class'BlowerThrowerPrimaryEmitter',self);
   }

   if(!bKillMomentum)
   {
      Dir = vector(Rotation);
      Velocity = Speed * Dir;
      Velocity.Z += TossZ;
   }

   //UpdateDefaultStaticMesh(default.StaticMesh);
   SetTimer(0.2, false);
}

// Overridden to prevent god from blowing up our nade
function TakeDamage( int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// kill the extra trail
simulated function Destroyed()
{
	if (ExtraTrail != none)
	{
		ExtraTrail.Kill();
		ExtraTrail.SetPhysics(PHYS_None);
	}

   if (Trail != none)
	{
		Trail.mRegen = False;
		Trail.SetPhysics(PHYS_None);
	}

	Super.Destroyed();
}

// acid sounds
simulated function Explode(vector HitLocation, vector HitNormal)
{
   if (bHasExploded)
      return;

   bHasExploded = True;

   PlaySound(ExplosionSound, SLOT_Misc, 0.2);

   BlowUp(HitLocation);
   Destroy();
}

defaultproperties
{
   StaticMeshRef="None"
   StaticMesh=None
   Speed=300.000000
   MaxSpeed=350.000000
   ExplosionSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   ExplosionDecal=Class'KFMod.VomitDecal'
   MyDamageType=Class'DamTypeAcidClusterFalk'
   Damage=180.000000
   ImpactDamage=180.000000
   DamageRadius=90.000000
   MomentumTransfer=0.000000
   bDynamicLight=False
   LightType=LT_None;
}
