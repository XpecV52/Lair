class PDWPickupFalk extends PDW.PDWPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=1600
   Weight=4.000000
   InventoryType=Class'PDWAssaultRifleFalk'
   BuyClipSize=30
   AmmoCost=15
   ItemName="KAC Personal Defense Weapon"
   ItemShortName="PDW"
   PickupMessage="You got a KAC Personal Defense Weapon"
   PowerValue=42
   SpeedValue=40
   RangeValue=100
}