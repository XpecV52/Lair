class DualiesFireFalk extends KFMod.DualiesFire;

var byte  BID; // bullet ID to prevent multiple hits on a single zed

function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other;
   local array<int>	     HitPoints;
   local array<Actor>     IgnoreActors;
   local Actor            DamageActor;
   local Actor            OldDamageActor;
   local int              i;
   local int              bHP;
   local byte             LoopCount;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X + 
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   While (LoopCount <= 10)
   {
      //log("DoTrace Loop");
      LoopCount++;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if(Other == None)
         break;

      else if (Other == Instigator || Other.Base == Instigator)
      {
         //log("Instigator");
         IgnoreActors[IgnoreActors.Length] = Other;
         Other.SetCollision(false);
         Start = HitLocation;
         continue;
      }

      if(ExtendedZCollision(Other) != None && Other.Owner != None)
         DamageActor = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if(KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            //log("KFMonster");

            if (DamageActor != OldDamageActor) // don't hit this zed twice
            {
               OldDamageActor = DamageActor;
               bHP            = KFMonster(DamageActor).Health;

               //log("old hp: "@bHP);

               if (bHP > 0)
               {
                  DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

                  //log("new hp: "@KFMonster(DamageActor).Health);

                  if (KFMonster(DamageActor).Health != bHP) // this zed was hit, break the loop
                     break;

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = Other;
                     Other.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
               {
                  //log("current hp = 0");
                  break;
               }
            }

            else
               break;
         }

         else
         {
            Other.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);
         }

         Start = HitLocation;
      }

      else if (HitScanBlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         //log("HitScanBlockingVolume");
         break;
      }
   }

   // Turn the collision back on for any actors we turned it off
   if (IgnoreActors.Length > 0)
   {
      for (i=0; i<IgnoreActors.Length; i++)
      {
         if(IgnoreActors[i] != None)
            IgnoreActors[i].SetCollision(true);
      }
   }
}

simulated function InitEffects()
{
    // don't even spawn on server
    if ( (Level.NetMode == NM_DedicatedServer) || (AIController(Instigator.Controller) != None) )
        return;
    if ( (FlashEmitterClass != None) && ((FlashEmitter == None) || FlashEmitter.bDeleteMe) )
    {
        FlashEmitter = Weapon.Spawn(FlashEmitterClass);
        Weapon.AttachToBone(FlashEmitter, KFWeapon(Weapon).default.FlashBoneName);
    }
    if ( (FlashEmitterClass != None) && ((Flash2Emitter == None) || Flash2Emitter.bDeleteMe) )
    {
        Flash2Emitter = Weapon.Spawn(FlashEmitterClass);
        Weapon.AttachToBone(Flash2Emitter, NineMMPlusDual(Weapon).default.altFlashBoneName);
    }

    if ( (SmokeEmitterClass != None) && ((SmokeEmitter == None) || SmokeEmitter.bDeleteMe) )
    {
        SmokeEmitter = Weapon.Spawn(SmokeEmitterClass);
    }

    if ( (ShellEjectClass != None) && ((ShellEjectEmitter == None) || ShellEjectEmitter.bDeleteMe) )
    {
        ShellEjectEmitter = Weapon.Spawn(ShellEjectClass);
        Weapon.AttachToBone(ShellEjectEmitter, ShellEjectBoneName);
    }

    if ( (ShellEjectClass != None) && ((ShellEject2Emitter == None) || ShellEject2Emitter.bDeleteMe) )
    {
        ShellEject2Emitter = Weapon.Spawn(ShellEjectClass);
        Weapon.AttachToBone(ShellEject2Emitter, ShellEject2BoneName);
    }
}

simulated function DestroyEffects()
{
    super.DestroyEffects();

    if (ShellEject2Emitter != None)
        ShellEject2Emitter.Destroy();

    if (Flash2Emitter != None)
        Flash2Emitter.Destroy();
}

function DrawMuzzleFlash(Canvas Canvas)
{
    super.DrawMuzzleFlash(Canvas);

    if (ShellEject2Emitter != None )
    {
        Canvas.DrawActor( ShellEject2Emitter, false, false, Weapon.DisplayFOV );
    }
}

function FlashMuzzleFlash()
{
    if (Flash2Emitter == none || FlashEmitter == none)
        return;

    if( KFWeap.bAimingRifle )
    {
        if ((FireAimedAnim == 'FireLeft_Iron') || (FireAimedAnim == 'LFireLeft_Iron') || (FireAimedAnim == 'BFireLeft_Iron'))
        {
            Flash2Emitter.Trigger(Weapon, Instigator);
            if (ShellEjectEmitter != None)
            {
                ShellEjectEmitter.Trigger(Weapon, Instigator);
            }
        }
        else
        {
            FlashEmitter.Trigger(Weapon, Instigator);
            if (ShellEject2Emitter != None)
            {
                ShellEject2Emitter.Trigger(Weapon, Instigator);
            }
        }
	}
	else
	{
        if((FireAnim == 'FireLeft') || (FireAnim == 'LFireLeft') || (FireAnim == 'BFireLeft'))
        {
            Flash2Emitter.Trigger(Weapon, Instigator);
            if (ShellEjectEmitter != None)
            {
                ShellEjectEmitter.Trigger(Weapon, Instigator);
            }
        }
        else
        {
            FlashEmitter.Trigger(Weapon, Instigator);
            if (ShellEject2Emitter != None)
            {
                ShellEject2Emitter.Trigger(Weapon, Instigator);
            }
        }
	}
}

// fixed for dualiesfalk
event ModeDoFire()
{
	local name bn;

	bn = NineMMPlusDual(Weapon).altFlashBoneName;
	NineMMPlusDual(Weapon).altFlashBoneName = NineMMPlusDual(Weapon).FlashBoneName;
	NineMMPlusDual(Weapon).FlashBoneName = bn;

	if (NineMMPlusDual(Weapon).MagAmmoRemaining >= 3)
	{
		if ((FireAnim == 'BFireRight') || (FireAimedAnim == 'BFireRight_Iron'))
		{
			FireAnim = 'FireRight';
			FireAimedAnim = 'FireRight_Iron';
		}
		else if ((FireAnim == 'BFireLeft') || (FireAimedAnim == 'BFireLeft_Iron'))
		{
			FireAnim = 'FireLeft';
			FireAimedAnim = 'FireLeft_Iron';
		}
	}
	
	Super(KFFire).ModeDoFire();
	
	if (NineMMPlusDual(Weapon).MagAmmoRemaining == 2)
	{
		if ((FireAnim == 'FireRight') || (FireAimedAnim == 'FireRight_Iron'))
		{
			FireAnim = 'LFireLeft';
			FireAimedAnim = 'LFireLeft_Iron';
		}
		//else if ((FireAnim == 'FireLeft') || (FireAimedAnim == 'FireLeft_Iron'))
		else
		{
			FireAnim = 'RFireRight';
			FireAimedAnim = 'RFireRight_Iron';
		}
	}
	else if (NineMMPlusDual(Weapon).MagAmmoRemaining == 1)
	{
		if ((FireAnim == 'RFireRight') || (FireAimedAnim == 'RFireRight_Iron'))
		{
			FireAnim = 'BFireLeft';
			FireAimedAnim = 'BFireLeft_Iron';
		}
		//else if ((FireAnim == 'LFireLeft') || (FireAimedAnim == 'LFireLeft_Iron'))
		else
		{
			FireAnim = 'BFireRight';
			FireAimedAnim = 'BFireRight_Iron';
		}
	}
	else
	{
		if ((FireAnim == 'FireRight') || (FireAimedAnim == 'FireRight_Iron') || (FireAnim == 'RFireRight') || (FireAimedAnim == 'RFireRight_Iron'))
		{
			FireAnim = 'FireLeft';
			FireAimedAnim = 'FireLeft_Iron';
		}
		else
		{
			FireAnim = 'FireRight';
			FireAimedAnim = 'FireRight_Iron';
		}
	}

	InitEffects();
}

defaultproperties
{
   DamageType=Class'DamTypeDualiesFalk'
   AmmoClass=Class'DualiesAmmoFalk'
   DamageMin=35
   DamageMax=35
   FireRate=0.10000
   RecoilRate=0.07000
}
