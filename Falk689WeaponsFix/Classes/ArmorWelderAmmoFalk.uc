class ArmorWelderAmmoFalk extends KFMod.WelderAmmo;

// attempt to remove some error log
simulated function CheckOutOfAmmo()
{
   if (AmmoAmount <= 0 && Owner != none && Pawn(Owner) != none && Pawn(Owner).Weapon != none)
      Pawn(Owner).Weapon.OutOfAmmo();
}

defaultproperties
{
}
