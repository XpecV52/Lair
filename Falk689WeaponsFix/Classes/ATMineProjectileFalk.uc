class ATMineProjectileFalk extends PipeBombProjectileFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx

// Override to prevent pipebomb light
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   // Setting sleep timer based on difficulty
   if (Level.Game.GameDifficulty < 3.0) 
      fUsedSleepTime = fSleepTimeNormal;

   else if (Level.Game.GameDifficulty < 5.0)
      fUsedSleepTime = fSleepTimeHard;

   else if (Level.Game.GameDifficulty < 6.0)
      fUsedSleepTime = fSleepTimeSuicidal;

   else if (Level.Game.GameDifficulty < 8.0)
      fUsedSleepTime = fSleepTimeHOE;

   else
      fUsedSleepTime = fSleepTimeBB;

   if (Role == ROLE_Authority)
   {
      Velocity = Speed * Vector(Rotation);
      //RandSpin(25000);
      bCanHitOwner = false;
      if (Instigator.HeadVolume.bWaterVolume)
      {
         bHitWater = true;
         Velocity = 0.6*Velocity;
      }
   }

   if (Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
         {
            fShouldKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI);
            fSirenImmune  = Vet.Static.KaboomSirenImmunity(KFPRI);
         }
      }
   }
}


// removed the beep
function Timer()
{
   local Pawn CheckPawn;
   local float ThreatLevel;

   if (!bHidden && !bTriggered)
   {
      if (ArmingCountDown >= 0)
      {
         ArmingCountDown -= 0.1;

         if (ArmingCountDown <= 0)
            SetTimer(1.0,True);
      }

      else
      {
         if (bEnemyDetected)
         {
            bAlwaysRelevant = true;
            Countdown--;

            if (CountDown <= 0)
            {
               Explode(Location, vector(Rotation));
               return;
            }
         }

         else
         {
            bAlwaysRelevant = false;
            CountDown       = Default.CountDown;
         }

         foreach VisibleCollidingActors(class'Pawn', CheckPawn, DetectionRadius, Location)
         {
            // just check if it's a zed and its role
            if (CheckPawn.Role == ROLE_Authority && KFMonster(CheckPawn) != none && KFMonster(CheckPawn).Health > 0)
            {
               ThreatLevel += KFMonster(CheckPawn).MotionDetectorThreat;

               // kinda pointless to keep checking if we're already going to go kaboom
               if (ThreatLevel >= ThreatThreshhold)
               {
                  bEnemyDetected = true;
                  SetTimer(0.1,True);
                  return;
               }
            }
         }

         // faster check if something is in range
         if (ThreatLevel > 0)
         {
            bEnemyDetected = false;
            SetTimer(0.3,True);
         }
         
         // standard check
         else
         {
            bEnemyDetected = false;
            SetTimer(fUsedSleepTime,True);
         }
      }
   }

   else
   {
      fShouldCheck = False;
      Destroy();
   }
}


defaultproperties
{
   MyDamageType=Class'KFMod.DamTypePipeBomb'
   ThreatThreshhold=1.00000          // two times a pipe bomb
   fCClass=class'ATMineClusterFalk'
   sFClass=class'ATMineSoundFix'
   ExplodeSoundRefs(0)="Inf_Weapons.antitankmine_explode01"
   Damage=1500
   DamageRadius=450.000000
   DetectionRadius=150.000000
   CountDown=4
   StaticMeshRef="LairStaticMeshes_SM.CustomReskins.ATMine"
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.ATMine'
   DrawScale=0.5
   fSleepTimeNormal=1.0
   fSleepTimeHard=0.9
   fSleepTimeSuicidal=0.8
   fSleepTimeHOE=0.7
   fUsedSleepTime=1.0
}
