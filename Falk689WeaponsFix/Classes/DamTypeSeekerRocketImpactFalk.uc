class DamTypeSeekerRocketImpactFalk extends KFMod.DamTypeSeekerRocketImpact;

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'SeekerSixRocketLauncherFalk'
   DeathString="%k killed %o (Seeker Six rocket impact)"
   bCheckForHeadShots=True
}
