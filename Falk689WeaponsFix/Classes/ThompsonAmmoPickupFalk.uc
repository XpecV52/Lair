class ThompsonAmmoPickupFalk extends KFMod.ThompsonAmmoPickup;

defaultproperties
{
    AmmoAmount=30
    InventoryType=Class'ThompsonAmmoFalk'
    PickupMessage="You found some .45 ACP rounds"
}