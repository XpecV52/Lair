class HuntingRiflePickupFalk extends BDHuntingRifleFinal.Hunting_RiflePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=8.000000
   cost=2300
   InventoryType=Class'HuntingRifleFalk'
   ItemName="Nosler M48 Hunting Rifle"
   ItemShortName="M48"
   BuyClipSize=5
   AmmoCost=10
   PickupMessage="You got a Nosler M48 Hunting Rifle"
   PowerValue=63
   SpeedValue=1
   RangeValue=100
}
