class DualDeagleFalk extends KFMod.DualDeagle;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

function bool HandlePickupQuery( pickup Item )
{
   if (Item.InventoryType==Class'DeagleFalk')
   {
      if(LastHasGunMsgTime < Level.TimeSeconds && PlayerController(Instigator.Controller) != none)
      {
         LastHasGunMsgTime = Level.TimeSeconds + 0.5;
         PlayerController(Instigator.Controller).ReceiveLocalizedMessage(Class'KFMainMessages', 1);
      }

      return True;
   }

   return Super.HandlePickupQuery(Item);
}

function GiveTo(pawn Other, optional Pickup Pickup)
{
	local Inventory I;
	local int OldAmmo;
	local bool bNoPickup;

	MagAmmoRemaining = 0;

	for (I=Other.Inventory; I!=None; I=I.Inventory)
	{
		if (DeagleFalk(I) != none)
		{
			if (WeaponPickup(Pickup)!= none)
			{
				WeaponPickup(Pickup).AmmoAmount[0] += Weapon(I).AmmoAmount(0);
			}

			else
			{
				OldAmmo = Weapon(I).AmmoAmount(0);
				bNoPickup = true;
			}

			MagAmmoRemaining = DeagleFalk(I).MagAmmoRemaining;

			I.Destroyed();
			I.Destroy();

			break;
		}
	}

	if (KFWeaponPickup(Pickup) != None && Pickup.bDropped)
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + KFWeaponPickup(Pickup).MagAmmoRemaining, 0, MagCapacity);
	}

	else
	{
		MagAmmoRemaining = Clamp(MagAmmoRemaining + Class'DeagleFalk'.Default.MagCapacity, 0, MagCapacity);
	}

	Super(Weapon).GiveTo(Other, Pickup);

	if (bNoPickup)
	{
      Ammo[0].AmmoAmount /= 2;
		AddAmmo(OldAmmo, 0);
		Clamp(Ammo[0].AmmoAmount, 0, MaxAmmo(0));
	}
}

simulated function bool PutDown()
{
   if ( Instigator.PendingWeapon.class == class'DeagleFalk' )
   {
      bIsReloading = false;
   }

   return super.PutDown();
}

simulated function BringUp(optional Weapon PrevWeapon)
{
   local KFPlayerController Player;

   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super.BringUp(PrevWeapon);

   Player = KFPlayerController(Instigator.Controller);

   if (Player != none && ClientGrenadeState != GN_BringUp)
   {
      Player.WeaponPulloutRemark(22); // DUAL DEAGLES
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

/*function DropFrom(vector StartLocation)
{
   local int m;
   local Pickup Pickup;
   local Inventory I;
   local int AmmoThrown, OtherAmmo;

   if( !bCanThrow )
      return;


   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

   if ( Instigator != None )
      DetachFromPawn(Instigator);

   if( Instigator.Health > 0 )
   {
      OtherAmmo = AmmoThrown / 2;
      AmmoThrown -= OtherAmmo;
      I = Spawn(Class'DeagleFalk');
      I.GiveTo(Instigator);
      Weapon(I).Ammo[0].AmmoAmount = OtherAmmo;
      DeagleFalk(I).MagAmmoRemaining = MagAmmoRemaining / 2;
      MagAmmoRemaining = Max(MagAmmoRemaining-DeagleFalk(I).MagAmmoRemaining,0);
   }

   Pickup = Spawn(Class'DeaglePickupFalk',,, StartLocation);

   if ( Pickup != None )
   {
      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);
      WeaponPickup(Pickup).AmmoAmount[0] = AmmoThrown;
      if( KFWeaponPickup(Pickup)!=None )
         KFWeaponPickup(Pickup).MagAmmoRemaining = MagAmmoRemaining;
      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;
   }

    Destroyed();
   Destroy();
}*/

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local Inventory            I;
   local vector               Direction;
   local int      AmmoThrown, OtherAmmo;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(Class'DeaglePickupFalk',,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(Class'DeaglePickupFalk',,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      AmmoThrown = AmmoAmount(0);

      if( Instigator.Health > 0 )
      {
         OtherAmmo = AmmoThrown / 2;
         AmmoThrown -= OtherAmmo;
         I = Spawn(Class'DeagleFalk');
         I.GiveTo(Instigator);
         Weapon(I).Ammo[0].AmmoAmount = OtherAmmo;
         DeagleFalk(I).MagAmmoRemaining = MagAmmoRemaining / 2;
         MagAmmoRemaining = Max(MagAmmoRemaining-DeagleFalk(I).MagAmmoRemaining,0);
      }

      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      WeaponPickup(Pickup).AmmoAmount[0] = AmmoThrown;

      if( KFWeaponPickup(Pickup)!=None )
         KFWeaponPickup(Pickup).MagAmmoRemaining = MagAmmoRemaining;

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroyed();
      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   MagCapacity=14
   ReloadRate=3.500000
   Priority=60
   FireModeClass(0)=Class'DualDeagleFireFalk'
   Description="Dual .50 caliber fun time. Higher stopping power than an MK23, but slower fire rate and with less ammo efficiency."
   PickupClass=Class'DualDeaglePickupFalk'
   ItemName="Dual IMI Desert Eagle MK XIXs"
   DemoReplacement=Class'DeagleFalk'
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
