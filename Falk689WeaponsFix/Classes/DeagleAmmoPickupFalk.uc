class DeagleAmmoPickupFalk extends KFMod.DeagleAmmoPickup;

defaultproperties
{
   AmmoAmount=14
   InventoryType=Class'DeagleAmmoFalk'
   PickupMessage="You found some .50 AE rounds"
}