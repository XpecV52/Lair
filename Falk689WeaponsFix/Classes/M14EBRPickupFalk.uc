class M14EBRPickupFalk extends KFMod.M14EBRPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'M14EBRBattleRifleFalk'
   ItemName="M14 Enhanced Battle Rifle"
   ItemShortName="M14 EBR"
   PickupMessage="You got an M14 Enhanced Battle Rifle"
   cost=3450
   BuyClipSize=20
   AmmoCost=10
   PowerValue=23
   SpeedValue=24
   RangeValue=100
}