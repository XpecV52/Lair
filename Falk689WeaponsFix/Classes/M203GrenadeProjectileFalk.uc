class M203GrenadeProjectileFalk extends M79GrenadeProjectileFalk;

defaultproperties
{
   ImpactDamageType=Class'DamTypeM203GrenadeFalk'
   Damage=420          // was 350 before april buff
   ImpactDamage=120    // was 100 before april buff
}