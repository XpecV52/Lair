class DamTypeThompsonFalk extends KFMod.DamTypeThompson;

defaultproperties
{
    WeaponClass=Class'ThompsonSMGFalk'
    DeathString="%k killed %o (M1A1)"
	HeadShotDamageMult=1.100000
}