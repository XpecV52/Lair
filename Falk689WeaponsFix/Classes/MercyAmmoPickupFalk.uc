class MercyAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
     KFPickupImage=Texture'KillingFloorHUD.ClassMenu.Deagle'
     AmmoAmount=10
     InventoryType=Class'Mercy.MercyAmmo'
     PickupMessage="Rounds (.300 JHP)"
     StaticMesh=None
}
