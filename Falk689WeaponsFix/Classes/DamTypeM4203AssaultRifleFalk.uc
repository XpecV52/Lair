class DamTypeM4203AssaultRifleFalk extends KFMod.DamTypeM4203AssaultRifle;

defaultproperties
{
    WeaponClass=Class'M4203AssaultRifleFalk'
    DeathString="%k killed %o (M4+M203)"
	HeadShotDamageMult=1.100000
}