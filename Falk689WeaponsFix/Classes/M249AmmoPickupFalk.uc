class M249AmmoPickupFalk extends M249Mut.M249AmmoPickup;

defaultproperties
{
   InventoryType=Class'M249AmmoFalk'
   AmmoAmount=25
   PickupMessage="You found 5.56mm NATO bullets"
}