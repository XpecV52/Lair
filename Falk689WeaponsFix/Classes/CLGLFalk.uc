class CLGLFalk extends KFWeaponShotgun;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

var 		byte		    SightUp;
var()		name	       ReloadLongAnim;
var()		float		    ReloadLongRate;

replication
{
	reliable if ( Role < ROLE_Authority)
		ServerToggleSight;
		
	reliable if ( Role == ROLE_Authority )
		ToggleSight, SightUp;
}

simulated function AltFire(float F)
{
	if (!FireMode[0].bIsFiring && !bIsReloading)
	{

		if(bAimingRifle)
		{
			Super.IronSightZoomOut();
			//Want the player to zoom out so the animation plays better
		}
		
		ToggleSight();
	}
}

//From Zedek the Plague Doctor
simulated function ToggleSight()
{
	// If we're in singleplayer
	if (Level.NetMode == NM_StandAlone || Level.NetMode == NM_ListenServer)
	{
		if (SightUp==1)
		{
			PlayAnim('SightDown', 1.0, 0.1);
			SightUp=0;
		}
		else
		{
			PlayAnim('SightUp', 1.0, 0.1);
			SightUp=1;
		}	
	}
	
	// Dedicated stuff
	else
	{
		// Tell the server to change things on its side
		if (Role < Role_Authority)
			ServerToggleSight();
			
		// Based on what the byte is BEFORE changing
		if (SightUp==1)
			PlayAnim('SightDown', 1.0, 0.1);
		else
			PlayAnim('SightUp', 1.0, 0.1);
	}
}

simulated function ServerToggleSight()
{
	if (SightUp==1)
		SightUp=0;
	else
		SightUp=1;
}
//End Zedek additions

simulated function WeaponTick(float dt)
{
	if(SightUp == 1)
   {
      IdleAimAnim= 'UPIdle_Iron';
      IdleAnim= 'UPIdle';
      PutDownAnim= 'UPPutDown';
      ReloadAnim= 'UPReload';
      ReloadLongAnim= 'UPReloadLong';
      SelectAnim= 'UPSelect';
   }

   else
   {
      IdleAimAnim= Default.IdleAimAnim;
      IdleAnim= Default.IdleAnim;
      PutDownAnim= Default.PutDownAnim;
      ReloadAnim= Default.ReloadAnim;
      ReloadLongAnim=Default.ReloadLongAnim;
      SelectAnim= Default.SelectAnim;
   }
	
	super.WeaponTick(dt);
}

exec function ReloadMeNow()
{
	local float ReloadMulti;

	if (!AllowReload())
		return;

	if (bHasAimingMode && bAimingRifle)
	{
		FireMode[1].bIsFiring = False;

		ZoomOut(false);
		if( Role < ROLE_Authority)
			ServerZoomOut(false);
	}

	if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
		ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);

	else
		ReloadMulti = 1.0;

	bIsReloading = true;
	ReloadTimer = Level.TimeSeconds;

	if (MagAmmoRemaining <= 0)
		ReloadRate = Default.ReloadLongRate / ReloadMulti;

	else if (MagAmmoRemaining >= 1)
		ReloadRate = Default.ReloadRate / ReloadMulti;

	if (bHoldToReload)
		NumLoadedThisReload = 0;

	ClientReload();
	Instigator.SetAnimAction(WeaponReloadAnim);

	if (Level.Game.NumPlayers > 1 && KFGameType(Level.Game).bWaveInProgress && KFPlayerController(Instigator.Controller) != none &&
		Level.TimeSeconds - KFPlayerController(Instigator.Controller).LastReloadMessageTime > KFPlayerController(Instigator.Controller).ReloadMessageDelay)
	{
		KFPlayerController(Instigator.Controller).Speech('AUTO', 2, "");
		KFPlayerController(Instigator.Controller).LastReloadMessageTime = Level.TimeSeconds;
	}
}

simulated function ClientReload()
{
	local float ReloadMulti;
	if ( bHasAimingMode && bAimingRifle )
	{
		FireMode[1].bIsFiring = False;

		ZoomOut(false);
		if( Role < ROLE_Authority)
			ServerZoomOut(false);
	}
	if ( KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none )
	{
		ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);
	}
	else
	{
		ReloadMulti = 1.0;
	}
	bIsReloading = true;
	if (MagAmmoRemaining <= 0)
	{
		PlayAnim(ReloadLongAnim, ReloadAnimRate*ReloadMulti, 0.1);
	}
	else if (MagAmmoRemaining >= 1)
	{
		PlayAnim(ReloadAnim, ReloadAnimRate*ReloadMulti, 0.1);
	}
}

function float GetAIRating()
{
	local AIController B;

	B = AIController(Instigator.Controller);
	if ( (B == None) || (B.Enemy == None) )
		return AIRating;

	return (AIRating + 0.0003 * FClamp(1500 - VSize(B.Enemy.Location - Instigator.Location),0,1000));
}

function byte BestMode()
{
	return 0;
}

function bool RecommendRangedAttack()
{
	return true;
}

function bool RecommendLongRangedAttack()
{
	return true;
}

function float SuggestAttackStyle()
{
	return -1.0;
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// permit throwing while reloading
simulated function bool CanThrow()
{
   if (bIsReloading)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   ReloadLongAnim="ReloadLong"
   ReloadAnim="Reload"
   WeaponReloadAnim="Reload_Shotgun"
   HudImage=Texture'CLGL_R.CLGL_HUD_UnSelected'
   SelectedHudImage=Texture'CLGL_R.CLGL_HUD_Selected'
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=65.000000
   bModeZeroCanDryFire=True
   SleeveNum=0
   TraderInfoTexture=Texture'CLGL_R.CLGL_HUD_Trader'
   bIsTier3Weapon=True
   MeshRef="CLGL_R.CLGLMesh1st"
   SkinRefs(1)="CLGL_R.CLGL_CMB"
   SelectSoundRef="CLGL_R.CLGLSelect"
   HudImageRef="CLGL_R.CLGL_HUD_UnSelected"
   SelectedHudImageRef="CLGL_R.CLGL_HUD_Selected"
   PlayerIronSightFOV=70.000000
   ZoomedDisplayFOV=40.000000
   FireModeClass(0)=Class'CLGLFireFalk'
   FireModeClass(1)=Class'KFMod.NoFire'
   PutDownAnim="PutDown"
   PutDownTime=0.360000
   SelectSound=Sound'CLGL_R.CLGLSelect'
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.650000
   CurrentRating=0.650000
   DisplayFOV=65.000000
   Priority=125
   GroupOffset=6
   PickupClass=Class'CLGLPickupFalk'
   PlayerViewOffset=(X=25.000000,Y=20.000000,Z=-6.000000)
   BobDamping=6.000000
   AttachmentClass=Class'CLGLAttachmentFalk'
   IconCoords=(X1=253,Y1=146,X2=333,Y2=181)
   LightType=LT_None
   LightBrightness=0.000000
   LightRadius=0.000000
   Mesh=SkeletalMesh'CLGL_R.CLGLMesh1st'
   Skins(1)=Combiner'CLGL_R.CLGL_CMB'
   ReloadLongRate=1.100000
   MagCapacity=4
   ReloadRate=0.850000
   ReloadAnimRate=1.000000
   Weight=6.000000
   Description="A pump-action grenade launcher loaded with four grenades. Two different ways to use the iron sight are available. Slightly lower damage output than an M79 grenade."
   InventoryGroup=3
   FBringUpSafety=0.1
   ItemName="China Lake Grenade Launcher"
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
