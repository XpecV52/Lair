class Walter2000SAPickupFalk extends Walter2000SA.Walter2000SAPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=1250
   Weight=6.000000
   BuyClipSize=10
   AmmoCost=10
   InventoryType=Class'Walter2000SAFalk'
   ItemName="Walther WA2000"
   ItemShortName="WA2000"
   PickupMessage="You got a Walther WA2000"
   PowerValue=29
   SpeedValue=33
   RangeValue=100
}