class M4A1IronBeastSAFireFalk extends KFMod.AA12Fire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<TrenchgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local TrenchgunBulletFalk FB;

   FB = TrenchgunBulletFalk(P);

   Super.PostSpawnProjectile(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;
}

defaultproperties
{
   RecoilRate=0.070000
   FireRate=0.109000
   AmmoClass=Class'M4A1IronBeastSAAmmoFalk'
   ProjectileClass=Class'M4A1IronBeastSABulletFalk'
   ProjPerFire=1
   FireAimedAnim="Fire_Iron"
   maxVerticalRecoilAngle=200
   maxHorizontalRecoilAngle=100
   bAttachSmokeEmitter=False
   ShellEjectClass=Class'ROEffects.KFShellEjectAK'
   ShellEjectBoneName="eject_bone"
   StereoFireSound=Sound'M4A1IronBeastSA_A.M4A1Iron_SND.M4A1Fire'
   bRandomPitchFireSound=False
   NoAmmoSoundRef="KF_AK47Snd.AK47_DryFire"
   bPawnRapidFireAnim=True
   TransientSoundVolume=1.800000
   FireLoopAnim="Fire"
   TweenTime=0.025000
   FireSound=Sound'M4A1IronBeastSA_A.M4A1Iron_SND.M4A1Fire'
   FireForce="AssaultRifleFire"
   AmmoPerFire=1
   ShakeRotMag=(X=50.000000,Y=50.000000,Z=350.000000)
   ShakeRotRate=(X=5000.000000,Y=5000.000000,Z=5000.000000)
   ShakeRotTime=0.750000
   ShakeOffsetMag=(X=6.000000,Y=3.000000,Z=7.500000)
   ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
   ShakeOffsetTime=1.250000
   BotRefireRate=0.990000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stSTG'
   aimerror=42.000000
   Spread=0.015000
   SpreadStyle=SS_Random
   FireSoundRef="KF_M4RifleSnd.M4Rifle_Fire_Single_M"
   StereoFireSoundRef="KF_M4RifleSnd.M4Rifle_Fire_Single_S"
}
