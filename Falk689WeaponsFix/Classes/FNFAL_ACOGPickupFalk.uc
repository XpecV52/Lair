class FNFAL_ACOGPickupFalk extends KFMod.FNFAL_ACOG_Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'FNFAL_ACOGAssaultRifleFalk'
   ItemName="FN Fusil Automatique Leger"
   ItemShortName="FAL"
   PickupMessage="You got an FN Fusil Automatique Leger"
   cost=3500
   BuyClipSize=20
   AmmoCost=10
   Weight=6.000000
   PowerValue=63
   SpeedValue=65
   RangeValue=100
}
