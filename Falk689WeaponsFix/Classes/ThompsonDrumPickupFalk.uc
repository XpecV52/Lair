class ThompsonDrumPickupFalk extends KFMod.ThompsonDrumPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'ThompsonDrumSMGFalk'
   ItemName="Thompson M1928A1 SMG"
   ItemShortName="M1928A1"
   cost=1350
   PickupMessage="You got a Thompson M1928A1 SMG"
   BuyClipSize=50
   AmmoCost=25
   PowerValue=40
   SpeedValue=65
   RangeValue=100
   Weight=5.000000
}