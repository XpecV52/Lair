class SVDPickupFalk extends SVDPickupBaseFalk;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=2600
   BuyClipSize=10
   AmmoCost=10
   InventoryType=Class'SVDFalk'
   ItemName="SVD Dragunov"
   ItemShortName="SVD"
   PickupMessage="You got an SVD Dragunov"
   PowerValue=36
   SpeedValue=33
   RangeValue=100
}
