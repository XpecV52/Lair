class BenelliBulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   MyDamageType=Class'DamTypeBenelliFalk'
   FDamage=35.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
}