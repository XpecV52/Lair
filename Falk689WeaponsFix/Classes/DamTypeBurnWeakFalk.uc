class DamTypeBurnWeakFalk extends KFMod.KFWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
   KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   bCheckForHeadShots=False
   DeathString="%k incinerated %o (Flames)."
   FemaleSuicide="%o roasted herself alive."
   MaleSuicide="%o roasted himself alive."
}

