class ZEDMKIIAltFireFalk extends ZEDMKIIAltFire;

defaultproperties
{
   ProjectileClass=Class'ZEDMKIISecondaryProjectileFalk'
   FireRate=0.72
   AmmoClass=Class'ZEDMKIIAmmoFalk'
   AmmoPerFire=10
}
