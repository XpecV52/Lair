class BullpupAmmoFalk extends KFMod.BullpupAmmo;

defaultproperties
{
    MaxAmmo=400
    InitialAmount=180
    AmmoPickupAmount=30
    PickupClass=Class'BullpupAmmoPickupFalk'
    ItemName="5.56x45mm NATO rounds"
}