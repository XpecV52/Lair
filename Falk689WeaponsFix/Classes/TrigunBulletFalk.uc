class TrigunBulletFalk extends AA12ExplosiveBulletFalk;

defaultproperties
{
   ImpactDamage=25.000000
   Damage=75.000000
   Speed=2500.000000
   MaxSpeed=2500.000000
   ArmDistSquared=70000.000000
   SoundVolume=127
   TransientSoundVolume=1.000000
   fSoundVolume=1.0
}
