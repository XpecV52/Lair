class PDWAmmoPickupFalk extends PDW.PDWAmmoPickup;

defaultproperties
{
   InventoryType=Class'PDWAmmoFalk'
   AmmoAmount=30
   PickupMessage="You found some 6x35mm rounds"
}