class M79PickupFalk extends KFMod.M79Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'M79GrenadeLauncherFalk'
   ItemName="M79 Grenade Launcher"
   PickupMessage="You got an M79 Grenade Launcher"
   ItemShortName="M79"
   BuyClipSize=1
   AmmoCost=5
   PowerValue=60
   SpeedValue=10
   RangeValue=100
   Weight=4.000000
   DrawScale=1.15
   cost=1350
}
