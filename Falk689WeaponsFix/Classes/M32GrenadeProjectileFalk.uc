class M32GrenadeProjectileFalk extends M79GrenadeProjectileFalk;

defaultproperties
{
   fCClass=class'M79ClusterFalk'
   Damage=420        // was 350 before april buff
   ImpactDamage=120  // was 100 before april buff
}