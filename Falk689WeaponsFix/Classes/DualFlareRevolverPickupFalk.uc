class DualFlareRevolverPickupFalk extends KFMod.DualFlareRevolverPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'DualFlareRevolverFalk'
   cost=1300
   ItemName="Dual Flare Revolvers"
   PickupMessage="You found another Flare Revolver"
   ItemShortName="Flare Revolvers"
   BuyClipSize=12
   AmmoCost=24
   PowerValue=50
   SpeedValue=40
   RangeValue=100
}
