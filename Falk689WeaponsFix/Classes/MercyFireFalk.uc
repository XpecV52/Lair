class MercyFireFalk extends AA12FireFalk;

var() int   BurstLength;   //How many bullets to fire in one burst
var() float BurstRate;     //Time between shots (should be smaller than FireRate).
var   bool  bBursting;
var   bool  fShouldBurst;
var   int   RoundsToFire;
var   float fSemiFireRate;

event ModeDoFire()
{
   if (fShouldBurst)
   {
      FireRate = 0.2;
      
      if (bBursting && RoundsToFire > 0)
         RoundsToFire--;

      //If not already firing, start a burst.
      if (!bBursting && AllowFire())
      {
         bBursting = true;
         RoundsToFire = BurstLength;
         SetTimer(BurstRate, true);
      }
      
      if (RoundsToFire < 1)
      {
         SetTimer(0, false);
         RoundsToFire = 0;
         bBursting = false;
         //PlayFireEnd();
         return;
      }
   }
   
   else
      FireRate = fSemiFireRate;
   
   if (bBursting)
   {
      if (KFWeapon(Weapon).MagAmmoRemaining > 0)
      {
         Super.ModeDoFire();
      
         if (RoundsToFire > 0)
            DoBurstAnimation();
      
         else
            EndBurstAnimation();
      }

      else
      {
         EndBurstAnimation();
         RoundsToFire = 0;
      }
   }
   
   else
      Super.ModeDoFire();	
}

function DoBurstAnimation()
{
   if (KFWeap.bAimingRifle)
      Weapon.PlayAnim(FireAimedAnim, FireAnimRate, TweenTime);
   
   else
      Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);
}

function EndBurstAnimation()
{
   Weapon.AnimStopLooping();
}

simulated function Timer()
{
   if (bBursting)
      ModeDoFire();
      
   else SetTimer(0, false);
}

defaultproperties
{
   AmmoPerFire=1
   ProjPerFire=1
   ProjectileClass=Class'MercyBulletFalk'
   BurstLength=4
   BurstRate=0.18000
   fSemiFireRate=0.22000
   FireRate=0.22000
   bRandomPitchFireSound=False
   StereoFireSound=Sound'KF_FNFALSnd.FNFAL_Fire_Single_S'
   bWaitForRelease=True
   bAttachSmokeEmitter=True
   TweenTime=0.000000
   AmmoClass=Class'MercyAmmoFalk'
   FireAimedAnim="Iron_Fire"
   RecoilRate=0.1
   maxVerticalRecoilAngle=400
   maxHorizontalRecoilAngle=50
   ShellEjectClass=Class'ROEffects.KFShellEjectHandCannon'
   ShellEjectBoneName="Shell_eject"
   TransientSoundVolume=1.800000
   TransientSoundRadius=500.000000
   //FireSound=Sound'Mercy_SND.sniper_shoot_crit'
   FireSound=SoundGroup'KF_HandcannonSnd.50AE_Fire'
   NoAmmoSound=Sound'KF_HandcannonSnd.50AE_DryFire'
   ShakeRotMag=(X=75.000000,Y=75.000000,Z=400.000000)
   ShakeRotRate=(X=12500.000000,Y=12500.000000,Z=10000.000000)
   ShakeRotTime=3.500000
   ShakeOffsetMag=(X=6.000000,Y=1.000000,Z=8.000000)
   ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
   ShakeOffsetTime=2.500000
   BotRefireRate=0.850000
   FlashEmitterClass=Class'ROEffects.MuzzleFlash1stKar'
   aimerror=50.000000
   Spread=0.004000
   SpreadStyle=SS_Random
   fShouldBurst=True
}
