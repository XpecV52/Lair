class SPSniperPickupFalk extends KFMod.SPSniperPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'SPSniperRifleFalk'
   ItemName="Steampunk Lee Enfield SMLE"
   ItemShortName="SP SMLE"
   PickupMessage="You got a Steampunk Lee Enfield SMLE"
   cost=2000
   BuyClipSize=10
   AmmoCost=20
   PowerValue=47
   SpeedValue=9
   RangeValue=100
}
