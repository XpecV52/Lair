class SpitfireTendrilFalk extends KFMod.FlameTendril;

var Actor      FLastDamaged;
var byte       BulletID;
var float      FDamage;
var int        FPenetrations;
var Controller fCInstigator;

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, FPenetrations, FLastDamaged, fCInstigator;
}

// not totally sure about this one
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   HurtWall = None;

   //log("HitWall");
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
      return;

   if (Wall.bStatic && Wall.bWorldGeometry)
   {
      Explode(Location + ExploWallOut * HitNormal, HitNormal);

      /*if (ImpactEffect != None && (Level.NetMode != NM_DedicatedServer))
         Spawn(ImpactEffect,,, Location, rotator(-HitNormal));*/

      if (Trail != None)
      {
         Trail.mRegen=False;
         Trail.SetPhysics(PHYS_None);
      }

      if (FlameTrail != none)
	   {
		   FlameTrail.Kill();
		   FlameTrail.SetPhysics(PHYS_None);
	   }

      Destroy();
   }
}

// attempt to fix point blank
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local int bHp;
   local Pawn HitPawn;

   if (Other == none || Other == Instigator || Other.Base == Instigator || Other == FLastDamaged || !Other.bBlockHitPointTraces || KFHumanPawn(Other) != none || KFHumanPawn(Other.Base) != none)
      return;

   FLastDamaged = Other;

   X = Vector(Rotation);

   HitPawn = Pawn(Other);

   if (HitPawn == none)
      HitPawn = Pawn(Other.Base);

   if (HitPawn != none)
   {
      bHP = HitPawn.Health;
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      HitPawn.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      // don't scale damage if we haven't damaged the zed
      if (bHP > 0 && HitPawn.Health < bHP)
      {
         FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

         // if we've struck through more than the max number of foes, destroy.
         if (FPenetrations >= MaxPenetrations)
            Destroy();

         FPenetrations++;
      }
   }

   // this may be needed since shotguns basically fire bugs
   else
   {
      Other.SetDelayedDamageInstigatorController(fCInstigator);
      Other.TakeDamage(FDamage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);

      FDamage *= PenDamageReduction; // Keep going, but lose effectiveness each time.

      // if we've struck through more than the max number of foes, destroy.
      if (FPenetrations >= MaxPenetrations)
         Destroy();

      FPenetrations++;
   }
}


// modified to pass BulletID to the zed
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
	local actor Victims;
	local float damageScale, dist;
	local vector dirs;
	local int NumKilled;
	local KFMonster KFMonsterVictim;
	local Pawn P;
	local KFPawn KFP;
	local array<Pawn> CheckedPawns;
	local int i;
	local bool bAlreadyChecked;

	if (bHurtEntry)
		return;

	bHurtEntry = true;

	foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
	{
		// don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
		if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
		 && ExtendedZCollision(Victims)==None )
		{
			dirs = Victims.Location - HitLocation;
			dist = FMax(1,VSize(dirs));
			dirs = dirs/dist;
			damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);
			if ( Victims == LastTouched )
				LastTouched = None;

			P = Pawn(Victims);

			if( P != none )
			{
		        for (i = 0; i < CheckedPawns.Length; i++)
				{
		        	if (CheckedPawns[i] == P)
					{
						bAlreadyChecked = true;
						break;
					}
				}

				if( bAlreadyChecked )
				{
					bAlreadyChecked = false;
					P = none;
					continue;
				}

				KFMonsterVictim = KFMonster(Victims);

				if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
				{
					KFMonsterVictim = none;
				}

				KFP = KFPawn(Victims);

				if( KFMonsterVictim != none )
				{
					damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
				}
				else if( KFP != none )
				{
				    damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
				}

				CheckedPawns[CheckedPawns.Length] = P;

				if ( damageScale <= 0)
				{
					P = none;
					continue;
				}
				else
				{
					//Victims = P;
					P = none;
				}
			}


			Victims.TakeDamage
			(
				damageScale * DamageAmount,
				Instigator,
				Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
				(damageScale * Momentum * dirs),
				DamageType,
            BulletID
			);

			if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
				Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);

			if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
			{
				NumKilled++;
			}
		}
	}
	if ( (LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
	{
		Victims = LastTouched;
		LastTouched = None;
		dirs = Victims.Location - HitLocation;
		dist = FMax(1,VSize(dirs));
		dirs = dirs/dist;
		damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
      Victims.SetDelayedDamageInstigatorController(fCInstigator);
		Victims.TakeDamage
		(
			damageScale * DamageAmount,
			Instigator,
			Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs,
			(damageScale * Momentum * dirs),
			DamageType,
         BulletID
		);
		if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
			Vehicle(Victims).DriverRadiusDamage(DamageAmount, DamageRadius, InstigatorController, DamageType, Momentum, HitLocation);
	}

	bHurtEntry = false;
}

defaultproperties
{
   FDamage=14.000000
   MaxPenetrations=3
   PenDamageReduction=0.500000
   MyDamageType=Class'DamTypeSpitfireFalk'
   Speed=2400.000000           // this is what actually influences the real range of the gun
   MaxSpeed=2400.000000        // this is what actually influences the real range of the gun
   HeadShotDamageMult=1.000000 // doesn't check headshots anyway
   ExplosionDecal=Class'BurnMarkLargeFalk'
}
