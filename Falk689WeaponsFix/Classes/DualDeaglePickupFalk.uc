class DualDeaglePickupFalk extends KFMod.DualDeaglePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=4.000000
	cost=700
	BuyClipSize=14
	PowerValue=21
    SpeedValue=47
    RangeValue=100
	AmmoCost=14
	ItemName="Dual IMI Desert Eagles MK XIX"
	ItemShortName="Deagles"
	AmmoItemName=".50 AE rounds"
	InventoryType=Class'DualDeagleFalk'
	PickupMessage="You found another IMI Desert Eagle MK XIX"
}
