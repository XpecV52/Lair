class SpasAmmoPickupFalk extends KFMod.ShotgunAmmoPickup;

defaultproperties
{
    AmmoAmount=8
    InventoryType=Class'SpasAmmoFalk'
    PickupMessage="You found some 12-gauge shells"
}