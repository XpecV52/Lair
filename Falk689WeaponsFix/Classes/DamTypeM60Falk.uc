class DamTypeM60Falk extends DamTypeSW76Falk;

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'M60Falk'
   DeathString="%k killed %o (M60)"
   KDamageImpulse=6500.000000
   KDeathVel=400.000000
   KDeathUpKick=40.000000
}