class AAR525SPickupFalk extends AAR525SPickupBaseFalk;

#exec OBJ LOAD FILE=LairStaticMeshes_SM.usx

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.AlienFlameSpitterPickup'
   cost=3500
   Weight=6.000000
   AmmoCost=30
   BuyClipSize=60
   InventoryType=Class'AAR525SFalk'
   ItemName="Alien Flame Spitter 525"
   ItemShortName="AFS525"
   PickupMessage="You got an Alien Flame Spitter 525"
   PowerValue=13
   SpeedValue=80
   RangeValue=60
}