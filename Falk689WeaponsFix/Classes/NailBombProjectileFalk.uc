class NailBombProjectileFalk extends NailGunProjectileFalk;

// fix attempt for abnormal damage in specific conditions
var bool fShouldHit;  // should we hit zeds?
var float fHitAfter;  // how much time after explosion we start to hit zeds
var float fCurHit;    // how much time after explosion we start to hit zeds

replication
{
   reliable if(Role == ROLE_Authority)
      fShouldHit;
}

simulated function PostBeginPlay()
{
   super(Projectile).PostBeginPlay();

   Velocity = Speed * Vector(Rotation); // starts off slower so combo can be done closer

   SetTimer(0.4, false);

   if ( Level.NetMode != NM_DedicatedServer )
   {
      if ( !PhysicsVolume.bWaterVolume )
      {

         Trail = Spawn(class'NailGunTracer',self);
         Trail.Lifespan = Lifespan;
      }
   }

   UpdateDefaultStaticMesh(default.StaticMesh);
}

// start hitting zeds after a while 
simulated function Tick(float DeltaTime)
{
   Super.Tick(DeltaTime);

   if (Role == ROLE_Authority && !fShouldHit)
   {
      if (fCurHit < fHitAfter) 
         fCurHit += DeltaTime;

      else
         fShouldHit = True;
   }

}

// behave yourself and stahp breaking the game
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   if (fShouldHit)
      Super.ProcessTouch(Other, HitLocation);
}

// ...pretty please?
simulated function HitWall( vector HitNormal, actor Wall )
{
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator || Wall == FLastDamaged || !Wall.bBlockHitPointTraces)
      return;

   if (!Wall.bStatic && !Wall.bWorldGeometry &&
      ((Mover(Wall) == None) || Mover(Wall).bDamageTriggered) )
   {
      if (Level.NetMode != NM_Client && fShouldHit)
      {
         if (Instigator == None || Instigator.Controller == None)
            Wall.SetDelayedDamageInstigatorController(InstigatorController);

         Wall.TakeDamage(FDamage, instigator, Location, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
      }

      Destroy();
      return;
   }

   SetRotation(rotator(Normal(Velocity)));

   SetPhysics(PHYS_Falling);

   if (Bounces > 0)
   {
      if ( !Level.bDropDetail && (FRand() < 0.4) )
         Playsound(ImpactSounds[Rand(6)]);

      //Velocity = 0.65 * (Velocity - 2.0*HitNormal*(Velocity dot HitNormal));
      Velocity = Velocity - 2.0*HitNormal*(Velocity dot HitNormal);
      Bounces--;

      // We bounced, change BulletID so we can hit a zed again
      BulletID++;

      FLastDamaged = none;

      if (!Level.bDropDetail && (Level.NetMode != NM_DedicatedServer))
         Spawn(class'ROEffects.ROBulletHitMetalEffect',,,Location, rotator(hitnormal));

      return;
   }

   else
   {
      if (ImpactEffect != None && (Level.NetMode != NM_DedicatedServer))
      {
         Spawn(ImpactEffect,,, Location, rotator(-HitNormal));
      }

      SetPhysics(PHYS_None);
      LifeSpan = 5.0;
   }

   bBounce = false;

   if (Trail != None)
   {
      Trail.mRegen=False;
      Trail.SetPhysics(PHYS_None);
   }
}

defaultproperties
{

   ImpactSounds[0]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   ImpactSounds[1]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   ImpactSounds[2]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   ImpactSounds[3]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   ImpactSounds[4]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   ImpactSounds[5]=Sound'ProjectileSounds.Bullets.Impact_Metal'
   Bounces=4
   Speed=2250.000000
   FDamage=85.000000
   BulletID=690
   PenDamageReduction=1.000000
   LifeSpan=20.000000
   StaticMesh=StaticMesh'EffectsSM.Weapons.Vlad_9000_Nail'
   MyDamageType=Class'DamTypeNailBombFalk'
   fShouldHit=False
   fHitAfter=0.01
}
