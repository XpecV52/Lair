class StingerAmmoPickupFalk extends KFAmmoPickup;

defaultproperties
{
     AmmoAmount=40
     InventoryType=Class'StingerAmmoFalk'
     PickupMessage="Rounds 7.62x51mm"
     StaticMesh=StaticMesh'KillingFloorStatics.L85Ammo'
}
