class PipeBombAmmoFalk extends KFMod.PipeBombAmmo;

defaultproperties
{
    MaxAmmo=2
    InitialAmount=1
    AmmoPickupAmount=1
    IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
    IconCoords=(X1=451,Y1=445,X2=510,Y2=500)
	PickupClass=Class'PipeBombAmmoPickupFalk'
}