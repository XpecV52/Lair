class LocalW1300_Compact_EditionPickupFalk extends W1300_Compact_Edition.W1300_Compact_EditionPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=7.000000
   InventoryType=Class'LocalW1300_Compact_EditionFalk'
   cost=1800
   BuyClipSize=4
   AmmoCost=8
   ItemName="W1300 Compact Edition"
   ItemShortName="W1300"
   PickupMessage="You got a W1300 Compact Edition"
   PowerValue=72
   SpeedValue=30
   RangeValue=100
}
