class BoomStickFireFalk extends KFMod.BoomStickAltFire;

var   byte  BID;
var   bool  bVeryLastShotAnim;

// set weapon to pending reload state
event ModeDoFire()
{
   if (!AllowFire())
      return;

   bVeryLastShotAnim = Weapon.AmmoAmount(0) <= AmmoPerFire;

   super(KFShotgunFire).ModeDoFire();

   if (Weapon != None && Weapon.Role == ROLE_Authority && bVeryLastShotAnim)
      BoomStickFalk(Weapon).SetPendingReload();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

// Overridden to stop this from doing the weirdest shit with mah boomstick
function PlayFiring()
{
   local float RandPitch;

   if (Weapon.Mesh != None)
      Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);

   if (Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() && Weapon.Instigator.IsFirstPerson() && StereoFireSound != none)
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   else
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   ClientPlayForceFeedback(FireForce);

   FireCount++;
}

// kind of allow fire only in proper situations?
simulated function bool AllowFire()
{
   if (KFWeapon(Weapon).bIsReloading)
      return false;

   /*if (KFPawn(Instigator).SecondaryItem!=none)
      return false;*/

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   /*if (Level.TimeSeconds - LastClickTime>FireRate)
      LastClickTime = Level.TimeSeconds;*/

   if (KFWeapon(Weapon).MagAmmoRemaining < 1)
      return false;

   return super(WeaponFire).AllowFire();
}

defaultproperties
{
   ProjectileClass=Class'BoomStickBulletFalk'
   AmmoClass=Class'BoomStickAmmoFalk'
   FireRate=0.466
   AmmoPerFire=1
   FireAnim="Fire"
}
