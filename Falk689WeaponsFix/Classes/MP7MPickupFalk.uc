class MP7MPickupFalk extends KFMod.MP7MPickup;

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local Inventory Copy;
      local Falk689GameTypeBase Flk;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (MP7MMedicGunFalk(Copy) != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none && Flk.InZedTime())
                  MP7MMedicGunFalk(Copy).StartForceReload();

               else if (MP7MMedicGunFalk(Copy).fStoredAmmo == -1)
               {
                  MP7MMedicGunFalk(Copy).fStoredAmmo        = fStoredAmmo;
                  MP7MMedicGunFalk(Copy).fPickupAmmoRestore = True;
                  MP7MMedicGunFalk(Copy).fPawnAmmoRestore   = True;
                  //Warn("Recovering from pickup: "@fStoredAmmo);
               }
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if ( KFWeapon(Copy) != none )
         {
            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
                  Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

defaultproperties
{
   InventoryType=Class'MP7MMedicGunFalk'
   ItemName="Medical Heckler & Koch MP7 SMG"
   ItemShortName="MP7-M"
   PickupMessage="You got a Medical Heckler & Koch MP7 SMG"
   PowerValue=12
   SpeedValue=75
   RangeValue=100
   BuyClipSize=20
   AmmoCost=10
   cost=950
   Weight=3.000000
}
