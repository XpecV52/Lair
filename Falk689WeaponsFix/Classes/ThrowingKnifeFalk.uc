class ThrowingKnifeFalk extends Nade;

#exec OBJ LOAD FILE=LairTextures_T.utx

var Actor      FLastDamaged;
var int        BulletID;
var float      HeadShotDamageMult;
var Sound      FImpactSound;
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var bool       fAlreadyHit;    // we already hit someone, don't do damage

replication
{
	reliable if(Role == ROLE_Authority)
		BulletID, fCInstigator, fAlreadyHit;
}

// Post death kill fix 
simulated function PostBeginPlay()
{
   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   Super(Projectile).PostBeginPlay();

   Velocity = Speed * Vector(Rotation);
}

// don't set timer, this shouldn't explode anyway
function PostNetBeginPlay()
{
}

// don't take damage from anything
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

// don't explode pls
simulated function Explode(vector HitLocation, vector HitNormal)
{
}

// nope
simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
}

// nope
simulated function BlowUp(vector HitLocation)
{
}

// nah
simulated function ClientSideTouch(Actor Other, Vector HitLocation)
{
}

// prevent setting ExplodeTimer twice
simulated function HitWall(vector HitNormal, actor Wall)
{
   if (fAlreadyHit)
   {
      Destroy();
      return;
   }

   else if (!fAlreadyHit && (Pawn(Wall) != None) || (GameObjective(Wall) != None))
      ProcessTouch(Wall, Location);

   else if (Level.NetMode != NM_DedicatedServer)
   {
      PlaySound(FImpactSound,,0.8);
      Spawn(Class'ROEffects.ROBulletHitMetalEffect',,,Location, rotator(HitNormal));
   }

   Destroy();
}


// attempt to force destroy the knife
simulated function Tick(float DeltaTime)
{
   Super.Tick(DeltaTime);

   if (fAlreadyHit)
      Destroy();
}



// point blank bullet fix
simulated function ProcessTouch(Actor Other, vector HitLocation)
{
   local vector X;
   local Vector TempHitLocation, HitNormal;
   local array<int>	HitPoints;
   local KFPawn HitPawn;

   if (Other == none || Other == Instigator || Other.Base == Instigator || !Other.bBlockHitPointTraces || fAlreadyHit)
      return;

   X = Vector(Rotation);

   if (ROBulletWhipAttachment(Other) != none)
   {
      if(!Other.Base.bDeleteMe)
      {
         Other = Instigator.HitPointTrace(TempHitLocation, HitNormal, HitLocation + (200 * X), HitPoints, HitLocation,, 1);

         if(Other == none || HitPoints.Length == 0)
            return;

         HitPawn = KFPawn(Other);

         if (Role == ROLE_Authority)
         {
            if (HitPawn != none)
            {
               if(!HitPawn.bDeleteMe)
               {
                  fAlreadyHit = True;
                  //warn("Knife 0:"@Level.TimeSeconds);
                  HitPawn.SetDelayedDamageInstigatorController(fCInstigator);
                  HitPawn.ProcessLocationalDamage(Damage, Instigator, TempHitLocation, MomentumTransfer * Normal(Velocity), MyDamageType,HitPoints);
                  Destroy();
               }
            }
         }
      }
   }

   else
   {
      if (Pawn(Other) != none)
      {
         fAlreadyHit = True;
         //warn("Knife 1:"@Level.TimeSeconds);
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Pawn(Other).TakeDamage(Damage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
         Destroy();
      }

      else if (Pawn(Other.Base) != none)
      {
         fAlreadyHit = True;
         //warn("Knife 2:"@Level.TimeSeconds);
         Other.Base.SetDelayedDamageInstigatorController(fCInstigator);
         Pawn(Other.Base).TakeDamage(Damage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), MyDamageType, BulletID);
         Destroy();
      }

      // this may be needed since shotguns basically fire bugs
      else
      {
         //warn("Knife WUT:"@Level.TimeSeconds);
         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(Damage, Instigator, HitLocation, MomentumTransfer * Normal(Velocity), class'DamTypeFragImpactFalk');
      }
   }
}

defaultproperties
{
   Speed=1750.000000
   Damage=300.000000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeThrowingKnifeFalk'
   StaticMesh=StaticMesh'LairStaticMeshes_SM.Grenades.BerserkerGrenade'
   FImpactSound=Sound'KF_KnifeSnd.Knife_HitMetal'
}
