class TrigunFireFalk extends AA12Explosive.AA12ExplosiveFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 1;

   if (BID > 200)
      BID = BID - 200;

   class<AA12ExplosiveBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// Added zed time demoman skills
function DoFireEffect()
{   
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);
 
	if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
	{
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
         ProjPerFire = Vet.Static.GetExplosiveProjPerFire(KFPRI, Default.ProjPerFire);
	}

   Super.DoFireEffect();
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local AA12ExplosiveBulletFalk FG;
   local KFPlayerReplicationInfo KFPRI;

   FG = AA12ExplosiveBulletFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI.ClientVeteranSkill != none)
      P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, Class'KFMod.DamTypeM79Grenade');

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   BWaitForRelease=True
   AmmoClass=Class'TrigunAmmoFalk'
   FireRate=0.400000
   ProjectileClass=Class'TrigunBulletFalk'
   Spread=1600.000000
}
