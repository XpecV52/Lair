class DualDeagleFireFalk extends KFMod.DualDeagleFire;

var byte  BID;                // bullet ID to prevent multiple hits on a single zed

function DoTrace(Vector Start, Rotator Dir)
{
   local Vector           X,Y,Z, End, HitLocation, HitNormal, ArcEnd;
   local Actor            Other;
   local array<int>	     HitPoints;
   local array<Actor>     IgnoreActors;
   local Actor            DamageActor;
   local Actor            OldDamageActor;
   local int              i;
   local int              bHP;
   local byte             LoopCount;

   BID++;

   if (BID > 200)
      BID = 1;

   MaxRange();

   Weapon.GetViewAxes(X, Y, Z);

   if (Weapon.WeaponCentered())
      ArcEnd = (Instigator.Location + Weapon.EffectOffset.X * X + 1.5 * Weapon.EffectOffset.Z * Z);

   else
   {
      ArcEnd = (Instigator.Location + Instigator.CalcDrawOffset(Weapon) + Weapon.EffectOffset.X * X + 
            Weapon.Hand * Weapon.EffectOffset.Y * Y + Weapon.EffectOffset.Z * Z);
   }

   X = Vector(Dir);
   End = Start + TraceRange * X;

   While (LoopCount <= 10)
   {
      //log("DoTrace Loop");
      LoopCount++;
      DamageActor = none;

      Other = Instigator.HitPointTrace(HitLocation, HitNormal, End, HitPoints, Start,, 1);

      if(Other == None)
         break;

      else if (Other == Instigator || Other.Base == Instigator)
      {
         //log("Instigator");
         IgnoreActors[IgnoreActors.Length] = Other;
         Other.SetCollision(false);
         Start = HitLocation;
         continue;
      }

      if(ExtendedZCollision(Other) != None && Other.Owner != None)
         DamageActor = Pawn(Other.Owner);

      if (!Other.bWorldGeometry && Other != Level)
      {
         if(KFMonster(Other) != None)
            DamageActor = Other;

         if (KFMonster(DamageActor) != None)
         {
            //log("KFMonster");

            if (DamageActor != OldDamageActor) // don't hit this zed twice
            {
               OldDamageActor = DamageActor;
               bHP            = KFMonster(DamageActor).Health;

               //log("old hp: "@bHP);

               if (bHP > 0)
               {
                  DamageActor.TakeDamage(DamageMax, Instigator, HitLocation, Momentum*X, DamageType, BID);

                  //log("new hp: "@KFMonster(DamageActor).Health);

                  if (KFMonster(DamageActor).Health != bHP) // this zed was hit, break the loop
                     break;

                  else // maybe we tried an headshot on a decapitated zed, ignore it
                  {
                     IgnoreActors[IgnoreActors.Length] = Other;
                     Other.SetCollision(false);
                     //log("ignore");
                  }
               }

               else
               {
                  //log("current hp = 0");
                  break;
               }
            }

            else
            {
               IgnoreActors[IgnoreActors.Length] = Other;
               Other.SetCollision(false);
               //log("ignore");
            }
         }

         else
         {
            //log("not a kf monster?");
            break;
         }

         Start = HitLocation;
      }

      else if (HitScanBlockingVolume(Other) == None)
      {
         if(KFWeaponAttachment(Weapon.ThirdPersonActor) != None)
            KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(Other, HitLocation, HitNormal);

         //log("HitScanBlockingVolume");
         break;
      }
   }

   // Turn the collision back on for any actors we turned it off
   if (IgnoreActors.Length > 0)
   {
      for (i=0; i<IgnoreActors.Length; i++)
      {
         if(IgnoreActors[i] != None)
            IgnoreActors[i].SetCollision(true);
      }
   }
}

defaultproperties
{
   DamageType=Class'DamTypeDualDeagleFalk'
   AmmoClass=Class'DualDeagleAmmoFalk'
   FireRate=0.13000
   RecoilRate=0.08000
   DamageMin=90
   DamageMax=90
}