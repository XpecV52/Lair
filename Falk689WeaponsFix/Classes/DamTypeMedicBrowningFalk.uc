class DamTypeMedicBrowningFalk extends KFProjectileWeaponDamageType
	abstract;
   
static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'MedicBrowningFalk'
   DeathString="%k killed %o (HP-M)"
   bRagdollBullet=True
   bBulletHit=True
   FlashFog=(X=600.000000)
   KDamageImpulse=4500.000000
   KDeathVel=200.000000
   KDeathUpKick=20.000000
   VehicleDamageScaling=0.800000
}