class LocalWinchesterPickupFalk extends KFMod.WinchesterPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	cost=1000
	ItemName="Winchester M1894"
	ItemShortName="M1894"
	AmmoItemName=".44 rounds"
	BuyClipSize=8
	AmmoCost=16
	InventoryType=Class'LocalWinchesterFalk'
	PickupMessage="You got a Winchester M1894"
	PowerValue=37
   SpeedValue=11
   RangeValue=100
	Weight=6.00000
}
