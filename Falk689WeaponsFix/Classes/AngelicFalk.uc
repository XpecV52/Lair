class AngelicFalk extends Angelic.Angelic;

#exec OBJ LOAD FILE=LairTextures_T.utx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

// don't load hud icons or the mesh
static function PreloadAssets(Inventory Inv, optional bool bSkipRefCount)
{
	local int i;

	if ( !bSkipRefCount )
	{
		default.ReferenceCount++;
	}

	//UpdateDefaultMesh(SkeletalMesh(DynamicLoadObject(default.MeshRef, class'SkeletalMesh')));
	default.SelectSound = sound(DynamicLoadObject(default.SelectSoundRef, class'sound'));

	for ( i = 0; i < default.SkinRefs.Length; i++ )
		default.Skins[i] = Material(DynamicLoadObject(default.SkinRefs[i], class'Material'));

	if ( KFWeapon(Inv) != none )
	{
		Inv.LinkMesh(default.Mesh);
		KFWeapon(Inv).SelectSound = default.SelectSound;

		for (i = 0; i < default.SkinRefs.Length; i++)
			Inv.Skins[i] = default.Skins[i];
	}
}

// don't randomly unload anything
static function bool UnloadAssets()
{
	default.ReferenceCount--;
	return default.ReferenceCount == 0;
}

// fix the just frame exploit on melee fire
simulated function bool ReadyToFire(int Mode)
{  
   if (FireMode[0].bIsFiring || FireMode[1].bIsFiring || FireMode[0].NextFireTime + 0.1 > Level.TimeSeconds || FireMode[1].NextFireTime + 0.1 > Level.TimeSeconds)
      return False;

   return True;
}

simulated function BringUp(optional Weapon PrevWeapon)
{
   local KFPlayerController Player;

   Super.BringUp(PrevWeapon);

   Player = KFPlayerController(Instigator.Controller);

   if (Player != none && ClientGrenadeState != GN_BringUp)
   {
      Player.WeaponPulloutRemark(24); // AXE
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();


      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   weaponRange=100.000000
   PutDownTime=0.360000
   BloodyMaterialRef="Angelic_T.Angelic"
   Weight=11.000000
   TraderInfoTexture=Texture'LairTextures_T.CustomReskins.AngelicTrader'
   MeshRef="Angelic_A.Angelic"
   Mesh=SkeletalMesh'Angelic_A.Angelic'
   SkinRefs(0)="Angelic_T.Angelic"
   Skins(0)=Texture'Angelic_T.Angelic'
   HudImageRef=""
   HudImage=Texture'LairTextures_T.CustomReskins.AngelicUnselected'
   SelectedHudImageRef=""
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.AngelicSelected'
   FireModeClass(0)=Class'AngelicFireFalk'
   FireModeClass(1)=Class'AngelicFireBFalk'
   Description="This greatsword has a devastating range and the second highest damage output amongst all melee weapons, but it's extremely heavy and expensive."
   PickupClass=Class'AngelicPickupFalk'
   AttachmentClass=Class'Angelic.AngelicAttachment'
   ItemName="Angelic Greatsword"
   Priority=60
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   ChopSlowRate=1.0 // you know that microfreeze you get when you swing a melee weapon? me neither..............
}
