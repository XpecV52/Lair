class SPShotgunPickupFalk extends KFMod.SPShotgunPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=3000
   Weight=8.000000
   BuyClipSize=10
   AmmoCost=20
   PowerValue=46
   SpeedValue=60
   RangeValue=100
   InventoryType=Class'SPShotgunFalk'
   ItemName="Multichamber Zed Thrower"
   ItemShortName="MZT"
   PickupMessage="You got a Multichamber Zed Thrower"
}