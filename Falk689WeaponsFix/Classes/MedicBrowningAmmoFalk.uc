class MedicBrowningAmmoFalk extends KFAmmunition;

#EXEC OBJ LOAD FILE=InterfaceContent.utx

defaultproperties
{
   AmmoPickupAmount=16
   MaxAmmo=64
   InitialAmount=24
   PickupClass=Class'MedicBrowningAmmoPickupFalk'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=338,Y1=40,X2=393,Y2=79)
   ItemName=".40 SW"
}
