class XMV850PickupFalk extends XMV850PickupBaseFalk;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// use FStoredAmmo instead of MagAmmoRemaining to have proper ammo amount on drop/pickup
function InitDroppedPickupFor(Inventory Inv)
{
   local KFWeapon W;
   local Inventory InvIt;
   local byte bSaveAmmo[2];
   local int m;

   W = KFWeapon(Inv);

   if (W != None)
   {
      //Check if any other weapon is using the same ammo
      for(InvIt = W.Owner.Inventory; InvIt!=none; InvIt=InvIt.Inventory)
      {
         if(Weapon(InvIt)!=none && InvIt!=W)
         {
            for(m=0; m < 2; m++)
            {
               if(Weapon(InvIt).AmmoClass[m] == W.AmmoClass[m])
                  bSaveAmmo[m] = 1;
            }
         }
      }

      if (bSaveAmmo[0] == 0)
      {
         if (XMV850Falk(W) != none)
            MagAmmoRemaining = XMV850Falk(W).FStoredAmmo;

         else
            MagAmmoRemaining = W.MagAmmoRemaining;

         AmmoAmount[0] = W.AmmoAmount(0);
      }

      if (bSaveAmmo[1] == 0)
         AmmoAmount[1] = W.AmmoAmount(1);

      SellValue = W.SellValue;
   }
   SetPhysics(PHYS_Falling);
   GotoState('FallingPickup');
   Inventory = Inv;
   bAlwaysRelevant = false;
   bOnlyReplicateHidden = false;
   bUpdateSimulatedPosition = true;
   bDropped = true;
   bIgnoreEncroachers = false; // handles case of dropping stuff on lifts etc
   NetUpdateFrequency = 8;
   bNoRespawn = true;

   if ( KFWeapon(Inventory) != none )
   {
      if ( KFWeapon(Inventory).bIsTier2Weapon )
      {
         if ( !KFWeapon(Inventory).bPreviouslyDropped && PlayerController(Pawn(Inventory.Owner).Controller) != none )
         {
            KFWeapon(Inventory).bPreviouslyDropped = true;
            KFSteamStatsAndAchievements(PlayerController(Pawn(Inventory.Owner).Controller).SteamStatsAndAchievements).AddDroppedTier2Weapon();
         }
      }
      else
      {
         bPreviouslyDropped = KFWeapon(Inventory).bPreviouslyDropped;
         DroppedBy = PlayerController(Pawn(W.Owner).Controller);
      }
   }
}

defaultproperties
{
   Weight=17.000000
   cost=4000
   AmmoCost=200
   BuyClipSize=200
   PowerValue=53
   SpeedValue=80
   RangeValue=100
   CorrespondingPerkIndex=7
   ItemName="XMV850 Gatling Gun"
   ItemShortName="XMV850"
   AmmoItemName="7.62x51mm rounds"
   InventoryType=Class'XMV850Falk'
   PickupMessage="You got the XMV850 Gatling Gun"
}
