class SVT40SAPickupFalk extends SVT40SAMut.SVT40SAPickup;

#exec OBJ LOAD FILE=LairTextures_T.utx

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=1600
   InventoryType=Class'SVT40SAFalk'
   ItemName="Tokarev SVT40"
   ItemShortName="SVT40"
   PickupMessage="You got a Tokarev SVT40"
   Weight=7.000000
   AmmoCost=10
   BuyClipSize=10
   PowerValue=32
   SpeedValue=33
   RangeValue=100
   AmmoItemName="7.62×54mmR rounds"
   Skins(0)=Texture'LairTextures_T.ZedsUpscale.SVT40Body'
   Skins(1)=Texture'LairTextures_T.ZedsUpscale.SVT40Scope'
}
