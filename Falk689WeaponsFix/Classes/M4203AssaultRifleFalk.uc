class M4203AssaultRifleFalk extends KFMod.M4203AssaultRifle;

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

simulated function bool ReadyToFire(int Mode)
{
    // Don't allow firing while reloading the shell
	if (AmmoAmount(1) > 0 && (((FireMode[1].NextFireTime - FireMode[1].FireRate * 0.06) > Level.TimeSeconds + FireMode[1].PreFireTime) || bIsReloading))
		return False;

   // attempt to allow fire on the last shot
	if (!bIsReloading && MagAmmoRemaining > 0 && Mode == 0 && AmmoAmount(1) <= 0 && M203FireFalk(FireMode[1]) != none &&
      ((FireMode[1].NextFireTime - FireMode[1].FireRate + M203FireFalk(FireMode[1]).FireLastRate) < Level.TimeSeconds + FireMode[1].PreFireTime))
   {
      FireMode[1].bIsFiring = False;
      return True;
   }

    return super.ReadyToFire(Mode);
}

// don't block throw or swap on no ammo and block while waiting for reload
simulated function bool CanThrow()
{
   // attempt to allow throw before its otherwise weird time on the last alt shot
   if ((!bIsReloading && (AmmoAmount(0) == 0 || AmmoAmount(1) == 0)) || ReadyToFire(0))
   {
      FireMode[1].bIsFiring = False;
      return True;
   }

   return Super.CanThrow();
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// Take into account our modified last fire rate for the last putdown
simulated function bool PutDown()
{
   local int Mode;

   InterruptReload();

   if (bIsReloading)
      return false;

   if( bAimingRifle )
      ZoomOut(False);

   // From Weapon.uc
   if (ClientState == WS_BringUp || ClientState == WS_ReadyToFire)
   {
      if ((Instigator.PendingWeapon != None) && !Instigator.PendingWeapon.bForceSwitch && AmmoAmount(1) > 0 && !ReadyToFire(0))
         DownDelay = FMax(DownDelay, FireMode[1].NextFireTime - Level.TimeSeconds - FireMode[1].FireRate*(1.f - MinReloadPct));

      if (Instigator.IsLocallyControlled())
      {
         for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
         {
            // if _RO_
            if (FireMode[Mode] == none)
               continue;
            // End _RO_

            if (FireMode[Mode].bIsFiring)
               ClientStopFire(Mode);
         }

         if (DownDelay <= 0  || KFPawn(Instigator).bIsQuickHealing > 0)
         {
            if (ClientState == WS_BringUp || KFPawn(Instigator).bIsQuickHealing > 0)
               TweenAnim(SelectAnim, PutDownTime);

            else if (HasAnim(PutDownAnim))
            {
               if (ClientGrenadeState == GN_TempDown || KFPawn(Instigator).bIsQuickHealing > 0)
                  PlayAnim(PutDownAnim, PutDownAnimRate * (PutDownTime/QuickPutDownTime), 0.0);

               else
                  PlayAnim(PutDownAnim, PutDownAnimRate, 0.0);
            }
         }
      }

      ClientState = WS_PutDown;

      if (Level.GRI.bFastWeaponSwitching)
         DownDelay = 0;

      if (DownDelay > 0)
         SetTimer(DownDelay, false);

      else
      {
         if( ClientGrenadeState == GN_TempDown )
            SetTimer(QuickPutDownTime, false);

         else
            SetTimer(PutDownTime, false);
      }
   }

   for (Mode = 0; Mode < NUM_FIRE_MODES; Mode++)
   {
      // if _RO_
      if (FireMode[Mode] == none)
         continue;
      // End _RO_

      FireMode[Mode].bServerDelayStartFire = false;
      FireMode[Mode].bServerDelayStopFire  = false;
   }

   Instigator.AmbientSound = None;
   OldWeapon = None;
   
   return true;
}

// take into account our last fire rate for the reload
simulated function bool AllowReload()
{
   local bool fResult;

   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;
   
   fResult = Super.AllowReload();

   if (!bIsReloading && MagAmmoRemaining < MagCapacity && AmmoAmount(0) > 0 && !fResult && AmmoAmount(1) <= 0 && !ReadyToFire(0))
      return True;

   return fResult;
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}

defaultproperties
{
   FireModeClass(1)=Class'M203FireFalk'
   FireModeClass(0)=Class'M4203FireFalk'
   PickupClass=Class'M4203PickupFalk'
   ItemName="Colt M4+M203 Carbine"
   Description="A standard Colt M4 with an attached M203 under-barrel grenade launcher that fires standard 40x46mm grenades."
   InventoryGroup=3
   Weight=6.000000
   ReloadRate=3.633
   MagCapacity=30
   Priority=195
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
