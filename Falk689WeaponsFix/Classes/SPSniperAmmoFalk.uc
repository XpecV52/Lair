class SPSniperAmmoFalk extends KFMod.SPSniperAmmo;

defaultproperties
{
	MaxAmmo=70
	InitialAmount=30
	AmmoPickupAmount=10
	PickupClass=Class'SPSniperAmmoPickupFalk'
	ItemName="SMLE rounds"
}