class LAWProjFalk extends KFMod.LAWProj;

var class<projectile> fCClass;              // cluster class, what we spawn on kaboom
var byte              BulletID;             // used to prevent multi hit on a single zed
var byte              fNClusters;           // How many clusters we spawn on kaboom
var byte              fSClusters;           // How many clusters we have spawned
var byte              fTry;                 // How many times we tried to spawn a cluster
var byte              fMaxTry;              // How many times we should try to spawn a cluster
var vector            fCSpawnLoc;           // Where to spawn clusters
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction
var vector            fSuccessPos;          // Success position so we don't have to do all the while again
var vector            fTempPos;             // Temp position so we can set success pos only on actual success
var float             fCLastCheck;          // When was the last zed-time check
var float             fCheckSecs;           // Time between checks
var float             fLifeTime;            // Time this projectile was spawned
var bool              fSuccessSpawn;        // have we found a place to spawn stuff?
var bool              fShouldKaboom;        // Should we spawn clusters?
var bool              fShouldCheck;         // should check cluster math on tick
var bool              fFailedToSetDamage;   // failed to set damage on fire
var Controller        fCInstigator;         // instigator controller, I don't use vanilla InstigatorController 'cause of reasons
var float             FDamage;              // hopefully prevent external powers to tweak our damage

replication
{
   reliable if(Role == ROLE_Authority)
		BulletID, fCInstigator, fSuccessSpawn, fSuccessPos, FDamage, fFailedToSetDamage;
}

// Initial clusters check if we're on zedtime on spawn
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
	local rotator SmokeRotation;

   if (Instigator != none)
   {
      if (Role == ROLE_Authority && fCInstigator == none && Instigator.Controller != None)
         fCInstigator = Instigator.Controller;

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
            fShouldKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI);
      }
   }

   BCInverse = 1 / BallisticCoefficient;

   if ( Level.NetMode != NM_DedicatedServer)
   {
      SmokeTrail = Spawn(class'SmokeTrailFalk',self);
      SmokeTrail.SetBase(self);
      SmokeRotation.Pitch = 32768;
      SmokeTrail.SetRelativeRotation(SmokeRotation);
   }

   OrigLoc = Location;

   if( !bDud )
   {
      Dir = vector(Rotation);
      Velocity = speed * Dir;
   }

   if (PhysicsVolume.bWaterVolume)
   {
      bHitWater = True;
      Velocity=0.6*Velocity;
   }

   super(Projectile).PostBeginPlay();
}


// Spawn clusters on explode if we're on zedtime, with second check
simulated function Explode(vector HitLocation, vector HitNormal)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local byte i;
   local Projectile P;
   local vector fPLocation;
   local vector SpawnCorrection;

   if (bHasExploded)
      return;

   SpawnCorrection = vect(0,0,0);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      // Check again if we should spawn explosive shrapnels

      if (Instigator != none)
      {
         if (!fShouldKaboom)
         {
            KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

            if (KFPRI != none)
            {
               Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

               if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
                  fShouldKaboom = True;
            }
         }

         if (fShouldKaboom)
         {
            // attempt to fix ground cluster spawn
            fPLocation = Instigator.Location + fCZRetryLoc;

            //log(Location.Z);
            //log(fPLocation.Z);

            if (Location.Z <= fPLocation.Z) // if the nade if under the player, correct spawn position
               SpawnCorrection = fCSpawnLoc;
         }
      }

      // Kaboom
      if (!bDud && fShouldKaboom)
      {
         for(i=0; i<fNClusters; i++)
         {
            class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

            P = Spawn(fCClass,,, Location + SpawnCorrection, RotRand(True));

            if (P != none) 
            {
               fSClusters++;
               P.Instigator   = Instigator;

               if (Vet == none)
               {
                  KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                  if (KFPRI != none)
                     Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
               }

               if (Vet != none)
                  P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, P.MyDamageType);

               if (M79ClusterFalk(P) != none)
                  M79ClusterFalk(P).fCInstigator = fCInstigator;
            }
         }

         //log("Initial spawn");
         //log(fSClusters);
      }
   }

   FalkExplode(HitLocation, HitNormal);
}


// Overridden to prevent clusters from blowing up our nade
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   if (damageType == class'SirenScreamDamage')
      Disintegrate(HitLocation, vect(0,0,1));

   else if ((!bDud && !bHasExploded) && 
         (InstigatedBy == Instigator && damageType != class'DamTypeExplosiveBulletFalk') || 
         (KFHumanPawn(InstigatedBy) == none && (damageType == class'DamTypeFrag' || damageType == class'DamTypeBurned')))
      Explode(HitLocation, vect(0,0,0));
}


// Timed check to see if we should spawn clusters
simulated function Tick(float DeltaTime)
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;
   local Projectile P;
   local byte fAttempt;

   Super.Tick(DeltaTime);

   if (Role == ROLE_Authority && fShouldCheck)
   {
      fLifeTime += DeltaTime;

      if (!bHasExploded && !fShouldKaboom && fLifeTime >= fCLastCheck + fCheckSecs && Instigator != none)
      {
         KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

         fCLastCheck = fLifeTime;

         if (KFPRI != none)
         {
            Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

            if (Vet != none && Vet.Static.DetonateBulletsControlBonus(KFPRI))
               fShouldKaboom = True;
         }
      }

      if (!bDud && fShouldKaboom && bHasExploded)
      {
         if (fSClusters < fNClusters)
         {
            // Try to spawn remaining clusters
            if (fTry < fMaxTry)
            {
               class<M79ClusterFalk>(fCClass).Default.BulletID = BulletID + fSClusters;

               if (fSuccessSpawn)
               {
                  P = Spawn(fCClass,,, fSuccessPos, RotRand(True));

                  if (P == None)
                     fSuccessSpawn = False;
               }

               if (!fSuccessSpawn)
               {
                  While (P == None && fAttempt < 27)
                  { 
                     fAttempt++; // we don't even test 0 since we're here for a reason

                     if (fAttempt >= 27)
                     {
                        //warn("FAIL"@fTry);
                        fACZRetryLoc += fCZRetryLoc;
                        fACXRetryLoc += fCXRetryLoc;
                        fACYRetryLoc += fCYRetryLoc;
                        fTry++;
                     }


                     else if (fAttempt >= 18)
                     {
                        fTempPos = FClusterQuad(Location - fACZRetryLoc, fAttempt - 18);
                        //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
                     }

                     else if (fAttempt >= 9)
                     {
                        fTempPos = FClusterQuad(Location + fACZRetryLoc, fAttempt - 9);
                        //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
                     }

                     else
                     {
                        fTempPos = FClusterQuad(Location, fAttempt);
                        //warn("First:"@fAttempt@"Location:"@fTempPos);
                     }

                     P = Spawn(fCClass,,, fTempPos, RotRand(True));
                  }
               }

               if (P != none)
               {
                  //warn("Spawned");
                  fSuccessPos   = fTempPos; 
                  fSuccessSpawn = True;
                  P.Instigator  = Instigator; 

                  fSClusters++;

                  fTry          = 0;

                  if (Vet == none)
                  {
                     KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                     if (KFPRI != none)
                        Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);
                  }

                  if (Vet != none)
                     P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), P.Damage, P.MyDamageType);

                  if (M79ClusterFalk(P) != none)
                     M79ClusterFalk(P).fCInstigator = fCInstigator;
               }
            }

            // Give up
            else
            {
               //warn("Giving up");
               fACZRetryLoc = fCZRetryLoc;
               fACXRetryLoc = fCXRetryLoc;
               fACYRetryLoc = fCYRetryLoc;
               fSClusters   = fNClusters;
               fTry = 0;
               Destroy();
            }
         }

         else
            Destroy();
      }
   }
}

// attempt to spawn clusters in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// Explode but check if we spawned all clusters before destroy
simulated function FalkExplode(vector HitLocation, vector HitNormal)
{
   local Controller C;
   local PlayerController  LocalPlayer;

   bHasExploded = True;

   // Don't explode if this is a dud
   if(bDud)
   {
      Velocity = vect(0,0,0);
      LifeSpan=1.0;
      SetPhysics(PHYS_Falling);
   }

   PlaySound(ExplosionSound,,2.0);

	Spawn(class'LAWKaboomFalk',,,HitLocation + HitNormal*20,rotator(HitNormal));
   Spawn(ExplosionDecal,self,,HitLocation, rotator(-HitNormal));

   BlowUp(HitLocation);

   //log("fSClusters "@fSClusters);
   //log("fNClusters "@fNClusters);

   if (Role != ROLE_Authority || !fShouldKaboom || fSClusters >= fNClusters)
   {
      //log("End");
      fShouldCheck = False;
      Destroy();
   }

   // Shake nearby players screens
   LocalPlayer = Level.GetLocalPlayerController();
   if ( (LocalPlayer != None) && (VSize(Location - LocalPlayer.ViewTarget.Location) < DamageRadius) )
      LocalPlayer.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);

   for ( C=Level.ControllerList; C!=None; C=C.NextController )
      if ( (PlayerController(C) != None) && (C != LocalPlayer)
            && (VSize(Location - PlayerController(C).ViewTarget.Location) < DamageRadius) )
         C.ShakeView(RotMag, RotRate, RotTime, OffsetMag, OffsetRate, OffsetTime);
}

// Touch only if we haven't exploded yet and fix point blank stuff
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   // Don't let it hit this player, or blow up on another player
   if (bHasExploded || Other == none || Other == Instigator || Other.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   // Don't allow hits on poeple on the same team
   if(KFHumanPawn(Other) != none && Instigator != none &&
         KFHumanPawn(Other).PlayerReplicationInfo.Team.TeamIndex == Instigator.PlayerReplicationInfo.Team.TeamIndex)
   {
      return;
   }

   // fixed shit happening with location and replication
   if (Instigator != none)
      OrigLoc = Instigator.Location;

   if (!bDud && ((VSizeSquared(Location - OrigLoc) < ArmDistSquared) || OrigLoc == vect(0,0,0)))
   {
      if (Role == ROLE_Authority)
      {
         AmbientSound = none;
         PlaySound(Sound'ProjectileSounds.PTRD_deflect04',,2.0);

         Other.SetDelayedDamageInstigatorController(fCInstigator);
         Other.TakeDamage(ImpactDamage, Instigator, HitLocation, Normal(Velocity), ImpactDamageType, BulletID);
      }

      bDud     = True;
      Velocity = vect(0,0,0);
      LifeSpan = 1.0;
      SetPhysics(PHYS_Falling);
   }

   if (!bDud)
      Explode(HitLocation,Normal(HitLocation-Other.Location));
}

simulated function HurtRadius(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local float damageScale, dist;
   local vector dirs;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local KFPawn KFP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;
   local KFPlayerReplicationInfo KFPRI;

   if ( bHurtEntry )
      return;

   // looks stupid but may help, maybe
   if (FDamage < Damage)
      FDamage = Damage;

   else if (Damage < FDamage)
      Damage = FDamage;

   bHurtEntry = true;

   // we failed to set damage on projectile spawn, set it now
   if (fFailedToSetDamage && Instigator != none)
   {
      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
      {
         warn("Delayed LAWProj set damage.");
         FDamage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Instigator), FDamage, MyDamageType);
         Damage = FDamage;
      }
   }

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         dirs = Victims.Location - HitLocation;
         dist = FMax(1,VSize(dirs));
         dirs = dirs/dist;
         damageScale = 1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius);
         Victims.SetDelayedDamageInstigatorController(fCInstigator);
         if ( Victims == LastTouched )
            LastTouched = None;

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            KFP = KFPawn(Victims);

            if( KFMonsterVictim != none )
            {
               damageScale *= KFMonsterVictim.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }
            else if( KFP != none )
            {
               damageScale *= KFP.GetExposureTo(HitLocation/*Location + 15 * -Normal(PhysicsVolume.Gravity)*/);
            }

            CheckedPawns[CheckedPawns.Length] = P;

            if ( damageScale <= 0)
            {
               P = none;
               continue;
            }
            else
            {
               //Victims = P;
               P = none;
            }
         }

         if(Victims == Instigator)
            damageScale *= 0.3;

         Victims.TakeDamage(damageScale * FDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs, (damageScale * Momentum * dirs), MyDamageType, BulletID);

         if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
            Vehicle(Victims).DriverRadiusDamage(FDamage, DamageRadius, InstigatorController, MyDamageType, Momentum, HitLocation);

         if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
         {
            NumKilled++;
         }
      }
   }
   if ((LastTouched != None) && (LastTouched != self) && (LastTouched.Role == ROLE_Authority) && !LastTouched.IsA('FluidSurfaceInfo') )
   {
      Victims = LastTouched;
      LastTouched = None;
      dirs = Victims.Location - HitLocation;
      dist = FMax(1,VSize(dirs));
      dirs = dirs/dist;
      damageScale = FMax(Victims.CollisionRadius/(Victims.CollisionRadius + Victims.CollisionHeight),1 - FMax(0,(dist - Victims.CollisionRadius)/DamageRadius));
      Victims.SetDelayedDamageInstigatorController(fCInstigator);

      if(Victims == Instigator)
         damageScale *= 0.3;

      Victims.TakeDamage(damageScale * FDamage, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius) * dirs, (damageScale * Momentum * dirs), MyDamageType, BulletID);

      if (Vehicle(Victims) != None && Vehicle(Victims).Health > 0)
         Vehicle(Victims).DriverRadiusDamage(FDamage, DamageRadius, InstigatorController, MyDamageType, Momentum, HitLocation);
   }

   bHurtEntry = false;
}


// attempt to just explode and make sound
simulated function BlowUp(vector HitLocation)
{
   HurtRadius(FDamage, DamageRadius, MyDamageType, MomentumTransfer, HitLocation);

   if (Role == ROLE_Authority)
      MakeNoise(1.0);
}

// not sure if I need this but better safe than taco
simulated function DelayedHurtRadius( float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation )
{
   HurtRadius(FDamage, DamageRadius, MyDamageType, Momentum, HitLocation);
}

defaultproperties
{
   ImpactDamageType=Class'DamTypeLawRocketImpactFalk'
   MyDamageType=Class'DamTypeLAWFalk'
   ImpactDamage=360
   Damage=1260
   FDamage=1260
   Speed=4000.000000
   MaxSpeed=4000.000000
   ArmDistSquared=190000
   DamageRadius=510
   fCClass=class'LAWClusterFalk'
   fNClusters=3
   fShouldKaboom=False
   fCheckSecs=0.500000
   fMaxTry=3
   fCSpawnLoc=(Z=10.000000)
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
   fShouldCheck=True
   ExplosionDecal=class'ScorchMarkFalk'
}
