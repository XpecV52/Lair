class KnifePickupFalk extends KFMod.KnifePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=10
   InventoryType=Class'KnifeFalk'
   ItemName="Military Combat Knife"
   ItemShortName="Knife"
   PickupMessage="You got a Military Combat Knife"
   SpeedValue=80
   RangeValue=16
   PowerValue=11
}
