class SinglePickupFalk extends NineMMPlusSinglePickup;

function Destroyed()
{
   if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
   {
      if (KFGameType(Level.Game) != none)
         KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
   }

   super(WeaponPickup).Destroyed();
}

function inventory SpawnCopy( pawn Other )
{
   local Inventory I;
   local class<Inventory> D;

   For( I=Other.Inventory; I!=None; I=I.Inventory )
   {
      if( SingleFalk(I)!=None )
      {
         if( Inventory!=None )
            Inventory.Destroy();

         D = Default.InventoryType;
         Default.InventoryType = Class'DualiesFalk';

         AmmoAmount[0] += SingleFalk(I).AmmoAmount(0);
         MagAmmoRemaining += SingleFalk(I).MagAmmoRemaining;

         I.Destroyed();
         I.Destroy();
         I = Super.SpawnCopy(Other);

         Default.InventoryType = D;
         return I;
      }
   }

   InventoryType = Default.InventoryType;
   Return Super.SpawnCopy(Other);
}

defaultproperties
{
   Weight=1.000000
   InventoryType=Class'SingleFalk'
   ItemName="Beretta 92FS M9A1"
   ItemShortName="M9A1"
   PickupMessage="You got a Beretta 92FS M9A1"
   cost=75
   BuyClipSize=15
   AmmoCost=8
   PowerValue=9
   SpeedValue=36
   RangeValue=100
   DrawScale=0.13
}
