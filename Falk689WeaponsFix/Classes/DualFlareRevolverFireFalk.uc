class DualFlareRevolverFireFalk extends KFMod.DualFlareRevolverFire;

var byte BID;

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<FlareRevolverProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local FlareRevolverProjectileFalk FB;

   FB = FlareRevolverProjectileFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{ 
   ProjectileClass=Class'FlareRevolverProjectileFalk'
   AmmoClass=Class'DualFlareRevolverAmmoFalk'
   FireRate=0.20000
}
