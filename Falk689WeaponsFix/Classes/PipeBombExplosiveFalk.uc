class PipeBombExplosiveFalk extends KFMod.PipeBombExplosive;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=KF_Weapons2_Trip_T.utx

var bool  fWeaponReady;
var float fBringUpTimer;
var float fCurBringUpTimer;

var float fAmmoPickupTime;       // reset to default on ammo pickup to fix a bug with the interface


replication
{
   reliable if(Role == ROLE_Authority)
      fWeaponReady, fAmmoPickupTime;
}

// called regardless of this being the current weapon
simulated function Tick(float dt)
{
   if (fAmmoPickupTime > 0)
      fAmmoPickupTime -= dt;
}

// pending reload function
simulated function WeaponTick(float dt)
{
   Super.WeaponTick(dt);

   if (Role == ROLE_Authority)
   {
      // reload related stuff
      if (!fWeaponReady)
      {
         if (fCurBringUpTimer < fBringUpTimer)
            fCurBringUpTimer += dt;

         else
         {
            fCurBringUpTimer = 0;
            fWeaponReady     = True;   
         }
      }
   }
}

// set weapon to non ready state
simulated function bool PutDown()
{
   local bool result;

   result = Super.PutDown();

   if (result)
      fWeaponReady = True;   

   return result;
}

// kind of try to do only reasonable stuff here
simulated function bool ReadyToFire(int Mode)
{  
   if (!fWeaponReady || FireMode[Mode].bIsFiring || !FireMode[Mode].AllowFire() || (FireMode[Mode].NextFireTime + 0.4 > Level.TimeSeconds))
      return False;

   return True;
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// first attempt to fix shit happening with the fire animation
simulated function AnimEnd(int channel)
{
    local name anim;
    local float frame, rate;

    GetAnimParams(0, anim, frame, rate);

    if (ClientState == WS_ReadyToFire)
    {
        if (anim == FireMode[0].FireAnim && ammoAmount(0) > 0 )
        {
            PlayAnim(SelectAnim, SelectAnimRate, 0.1);
        }
        /*else if ((FireMode[0] == None || !FireMode[0].bIsFiring) && (FireMode[1] == None || !FireMode[1].bIsFiring))
        {
            PlayIdle();
        }*/
    }
}

// just nope
simulated function ServerInterruptReload()
{
}

// lol nope
simulated function ClientInterruptReload()
{
}

// don't play idle here since it kind of bugs everything
simulated function ClientFinishReloading()
{
	bIsReloading = false;

	if(Instigator.PendingWeapon != none && Instigator.PendingWeapon != self)
		Instigator.Controller.ClientSwitchToBestWeapon();
}

defaultproperties
{
   FireModeClass[0]=Class'PipeBombFireFalk'
   PickupClass=Class'PipeBombPickupFalk'
   ItemName="Pipe Bombs"
   Description="These are proximity explosives, quick to drop on the ground. They trigger under light pressure, inflicting fair damage to the specimens nearby."
   Weight=1.000000
   Mesh=SkeletalMesh'LairAnimations_A.PipebombMesh'
   Skins(0)=Combiner'KF_Weapons2_Trip_T.Special.pipebomb_cmb'
   Skins(1)=Texture'KF_Weapons2_Trip_T.Special.Pipebomb_RLight_OFF'
   Skins(2)=Shader'KF_Weapons2_Trip_T.Special.Pipebomb_GLight_shdr'
   fBringUpTimer=0.5
   HudImage=Texture'KillingFloor2HUD.WeaponSelect.Pipe_Bomb_unselected'
   SelectedHudImage=Texture'KillingFloor2HUD.WeaponSelect.Pipe_Bomb'
   PutDownAnimRate=3.0
   PutDownTime=0.15
   InventoryGroup=4
   Priority=30
   fAmmoPickupTime=0.5
}
