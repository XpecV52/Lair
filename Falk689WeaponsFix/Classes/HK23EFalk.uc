class HK23EFalk extends KFWeapon;

#exec OBJ LOAD FILE=KillingFloorWeapons.utx
#exec OBJ LOAD FILE=KillingFloorHUD.utx
#exec OBJ LOAD FILE=Inf_Weapons_Foley.uax
#exec OBJ LOAD FILE=HK23ESA_A.ukx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

// Use alt fire to switch fire modes
simulated function AltFire(float F)
{
   if(ReadyToFire(0))
   {
      DoToggle();
   }
}

function bool RecommendRangedAttack()
{
   return true;
}

//TODO: LONG ranged?
function bool RecommendLongRangedAttack()
{
   return true;
}

function float SuggestAttackStyle()
{
   return -1.0;
}

exec function SwitchModes()
{
   DoToggle();
}

function float GetAIRating()
{
   local Bot B;

   B = Bot(Instigator.Controller);
   if ( (B == None) || (B.Enemy == None) )
      return AIRating;

   return AIRating;
}

function byte BestMode()
{
   return 0;
}

simulated function SetZoomBlendColor(Canvas c)
{
   local Byte    val;
   local Color   clr;
   local Color   fog;

   clr.R = 255;
   clr.G = 255;
   clr.B = 255;
   clr.A = 255;

   if( Instigator.Region.Zone.bDistanceFog )
   {
      fog = Instigator.Region.Zone.DistanceFogColor;
      val = 0;
      val = Max( val, fog.R);
      val = Max( val, fog.G);
      val = Max( val, fog.B);
      if( val > 128 )
      {
         val -= 128;
         clr.R -= val;
         clr.G -= val;
         clr.B -= val;
      }
   }
   c.DrawColor = clr;
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;
      return;
   }

   if (Ammo[0] != None)
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if(WP!=none && WP.bThrown==true)
            InitialAmount = WP.AmmoAmount[m];
         else
            MagAmmoRemaining = MagCapacity;

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   MagCapacity=90
   ReloadRate=5.260000
   ReloadAnim="Reload"
   ReloadAnimRate=1.000000
   BringUpTime=0.350000
   WeaponReloadAnim="Reload_AK47"
   HudImage=Texture'HK23ESA_A.HK23_tex.HK23Unselect'
   SelectedHudImage=Texture'HK23ESA_A.HK23_tex.HK23Select'
   Weight=10.000000
   bHasAimingMode=True
   IdleAimAnim="Idle"
   StandardDisplayFOV=60.000000
   bModeZeroCanDryFire=True
   SleeveNum=3
   TraderInfoTexture=Texture'HK23ESA_A.HK23_tex.HK23Trader'
   bIsTier2Weapon=True
   PlayerIronSightFOV=65.000000
   ZoomedDisplayFOV=32.000000
   FireModeClass(0)=Class'HK23EFireFalk'
   FireModeClass(1)=Class'KFMod.NoFire'
   PutDownAnim="Put_Down"
   PutDownTime=0.64000
   SelectSound=Sound'HK23ESA_A.HK23_SND.HK23Select'
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.550000
   CurrentRating=0.550000
   bShowChargingBar=True
   Description="The HK23 is a german 5.56x45mm GPMG, developed in 1972 by Heckler & Koch. Slightly slower reload rate than an M249. Higher magazine capacity than a THR40, but with lower damage output."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=60.000000
   Priority=20
   CustomCrosshair=11
   CustomCrossHairTextureName="Crosshairs.HUD.Crosshair_Cross5"
   InventoryGroup=4
   GroupOffset=7
   PickupClass=Class'HK23EPickupFalk'
   PlayerViewOffset=(X=1.000000,Y=12.000000,Z=-1.000000)
   BobDamping=6.000000
   AttachmentClass=Class'HK23EAttachmentFalk'
   IconCoords=(X1=245,Y1=39,X2=329,Y2=79)
   ItemName="HK23E Light Machine Gun"
   Mesh=SkeletalMesh'HK23ESA_A.HK23ESA_Mesh'
   Skins(0)=Combiner'HK23ESA_A.HK23_tex.HK23Done1_cmb'
   Skins(1)=Combiner'HK23ESA_A.HK23_tex.HK23Done2_cmb'
   Skins(2)=Combiner'HK23ESA_A.HK23_tex.HK23Grip_cmb'
   Skins(3)=Combiner'KF_Weapons_Trip_T.hands.hands_1stP_military_cmb'
   TransientSoundVolume=1.250000
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
