class FNFALAmmoFalk extends KFMOD.FNFALAmmo;

defaultproperties
{
   PickupClass=Class'FNFALAmmoPickupFalk'
   MaxAmmo=240
   InitialAmount=100
   AmmoPickupAmount=20
   ItemName="7.62x51mm NATO rounds"
}