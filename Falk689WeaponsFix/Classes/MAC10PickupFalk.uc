class MAC10PickupFalk extends KFMod.MAC10Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=3.000000
   InventoryType=Class'MAC10MPFalk'
   ItemName="Ingram MAC-10"
   ItemShortName="MAC-10"
   PickupMessage="You got an Ingram MAC-10"
   cost=500
   BuyClipSize=30
   AmmoCost=15
   PowerValue=10
   SpeedValue=90
   RangeValue=100
}