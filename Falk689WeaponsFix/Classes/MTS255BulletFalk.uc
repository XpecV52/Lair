class MTS255BulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=25.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeMTS255Falk'
}