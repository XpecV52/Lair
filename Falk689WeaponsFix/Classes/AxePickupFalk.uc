class AxePickupFalk extends KFMod.AxePickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
    Weight=5.000000
    cost=650
    PowerValue=51
    SpeedValue=35
    RangeValue=30
    ItemName="Fire Axe"
	ItemShortName="Axe"
    InventoryType=Class'AxeFalk'
    PickupMessage="You got a Fire Axe"
}
