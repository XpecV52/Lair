class AK47LLIAmmoPickupFalk extends KFMod.AK47AmmoPickup;

defaultproperties
{
    AmmoAmount=30
    InventoryType=Class'AK47LLIAmmoFalk'
    PickupMessage="You found 7.62x39mm rounds"
}
