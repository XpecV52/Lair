class DamTypeTrenchgunFalk extends DamTypeTrenchgun;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount)
{
	KFStatsAndAchievements.AddFlameThrowerDamage(Amount);
}

defaultproperties
{
   bDealBurningDamage=True
   WeaponClass=Class'TrenchgunFalk'
   DeathString="%k killed %o (M1897)"
}
