class FlaresThrowFalk extends Flares.FlareThrow;

var string  fThrowPlayerMsg;     // message when the trader is skipped with only one player alive
var string  fThrowMaxMsg;        // message when the trader is skipped when more than one players are alive

var int     fMaxPlayerFlares;    // maximum active flares per player
var int     fMaxTotalFlares;     // maximum active flares in total


// allow the gametype to block fire
simulated function bool AllowFire()
{
   local FHumanPawn  FHP;
   local bool        fResult;

   fResult = Super.AllowFire();

   if (fResult)
   {
      if (Instigator != none)
      {
         FHP = FHumanPawn(Instigator);

         if (FHP != none)
         {
            if (FHP.fPlayerFlares >= fMaxPlayerFlares)
            {
               if (Level.TimeSeconds > FHP.fLastFlareMsg + FHP.fFlareMsgCD)
               {
                  PlayerController(Instigator.Controller).ClientMessage(fThrowPlayerMsg);
                  FHP.fLastFlareMsg = Level.TimeSeconds;
               }

               return False;
            }


            if (FHP.fTotalFlares >= fMaxTotalFlares)
            {

               if (Level.TimeSeconds > FHP.fLastFlareMsg + FHP.fFlareMsgCD)
               {
                  PlayerController(Instigator.Controller).ClientMessage(fThrowMaxMsg);
                  FHP.fLastFlareMsg = Level.TimeSeconds;
               }

               return False;
            }
         }
      }

      return True;
   }

   return False;
}


// switch the weapon after fire
function PlayFireEnd()
{
   local FHumanPawn FHP;

   if (Weapon.AmmoAmount(0) <= AmmoPerFire)
   {
      FHP = FHumanPawn(Instigator);

      if (FHP != none)
         FHP.FSwitchToBestWeapon();
   } 
}


defaultproperties
{
   ProjectileClass=Class'FlaresThrownFalk'
   AmmoClass=Class'FlareAmmoFalk'

   fThrowPlayerMsg="You've already thrown 3 flares, wait for one to burn out in order to throw more"
   fThrowMaxMsg="There are already 9 burning flares around, wait for one to burn out in order to throw more"

   fMaxPlayerFlares=3
   fMaxTotalFlares=9
}
