class BoomStickPickupFalk extends KFMod.BoomStickPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   Weight=9.000000
   InventoryType=Class'BoomStickFalk'
   ItemName="Stoeger Coach Gun"
   ItemShortName="SCG"
   PickupMessage="You got a Stoeger Coach Gun"
   cost=2500
   BuyClipSize=2
   AmmoCost=6
   PowerValue=100
   SpeedValue=15
   RangeValue=100
}
