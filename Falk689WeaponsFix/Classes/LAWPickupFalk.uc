class LAWPickupFalk extends KFMod.LAWPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'LAWFalk'
   ItemName="Light Antiarmor Weapon 80"
   ItemShortName="LAW80"
   PickupMessage="You got a Light Antiarmor Weapon 80"
   cost=3000
   BuyClipSize=1
   AmmoCost=20
   Weight=13.00000
   PowerValue=95
   SpeedValue=11
   RangeValue=100
}
