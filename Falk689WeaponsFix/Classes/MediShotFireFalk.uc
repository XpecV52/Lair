class MediShotFireFalk extends KFShotgunFire;

var byte BID;

simulated function bool AllowFire()
{
   if ((MediShotFalk(Weapon)      != None && MediShotFalk(Weapon).fLoadingLastBullet) ||
       (LocalMediShotFalk(Weapon) != None && LocalMediShotFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	if(KFWeapon(Weapon).bIsReloading && KFWeapon(Weapon).MagAmmoRemaining < 2)
		return false;

	if(KFPawn(Instigator).bThrowingNade)
		return false;

	if(Level.TimeSeconds - LastClickTime > FireRate)
		LastClickTime = Level.TimeSeconds;

	if(KFWeapon(Weapon).MagAmmoRemaining < 1)
    	return false;

	return super(WeaponFire).AllowFire();
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 210)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
     maxVerticalRecoilAngle=1500
     maxHorizontalRecoilAngle=900
     FireAimedAnim="Fire_Iron"
     StereoFireSound=SoundGroup'KF_PumpSGSnd.SG_FireST'
     bRandomPitchFireSound=False
     ProjPerFire=7
     bWaitForRelease=True
     bAttachSmokeEmitter=True
     TransientSoundVolume=2.000000
     TransientSoundRadius=500.000000
     FireAnimRate=0.950000
     FireSound=SoundGroup'KF_PumpSGSnd.SG_Fire'
     NoAmmoSound=Sound'KF_PumpSGSnd.SG_DryFire'
     FireRate=0.965000
     AmmoClass=Class'MediShotAmmoFalk'
     ShakeRotMag=(X=50.000000,Y=50.000000,Z=400.000000)
     ShakeRotRate=(X=12500.000000,Y=12500.000000,Z=12500.000000)
     ShakeRotTime=5.000000
     ShakeOffsetMag=(X=6.000000,Y=2.000000,Z=10.000000)
     ShakeOffsetRate=(X=1000.000000,Y=1000.000000,Z=1000.000000)
     ShakeOffsetTime=3.000000
     ProjectileClass=Class'MediShotBulletFalk'
     BotRefireRate=1.500000
     FlashEmitterClass=Class'ROEffects.MuzzleFlash1stKar'
     aimerror=1.000000
     Spread=1125.000000
}
