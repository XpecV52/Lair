class M99PickupFalk extends KFMod.M99Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'M99SniperRifleFalk'
   ItemName="Barrett M99 Sniper Rifle"
   ItemShortName="M99"
   PickupMessage="You got a Barrett M99 Sniper Rifle"
   cost=4000
   BuyClipSize=1
   AmmoCost=20
   Weight=13.0000
   PowerValue=100
   SpeedValue=1
   RangeValue=100
}