class AA12BulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   MomentumTransfer=30000.000000
   FDamage=30.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeAA12Falk'
}