class DamTypeMKb42AssaultRifleFalk extends KFMod.DamTypeMKb42AssaultRifle;

defaultproperties
{
    WeaponClass=Class'MKb42AssaultRifleFalk'
    DeathString="%k killed %o (StG44)"
	HeadShotDamageMult=1.100000
}