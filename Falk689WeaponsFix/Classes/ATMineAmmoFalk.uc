class ATMineAmmoFalk extends ATMine.ATMineAmmo;

defaultproperties
{
   MaxAmmo=2
   InitialAmount=1
   AmmoPickupAmount=1
   PickupClass=Class'ATMineAmmoPickupFalk'
}