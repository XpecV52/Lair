class WinchesterAmmoFalk extends KFMod.WinchesterAmmo;

defaultproperties
{
    MaxAmmo=64
    InitialAmount=24
    AmmoPickupAmount=8
    PickupClass=Class'WinchesterAmmoPickupFalk'
    ItemName=".44 rounds"
}