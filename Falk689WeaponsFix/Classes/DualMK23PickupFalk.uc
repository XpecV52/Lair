class DualMK23PickupFalk extends KFMod.DualMK23Pickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=500
   BuyClipSize=24
   PowerValue=16
   SpeedValue=49
   RangeValue=100
   AmmoCost=24
   ItemName="Dual Heckler & Koch MK23s"
   ItemShortName="MK23s"
   AmmoItemName=".45 ACP rounds"
   InventoryType=Class'DualMK23PistolFalk'
   PickupMessage="You found another Heckler & Koch MK23"
}
