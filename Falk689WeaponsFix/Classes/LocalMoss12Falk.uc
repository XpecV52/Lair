class LocalMoss12Falk extends LocalMoss12BaseFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// attempt to see the whole fockin reload animation in server
exec function ReloadMeNow()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   Super.ReloadMeNow();
}

// attempt to see the whole fockin reload animation
simulated function ClientReload()
{
   if (MagAmmoRemaining <= 0)
      fShouldBreechReload = True;

   ReloadRate = Default.ReloadRate;

   Super.ClientReload();
}

// second attempt to see the whole fockin reload animation
simulated function AddReloadedAmmo()
{
   if (AmmoAmount(0) > 0)
      ++MagAmmoRemaining;

   // last bullet to load, don't interrupt or drop
   if (fShouldBreechReload && AmmoAmount(0) > MagCapacity - 1 && MagAmmoRemaining == MagCapacity - 1)
   {
      ReloadRate         = fLastBulletRate;
      bCanThrow          = False;
      fLoadingLastBullet = True;

   }

   // restore vanilla stuff
   else if (MagAmmoRemaining == MagCapacity)
   {
      bCanThrow           = True;
      fShouldBreechReload = False;
      fLoadingLastBullet  = False;
   }
}

// third attempt to see the whole fockin reload
simulated function bool InterruptReload()
{
   if (fLoadingLastBullet)
      return False;

   if (Super.InterruptReload())
   {
      fShouldBreechReload = False;
      bCanThrow           = True;

      return True;
   }

   return False;
}

// attempt to fix more and more weird shit happening with the reload
simulated function ClientInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ClientInterruptReload();
}

// more attempt to fix that reload on server
simulated function ServerInterruptReload()
{
   if (fLoadingLastBullet)
      return;

   fShouldBreechReload = False;
   bCanThrow           = True;
   Super.ServerInterruptReload();
}

// permit throwing while reloading, blocking it during the last bullet
simulated function bool CanThrow()
{
   if (bIsReloading && !fLoadingLastBullet)
   {
      InterruptReload();
      return True;
   }

   return Super.CanThrow();
}

// just don't
simulated function DoAutoSwitch(){}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

defaultproperties
{
   FireModeClass(0)=Class'Moss12FireFalk'
   FireModeClass(1)=Class'KFMod.NoFire'
   PickupClass=Class'LocalMoss12PickupFalk'
   Priority=5
   Weight=5.000000
   MagCapacity=6
   ItemName="Moss12 Shotgun"
   Description="A lightweight pump-action shotgun. Weaker stopping power than a Benelli M3. Don't tell Nathan we have it."
   ReloadAnimRate=0.9
   ReloadRate=0.69
   fLastBulletRate=1.7
   Mesh=SkeletalMesh'LairAnimations_A.Moss12Mesh'
   BringUpTime=0.44
   ReloadAnim="Reload"
   WeaponReloadAnim="Reload_Shotgun"
   HudImage=Texture'KF_Moss12.Trader_Moss12.Trader_Moss12_unselected'
   SelectedHudImage=Texture'KF_Moss12.Trader_Moss12.Trader_Moss12_selected'
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=65.000000
   bModeZeroCanDryFire=True
   SleeveNum=0
   TraderInfoTexture=Texture'KF_Moss12.Trader_Moss12.Trader_Moss12'
   PlayerIronSightFOV=70.000000
   ZoomedDisplayFOV=40.000000
   SelectAnim="Select"
   PutDownAnim="PutDown"
   SelectSound=Sound'KF_Moss12Snd.SG_Select'
   AIRating=0.600000
   CurrentRating=0.600000
   DisplayFOV=65.000000
   InventoryGroup=3
   GroupOffset=1
   PlayerViewOffset=(X=20.000000,Y=18.750000,Z=-7.500000)
   BobDamping=7.000000
   AttachmentClass=Class'Moss12Mut.Moss12Attachment'
   IconCoords=(X1=169,Y1=172,X2=245,Y2=208)
   Skins(0)=Combiner'KF_Moss12.Moss12.Moss12_2048x2048'
   TransientSoundVolume=1.000000
   bTorchEnabled=False
   fMaxTry=3
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
