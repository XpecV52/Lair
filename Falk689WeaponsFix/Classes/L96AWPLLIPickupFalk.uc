class L96AWPLLIPickupFalk extends L96AWPLLIPickupBaseFalk;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=3700
   BuyClipSize=5
   AmmoCost=15
   InventoryType=Class'L96AWPLLIFalk'
   ItemName="L96 Arctic Warfare Rifle"
   ItemShortName="L96 AWP"
   PickupMessage="You got an L96 Arctic Warfare Rifle"
   PowerValue=87
   SpeedValue=1
   RangeValue=100
}
