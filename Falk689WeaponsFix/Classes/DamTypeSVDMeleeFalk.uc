class DamTypeSVDMeleeFalk extends KFMod.DamTypeMelee;

defaultproperties
{
    HeadShotDamageMult=1.100000
    WeaponClass=Class'SVDFalk'
    KDamageImpulse=200.000000
    KDeathVel=20.000000
    KDeathUpKick=0.100000
}