class cz805bPickupFalk extends cz805SA.cz805bPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'cz805bFalk'
   cost=2750
   AmmoCost=13
   Weight=6.000000
   BuyClipSize=25
   ItemName="CZ 805 Bren A1"
   ItemShortName="CZ 805"
   PickupMessage="You got a CZ 805 Bren A1"
   PowerValue=47
   SpeedValue=50
   RangeValue=100
}