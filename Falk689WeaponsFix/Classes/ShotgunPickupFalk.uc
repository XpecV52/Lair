class ShotgunPickupFalk extends KFMod.ShotgunPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	ItemName="Benelli M3 Super 90 Shotgun"
	ItemShortName="Benelli M3"
	InventoryType=Class'ShotgunFalk'
	PickupMessage="You got a Benelli M3 Super 90 Shotgun"
	cost=500
	BuyClipSize=8
	AmmoCost=16
	PowerValue=49
    SpeedValue=30
    RangeValue=100
}
