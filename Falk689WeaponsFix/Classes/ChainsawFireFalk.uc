class ChainsawFireFalk extends KFMod.ChainsawFire;

var byte  BID; // bullet ID to prevent multiple hits on a single zed

state FireLoop
{
   function BeginState()
   {
      // pls stop
      if (Weapon.GetFireMode(1).IsFiring())
         StopFiring();

      NextFireTime = Level.TimeSeconds - 0.1; //fire now!

      Weapon.LoopAnim(FireLoopAnim, FireLoopAnimRate, TweenTime);
      Weapon.PlayOwnedSound(FireStartSound,SLOT_Interact,AmbientFireVolume/127,,AmbientFireSoundRadius,,false);

      SetTimer(Weapon.GetSoundDuration(FireStartSound) / (1.1/Weapon.Level.TimeDilation), False);
   }

   function Timer()
   {
      PlayAmbientSound(AmbientFireSound);
   }

   // Overriden because we play an anbient fire sound
   function PlayFiring() {}
   function ServerPlayFiring() {}

   function EndState()
   {
      Weapon.AnimStopLooping();
      PlayAmbientSound(none);
      Weapon.PlayOwnedSound(FireEndSound,SLOT_Interact,AmbientFireVolume/127,,AmbientFireSoundRadius,,false);
      SetTimer(Weapon.GetSoundDuration(FireEndSound) / (1.1/Weapon.Level.TimeDilation), False);
      Weapon.StopFire(ThisModeNum);
   }

   function StopFiring()
   {
      GotoState('');
   }

   function ModeTick(float dt)
   {
      Super.ModeTick(dt);

      if ( !bIsFiring ||  !AllowFire()  )  // stopped firing, magazine empty
      {
         GotoState('');
         return;
      }
   }
}


function DoFireEffect()
{
   local KFMeleeGun kf;
   local Actor HitActor;
   local vector StartTrace, EndTrace, HitLocation, HitNormal;
   local rotator PointRot;
   local int fDamage;
   local bool bBackStabbed;

   BID++;

   if (BID > 200)
      BID = 1;

   fDamage = MeleeDamage;

   if(KFMeleeGun(Weapon) == none)
      return;

   kf = KFMeleeGun(Weapon);	

   If( !KFWeapon(Weapon).bNoHit )
   {
      StartTrace = Instigator.Location + Instigator.EyePosition();

      if( Instigator.Controller!=None && PlayerController(Instigator.Controller)==None && Instigator.Controller.Enemy!=None )
         PointRot = rotator(Instigator.Controller.Enemy.Location-StartTrace); // Give aimbot for bots.

      else
         PointRot = Instigator.GetViewRotation();

      EndTrace = StartTrace + vector(PointRot)*weaponRange;
      HitActor = Instigator.Trace( HitLocation, HitNormal, EndTrace, StartTrace, true);

      if (HitActor!=None)
      {
         //ImpactShakeView();

         if( HitActor.IsA('ExtendedZCollision') && HitActor.Base != none && HitActor.Base.IsA('KFMonster') )
            HitActor = HitActor.Base;

         if ( (HitActor.IsA('KFMonster') || HitActor.IsA('KFHumanPawn')) && KFMeleeGun(Weapon).BloodyMaterial!=none )
         {
            Weapon.Skins[KFMeleeGun(Weapon).BloodSkinSwitchArray] = KFMeleeGun(Weapon).BloodyMaterial;
            Weapon.texture = Weapon.default.Texture;
         }

         if( Level.NetMode==NM_Client )
            Return;

         if( HitActor.IsA('Pawn') && !HitActor.IsA('Vehicle')
               && (Normal(HitActor.Location-Instigator.Location) Dot vector(HitActor.Rotation))>0 )
         {
            bBackStabbed = true;
            fDamage*=2; // Backstab >:P
         }

         if( (KFMonster(HitActor)!=none) )
         {
            //	log(VSize(Instigator.Velocity));

            KFMonster(HitActor).bBackstabbed = bBackStabbed;

            HitActor.TakeDamage(fDamage, Instigator, HitLocation, vector(PointRot), hitDamageClass, BID);

            if(MeleeHitSounds.Length > 0)
               Weapon.PlaySound(MeleeHitSounds[Rand(MeleeHitSounds.length)],SLOT_None,MeleeHitVolume,,,,false);

            if(VSize(Instigator.Velocity) > 300 && KFMonster(HitActor).Mass <= Instigator.Mass)
               KFMonster(HitActor).FlipOver();

         }

         else
         {
            HitActor.TakeDamage(fDamage, Instigator, HitLocation, vector(PointRot), hitDamageClass, BID) ;

            if (KFWeaponAttachment(Weapon.ThirdPersonActor)!=None)
               KFWeaponAttachment(Weapon.ThirdPersonActor).UpdateHit(HitActor,HitLocation,HitNormal);
         }
      }
   }
}

event ModeDoFire()
{
	local float Rec;

	if( AllowFire() && IsInState('FireLoop'))
	{
    	Rec = GetFireSpeed();

    	FireRate = default.FireRate/Rec;
    	FireAnimRate = default.FireAnimRate*Rec;
    	ReloadAnimRate = default.ReloadAnimRate*Rec;

    	/*if (MaxHoldTime > 0.0)
    		HoldTime = FMin(HoldTime, MaxHoldTime);*/

    	// server
    	if (Weapon.Role == ROLE_Authority)
    	{
    		Weapon.ConsumeAmmo(ThisModeNum, Load);
    		DoFireEffect();

    		HoldTime = 0;   // if bot decides to stop firing, HoldTime must be reset first

    		if ((Instigator == None) || (Instigator.Controller == None))
    			return;

    		if ( AIController(Instigator.Controller) != None )
    			AIController(Instigator.Controller).WeaponFireAgain(BotRefireRate, true);

    		Instigator.DeactivateSpawnProtection();
    	}

    	// client
    	if (Instigator.IsLocallyControlled())
    	{
    		ShakeView();
    		PlayFiring();
    		FlashMuzzleFlash();
    		StartMuzzleSmoke();
    		ClientPlayForceFeedback(FireForce);
    	}
    	else // server
    		ServerPlayFiring();

    	Weapon.IncrementFlashCount(ThisModeNum);

    	// set the next firing time. must be careful here so client and server do not get out of sync
    	if (bFireOnRelease)
    	{
    		/*if (bIsFiring)
    			NextFireTime += MaxHoldTime + FireRate;
    		else*/
    			NextFireTime = Level.TimeSeconds + FireRate;
    	}
    	else
    	{
    		NextFireTime += FireRate;
    		NextFireTime = FMax(NextFireTime, Level.TimeSeconds);
    	}

    	Load = AmmoPerFire;
    	HoldTime = 0;

    	if (Instigator.PendingWeapon != Weapon && Instigator.PendingWeapon != None)
    	{
    		bIsFiring = false;
    		Weapon.PutDown();
    	}

    	if (Weapon.Owner != none && Weapon.Owner.Physics != PHYS_Falling)
    	{
         Weapon.Owner.Velocity.x *= KFMeleeGun(Weapon).ChopSlowRate;
        	Weapon.Owner.Velocity.y *= KFMeleeGun(Weapon).ChopSlowRate;
    	}
	}
}

// Kind of don't interrupt alt fire pls
function StartFiring()
{
   if (!AllowFire())
      return;

   GotoState('FireLoop');
}

defaultproperties
{
   AmbientFireVolume=204
   MeleeHitVolume=0.500000
   TransientSoundVolume=1.600000
   hitDamageClass=Class'DamTypeChainsawFalk'
   FireRate=0.100000
   MeleeDamage=28
   weaponrange=75.000000
}
