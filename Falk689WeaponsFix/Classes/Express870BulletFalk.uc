class Express870BulletFalk extends ShotgunBulletFalk;

defaultproperties
{
   FDamage=39.000000
   MaxPenetrations=2
   PenDamageReduction=0.500000
   HeadShotDamageMult=1.100000
   MyDamageType=Class'DamTypeExpress870Falk'
}