class MedicBrowningFalk extends MP7MMedicGunFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (MedicBrowningPickupFalk(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         MedicBrowningPickupFalk(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      //if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}


// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   AmmoRegenRate=0.300000 // standard slow recharge rate for non-smg
   MagCapacity=8
   ReloadRate=1.960000
   ReloadAnim="Reload"
   ReloadAnimRate=1.000000
   WeaponReloadAnim="Reload_Single9mm"
   Weight=2.000000
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=60.000000
   bModeZeroCanDryFire=True
   TraderInfoTexture=Texture'LairTextures_T.CustomReskins.BrowningUnselect'
   bIsTier2Weapon=True
   MeshRef="LairAnimations_A.BrowningMesh"
   Mesh=SkeletalMesh'LairAnimations_A.BrowningMesh'
   HudImage=Texture'LairTextures_T.CustomReskins.BrowningUnselect'
   SelectedHudImage=Texture'LairTextures_T.CustomReskins.BrowningSelect'
   HudImageRef="LairTextures_T.CustomReskins.BrowningUnselect"
   SelectedHudImageRef="LairTextures_T.CustomReskins.BrowningSelect"
   ZoomedDisplayFOV=50.000000
   FireModeClass(0)=Class'MedicBrowningFireFalk'
   FireModeClass(1)=Class'MedicBrowningAltFireFalk'
   PutDownAnim="PutDown"
   SelectSound=Sound'KF_HandcannonSnd.50AE_Select'
   AIRating=0.450000
   CurrentRating=0.450000
   bShowChargingBar=True
   BringUpTime=0.730000
   Description="This Browning pistol is a semi-automatic handgun featuring a scarce ammo efficiency yet an amazing stopping power. This special version is modified to shoot healing darts and features a magazine of eight rounds. Compared to other medical guns, this one features a slower recharge rate in terms of medical darts, a slower fire rate, and is less effective. Very fast reload, but watch out for the expensive ammo."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=60.000000
   Priority=35
   InventoryGroup=2
   GroupOffset=3
   PickupClass=Class'MedicBrowningPickupFalk'
   PlayerViewOffset=(X=25.000000,Y=18.750000,Z=-7.00000)
   BobDamping=6.000000
   AttachmentClass=Class'MedicBrowningAttachmentFalk'
   IconCoords=(X1=250,Y1=110,X2=330,Y2=145)
   ItemName="Medical Browning High-Power"
   bUseDynamicLights=True
   Skins(0)="LairTextures_T.CustomReskins.Browning1"
   Skins(1)="LairTextures_T.CustomReskins.Browning2"
   SkinRefs(0)="LairTextures_T.CustomReskins.Browning1"
   SkinRefs(1)="LairTextures_T.CustomReskins.Browning2"
   Sleevenum=2
   TransientSoundVolume=1.000000
   FHUDGreyThreshold=35
}
