class LocalTrenchgunPickupFalk extends KFMod.TrenchgunPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
	Weight=7.000000
	cost=1250
	BuyClipSize=6
	AmmoCost=12
	PowerValue=56
   SpeedValue=10
   RangeValue=100
	ItemName="Gentleman's Disclosure"
	ItemShortName="Disclosure"
	AmmoItemName="Dragon's breath shells"
	InventoryType=Class'LocalTrenchgunFalk'
	PickupMessage="You got a Gentleman's Disclosure"
	StaticMesh=StaticMesh'LairStaticMeshes_SM.CustomReskins.SteampunkTrenchgunPickup'
}
