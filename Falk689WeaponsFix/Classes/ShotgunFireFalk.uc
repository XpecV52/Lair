class ShotgunFireFalk extends KFMod.ShotgunFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((ShotgunFalk(Weapon)      != None && ShotgunFalk(Weapon).fLoadingLastBullet) ||
       (LocalShotgunFalk(Weapon) != None && LocalShotgunFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 210)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   ProjectileClass=Class'ShotgunBulletFalk'
   AmmoClass=Class'ShotgunAmmoFalk'
   Spread=1125.00000
   FireRate=0.965
}
