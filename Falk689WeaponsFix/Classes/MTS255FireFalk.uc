class MTS255FireFalk extends MTS255.MTS255Fire;

var byte BID;

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 210)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}


// kffire function to prevent interrupting the reload
simulated function bool AllowFire()
{
	if (KFWeapon(Weapon).bIsReloading)
		return false;

	if (KFPawn(Instigator).SecondaryItem!=none)
		return false;

	if (KFPawn(Instigator).bThrowingNade)
		return false;

	if (KFWeapon(Weapon).MagAmmoRemaining < 1)
	{
    	if( Level.TimeSeconds - LastClickTime>FireRate )
    	{
    		LastClickTime = Level.TimeSeconds;
    	}

		if( AIController(Instigator.Controller)!=None )
			KFWeapon(Weapon).ReloadMeNow();
		return false;
	}

	return Super(WeaponFire).AllowFire();
}


defaultproperties
{
   ProjectileClass=Class'MTS255BulletFalk'
   FireRate=0.250000
   AmmoClass=Class'MTS255AmmoFalk'
   ProjPerFire=7
   Spread=1125.000000
}
