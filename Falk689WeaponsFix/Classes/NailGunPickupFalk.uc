class NailGunPickupFalk extends KFMod.NailGunPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   InventoryType=Class'NailGunFalk'
   ItemName="Vlad 9000 The Impaler"
   ItemShortName="Vlad"
   PickupMessage="You got a Vlad 9000 The Impaler"
   cost=1200
   BuyClipSize=6
   AmmoCost=12
   PowerValue=49
   SpeedValue=50
   RangeValue=100
}