class DamTypeXMV850Falk extends DamTypeSW76Falk;

defaultproperties
{
   WeaponClass=Class'XMV850Falk'
   bCheckForHeadShots=True
   HeadShotDamageMult=1.100000
   DeathString="%k killed %o (XMV850)"
   KDamageImpulse=5250.000000
   KDeathVel=425.000000
   KDeathUpKick=45.000000
}
