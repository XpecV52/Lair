class XMV850AmmoFalk extends XMV850.XMV850Ammo;

defaultproperties
{
   MaxAmmo=600
   InitialAmount=200
   AmmoPickupAmount=50 // one quarter of magazine, heavy weapons standard
   PickupClass=Class'XMV850AmmoPickupFalk'
}