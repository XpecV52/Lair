class BaseballBatPickupFalk extends BaseballBat.BaseballBatPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=4.000000
   cost=1350
   InventoryType=Class'BaseballBatFalk'
   ItemName="Nailed Baseball Bat"
   ItemShortName="Nailbat"
   PickupMessage="You got a Nailed Baseball Bat"
   PowerValue=45
   SpeedValue=70
   RangeValue=42
   DrawScale=1.250000
}
