class PipeBombFireFalk extends KFMod.PipeBombFire;

var byte BID;

// implement our switch on timer since it should kind of work
function Timer()
{
   local FHumanPawn FHP;

   Weapon.ConsumeAmmo(ThisModeNum, Load);
   DoFireEffect();
   Weapon.PlaySound(Sound'KF_AxeSnd.Axe_Fire',SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,,false);

   if (Weapon.AmmoAmount(0) <= 0 && Instigator != none)
   {
      FHP = FHumanPawn(Instigator);

      if (FHP != none)
         FHP.FSwitchToBestWeapon();

      Weapon.Destroy();
   }
}

// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID += 4;

   if (BID > 200)
      BID = BID - 200;

   class<PipeBombProjectileFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator, damage modifiers and other perk settings
function PostSpawnProjectile(Projectile P)
{
   local PipeBombProjectileFalk FG;
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes> Vet;

   FG = PipeBombProjectileFalk(P);

   if (FG != none && Instigator != none)
      FG.fCInstigator = Instigator.Controller;

   KFPRI       = KFPlayerReplicationInfo(Weapon.Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      if (FG != none)
      { 
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none)
         {
            FG.fStoredKaboom = Vet.Static.DetonateBulletsControlBonus(KFPRI, True);
            FG.fDamageMulti  = float(Vet.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), 1000, P.MyDamageType)) / 1000.0;
            P.Damage        *= FG.fDamageMulti;
         }
 
         //warn("NEW:"@P.Damage@"- MULTI:"@FG.fDamageMulti@"- FStoredKaboom:"@FG.fStoredKaboom@Vet.Static.DetonateBulletsControlBonus(KFPRI, True)@"- DamType:"@P.MyDamageType);
      }

      else
      {
         P.Damage = KFPRI.ClientVeteranSkill.Static.AddDamage(KFPRI, None, KFPawn(Weapon.Instigator), P.Damage, P.MyDamageType);
         //warn("FALLBACK:"@P.DAmage);
      }

      //warn("Damage:"@P.Damage);
   }

   Super.PostSpawnProjectile(P);
}

// removed some weird shit
simulated function bool AllowFire()
{
   if (KFPawn(Instigator).SecondaryItem != none)
      return false;

   if (KFPawn(Instigator).bThrowingNade)
      return false;

   if (Weapon != none && PipeBombExplosiveFalk(Weapon) != none && PipeBombExplosiveFalk(Weapon).fAmmoPickupTime > 0)
      return false;

   return super(WeaponFire).AllowFire();
}


// last fire anim when out of ammo
function PlayFiring()
{
   local float RandPitch;

   if (Weapon.Mesh != None)
      Weapon.PlayAnim(FireAnim, FireAnimRate, TweenTime);

   if (Weapon.Instigator != none && Weapon.Instigator.IsLocallyControlled() &&
       Weapon.Instigator.IsFirstPerson() && StereoFireSound != none)
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(StereoFireSound,SLOT_Interact,TransientSoundVolume * 0.85,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   else
   {
      if (bRandomPitchFireSound)
      {
         RandPitch = FRand() * RandomPitchAdjustAmt;

         if (FRand() < 0.5)
            RandPitch *= -1.0;
      }

      Weapon.PlayOwnedSound(FireSound,SLOT_Interact,TransientSoundVolume,,TransientSoundRadius,(1.0 + RandPitch),false);
   }

   ClientPlayForceFeedback(FireForce);  // jdf

   FireCount++;
}

// just spawn this ammo, too late to stop now
event ModeDoFire()
{
    /*if (!AllowFire())
        return;*/

    if (MaxHoldTime > 0.0)
        HoldTime = FMin(HoldTime, MaxHoldTime);

    // server
    if (Weapon.Role == ROLE_Authority)
    {
        // Consume ammo, etc later
        SetTimer(ProjectileSpawnDelay, False);

		HoldTime = 0;	// if bot decides to stop firing, HoldTime must be reset first
        if ( (Instigator == None) || (Instigator.Controller == None) )
			return;

        if ( AIController(Instigator.Controller) != None )
            AIController(Instigator.Controller).WeaponFireAgain(BotRefireRate, true);

        Instigator.DeactivateSpawnProtection();
    }

    // client
    if (Instigator.IsLocallyControlled())
    {
        //ShakeView();
        PlayFiring();
        FlashMuzzleFlash();
        StartMuzzleSmoke();
    }
    else // server
    {
        ServerPlayFiring();
    }

    Weapon.IncrementFlashCount(ThisModeNum);

    // set the next firing time. must be careful here so client and server do not get out of sync
    if (bFireOnRelease)
    {
        if (bIsFiring)
            NextFireTime += MaxHoldTime + FireRate;
        else
            NextFireTime = Level.TimeSeconds + FireRate;
    }
    else
    {
        NextFireTime += FireRate;
        NextFireTime = FMax(NextFireTime, Level.TimeSeconds);
    }

    Load = AmmoPerFire;
    HoldTime = 0;

    if (Instigator.PendingWeapon != Weapon && Instigator.PendingWeapon != None)
    {
        bIsFiring = false;
        Weapon.PutDown();
    }
}

defaultproperties
{
   ProjectileClass=Class'PipeBombProjectileFalk'
   ProjectileSpawnDelay=0.75
   FireRate=1.15
   FireAnimRate=1.52
   FireAnim="Toss"
   bRandomPitchFireSound=False
   ProjPerFire=1
   ProjSpawnOffset=(X=10.000000,Y=0.000000,Z=-40.000000)
   bWaitForRelease=True
   TransientSoundVolume=2.000000
   TransientSoundRadius=500.000000
   NoAmmoSound=None
   AmmoClass=Class'PipeBombAmmoFalk'
   BotRefireRate=0.250000
   aimerror=1.000000
   Spread=15.000000
}
