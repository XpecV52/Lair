class MAC10AmmoFalk extends KFMod.MAC10Ammo;

defaultproperties
{
    MaxAmmo=210
	InitialAmount=90
	AmmoPickupAmount=30
	PickupClass=Class'MAC10AmmoPickupFalk'
	ItemName=".45 ACP rounds"
}