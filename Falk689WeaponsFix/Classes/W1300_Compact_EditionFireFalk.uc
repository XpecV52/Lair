class W1300_Compact_EditionFireFalk extends W1300_Compact_Edition.W1300_Compact_EditionFire;

var byte BID;

// attempt to see the whole fockin reload anim, step dunno
simulated function bool AllowFire()
{
   if ((W1300_Compact_EditionFalk(Weapon)      != None && W1300_Compact_EditionFalk(Weapon).fLoadingLastBullet) ||
       (LocalW1300_Compact_EditionFalk(Weapon) != None && LocalW1300_Compact_EditionFalk(Weapon).fLoadingLastBullet))
   {
      return False;
   }

	return Super.AllowFire();
}

// Added zed time support skills
function DoFireEffect()
{
   local KFPlayerReplicationInfo KFPRI;
   local class<FVeterancyTypes>   Vet;

   KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

   if (KFPRI != none && KFPRI.ClientVeteranSkill != none)
   {
      Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

      if (Vet != none)
      {
         ProjPerFire = Vet.Static.GetShotgunProjPerFire(KFPRI, Default.ProjPerFire, Weapon);
         Spread      = Default.Spread * Vet.Static.GetShotgunSpreadMultiplier(KFPRI, Weapon);
      }
   }

	// if wide spread is on give us some additional spread
   if (W1300_Compact_EditionFalk(Weapon) != none && W1300_Compact_EditionFalk(Weapon).bWideSpread)
	   Spread *= 2.00;

   Super.DoFireEffect();
}


// alternative point blank fix
function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
   BID++;

   if (BID > 200)
      BID = 1;

   class<ShotgunBulletFalk>(ProjectileClass).default.BulletID = BID;

   return Super.SpawnProjectile(Start, Dir);
}

// early setting fCInsigator
function PostSpawnProjectile(Projectile P)
{
   local ShotgunBulletFalk FB;

   FB = ShotgunBulletFalk(P);

   if (FB != none && Instigator != none)
      FB.fCInstigator = Instigator.Controller;

   Super.PostSpawnProjectile(P);
}

defaultproperties
{
   AmmoClass=Class'W1300_Compact_EditionAmmoFalk'
   FireRate=0.965000
   ProjPerFire=9
   ProjectileClass=Class'W1300_Compact_EditionBulletFalk'
}
