class M79AmmoFalk extends KFMod.M79Ammo;

defaultproperties
{
    MaxAmmo=24
    InitialAmount=12
    AmmoPickupAmount=4
    PickupClass=Class'M79AmmoPickupFalk'
    ItemName="40x46mm grenades"
}
