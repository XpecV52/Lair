class DamTypeDualMK23PistolFalk extends DamTypeMK23PistolFalk;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FSharpshooterDamage', Amount);
}

defaultproperties
{
   WeaponClass=Class'DualMK23PistolFalk'
   DeathString="ÿ%k killed %o (MK23s)"
   HeadShotDamageMult=1.100000
}