class Dual44MagnumAmmoFalk extends KFAmmunition;

defaultproperties
{
   AmmoPickupAmount=12
   MaxAmmo=90
   InitialAmount=84
   PickupClass=Class'Dual44MagnumFalk'
   IconMaterial=Texture'KillingFloorHUD.Generic.HUD'
   IconCoords=(X1=338,Y1=40,X2=393,Y2=79)
   ItemName="44 Magnum bullets"
}
