class MenogPickupFalk extends Menog.MenogPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   Weight=8.000000
   cost=2200
   Description="Menog mace from the game 'Prince of Persia: Warrior Within'. While it's secondary attack is as lethal as the Dwarven Axe one, its primary is slightly quicker and more powerful."
   InventoryType=Class'MenogFalk'
   ItemName="Menog Mace"
   ItemShortName="Menog"
   PickupMessage="You got a Menog Mace"
   PowerValue=73
   SpeedValue=35
   RangeValue=40
}
