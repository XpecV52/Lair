class FPatHPLeft extends Mutator;

function PostBeginPlay()
{
	SetTimer(1.0, True);
}

function Timer()
{
	if (Level.Game.IsInState('MatchOver'))
   {
		ShowHP();
		SetTimer(0.0, False);
	}
}

function ShowHP()
{
	local Controller C;
	local FalkZombieBossBase Boss;
	local string Msg;

	foreach DynamicActors(class'FalkZombieBossBase', Boss)
		break;

	if (Boss != None && Boss.Health > 0)
   {
		Msg = "Patriarch HP = "$Boss.Health;//$"/"$int(Boss.HealthMax)$" ("$(Boss.Health/Boss.HealthMax*100)$"%)";

		for (C = Level.ControllerList; C != None; C = C.NextController)
			if (PlayerController(C) != None)
				PlayerController(C).ClientMessage(Msg);
	}
}

defaultproperties
{
     bAddToServerPackages=True
     GroupName="KF-InfoMut"
     FriendlyName="Lair - Patriarch HP Left"
     Description="Shows the remaining health of the Patriarch if the squad wipes."
}
