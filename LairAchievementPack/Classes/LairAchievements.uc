class LairAchievements extends Falk689ServerAchievements.AchievementPackPartImpl;

var bool playedBossWave;
var bool fMatchHasEnded;

enum LairIndex 
{
   MEDIC_A,
   SUPPORT_A,
   SHARP_A,
   COMMANDO_A,
   ZERK_A,
   FIREBUG_A,
   DEMO_A,
   ARTI_A,
   KILL_A1,
   KILL_A2,
   KILL_A3,
   MAPS_A,
   MAPS_B,
   LEVEL_A
};


// start timer
function PostBeginPlay()
{
   SetTimer(1.0, false);
}

// kill achievements
event killedMonster(Pawn target, class<DamageType> damageType, bool headshot) 
{
   addProgress(LairIndex.KILL_A1, 1);
   addProgress(LairIndex.KILL_A2, 1);
   addProgress(LairIndex.KILL_A3, 1);

   // Medic
   if (damageType == class'DamTypeRU556Falk' && FalkZombieCrawlerBase(target) != none)
      addProgress(LairIndex.MEDIC_A, 1);

   // Support
   else if ((damageType == class'DamTypeW1300_Compact_EditionFalk' || damageType == class'DamTypeW1300BleedOutFalk') && FalkZombieBloatBase(target) != none)
      addProgress(LairIndex.SUPPORT_A, 1);
   
   // Sharp
   else if (damageType == class'DamTypeWalter2000SAFalk' && headshot && FalkMonster(target) != none && FalkMonster(target).bZapped)
      addProgress(LairIndex.SHARP_A, 1);

   // Commando
   else if (damageType == class'DamTypeAK12LLIFalk' && FalkZombieSirenBase(target) != none)
      addProgress(LairIndex.COMMANDO_A, 1);

   // Zerk
   else if (damageType == class'DamTypeAngelicFalk' && FalkZombieGorefastBase(target) != none && headshot)
      addProgress(LairIndex.ZERK_A, 1);

   // Firebug
   else if ((damageType == class'DamTypeSpitfireFalk'                   ||
            (damageType == class'DamTypeBurnWeakFalk'                   && 
             ownerController     != none                                &&
             FalkMonster(target) != none                                &&
             FalkMonster(target).fSpitFire                              &&
             FalkMonster(target).fSpitFiredBy == ownerController.Pawn)) &&
             FalkZombieScrakeBase(target) != none)
   {
      addProgress(LairIndex.FIREBUG_A, 1);
   }

   // Demo
   else if ((damageType == class'DamTypeLAWFalk' || damageType == class'DamTypeLAWExplosiveBulletFalk') &&
             FalkZombieFleshpoundBase(target) != none)
   {
      addProgress(LairIndex.DEMO_A, 1);
   }

   // Artillerist
   if (FalkZombieHuskBase(target) != none     &&
       FalkZombieHuskBase(target).fFrozen     &&
       damageType == class'DamTypeM60Falk')
   {
      addProgress(LairIndex.ARTI_A, 1);
   }
}

// survive achievement
event matchEnd(string mapname, float difficulty, int length, byte result, int waveNum)
{ 
    if (!fMatchHasEnded && ownerController != none && playedBossWave && result == 2 && !ownerController.IsDead() && ownerController.Pawn != none)
    {
         fMatchHasEnded = True;

         addProgress(LairIndex.MAPS_A, 1);

         if (difficulty >= 8.0)
            addProgress(LairIndex.MAPS_B, 1);
    }
}

// setting true if spawned on end wave
event waveStart(int waveNum)
{
    if (ownerController != none && !ownerController.PlayerReplicationInfo.bOnlySpectator &&
        !ownerController.IsDead() && ownerController.Pawn != none)
    {
        if (waveNum == KFGameType(Level.Game).FinalWave + 1)
            playedBossWave = true;
    }
}

// setting false if died on end wave
event playerDied(Controller killer, class<DamageType> damageType, int waveNum)
{
   playedBossWave = false;
}

// setting unlock states in our list
event waveEnd(int waveNum)
{
   local FHumanPawn FP;
   local int i;

   if (ownerController != none)
   {
      FP = FHumanPawn(ownerController.Pawn);

      if (FP != none)
      {
         for (i=0; i<achievements.Length; i++)
         {
            if (achievements[i].completed > 0)
               FP.SetUnlocked(i, True);
         }
      }

      else if (ownerController.IsDead() || FP == none)
         playedBossWave = false;
   }
}

// other achievements
function Timer()
{ 
   local KFPlayerReplicationInfo KFPRI;

   if (ownerController != none)
   {
      KFPRI = KFPlayerReplicationInfo(ownerController.PlayerReplicationInfo);

      if (KFPRI != none && KFPRI.ClientVeteranSkillLevel >= 20)
      {
         achievementCompleted(LairIndex.LEVEL_A);
      }

      SetTimer(1.0, false);
   }
}



defaultproperties
{
   packName="Lair Achievements"
   achievements(0)=(title="Hypocritical Oath",description="Kill 175 crawlers with an SR556-M to unlock the LR300",maxProgress=175,notifyIncrement=0.20)
   achievements(1)=(title="That's Just Nasty",description="Kill 100 bloats with a W1300 to unlock the KS23",maxProgress=100,notifyIncrement=0.20)
   achievements(2)=(title="Slow Down You Assholes",description="Kill 100 zapped zeds with headshots using a WA2000 to unlock the L96 AWP",maxProgress=100,notifyIncrement=0.20)
   achievements(3)=(title="Sound Of Sirens",description="Kill 50 sirens with a Silenced AK12 to unlock the AAR525",maxProgress=50,notifyIncrement=0.34)
   achievements(4)=(title="An Even Greater One",description="Decapitate 150 gorefasts using the Angelic to unlock the Caius's GS",maxProgress=150,notifyIncrement=0.34)
   achievements(5)=(title="I Didn't Enrage It",description="Burn 15 scrakes to death with the Spitfire to unlock the AFS525",maxProgress=15,notifyIncrement=0.34)
   achievements(6)=(title="LAW Of The Lair",description="Blow 10 fleshpounds up using the LAW80 to unlock the Explosive AA12",maxProgress=10,notifyIncrement=0.50)
   achievements(7)=(title="You Husked For It",description="Kill 20 frozen husks with an M60 to unlock the XMV850",maxProgress=20,notifyIncrement=0.25)
   achievements(8)=(title="Quarantine Failure",description="Annihilate 20,000 specimens",maxProgress=20000,notifyIncrement=0.10)
   achievements(9)=(title="Unstoppable Pandemonium",description="Annihilate 50,000 specimens",maxProgress=50000,notifyIncrement=0.10)
   achievements(10)=(title="Worldwide Apocalypse",description="Annihilate 99,999 specimens",maxProgress=99999,notifyIncrement=0.10)
   achievements(11)=(title="Bloody Millwall Fan",description="Outlive the Patriarch 20 times",maxProgress=20,notifyIncrement=0.05)
   achievements(12)=(title="I Thought You Were The Best",description="Outlive the Patriarch on Bloodbath difficulty",maxProgress=1,notifyIncrement=1.0)
   achievements(13)=(title="Superior Genetics",description="Reach level 20 with any perk")
}
