class FVetSharpshooter extends VeterancyTypesFalk;

static function AddCustomStats( ClientPerkRepLink Other )
{
   // Needed for custom sharpshooter damage progress
   Other.AddCustomValue(Class'Falk689WeaponsFix.FSharpshooterDamage');   
}

static function int GetPerkProgressInt(ClientPerkRepLink StatOther, out int FinalInt, byte CurLevel, byte ReqNum)
{
   switch(CurLevel)
   {
   case 0:
      if(ReqNum == 0)
         FinalInt = 1;         
      else
         FinalInt = 1;
      break;
         
   case 1:
      if(ReqNum == 0)
         FinalInt = 5;
      else
         FinalInt = 100;
      break;
         
   case 2:
      if(ReqNum == 0)
         FinalInt = 10;
      else
         FinalInt = 1000;
      break;

   case 3:
      if(ReqNum == 0)
         FinalInt = 30;
      else
         FinalInt = 3500;
      break;  
      
   case 4:
      if(ReqNum == 0)
         FinalInt = 85;
      else
         FinalInt = 8000;
      break;
         
   case 5:
      if(ReqNum == 0)
         FinalInt = 150;
      else
         FinalInt = 15000;
      break;
      
   case 6:
      if(ReqNum == 0)
         FinalInt = 250;
      else
         FinalInt = 30000;
      break;
      
   case 7:
      if(ReqNum == 0)
         FinalInt = 400;
      else
         FinalInt = 70000;
      break;
      
   case 8:
      if(ReqNum == 0)
         FinalInt = 600;
      else
         FinalInt = 150000;
      break;
      
   case 9:
      if(ReqNum == 0)
         FinalInt = 850;
      else
         FinalInt = 300000;
      break;
      
   case 10:
      if(ReqNum == 0)
         FinalInt = 1200;
      else
         FinalInt = 500000;
      break;
      
   case 11:
      if(ReqNum == 0)
         FinalInt = 1700;
      else
         FinalInt = 750000;
      break;
         
   case 12:
      if(ReqNum == 0)
         FinalInt = 2500;
      else
         FinalInt = 1100000;
      break; 
         
   case 13:
      if(ReqNum == 0)
         FinalInt = 3500;
      else
         FinalInt = 1500000;
      break; 
         
   case 14:
      if(ReqNum == 0)
         FinalInt = 4700;
      else
         FinalInt = 2200000;
      break; 
         
   case 15:
      if(ReqNum == 0)
         FinalInt = 6000;
      else
         FinalInt = 3000000;
      break; 
         
   case 16:
      if(ReqNum == 0)
         FinalInt = 7500;
      else
         FinalInt = 4000000;
      break; 
         
   case 17:
      if(ReqNum == 0)
         FinalInt = 9200;
      else
         FinalInt = 5200000;
      break; 
         
   case 18:
      if(ReqNum == 0)
         FinalInt = 11000;
      else
         FinalInt = 6500000;
      break; 
         
   case 19:
      if(ReqNum == 0)
         FinalInt = 13000;
      else
         FinalInt = 8000000;
      break; 
         
   case 20:
      if(ReqNum == 0)
         FinalInt = 15500;
      else
         FinalInt = 10000000;
      break;
		
   default:
      if(ReqNum == 0)
         FinalInt = 15500 + GetDoubleScaling(CurLevel, 350);
      else
         FinalInt = 10000000 + GetDoubleScaling(CurLevel, 500000);
      break;
   }
   
   if(ReqNum == 0)
      return Min(StatOther.RHeadshotKillsStat, FinalInt);
         
   return Min(StatOther.GetCustomValueInt(Class'Falk689WeaponsFix.FSharpshooterDamage'), FinalInt);
}


// add headshot damage
static function float GetHeadShotDamMulti(KFPlayerReplicationInfo KFPRI, KFPawn P, class<DamageType> DmgType)
{
   if (DmgType == class'DamTypeCrossbowFalk'         ||
       DmgType == class'DamTypeCrossbowHeadShotFalk' ||
       DmgType == class'DamTypeWinchesterFalk'       ||
	    DmgType == class'DamTypeDeagleFalk'           ||
	    DmgType == class'DamTypeDualDeagleFalk'       ||
	    DmgType == class'DamTypeMagnum44PistolFalk'   ||
	    DmgType == class'DamTypeDual44MagnumFalk'     ||
       DmgType == class'DamTypeMK23PistolFalk'       ||
       DmgType == class'DamTypeDualMK23PistolFalk'   ||
       DmgType == class'DamTypeM99SniperRifleFalk'   ||
       DmgType == class'DamTypeM14EBRFalk'           ||
       DmgType == class'DamTypeSPSniperFalk'         ||
	    DmgType == class'DamTypeDualiesFalk'          ||
	    DmgType == class'DamTypeWalter2000SAFalk'     ||
	    DmgType == class'DamTypeHuntingRifleFalk'     ||
       DmgType == class'DamTypeSVT40SAFalk'          ||
	    DmgType == class'DamTypeL96AWPLLIFalk'        ||
       DmgType == class'DamTypeSVDFalk')
   {
      // Zed Time headshot damage boost
      if (static.InZedTime(KFPRI))
      {
         if (KFPRI.ClientVeteranSkillLevel >= 20)
            return 1.95;
            
         if (KFPRI.ClientVeteranSkillLevel >= 6)
            return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.28; // normal scaling + zed time bonus
      }

      // Normal headshot damage multiplier
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.70;
            
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;
   }
   
   return 1.0;
}


// reduce recoil with sharp weapons
static function float ModifyRecoilSpread(KFPlayerReplicationInfo KFPRI, WeaponFire Other, out float Recoil)
{
	Recoil = 1.0;
	
	if (CrossbowFalk(Other.Weapon)           != none ||
	    WinchesterFalk(Other.Weapon)         != none ||
	    LocalWinchesterFalk(Other.Weapon)    != none ||
       SingleFalk(Other.Weapon)             != none || 
       DualiesFalk(Other.Weapon)            != none ||
       MK23PistolFalk(Other.Weapon)         != none ||
       DualMK23PistolFalk(Other.Weapon)     != none ||
       Magnum44PistolFalk(Other.Weapon)     != none ||
       Dual44MagnumFalk(Other.Weapon)       != none ||
       DeagleFalk(Other.Weapon)             != none ||
       DualDeagleFalk(Other.Weapon)         != none ||
	    M14EBRBattleRifleFalk(Other.Weapon)  != none ||
	    M99SniperRifleFalk(Other.Weapon)     != none ||
       SPSniperRifleFalk(Other.Weapon)      != none ||
       HuntingRifleFalk(Other.Weapon)       != none ||
       SVT40SAFalk(Other.Weapon)            != none ||
       SVDFalk(Other.Weapon)                != none ||
	    L96AWPLLIFalk(Other.Weapon)          != none ||
       Walter2000SAFalk(Other.Weapon)       != none)
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         Recoil -= 0.9;
      
      else
         Recoil -= (KFPRI.ClientVeteranSkillLevel + 1) * 0.04;
	}
	
	Return Recoil;
}


// Add damage to sharp weapons
static function int AddDamage(KFPlayerReplicationInfo KFPRI, KFMonster Injured, KFPawn DamageTaker, int InDamage, class<DamageType> DmgType)
{
   if (DmgType == class'DamTypeCrossbowFalk'         ||
       DmgType == class'DamTypeCrossbowHeadShotFalk' ||
       DmgType == class'DamTypeWinchesterFalk'       ||
	    DmgType == class'DamTypeDeagleFalk'           ||
       DmgType == class'DamTypeDualDeagleFalk'       ||
	    DmgType == class'DamTypeMagnum44PistolFalk'   ||
	    DmgType == class'DamTypeDual44MagnumFalk'     ||
	    DmgType == class'DamTypeMK23PistolFalk'       ||
	    DmgType == class'DamTypeDualMK23PistolFalk'   ||
       DmgType == class'DamTypeM99SniperRifleFalk'   ||
       DmgType == class'DamTypeM14EBRFalk'           ||
       DmgType == class'DamTypeSPSniperFalk'         ||
       DmgType == class'DamTypeDualiesFalk'          ||
       DmgType == class'DamTypeHuntingRifleFalk'     ||
       DmgType == class'DamTypeSVT40SAFalk'          ||
       DmgType == class'DamTypeSVDFalk'              ||
	   DmgType == class'DamTypeL96AWPLLIFalk'        ||
       DmgType == class'DamTypeWalter2000SAFalk')
   {
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return InDamage * 1.25;
	   
	   else if (KFPRI.ClientVeteranSkillLevel >= 15)
	      return InDamage * 1.20;
	   
	   else if (KFPRI.ClientVeteranSkillLevel >= 12)
	      return InDamage * 1.15;
	   
	   else if (KFPRI.ClientVeteranSkillLevel >= 9)
	      return InDamage * 1.12;
	   
	   else if (KFPRI.ClientVeteranSkillLevel >= 6)
	      return InDamage * 1.09;
	   
	   else if (KFPRI.ClientVeteranSkillLevel >= 3)
	      return InDamage * 1.06;
	   
	   return InDamage * 1.03;
   } 
   
   return Super.AddDamage(KFPRI, Injured, DamageTaker, InDamage, DmgType);
}

// Faster reload
static function float GetReloadSpeedModifier(KFPlayerReplicationInfo KFPRI, KFWeapon Other)
{
	if (CrossbowFalk(Other)           != none ||
	     WinchesterFalk(Other)        != none ||
	     LocalWinchesterFalk(Other)   != none ||
		  SingleFalk(Other)            != none ||
	     DualiesFalk(Other)           != none ||
        DeagleFalk(Other)            != none ||
        DualDeagleFalk(Other)        != none ||
        MK23PistolFalk(Other)        != none ||
        DualMK23PistolFalk(Other)    != none ||
        M14EBRBattleRifleFalk(Other) != none ||
        Magnum44PistolFalk(Other)    != none ||
        Dual44MagnumFalk(Other)      != none ||
        SPSniperRifleFalk(Other)     != none ||
        Walter2000SAFalk(Other)      != none ||
        HuntingRifleFalk(Other)      != none ||
        SVT40SAFalk(Other)           != none ||
		  L96AWPLLIFalk(Other)         != none ||
        SVDFalk(Other)               != none)
	{
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.65;
	   
      return 0.03 * KFPRI.ClientVeteranSkillLevel + 1.03;  
	}
	
	return 1.0;
}


// Faster fire rate on M1894 and SP SMLE
static function float GetFireSpeedMod(KFPlayerReplicationInfo KFPRI, Weapon Other)
{
   if (SPSniperRifleFalk(Other)     != none ||
	    WinchesterFalk(Other)        != none ||
	    LocalWinchesterFalk(Other)   != none ||
       CrossbowFalk(Other)          != none)
	{
      // no scaling here, just return the top value
	   if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.35;
	   
      // scaling for level 10 and higher
	   if (KFPRI.ClientVeteranSkillLevel > 9)
         return 0.02 * (KFPRI.ClientVeteranSkillLevel - 9) + 1.1;

      // normal scaling
      return 0.01 * KFPRI.ClientVeteranSkillLevel + 1.01;
	}
	
	return 1.0;
}

// Add plasma grenades
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
	return class'PlasmaNadeFalk';
}


// Reset Compound Crossbow ammo cost
static function float GetAmmoCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
	return 1.0;
}


// Add extra ammo for M48/L96 AWP/M99
static function float AddExtraAmmoFor(KFPlayerReplicationInfo KFPRI, Class<Ammunition> AmmoType)
{
   if (AmmoType == class'HuntingRifleAmmoFalk' || AmmoType == class'M99AmmoFalk' || AmmoType == class'L96AWPLLIAmmoFalk')
	{
      if (KFPRI.ClientVeteranSkillLevel >= 20)
         return 1.40;
         
      else if (KFPRI.ClientVeteranSkillLevel >= 15)
         return 1.20;
         
      else if (KFPRI.ClientVeteranSkillLevel >= 12)
         return 1.10;
   }
	
	return 1.0;
}


// Change the cost of sharp weapons
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   local float result;

   result = Super.GetCostScaling(KFPRI, Item);

   if (result > 1.0)
      return result;
   
   switch (KFPRI.ClientVeteranSkillLevel)
   {
      case 0:
         result = 0.97;
         break;
            
      case 1:
         result = 0.94;
         break;
            
      case 2:
         result = 0.91;
         break;
            
      case 3:
         result = 0.88;
         break;
            
      case 4:
         result = 0.85;
         break;
            
      case 5:
         result = 0.82;
         break;
            
      case 6:
         result = 0.79;
         break;
            
      case 7:
         result = 0.76;
         break;
            
      case 8:
         result = 0.73;
         break;
            
      case 9:
         result = 0.70;
         break;
            
      case 10:
         result = 0.67;
         break;
            
      case 11:
         result = 0.64;
         break;
            
      case 12:
         result = 0.60;
         break;

      case 13:
         result = 0.56;
         break;
            
      case 14:
         result = 0.52;
         break;

      case 15:
         result = 0.48;
         break;

      case 16:
         result = 0.44;
         break;

      case 17:
         result = 0.40;
         break;
            
      case 18:
         result = 0.36;
         break;
            
      case 19:
         result = 0.32;
         break;
            
      default:
         result = 0.25;
         break;
   }

	if (Item == class'CrossbowPickupFalk'         ||
       Item == class'SinglePickupFalk'           ||
       Item == class'DualiesPickupFalk'          ||
       Item == class'WinchesterPickupFalk'       ||
       Item == class'LocalWinchesterPickupFalk'  ||
       Item == class'DeaglePickupFalk'           ||
       Item == class'DualDeaglePickupFalk'       ||
       Item == class'MK23PickupFalk'             ||
       Item == class'DualMK23PickupFalk'         ||
       Item == class'M14EBRPickupFalk'           ||
       Item == class'Magnum44PickupFalk'         ||
       Item == class'Dual44MagnumPickupFalk'     ||
       Item == class'SPSniperPickupFalk'         ||
       Item == class'M99PickupFalk'              ||
       Item == class'Walter2000SAPickupFalk'     ||
       Item == class'HuntingRiflePickupFalk'     ||
       Item == class'SVT40SAPickupFalk'          ||
	    Item == class'L96AWPLLIPickupFalk'        ||
       Item == class'SVDPickupFalk')  
	       return result;
	       
	return Super.GetCostScaling(KFPRI, Item);
}

// Set number times Zed Time can be extended
static function int ZedTimeExtensions(KFPlayerReplicationInfo KFPRI)
{
	if (KFPRI.ClientVeteranSkillLevel >= 20)
	  return 3;
	
	else if (KFPRI.ClientVeteranSkillLevel >= 15)
	  return 2;
	
	else if (KFPRI.ClientVeteranSkillLevel >= 12)
	  return 1;
		
	return 0;
}


// Give Extra Items as Default
static function AddDefaultInventory(KFPlayerReplicationInfo KFPRI, Pawn P)
{ 
   // If Level 20 or Higher give them SVT40
   if (KFPRI.ClientVeteranSkillLevel >= 20)
      FalkAddPerkedWeapon(class'SVT40SAFalk', KFPRI, P);
      
   // If Level 15 or Higher give them WA2000
   else if (KFPRI.ClientVeteranSkillLevel >= 15)
      FalkAddPerkedWeapon(class'Walter2000SAFalk', KFPRI, P);
      
	// If Level 12 or Higher give them M1873
   else if (KFPRI.ClientVeteranSkillLevel >= 12)
      FalkAddPerkedWeapon(class'WinchesterFalk', KFPRI, P);
   
   // If Level 9 or Higher, give them MK23
   else if (KFPRI.ClientVeteranSkillLevel >= 9)
      FalkAddPerkedWeapon(class'MK23PistolFalk', KFPRI, P);

   super.AddDefaultInventory(KFPRI, P);
}


// Skill based sharpshooter bonus on zed time
static function bool SharpZedTimeBonus(KFPlayerReplicationInfo KFPRI, class<DamageType> damageType)
{
   if ((KFPRI.ClientVeteranSkillLevel >= 6 && static.CanUseZedTimeSkill(KFPRI))      &&
      (damageType == class'DamTypeCrossbowFalk'                                      ||
       damageType == class'DamTypeCrossbowHeadShotFalk'                              ||
       damageType == class'DamTypeWinchesterFalk'                                    ||
	    damageType == class'DamTypeDeagleFalk'                                        ||
	    damageType == class'DamTypeDualDeagleFalk'                                    ||
	    damageType == class'DamTypeMagnum44PistolFalk'                                ||
	    damageType == class'DamTypeDual44MagnumFalk'                                  ||
       damageType == class'DamTypeMK23PistolFalk'                                    ||
       damageType == class'DamTypeDualMK23PistolFalk'                                ||
       damageType == class'DamTypeM99SniperRifleFalk'                                ||
       damageType == class'DamTypeM14EBRFalk'                                        ||
       damageType == class'DamTypeSPSniperFalk'                                      ||
	    damageType == class'DamTypeDualiesFalk'                                       ||
	    damageType == class'DamTypeWalter2000SAFalk'                                  ||
	    damageType == class'DamTypeHuntingRifleFalk'                                  ||
       damageType == class'DamTypeSVT40SAFalk'                                       ||
	    damageType == class'DamTypeL96AWPLLIFalk'                                     ||
       damageType == class'DamTypeSVDFalk'))
   {
      return static.ZedTimeSkillUsed(KFPRI);
   }

   return False;
}

defaultproperties
{
	PerkIndex=2

   ZTECD=0.5
   ZTSCD=0.01

	OnHUDIcon=Texture'KillingFloorHUD.Perks.Perk_SharpShooter'
	OnHUDGoldIcon=Texture'KillingFloor2HUD.Perk_Icons.Perk_SharpShooter_Gold'
	VeterancyName="Sharpshooter"
	
   NumRequirements=2
   
   Requirements[0]="Perform %x headshots with sharpshooter weapons"
   Requirements[1]="Deal %x damage with sharpshooter weapons"

   SRLevelEffects(0)="3% more damage with sharpshooter weapons|4% less recoil with sharpshooter weapons|3% faster reload with sharpshooter weapons|1% faster fire rate with M1894/SP SMLE|3% extra headshot damage with sharpshooter weapons|3% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(1)="3% more damage with sharpshooter weapons|8% less recoil with sharpshooter weapons|6% faster reload with sharpshooter weapons|2% faster fire rate with M1894/SP SMLE|6% extra headshot damage with sharpshooter weapons|6% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(2)="3% more damage with sharpshooter weapons|12% less recoil with sharpshooter weapons|9% faster reload with sharpshooter weapons|3% faster fire rate with M1894/SP SMLE|9% extra headshot damage with sharpshooter weapons|9% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(3)="6% more damage with sharpshooter weapons|16% less recoil with sharpshooter weapons|12% faster reload with sharpshooter weapons|4% faster fire rate with M1894/SP SMLE|12% extra headshot damage with sharpshooter weapons|12% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(4)="6% more damage with sharpshooter weapons|20% less recoil with sharpshooter weapons|15% faster reload with sharpshooter weapons|5% faster fire rate with M1894/SP SMLE|15% extra headshot damage with sharpshooter weapons|15% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(5)="6% more damage with sharpshooter weapons|24% less recoil with sharpshooter weapons|18% faster reload with sharpshooter weapons|6% faster fire rate with M1894/SP SMLE|18% extra headshot damage with sharpshooter weapons|18% discount on sharpshooter weapons|Equipped with plasma grenades"
   SRLevelEffects(6)="9% more damage with sharpshooter weapons|28% less recoil with sharpshooter weapons|21% faster reload with sharpshooter weapons|7% faster fire rate with M1894/SP SMLE|21% extra headshot damage with sharpshooter weapons|21% discount on sharpshooter weapons|Equipped with plasma grenades|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(7)="9% more damage with sharpshooter weapons|32% less recoil with sharpshooter weapons|24% faster reload with sharpshooter weapons|8% faster fire rate with M1894/SP SMLE|24% extra headshot damage with sharpshooter weapons|24% discount on sharpshooter weapons|Equipped with plasma grenades|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(8)="9% more damage with sharpshooter weapons|36% less recoil with sharpshooter weapons|27% faster reload with sharpshooter weapons|9% faster fire rate with M1894/SP SMLE|27% extra headshot damage with sharpshooter weapons|27% discount on sharpshooter weapons|Equipped with plasma grenades|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(9)="12% more damage with sharpshooter weapons|40% less recoil with sharpshooter weapons|30% faster reload with sharpshooter weapons|10% faster fire rate with M1894/SP SMLE|30% extra headshot damage with sharpshooter weapons|30% discount on sharpshooter weapons|Equipped with plasma grenades|Spawn with an MK23|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(10)="12% more damage with sharpshooter weapons|44% less recoil with sharpshooter weapons|33% faster reload with sharpshooter weapons|12% faster fire rate with M1894/SP SMLE|33% extra headshot damage with sharpshooter weapons|33% discount on sharpshooter weapons|Equipped with plasma grenades|Spawn with an MK23|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(11)="12% more damage with sharpshooter weapons|48% less recoil with sharpshooter weapons|36% faster reload with sharpshooter weapons|14% faster fire rate with M1894/SP SMLE|36% extra headshot damage with sharpshooter weapons|36% discount on sharpshooter weapons|Equipped with plasma grenades|Spawn with an MK23|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(12)="15% more damage with sharpshooter weapons|52% less recoil with sharpshooter weapons|39% faster reload with sharpshooter weapons|16% faster fire rate with M1894/SP SMLE|39% extra headshot damage with sharpshooter weapons|10% extra M48/L96 AWP/M99 ammo|40% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 1 zed-time extension|Spawn with an M1873|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(13)="15% more damage with sharpshooter weapons|56% less recoil with sharpshooter weapons|42% faster reload with sharpshooter weapons|18% faster fire rate with M1894/SP SMLE|42% extra headshot damage with sharpshooter weapons|10% extra M48/L96 AWP/M99 ammo|44% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 1 zed-time extension|Spawn with an M1873|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(14)="15% more damage with sharpshooter weapons|60% less recoil with sharpshooter weapons|45% faster reload with sharpshooter weapons|20% faster fire rate with M1894/SP SMLE|45% extra headshot damage with sharpshooter weapons|10% extra M48/L96 AWP/M99 ammo|48% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 1 zed-time extension|Spawn with an M1873|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(15)="20% more damage with sharpshooter weapons|64% less recoil with sharpshooter weapons|48% faster reload with sharpshooter weapons|22% faster fire rate with M1894/SP SMLE|48% extra headshot damage with sharpshooter weapons|20% extra M48/L96 AWP/M99 ammo|52% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 2 zed-time extensions|Spawn with a WA2000|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(16)="20% more damage with sharpshooter weapons|68% less recoil with sharpshooter weapons|51% faster reload with sharpshooter weapons|24% faster fire rate with M1894/SP SMLE|51% extra headshot damage with sharpshooter weapons|20% extra M48/L96 AWP/M99 ammo|56% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 2 zed-time extensions|Spawn with a WA2000|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(17)="20% more damage with sharpshooter weapons|72% less recoil with sharpshooter weapons|54% faster reload with sharpshooter weapons|26% faster fire rate with M1894/SP SMLE|54% extra headshot damage with sharpshooter weapons|20% extra M48/L96 AWP/M99 ammo|60% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 2 zed-time extensions|Spawn with a WA2000|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(18)="20% more damage with sharpshooter weapons|76% less recoil with sharpshooter weapons|57% faster reload with sharpshooter weapons|28% faster fire rate with M1894/SP SMLE|57% extra headshot damage with sharpshooter weapons|20% extra M48/L96 AWP/M99 ammo|64% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 2 zed-time extensions|Spawn with a WA2000|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(19)="20% more damage with sharpshooter weapons|80% less recoil with sharpshooter weapons|60% faster reload with sharpshooter weapons|30% faster fire rate with M1894/SP SMLE|60% extra headshot damage with sharpshooter weapons|20% extra M48/L96 AWP/M99 ammo|68% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 2 zed-time extensions|Spawn with a WA2000|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"
   SRLevelEffects(20)="25% more damage with sharpshooter weapons|90% less recoil with sharpshooter weapons|65% faster reload with sharpshooter weapons|35% faster fire rate with M1894/SP SMLE|70% extra headshot damage with sharpshooter weapons|40% extra M48/L96 AWP/M99 ammo|75% discount on sharpshooter weapons|Equipped with plasma grenades|Up to 3 zed-time extensions|Spawn with an SVT40|Zed-time skill: removing a specimen head using a sharpshooter weapon will cause it to explode in a plasma wave once, and you get additional 25% headshot multiplier with sharpshooter weapons"

