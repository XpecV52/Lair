class DamTypeLr300M203MedGun extends KFProjectileWeaponDamageType
	abstract;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'Falk689WeaponsFix.FMedicDamage', Amount);
}

defaultproperties
{
    WeaponClass=Class'Lr300M203MedGun'
    DeathString="%k killed %o (LR300 203)"
    FemaleSuicide="%o shot herself in the foot."
    MaleSuicide="%o shot himself in the foot."
    bRagdollBullet=True
    KDamageImpulse=6500.000000
    KDeathVel=175.000000
    KDeathUpKick=20.000000
	HeadShotDamageMult=1.100000
}
