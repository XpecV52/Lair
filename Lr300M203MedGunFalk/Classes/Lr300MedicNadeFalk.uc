class Lr300MedicNadeFalk extends Nade;

#exec OBJ LOAD FILE=KF_GrenadeSnd.uax
#exec OBJ LOAD FILE=Inf_WeaponsTwo.uax

var() vector ShakeRotMag;           // how far to rot view
var() vector ShakeRotRate;          // how fast to rot view
var() float  ShakeRotTime;          // how much time to rot the instigator's view
var() vector ShakeOffsetMag;        // max view offset vertically
var() vector ShakeOffsetRate;       // how fast to offset view vertically
var() float  ShakeOffsetTime;       // how much time to offset view

var() vector RotMag;            // how far to rot view
var() vector RotRate;           // how fast to rot view
var() float  RotTime;           // how much time to rot the instigator's view
var() vector OffsetMag;         // max view offset vertically
var() vector OffsetRate;        // how fast to offset view vertically
var() float  OffsetTime;        // how much time to offset view

var()   int     HealBoostAmount;// How much we heal a player by default with the medic nade

var     int     TotalHeals;     // The total number of times this nade has healed (or hurt enemies)
var()   int     MaxHeals;       // The total number of times this nade will heal (or hurt enemies) until its done healing
var     float   NextHealTime;   // The next time that this nade will heal friendlies or hurt enemies
var()   float   HealInterval;   // How often to do healing

var()   sound   ExplosionSound; // The sound of the rocket exploding

var localized   string  SuccessfulHealMessage;

var 	int		MaxNumberOfPlayers;

var   bool     bNeedToPlayEffects; // Whether or not effects have been played yet
var   byte     BulletID;             // bullet ID, passed at takedamage to prevent multiple shots
var Controller fCInstigator;   // instigator controller, I don't use vanilla InstigatorController 'cause of reasons


replication
{
   reliable if (Role==ROLE_Authority)
      bNeedToPlayEffects, fCInstigator;
}

// post-death fix
simulated function PostBeginPlay()
{
   local KFPlayerReplicationInfo   KFPRI;
   local class<FVeterancyTypes>    Vet;

   if (Role == ROLE_Authority && Instigator != none && fCInstigator == none && Instigator.Controller != None)
		fCInstigator = Instigator.Controller;

   if (Instigator != none)
   {
      if (Role == ROLE_Authority && fCInstigator == none && Instigator.Controller != None)
         fCInstigator = Instigator.Controller;

      KFPRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

      if (KFPRI != none)
      {
         Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

         if (Vet != none) 
         {
            class<DamTypeMedicNadeFalk>(MyDamageType).Default.fDotMultipler = Vet.Static.MedicDotDamage(KFPRI);

            //warn("New Dot Multi:"@class<DamTypeMedicNadeFalk>(MyDamageType).Default.fDotMultipler);
         }
      }
   }

   Super.PostBeginPlay();
}

simulated function PostNetReceive()
{
   super.PostNetReceive();
   if( !bHasExploded && bNeedToPlayEffects )
   {
      bNeedToPlayEffects = false;
      Explode(Location, vect(0,0,1));
   }
}

simulated function Explode(vector HitLocation, vector HitNormal)
{
   bHasExploded = True;
   BlowUp(HitLocation);

   PlaySound(ExplosionSound,,TransientSoundVolume);

   if (Role == ROLE_Authority)
   {
      bNeedToPlayEffects = true;
      AmbientSound=Sound'Inf_WeaponsTwo.smoke_loop';
   }

   if (EffectIsRelevant(Location,false))
      Spawn(Class'FNadeHealing',,, HitLocation, rotator(vect(0,0,1)));
}

function Timer()
{
   if( !bHidden )
   {
      if( !bHasExploded )
      {
         Explode(Location, vect(0,0,1));
      }
   }
   else if( bDisintegrated )
   {
      AmbientSound=none;
      Destroy();
   }
}

simulated function BlowUp(vector HitLocation)
{
   HealOrHurt(Damage,DamageRadius, MyDamageType, MomentumTransfer, HitLocation);
   if ( Role == ROLE_Authority )
      MakeNoise(1.0);
}

function HealOrHurt(float DamageAmount, float DamageRadius, class<DamageType> DamageType, float Momentum, vector HitLocation)
{
   local actor Victims;
   local float damageScale;
   local vector dir;
   local int NumKilled;
   local KFMonster KFMonsterVictim;
   local Pawn P;
   local FHumanPawn FP;
   local array<Pawn> CheckedPawns;
   local int i;
   local bool bAlreadyChecked;
   // Healing
   local KFPlayerReplicationInfo PRI;
   local int MedicReward;
   local float fMedicReward;
   local float HealSum; // for modifying based on perks
   local int PlayersHealed;
   local bool fShouldReward;
   local Falk689GameTypeBase Flk;

   if (bHurtEntry)
      return;

   NextHealTime = Level.TimeSeconds + HealInterval;

   bHurtEntry = true;

   /*if( Fear != none )
   {
      Fear.StartleBots();
   }*/

   foreach CollidingActors (class 'Actor', Victims, DamageRadius, HitLocation)
   {
      // don't let blast damage affect fluid - VisibleCollisingActors doesn't really work for them - jag
      if( (Victims != self) && (Hurtwall != Victims) && (Victims.Role == ROLE_Authority) && !Victims.IsA('FluidSurfaceInfo')
            && ExtendedZCollision(Victims)==None )
      {
         if( (Instigator==None || Instigator.Health<=0) && KFPawn(Victims)!=None )
            Continue;

         damageScale = 1.0;

         Victims.SetDelayedDamageInstigatorController(fCInstigator);

         P = Pawn(Victims);

         if( P != none )
         {
            for (i = 0; i < CheckedPawns.Length; i++)
            {
               if (CheckedPawns[i] == P)
               {
                  bAlreadyChecked = true;
                  break;
               }
            }

            if( bAlreadyChecked )
            {
               bAlreadyChecked = false;
               P = none;
               continue;
            }

            KFMonsterVictim = KFMonster(Victims);

            if( KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
            {
               KFMonsterVictim = none;
            }

            FP = FHumanPawn(Victims);

            if (KFMonsterVictim != none)
               damageScale *= KFMonsterVictim.GetExposureTo(Location + 15 * -Normal(PhysicsVolume.Gravity));

            else if (FP != none)
               damageScale *= FP.GetExposureTo(Location + 15 * -Normal(PhysicsVolume.Gravity));

            CheckedPawns[CheckedPawns.Length] = P;

            if ( damageScale <= 0)
            {
               P = none;
               continue;
            }
            else
            {
               //Victims = P;
               P = none;
            }
         }
         else
         {
            continue;
         }

         if (FP == none)
         {
            //log(Level.TimeSeconds@"Hurting "$Victims$" for "$(damageScale * DamageAmount)$" damage");

            if( Pawn(Victims) != none && Pawn(Victims).Health > 0 )
            {
               /*Victims.TakeDamage(damageScale * DamageAmount, Instigator, Victims.Location - 0.5 * (Victims.CollisionHeight + Victims.CollisionRadius)
                     * dir,(damageScale * Momentum * dir),DamageType);*/

               Victims.TakeDamage(Damage, Instigator, Victims.Location, dir, DamageType, BulletID);

               if( Role == ROLE_Authority && KFMonsterVictim != none && KFMonsterVictim.Health <= 0 )
               {
                  NumKilled++;
               }
            }
         }
         else
         {
            if( Instigator != none && FP.Health > 0 && FP.Health < FP.HealthMax )
            {
               if (FP.bCanBeHealed)
               {
                  PlayersHealed += 1;
                  MedicReward = HealBoostAmount;

                  PRI = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo);

                  if ( PRI != none && PRI.ClientVeteranSkill != none )
                  {
                     MedicReward *= PRI.ClientVeteranSkill.Static.GetHealPotency(PRI);
                  }

                  HealSum = MedicReward;

                  if ( (FP.Health + FP.fHealthToGive + MedicReward) > FP.HealthMax )
                  {
                     MedicReward = FP.HealthMax - (FP.Health + FP.fHealthToGive);
                     if ( MedicReward < 0 )
                     {
                        MedicReward = 0;
                     }
                  }

                  //log(Level.TimeSeconds@"Healing "$FP$" for "$HealSum$" base healamount "$HealBoostAmount$" health");
                  FP.GiveHealth(HealSum, FP.HealthMax);

                  if (PRI != None)
                  {
                     if (MedicReward > 0 && KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements) != none)
                     {
                        KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements).AddDamageHealed(MedicReward, false, true);
                     }

                     if (PlayerController(Instigator.Controller) != none)
                     {
                        // medic reward block stuff
                        Flk = Falk689GameTypeBase(Level.Game);

                        if (Flk != none)
                           fShouldReward = Flk.ShouldRewardMedic(PlayerController(Instigator.Controller));
                     }

                     if (fShouldReward)
                     {
                        fMedicReward = float(MedicReward) * 0.3; // 20hp healed = £ 6

                        // difficulty scaling
                        if (Level.Game.GameDifficulty >= 8.0) // Bloodbath
                           fMedicReward *= 0.34;

                        else if (Level.Game.GameDifficulty >= 7.0) // Hell on Earth
                           fMedicReward *= 0.5;

                        else if (Level.Game.GameDifficulty >= 5.0 ) // Suicidal
                           fMedicReward *= 0.67;

                        else if (Level.Game.GameDifficulty >= 4.0 ) // Hard
                           fMedicReward *= 0.84;

                        MedicReward = int(fMedicReward);

                        PRI.ReceiveRewardForHealing(MedicReward, FP);
                     }

                     if (KFHumanPawn(Instigator) != none)
                        KFHumanPawn(Instigator).AlphaAmount = 255;

                     if (PlayerController(Instigator.Controller) != none)
                     {
                        PlayerController(Instigator.Controller).ClientMessage(SuccessfulHealMessage$FP.GetPlayerName(), 'CriticalEvent');
                     }
                  }
               }
            }
         }

         FP = none;
      }

      BulletID += 10;

      if (BulletID >= 240)
         BulletID = Default.BulletID;

      if (PlayersHealed >= MaxNumberOfPlayers)
      {
         if (PRI != none)
         {
            KFSteamStatsAndAchievements(PRI.SteamStatsAndAchievements).HealedTeamWithMedicGrenade();
         }
      }
   }

   bHurtEntry = false;
}

// Siren override
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
}

function Tick( float DeltaTime )
{
   if (Role < ROLE_Authority)
      return;

   if( TotalHeals < MaxHeals && NextHealTime > 0 &&  NextHealTime < Level.TimeSeconds )
   {
      TotalHeals += 1;

      HealOrHurt(Damage,DamageRadius, MyDamageType, MomentumTransfer, Location);

      if( TotalHeals >= MaxHeals )
      {
         AmbientSound=none;
      }
   }
}

// just kaboom
simulated singular function HitWall(vector HitNormal, actor Wall)
{
   // Don't let it hit this player, or blow up on another player
   if (Wall == none || Wall == Instigator || Wall.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if (KFBulletWhipAttachment(Wall) != none)
      return;

   SetPhysics(PHYS_None);

   if (bHasExploded)
      return;

   DesiredRotation = Rotation;
   DesiredRotation.Roll = 0;
   DesiredRotation.Pitch = 0;
   SetRotation(DesiredRotation);

   Explode(Location, HitNormal);
}


// I said kaboom
simulated function ProcessTouch(Actor Other, Vector HitLocation)
{
   // Don't let it hit this player, or blow up on another player
   if (bHasExploded || Other == none || Other == Instigator || Other.Base == Instigator)
      return;

   // Don't collide with bullet whip attachments
   if(KFBulletWhipAttachment(Other) != none)
      return;

   Velocity = vect(0,0,0);
   SetPhysics(PHYS_Falling);

   Explode(HitLocation,Normal(HitLocation-Other.Location));
}

defaultproperties
{
   ShakeRotMag=(X=600.000000,Y=600.000000,Z=600.000000)
   ShakeRotRate=(X=12500.000000,Y=12500.000000,Z=12500.000000)
   ShakeRotTime=6.000000
   ShakeOffsetMag=(X=5.000000,Y=10.000000,Z=5.000000)
   ShakeOffsetRate=(X=300.000000,Y=300.000000,Z=300.000000)
   ShakeOffsetTime=3.500000
   RotMag=(X=700.000000,Y=700.000000,Z=700.000000)
   RotRate=(X=12500.000000,Y=12500.000000,Z=12500.000000)
   RotTime=6.000000
   OffsetMag=(X=5.000000,Y=10.000000,Z=7.000000)
   OffsetRate=(X=300.000000,Y=300.000000,Z=300.000000)
   OffsetTime=3.500000
   HealBoostAmount=10
   MaxHeals=7               // it's actually eight ticks like this
   HealInterval=1.000000
   ExplosionSound=SoundGroup'KF_GrenadeSnd.NadeBase.MedicNade_Explode'
   SuccessfulHealMessage="You healed "
   MaxNumberOfPlayers=12
   Speed=2000.000000
   MaxSpeed=2000.000000
   Damage=50.000000
   DamageRadius=300.000000
   MyDamageType=Class'Falk689WeaponsFix.DamTypeMedicNadeFalk'
   ExplosionDecal=Class'KFMod.MedicNadeDecal'
   StaticMesh=StaticMesh'kf_generic_sm.Bullet_Shells.40mm_Warhead'
   bUpdateSimulatedPosition=True
   LifeSpan=10.000000
   DrawScale=2.500000
   SoundVolume=150
   SoundRadius=100.000000
   TransientSoundVolume=2.000000
   TransientSoundRadius=200.000000
}
