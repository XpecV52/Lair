class Lr300M203MedGunPickup extends Lr300M203MedGunPickupBase;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
   cost=4000
   Weight=7.000000
   AmmoCost=10
   BuyClipSize=20
   PowerValue=23
   SpeedValue=50
   RangeValue=100
   ItemName="Medical LR300+M203 Assault Rifle"
   ItemShortName="LR300-M"
   SecondaryAmmoShortName="M203 Mednades"
   PickupMessage="You got a Medical LR300+M203 Assault Rifle"
   AmmoItemName="5.56x45mm NATO rounds"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   EquipmentCategoryID=3
   CorrespondingPerkIndex=0
   InventoryType=Class'Lr300M203MedGun'
   PickupSound=Sound'KF_SCARSnd.SCAR_Pickup'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'LR300M203_A.lr300_stc.w_lr300m203'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
}
