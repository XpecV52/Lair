class RU556Pickup extends KFWeaponPickup;

var int fStoredAmmo; // remaining ammo stored into the pickup when dropped

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// set fStoredAmmo to the weapon class on pickup
auto state pickup
{
   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(20, false);
      }
   }

   // When touched by an actor.  Let's mod this to account for Weights. (Player can't pickup items)
   // IF he's exceeding his max carry weight.
   function Touch(Actor Other)
   {
      local Inventory Copy;
      local Falk689GameTypeBase Flk;

      if ( KFHumanPawn(Other) != none && !CheckCanCarry(KFHumanPawn(Other)) )
      {
         return;
      }

      // If touched by a player pawn, let him pick this up.
      if ( ValidTouch(Other) )
      {
         Copy = SpawnCopy(Pawn(Other));

         AnnouncePickup(Pawn(Other));
         SetRespawn();
 
         if (Copy != None)
         {
            if (RU556AssaultRifle(Copy) != none)
            {
               Flk = Falk689GameTypeBase(Level.Game);

               if (Flk != none && Flk.InZedTime())
                  RU556AssaultRifle(Copy).StartForceReload();

               else if (RU556AssaultRifle(Copy).fStoredAmmo == -1)
               {
                  RU556AssaultRifle(Copy).fStoredAmmo        = fStoredAmmo;
                  RU556AssaultRifle(Copy).fPickupAmmoRestore = True;
                  RU556AssaultRifle(Copy).fPawnAmmoRestore   = True;
                  //Warn("Recovering from pickup: "@fStoredAmmo);
               }
            }

            Copy.PickupFunction(Pawn(Other));
         }

         if ( MySpawner != none && KFGameType(Level.Game) != none )
         {
            KFGameType(Level.Game).WeaponPickedUp(MySpawner);
         }

         if ( KFWeapon(Copy) != none )
         {
            KFWeapon(Copy).SellValue = SellValue;
            KFWeapon(Copy).bPreviouslyDropped = bDropped;

            if ( !bPreviouslyDropped && KFWeapon(Copy).bIsTier3Weapon &&
                  Pawn(Other).Controller != none && Pawn(Other).Controller != DroppedBy )
            {
               KFWeapon(Copy).Tier3WeaponGiver = DroppedBy;
            }
         }
      }
   }
}

defaultproperties
{
   Weight=6.000000
   cost=3400
   AmmoCost=15
   BuyClipSize=31
   PowerValue=42
   SpeedValue=40
   RangeValue=100
   ItemName="Medical Ruger SR556 Carbine"
   ItemShortName="SR556-M"
   AmmoItemName="5.56x45mm NATO rounds"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   CorrespondingPerkIndex=0
   EquipmentCategoryID=3
   InventoryType=Class'RU556Falk.RU556AssaultRifle'
   PickupMessage="You got a Medical Ruger SR556 Carbine"
   PickupSound=Sound'RU556SND.RU556SND.RU556Pickup'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'LairStaticMeshes_SM.RU556_Pickup.RU556_Pickup'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
   DrawScale=1.10000
}
