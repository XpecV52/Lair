class DamTypeRU556Falk extends KFProjectileWeaponDamageType;

static function AwardDamage(KFSteamStatsAndAchievements KFStatsAndAchievements, int Amount) 
{
   if(SRStatsBase(KFStatsAndAchievements) != None && SRStatsBase(KFStatsAndAchievements).Rep != None)
      SRStatsBase(KFStatsAndAchievements).Rep.ProgressCustomValue(Class'FMedicDamage', Amount);
}

defaultproperties
{
   HeadShotDamageMult=1.100000
   WeaponClass=Class'RU556Falk.RU556AssaultRifle'
   DeathString="%k killed %o (SR556-M)"
   FemaleSuicide="%o shot herself in the foot."
   MaleSuicide="%o shot himself in the foot."
   bRagdollBullet=True
   KDamageImpulse=5500.000000
   KDeathVel=175.000000
   KDeathUpKick=20.000000
}
