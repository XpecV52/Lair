class RU556AssaultRifle extends MP7MMedicGunFalk;

#exec OBJ LOAD FILE=LairAnimations_A.ukx

var() 		name 			ReloadShortAnim;
var() 		float 		ReloadShortRate;

simulated function SetZoomBlendColor(Canvas c)
{
   local Byte    val;
   local Color   clr;
   local Color   fog;
   clr.R = 255;
   clr.G = 255;
   clr.B = 255;
   clr.A = 255;
   if( Instigator.Region.Zone.bDistanceFog )
   {
      fog = Instigator.Region.Zone.DistanceFogColor;
      val = 0;
      val = Max( val, fog.R);
      val = Max( val, fog.G);
      val = Max( val, fog.B);
      if( val > 128 )
      {
         val -= 128;
         clr.R -= val;
         clr.G -= val;
         clr.B -= val;
      }
   }
   c.DrawColor = clr;
}

simulated function Notify_ShowBullets()
{
   local int AvailableAmmo;
   AvailableAmmo = AmmoAmount(0);
   if (AvailableAmmo == 0)
   {
      SetBoneScale (0, 0.0, 'SMGAKS74U_bullet_01');
      SetBoneScale (1, 0.0, 'SMGAKS74U_bullet_02');
   }
   else if (AvailableAmmo == 1)
   {
      SetBoneScale (0, 0.0, 'SMGAKS74U_bullet_01');
      SetBoneScale (1, 0.0, 'SMGAKS74U_bullet_02');
   }
   else
   {
      SetBoneScale (0, 1.0, 'SMGAKS74U_bullet_01');
      SetBoneScale (1, 1.0, 'SMGAKS74U_bullet_02');
   }
}

simulated function Notify_HideBullets()
{
   if (MagAmmoRemaining <= 0)
   {
      SetBoneScale (1, 0.0, 'SMGAKS74U_bullet_01');
   }
   else if (MagAmmoRemaining <= 1)
   {
      SetBoneScale (0, 0.0, 'SMGAKS74U_bullet_02');
   }
}

exec function ReloadMeNow()
{
   local float ReloadMulti;
   if(!AllowReload())
      return;
   if ( bHasAimingMode && bAimingRifle )
   {
      FireMode[1].bIsFiring = False;

      ZoomOut(false);
      if( Role < ROLE_Authority)
         ServerZoomOut(false);
   }
   if ( KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none )
   {
      ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);
   }
   else
   {
      ReloadMulti = 1.0;
   }
   bIsReloading = true;
   ReloadTimer = Level.TimeSeconds;
   if (MagAmmoRemaining <= 0)
   {
      ReloadRate = Default.ReloadRate / ReloadMulti;
   }
   else if (MagAmmoRemaining >= 1)
   {
      ReloadRate = Default.ReloadShortRate / ReloadMulti;
   }
   if( bHoldToReload )
   {
      NumLoadedThisReload = 0;
   }
   ClientReload();
   Instigator.SetAnimAction(WeaponReloadAnim);
   if ( Level.Game.NumPlayers > 1 && KFGameType(Level.Game).bWaveInProgress && KFPlayerController(Instigator.Controller) != none &&
         Level.TimeSeconds - KFPlayerController(Instigator.Controller).LastReloadMessageTime > KFPlayerController(Instigator.Controller).ReloadMessageDelay )
   {
      KFPlayerController(Instigator.Controller).Speech('AUTO', 2, "");
      KFPlayerController(Instigator.Controller).LastReloadMessageTime = Level.TimeSeconds;
   }
}

simulated function ClientReload()
{
   local float ReloadMulti;
   if ( bHasAimingMode && bAimingRifle )
   {
      FireMode[1].bIsFiring = False;

      ZoomOut(false);
      if( Role < ROLE_Authority)
         ServerZoomOut(false);
   }
   if ( KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none )
   {
      ReloadMulti = KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.Static.GetReloadSpeedModifier(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), self);
   }
   else
   {
      ReloadMulti = 1.0;
   }
   bIsReloading = true;
   if (MagAmmoRemaining <= 0)
   {
      PlayAnim(ReloadAnim, ReloadAnimRate*ReloadMulti, 0.1);
   }
   else if (MagAmmoRemaining >= 1)
   {
      PlayAnim(ReloadShortAnim, ReloadAnimRate*ReloadMulti, 0.1);
   }
}


// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
      {
         DetachFromPawn(Instigator);

         if (FHumanPawn(Instigator) != none && fHealingAmmoFalk > -1)
         {
            //warn("Storing ammo to the pawn: "@fHealingAmmoFalk);
            FHumanPawn(Instigator).fStoredSyringeAmmo = fHealingAmmoFalk;
            FHumanPawn(Instigator).fStoredSyringeTime = Level.TimeSeconds;
         }
      }

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      // storing ammo into the pickup
      if (RU556Pickup(Pickup) != none)
      {
         //warn("Storing ammo to the pickup: "@fHealingAmmoFalk);
         RU556Pickup(Pickup).fStoredAmmo = fHealingAmmoFalk;
      }

      if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

defaultproperties
{
   AmmoRegenRate=0.300000 // standard slow recharge rate for non-smg
   HealBoostAmount=20
   Weight=6.000000
   MagCapacity=30
   InventoryGroup=4
   FireModeClass(0)=Class'RU556Fire'
   FireModeClass(1)=Class'RU556AltFire'
   Description="The SR556 is a semi-automatic rifle carbine. This version has been modified to fire fast-travelling healing darts. Greater stopping power and larger magazine than an M7A3-M. Slow medical darts recharge rate. Equipped with an Aimpoint sight."
   ItemName="Medical Ruger SR556 Carbine"
   ReloadShortAnim="ReloadSA"
   ReloadShortRate=3.270000
   ReloadRate=4.100000
   ReloadAnim="Reload_EmptySA"
   ReloadAnimRate=1.000000
   WeaponReloadAnim="Reload_SCAR"
   HudImage=Texture'LairTextures_T.RU556.RU556Unselect'
   HudImageRef="LairTextures_T.RU556.RU556Unselect"
   SelectedHudImage=Texture'LairTextures_T.RU556.RU556Select'
   SelectedHudImageRef="LairTextures_T.RU556.RU556Select"
   bHasAimingMode=True
   IdleAimAnim="Idle"
   StandardDisplayFOV=75.000000
   bModeZeroCanDryFire=True
   SleeveNum=0
   TraderInfoTexture=Texture'LairTextures_T.RU556.RU556Trader'
   bIsTier2Weapon=True
   PlayerIronSightFOV=65.000000
   ZoomedDisplayFOV=32.000000
   PutDownAnim="Put_Down"
   PutDownTime=0.30000
   PutDownAnimRate=1.00000
   BringUpTime=1.150000
   SelectSound=Sound'RU556SND.RU556SND.RU556Select'
   SelectSoundRef="RU556SND.RU556SND.RU556Select"
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.550000
   CurrentRating=0.550000
   bShowChargingBar=True
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=75.000000
   Priority=110
   CustomCrosshair=11
   CustomCrossHairTextureName="Crosshairs.HUD.Crosshair_Cross5"
   PlayerViewOffset=(X=3.000000,Y=16.000000,Z=-4.000000)
   BobDamping=6.000000
   PickupClass=Class'RU556Falk.RU556Pickup'
   IconCoords=(X1=245,Y1=39,X2=329,Y2=79)
   MeshRef="LairAnimations_A.RU556Mesh"
   SkinRefs(0)="KF_Weapons_Trip_T.hands.hands_1stP_military_cmb"
   SkinRefs(1)="LairTextures_T.RU556.RU556ExtrasCombiner"
   SkinRefs(2)="LairTextures_T.RU556.RU556MainCombiner"
   SkinRefs(3)="LairTextures_T.RU556.RU556Bullet"
   Mesh=SkeletalMesh'LairAnimations_A.RU556Mesh'
   Skins(0)=Combiner'KF_Weapons_Trip_T.hands.hands_1stP_military_cmb'
   Skins(1)=Combiner'LairTextures_T.RU556.RU556ExtrasCombiner'
   Skins(2)=Combiner'LairTextures_T.RU556.RU556MainCombiner'
   Skins(3)=Texture'LairTextures_T.RU556.RU556Bullet'
   TransientSoundVolume=1.250000
   AttachmentClass=Class'RU556Attachment'
}
