class RU556AmmoPickup extends KFAmmoPickup;

defaultproperties
{
   AmmoAmount=30
   InventoryType=Class'RU556Ammo'
   PickupMessage="You found some 5.56x45mm NATO rounds"
   StaticMesh=StaticMesh'KillingFloorStatics.L85Ammo'
}
