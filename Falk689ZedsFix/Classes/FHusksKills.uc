class FHusksKills extends SRCustomProgressInt;

function NotifyPlayerKill(Pawn Killed, class<DamageType> damageType)
{  
   if (FalkZombieHusk(Killed)                              != None  &&
       (class<DamTypeFlamethrower>(damageType)             != None  ||
        class<DamTypeBurnStrongFalk>(damageType)           != None  ||
        class<DamTypeBurnWeakFalk>(damageType)             != None  ||
        class<DamTypeHuskGunProjectileImpact>(damageType)  != None  ||
        class<DamTypeFlareProjectileImpact>(damageType)    != None  ||
        class<DamTypeMAC10MP>(damageType)                  != None  ||
        class<DamTypeMAC10MPIncFalk>(damageType)           != None  ||
        class<DamTypeTrenchgunFalk>(damageType)            != None  ||
        class<DamTypeHuskGun>(damageType)                  != None  ||
        class<DamTypeBurned>(damageType)                   != None  ||
        class<DamTypeFlameNadeFalk>(damageType)            != None  ||
        class<DamTypeAAR525S>(damageType)                  != None  ||
        class<DamTypeM4A1IronBeastSAFalk>(damageType)      != None  ||
        class<DamTypeSpitfireFalk>(damageType)             != None))
   {
      IncrementProgress(1);
   }
}

defaultproperties 
{
   ProgressName = "Custom husks kills"
}
