/* MEDIUM ZED */

class FalkZombieBruteBase extends FalkMonster;

#exec OBJ LOAD FILE=WPC_BRUTE_T.utx
#exec OBJ LOAD FILE=KFWeaponSound.uax
#exec OBJ LOAD FILE=LairBrute_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool  bChargingPlayer;
var bool  bClientCharge;
var bool  bFrustrated;
var int   MaxRageCounter; // Maximum amount of players we can hit before calming down
var int   RageCounter; // Decreases each time we successfully hit a player
var float RageSpeedTween;
var float LastDamagedTime;
var int   BlockHitsLanded; // Hits made while blocking or raging

var name  ChargingAnim;
var Sound RageSound;

// View shaking for players
var() vector	ShakeViewRotMag;
var() vector	ShakeViewRotRate;
var() float		ShakeViewRotTime;
var() vector	ShakeViewOffsetMag;
var() vector	ShakeViewOffsetRate;
var() float		ShakeViewOffsetTime;

var float  PushForce;
var vector PushAdd;          // Used to add additional height to push
var float  RageDamageMul;    // Multiplier for hit damage when raging
var float  RageBumpDamage;   // Damage done when we hit other specimens while raging
var float  BlockAddScale;    // Additional head scale when blocking
var bool   bBlockedHS;
var bool   bBlocking;
var bool   bServerBlock;
var bool   bClientBlock;
var float  BlockDmgMul;      // Multiplier for damage taken from blocked shots
var float  BlockFireDmgMul;

// falk689 stuff
var byte   fStunReset;       // used to set the speed back after a stun
var byte   fFreezeReset;     // used to set the speed back after a freeze
var byte   fStunAnimStop;    // how many times we stop the walk animation on stun
var float  fBruteStunTime;   // when this brute was stunned
var float  fStunDuration;    // how many seconds the stuns lasts
var float  fRageTick;        // how many seconds to wait between checks if we should rage
var float  fCurRageTick;     // current rage timer tick
var float  fTempHealth;      // we store health here so we can compare it every check
var bool   fDelayZap;        // if true we'll delay a zap time on tick
var float  fZapDelay;        // how much to delay that zap
var float  BruteFreezeCheck; // seconds between speed checks after a freeze

var int    NrmRDThreshold;   // Rage damage threshold on normal
var int    HrdRDThreshold;   // Rage damage threshold on hard
var int    SuiRDThreshold;   // Rage damage threshold on suicidal
var int    HOERDThreshold;   // Rage damage threshold on hell on earth
var int    BBRDThreshold;    // Rage damage threshold on bloodbath
var int    CurRDThreshold;   // currently used threshold for rage damage

var float fBruteAttackDelay; // delay between FalkFire calls
var int   fBruteID;          // debug id, used to make debug shit easier for us

var float fBruteZapFixTick;  // time between checks after zap to fix the block on clients
var int   fBruteZapFixLeft;  // how many times we call the fix on the clients

replication
{
	reliable if(Role == ROLE_Authority)
		bChargingPlayer, bServerBlock, fBruteStunTime;
}

defaultproperties
{
	MenuName="Brute"
	
	MeleeAnims(0)="BruteAttack1"
	MeleeAnims(1)="BruteAttack2"
	MeleeAnims(2)="BruteBlockSlam"
	MovementAnims(0)="BruteWalkC"
	MovementAnims(1)="BruteWalkC"
	WalkAnims(0)="BruteWalkC"
	WalkAnims(1)="BruteWalkC"
	WalkAnims(2)="RunL"
	WalkAnims(3)="RunR"
	IdleCrouchAnim="BruteIdle"
	IdleWeaponAnim="BruteIdle"
	IdleRestAnim="BruteIdle"
	IdleHeavyAnim="BruteIdle"
	IdleRifleAnim="BruteIdle"
	ChargingAnim="BruteRun"

   ZombieDamType[0]=Class'DamTypeBruteAttackFalk'
   ZombieDamType[1]=Class'DamTypeBruteAttackFalk'
   ZombieDamType[2]=Class'DamTypeBruteAttackFalk'
		
	KFRagdollName="FleshPound_Trip"
   RageSound=SoundGroup'WPC_Brute_S.Brute.Brute_Rage'
	RagDeathUpKick=100.000000
	SeveredArmAttachScale=1.300000
	SeveredLegAttachScale=1.200000
	SeveredHeadAttachScale=1.500000
   DamageToMonsterScale=8.000000
	
	ShakeViewRotMag=(X=500.000000,Y=500.000000,Z=600.000000)
	ShakeViewRotRate=(X=12500.000000,Y=12500.000000,Z=12500.000000)
	ShakeViewRotTime=6.000000
	ShakeViewOffsetMag=(X=5.000000,Y=10.000000,Z=5.000000)
	ShakeViewOffsetRate=(X=300.000000,Y=300.000000,Z=300.000000)
	ShakeViewOffsetTime=3.500000
	
	CollisionRadius=26
	CollisionHeight=44
	bUseExtendedCollision=True
	ColOffset=(Z=52.000000)
	ColRadius=35
	ColHeight=25
	HeadHeight=2.500000
	HeadScale=1.300000
	OnlineHeadshotOffset=(X=22.000000,Z=68.000000)
	OnlineHeadshotScale=1.300000
	BlockAddScale=2.5

	DamageForce=25000
	MeleeRange=85
	RageBumpDamage=5
	SpinDamConst=20.000000
	SpinDamRand=20.000000
	PushForce=860
	PushAdd=(Z=150)//180

	bMeleeStunImmune=false
   bStunImmune=false
	GroundSpeed=125.000000
	WaterSpeed=125.000000
	BlockDmgMul=0.1
	BlockFireDmgMul=1.0

	PrePivot=(Z=0.000000)
	RotationRate=(Yaw=45000,Roll=0)
	
	ZombieFlag=3
	bFatAss=true
	Intelligence=BRAINS_Human
	
	bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False

   MeleeDamage=8
   RageDamageMul=1.25
   HealthMax=700
   Health=700
   HeadHealth=125.000000
   fHSKThreshold=0.179            // brute doesn't have a bleedout mechanic anyway
   fDeathDrama=0.06               // medium zeds standard
   ScoringValue=20                // medium zeds standard
   BleedOutDuration=5.000000      // brute doesn't have a bleedout mechanic anyway
   PlayerCountHealthScale=0.1     // medium zeds standard
   PlayerNumHeadHealthScale=0.1   // medium zeds standard
   Mass=400.000000                // medium zeds standard
   MotionDetectorThreat=0.500000  // a single medium zed triggers a pipe, two trigger an ATM
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=10.000000         // medium zeds standard
   fShotgunWeakness=True
   ZapDuration=3.5                // medium zeds standard
   fStunThresh=400                // medium zeds standard
   fShouldRage=True
   fShouldExitRage=True

   NrmRDThreshold=225
   HrdRDThreshold=200
   SuiRDThreshold=175
   HOERDThreshold=150
   BBRDThreshold=125
   CurRDThreshold=225

   fStunDuration=4.0

   fRageTick=2.0
   fRageSpeed=1.9
   fNormalRageSpeed=1.9
   fHardRageSpeed=1.9
   fSuicidalRageSpeed=1.9
   fHOERageSpeed=1.9
   fBBRageSpeed=1.9
   fRageSpeedCap=300.0
   fStunAnimStop=5
   fZapDelay=0.2
   fBruteAttackDelay=0.2
   BruteFreezeCheck=0.1
   fFleshpoundRageImmune=True

   fBruteZapFixTick=0.2
   fBruteZapFixLeft=5
}
