/* LIGHT ZED */

class FalkZombieCrawler extends FalkZombieCrawlerBase
   abstract;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairCrawler_A.ukx

function bool DoPounce()
{
   if (fFrozen || bZapped || bIsCrouched || bWantsToCrouch || (Physics != PHYS_Walking) || VSize(Location - Controller.Target.Location) > (MeleeRange * 5))
      return false;

   Velocity = Normal(Controller.Target.Location-Location)*PounceSpeed;
   Velocity.Z = JumpZ;
   SetPhysics(PHYS_Falling);
   ZombieSpringAnim();
   bPouncing=true;
   return true;
}

simulated function ZombieSpringAnim()
{
   if (!fFrozen)
   {
      SetAnimAction('ZombieSpring');
   }
}

event Landed(vector HitNormal)
{
   bPouncing=false;
   super.Landed(HitNormal);
}

event Bump(actor Other)
{
   // TODO: is there a better way - lololololol - Falk689
   if(bPouncing && KFHumanPawn(Other)!=none )
   {
      KFHumanPawn(Other).TakeDamage(((MeleeDamage - (MeleeDamage * 0.05)) + (MeleeDamage * (FRand() * 0.1))), self ,self.Location,self.velocity, class 'KFmod.ZombieMeleeDamage');
      if (KFHumanPawn(Other).Health <=0)
      {
         //TODO - move this to humanpawn.takedamage? Also see KFMonster.MeleeDamageTarget
         KFHumanPawn(Other).SpawnGibs(self.rotation, 1);
      }
      //After impact, there'll be no momentum for further bumps
      bPouncing=false;
   }
}

// Blend his attacks so he can hit you in mid air.
simulated function int DoAnimAction( name AnimName )
{
   if (fFrozen)
      return 0;

   if( AnimName=='InAir_Attack1' || AnimName=='InAir_Attack2' )
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim(AnimName,, 0.0, 1);
      return 1;
   }

   if( AnimName=='HitF' )
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, NeckBone);
      PlayAnim(AnimName,, 0.0, 1);
      return 1;
   }

   if( AnimName=='ZombieSpring' )
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;
      PlayAnim(AnimName,,0.02);
      return 0;
   }

   return Super.DoAnimAction(AnimName);
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction=='' || fFrozen)
      return;

   if(NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(2);
      if( Physics == PHYS_Falling )
      {
         NewAction = MeleeAirAnims[meleeAnimIndex];
      }
      else
      {
         NewAction = meleeAnims[meleeAnimIndex];
      }
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }
   ExpectingChannel = DoAnimAction(NewAction);

   if (AnimNeedsWait(NewAction))
      bWaitForAnim = true;

   if (Level.NetMode != NM_Client)
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

// The animation is full body and should set the bWaitForAnim flag
simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'KnockDown' || TestAnim == 'ZombieSpring' || TestAnim == 'DoorBash')
      return true;

   return false;
}

// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieCrawler(P) != none || Super.SameSpeciesAs(P));
}


static simulated function PreCacheMaterials(LevelInfo myLevel)
{//should be derived and used.
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.CrawlerCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.CrawlerEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.crawler_diff');
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Crawler;
}

// don't interrupt the leap attack
simulated function bool HitCanInterruptAction()
{
   if (bPouncing)
      return false;

   return !bShotAnim;
}

// don't stun during the leap attack
function bool FlipOver()
{
   if (!bPouncing)
      Super.FlipOver();

   return false;
}

defaultproperties
{
   EventClasses(0)="KFChar.ZombieCrawler_STANDARD"
   ControllerClass=Class'FalkCrawlerController'
}
