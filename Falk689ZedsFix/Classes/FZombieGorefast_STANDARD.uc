/* LIGHT ZED */

class FZombieGorefast_STANDARD extends FalkZombieGorefast;

#exec OBJ LOAD FILE=LairGorefast_A.ukx

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmGorefast'
   DetachedLegClass=Class'KFChar.SeveredLegGorefast'
   DetachedHeadClass=Class'KFChar.SeveredHeadGorefast'
   BurntDetachedHeadClass=Class'GorefastSeveredBurntHead'
   BurntDetachedArmClass=Class'GorefastSeveredBurntArm'
   BurntDetachedLegClass=Class'GorefastSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.GoreFast.Gorefast_Challenge'
   AmbientSound=Sound'KF_BaseGorefast.Gorefast_Idle'
   Mesh=SkeletalMesh'LairGorefast_A.GoreFast_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.GorefastCombinerFinal'
}
