class FalkBossController extends FMonsterController;

// again, this is here 'cause I'm lazy

var NavigationPoint HidingSpots;

var     float       WaitAnimTimeout;    // How long until the Anim we are waiting for is completed; Hack so the server doesn't get stuck in idle when its doing the Rage anim
var     int         AnimWaitChannel;    // The channel we are waiting to end in WaitForAnim
var     bool        bAlreadyFoundEnemy; // The Boss has already found an enemy at least once
var     bool        fRandomEnemy;       // select a random enemy with find new enemy
//var     bool        fDistEnemy;         // select the closer available enemy
var     bool        fRandomDist;        // select a random nearby enemy with find new enemy
var     bool        fSeeNewEnemy;       // store bSeeEnemy for external usage
//var     Pawn        fIgnorePawn;        // don't use this pawn for random enemy search

// pawn list for random target
struct fTargetList
{
   var Pawn  P;      // our target
   var float Dist;   // its distance
};


replication
{
   reliable if (Role==ROLE_Authority)
      fRandomEnemy, fSeeNewEnemy;
}

function bool CanKillMeYet()
{
    return false;
}

function TimedFireWeaponAtEnemy()
{
	if ( (Enemy == None) || FireWeaponAt(Enemy) )
		SetCombatTimer();
	else
		SetTimer(0.01, True);
}

// return a random target
function fGetRandomTarget()
{
   local array<KFHumanPawn> pTmpList;
   local KFHumanPawn P;
   local byte Random;

   foreach DynamicActors(class'KFHumanPawn', P)
   {
      if (P.Health > 0)
      {
         PTmpList.insert(0, 1);
         PTmpList[0] = P;
      }
   }

   Random = Rand(PTmpList.Length);

   target = PTmpList[Random];
}

// get new enemy or random
function bool FindNewEnemy()
{
   local Pawn BestEnemy;
   local Controller PC;
   local KFHumanPawn Human;
   local float BestDist, NewDist;
   local float AverageDist;
   local array<fTargetList> pTmpList;
   local byte Random, i;

   if (KFM.bNoAutoHuntEnemies)
   {
      //warn(1);
      Return False;
   }

   //warn("Find New Enemy, Random:"@fRandomEnemy@"Random Dist:"@fRandomDist);

   for (PC=Level.ControllerList; PC!=None; PC=PC.NextController)
   {
      Human = KFHumanPawn(PC.Pawn);

      if (Human != none && Human.Health > 0 && !Human.bPendingDelete)
      {
         //warn("Human:"@Human);
         if (fRandomEnemy)
         {
            pTmpList.insert(0, 1);
            pTmpList[0].P = Human;

            if (fRandomDist)
            {
               NewDist = VSizeSquared(Human.Location - Pawn.Location);
               AverageDist += NewDist;
               pTmpList[0].Dist = NewDist;
            }
         }

         else/* if (fDistEnemy)*/
         {
            NewDist = VSizeSquared(Human.Location - Pawn.Location);

            if (BestEnemy == none || (NewDist < BestDist))
            {
               BestEnemy = Human;
               BestDist  = NewDist;
               fSeeNewEnemy = CanSee(Human);
            }
         }

         /*else
         {
            ThreatLevel = Human.AssessThreatTo(self, true);

            if (ThreatLevel <= 0)
               continue;

            else if (ThreatLevel > HighestThreatLevel)
            {
               HighestThreatLevel = ThreatLevel;
               BestEnemy = Human;
               fSeeNewEnemy = CanSee(Human);
            }
         }*/
      }
   }

   //if (fDistEnemy)
   //   fDistEnemy = False;

   if (fRandomEnemy)
   {
      if (pTmpList.Length > 0)
      {
         fRandomEnemy = false;

         // remove far players from the list
         if (fRandomDist)
         {
            fRandomDist = false;

            if (PTmpList.Length > 2)
            {
               AverageDist = AverageDist / PTmpList.Length;

               for (i=0; i<PTmpList.Length; i++)
               {
                  if (PTmpList[i].Dist > AverageDist)
                  {
                     PTmpList.Remove(i, 1);
                     i--;
                  }
               }
            }
         }

         // random check
         if (PTmpList.Length > 1)
         {
            Random = Rand(PTmpList.Length);
            BestEnemy = PTmpList[Random].P;
            fSeeNewEnemy = CanSee(BestEnemy);
         }

         else if (PTmpList.Length == 1)
         {
            BestEnemy = PTmpList[0].P;
            fSeeNewEnemy = CanSee(BestEnemy);
         }
      }

      //warn(2);
      return false;
   }

   if (BestEnemy == Enemy)
   {
      //warn(3);
      return false;
   }

   if (BestEnemy != None)
   {
      ChangeEnemy(BestEnemy, fSeeNewEnemy);
      //warn(4);
      return true;
   }

   //warn(5);
   return false;
}

// Overridden to support a quick initial attack to get the boss to the players quickly
function FightEnemy(bool bCanCharge)
{
   if (KFM.bShotAnim)
   {
      GoToState('WaitForAnim');
      Return;
   }
   if (KFM.MeleeRange != KFM.default.MeleeRange)
      KFM.MeleeRange = KFM.default.MeleeRange;

   if ( Enemy == none || Enemy.Health <= 0 )
      FindNewEnemy();

   if ( (Enemy == FailedHuntEnemy) && (Level.TimeSeconds == FailedHuntTime) )
   {
      //	if ( Enemy.Controller.bIsPlayer )
      //	FindNewEnemy();

      if ( Enemy == FailedHuntEnemy )
      {
         GoalString = "FAILED HUNT - HANG OUT";
         if ( EnemyVisible() )
            bCanCharge = false;
      }
   }
   if ( !EnemyVisible() )
   {
      // Added sneakcount hack to try and fix the endless loop crash. Try and track down what was causing this later - Ramm
      if( bAlreadyFoundEnemy || FalkZombieBoss(Pawn).SneakCount > 2 )
      {
         bAlreadyFoundEnemy = true;
         GoalString = "Hunt";
         GotoState('ZombieHunt');
      }
      else
      {
         // Added sneakcount hack to try and fix the endless loop crash. Try and track down what was causing this later - Ramm
         FalkZombieBoss(Pawn).SneakCount++;
         GoalString = "InitialHunt";
         GotoState('InitialHunting');
      }
      return;
   }

   // see enemy - decide whether to charge it or strafe around/stand and fire
   Target = Enemy;
   GoalString = "Charge";
   PathFindState = 2;
   DoCharge();
}

// Get the boss to the players quickly after initial spawn
state InitialHunting extends Hunting
{
   event SeePlayer(Pawn SeenPlayer)
   {
      super.SeePlayer(SeenPlayer);
      bAlreadyFoundEnemy = true;
      GoalString = "Hunt";
      GotoState('ZombieHunt');
   }

   function BeginState()
   {
      local float ZDif;

      // Added sneakcount hack to try and fix the endless loop crash. Try and track down what was causing this later - Ramm
      FalkZombieBoss(Pawn).SneakCount++;

      if( Pawn.CollisionRadius>27 || Pawn.CollisionHeight>46 )
      {
         ZDif = Pawn.CollisionHeight-44;
         Pawn.SetCollisionSize(24,44);
         Pawn.MoveSmooth(vect(0,0,-1)*ZDif);
      }

      super.BeginState();
   }
   function EndState()
   {
      local float ZDif;

      if( Pawn.CollisionRadius!=Pawn.Default.CollisionRadius || Pawn.CollisionHeight!=Pawn.Default.CollisionHeight )
      {
         ZDif = Pawn.Default.CollisionRadius-44;
         Pawn.MoveSmooth(vect(0,0,1)*ZDif);
         Pawn.SetCollisionSize(Pawn.Default.CollisionRadius,Pawn.Default.CollisionHeight);
      }

      super.EndState();
   }
}

state ZombieCharge
{
   function bool StrafeFromDamage(float Damage, class<DamageType> DamageType, bool bFindDest)
   {
      return false;
   }

   // I suspect this function causes bloats to get confused
   function bool TryStrafe(vector sideDir)
   {
      return false;
   }

   function Timer()
   {
      Disable('NotifyBump');
      Target = Enemy;
      TimedFireWeaponAtEnemy();
   }

WaitForAnim:

   if ( Monster(Pawn).bShotAnim )
   {
      Goto('Moving');
   }
   if ( !FindBestPathToward(Enemy, false,true) )
      GotoState('ZombieRestFormation');
Moving:
   MoveToward(Enemy);
   WhatToDoNext(17);
   if ( bSoaking )
      SoakStop("STUCK IN CHARGING!");
}

state RunSomewhere
{
   Ignores HearNoise,DamageAttitudeTo,Tick,EnemyChanged,Startle;

   function BeginState()
   {
      HidingSpots = None;
      Enemy = None;
      SetTimer(0.1,True);
   }
   event SeePlayer(Pawn SeenPlayer)
   {
      SetEnemy(SeenPlayer);
   }
   function Timer()
   {
      if( Enemy==None )
         Return;
      Target = Enemy;

      if (FalkZombieBoss(KFM) != none)
         FalkZombieBoss(KFM).FalkRangedAttack(Target);

      else
         warn("WARNING: Wrong pat?");
   }
Begin:
   if( Pawn.Physics==PHYS_Falling )
      WaitForLanding();
   While( KFM.bShotAnim )
      Sleep(0.25);
   if( HidingSpots==None )
      HidingSpots = FindRandomDest();
   if( HidingSpots==None )
      FalkZombieBoss(Pawn).BeginHealing();
   if( ActorReachable(HidingSpots) )
   {
      MoveTarget = HidingSpots;
      HidingSpots = None;
   }
   else FindBestPathToward(HidingSpots,True,False);
   if( MoveTarget==None )
      FalkZombieBoss(Pawn).BeginHealing();
   if( Enemy!=None && VSize(Enemy.Location-Pawn.Location)<100 )
      MoveToward(MoveTarget,Enemy,,False);
   else MoveToward(MoveTarget,MoveTarget,,False);
   if( HidingSpots==None || !PlayerSeesMe() )
      FalkZombieBoss(Pawn).BeginHealing();
   GoTo'Begin';
}
State SyrRetreat
{
   Ignores HearNoise,DamageAttitudeTo,Tick,EnemyChanged,Startle;

   function BeginState()
   {
      HidingSpots = None;
      Enemy = None;
      SetTimer(0.1,True);
   }
   event SeePlayer(Pawn SeenPlayer)
   {
      SetEnemy(SeenPlayer);
   }
   function Timer()
   {
      if( Enemy==None )
         Return;
      Target = Enemy;

      if (FalkZombieBoss(KFM) != none)
         FalkZombieBoss(KFM).FalkRangedAttack(Target);

      else
         warn("WARNING: Wrong pat?");
   }
   function FindHideSpot()
   {
      local NavigationPoint N,BN;
      local float Dist,BDist,MDist;
      local vector EnemyDir;

      if( Enemy==None )
      {
         HidingSpots = FindRandomDest();
         Return;
      }
      EnemyDir = Normal(Enemy.Location-Pawn.Location);
      For( N=Level.NavigationPointList; N!=None; N=N.NextNavigationPoint )
      {
         MDist = VSize(N.Location-Pawn.Location);
         if( MDist<2500 && !FastTrace(N.Location,Enemy.Location) && FindPathToward(N)!=None )
         {
            Dist = VSize(N.Location-Enemy.Location)/FMax(MDist/800.f,1.5);
            if( (EnemyDir Dot Normal(Enemy.Location-N.Location))<0.2 )
               Dist/=10;
            if( BN==None || BDist<Dist )
            {
               BN = N;
               BDist = Dist;
            }
         }
      }
      if( BN==None )
         HidingSpots = FindRandomDest();
      else HidingSpots = BN;
   }
Begin:
   if( Pawn.Physics==PHYS_Falling )
      WaitForLanding();
   While( KFM.bShotAnim )
      Sleep(0.25);
   if( HidingSpots==None )
      FindHideSpot();
   if( HidingSpots==None )
      FalkZombieBoss(Pawn).BeginHealing();
   if( ActorReachable(HidingSpots) )
   {
      MoveTarget = HidingSpots;
      HidingSpots = None;
   }
   else FindBestPathToward(HidingSpots,True,False);
   if( MoveTarget==None )
      FalkZombieBoss(Pawn).BeginHealing();
   if( Enemy!=None && VSize(Enemy.Location-Pawn.Location)<100 )
      MoveToward(MoveTarget,Enemy,,False);
   else MoveToward(MoveTarget,MoveTarget,,False);
   if( HidingSpots==None )
      FalkZombieBoss(Pawn).BeginHealing();
   GoTo'Begin';
}
function bool PlayerSeesMe()
{
   local Controller C;

   For( C=Level.ControllerList; C!=None; C=C.NextController )
   {
      if( C.bIsPlayer && C.Pawn!=None && C.Pawn!=Pawn && LineOfSightTo(C.Pawn) )
         Return True;
   }
   Return False;
}

// simplified state
state WaitForAnim
{
   Ignores SeePlayer, HearNoise, Timer, EnemyNotVisible, NotifyBump, Startle;

   event AnimEnd(int Channel)
   {
      //warn("Controller AnimEnd:"@Level.TimeSeconds);

      if (bUseFreezeHack && FalkMonster(Pawn) != none && !FalkMonster(Pawn).fFrozen)
      {
         if (Pawn != None)
         {
            Pawn.AccelRate = Pawn.Default.AccelRate;
            Pawn.GroundSpeed = Pawn.Default.GroundSpeed;
         }

         bUseFreezeHack = False;
         FindNewEnemy();
      }

      Pawn.AnimEnd(Channel);

      //if (!Monster(Pawn).bShotAnim)
      //   WhatToDoNext(99);
   }

   // removed a dubious fix on the parent class
   function BeginState()
   {
      //warn("Controller WaitForAnimState begin time:"@Level.TimeSeconds);
   }

   function Tick(float Delta)
   {
      Global.Tick(Delta);

      if (bUseFreezeHack)
      {
         MoveTarget = None;
         MoveTimer = -1;
         Pawn.Acceleration = vect(0,0,0);
         Pawn.GroundSpeed = 1;
         Pawn.AccelRate = 0;
      }
   }
}

// pick a target the falk way - our enemy should be human, no matter what
/*function pawn FalkPickTarget(out float bestAim, out float bestDist, vector FireDir, vector projStart, float MaxRange, optional int fZedID)
{
   local pawn result;

   result = PickTarget(bestAim, bestDist, FireDir, projStart, MaxRange);

   // for some reason PickTarget sometimes return none, let's start by filling the gap
   if (result == none)
   {
      if (Enemy != none)
      {
         //warn("ZED"@fZedID@"Fill Enemy"@Level.TimeSeconds);
         result = Enemy;
      }

      else if (target != none && pawn(target) != none)
      {
         //warn("ZED"@fZedID@"Fill Target"@Level.TimeSeconds);
         result = pawn(target);
      }

      else if (focus != none && pawn(focus) != none)
      {
         //warn("ZED"@fZedID@"Fill Focus 3"@Level.TimeSeconds);
         result = pawn(focus);
      }
   }

   // now, since sometimes it picks wrong targets, check if it's a human and replace otherwise
   if (KFHumanPawn(result) == none)
   {
      if (Enemy != none && KFHumanPawn(Enemy) != none)
      {
         //warn("ZED"@fZedID@"Set KFHumanPawn Enemy"@Level.TimeSeconds);
         return Enemy;
      }

      if (target != none && KFHumanPawn(target) != none)
      {
         //warn("ZED"@fZedID@"Set KFHumanPawn Target"@Level.TimeSeconds);
         return pawn(target);
      }

      if (focus != none && KFHumanPawn(focus) != none)
      {
         //warn("ZED"@fZedID@"Set KFHumanPawn Focus"@Level.TimeSeconds);
         return pawn(focus);
      }
   }

   //warn("ZED"@fZedID@"Standard Target:"@result@"- Time:"@Level.TimeSeconds);
   return result;
}*/

defaultproperties
{
}
