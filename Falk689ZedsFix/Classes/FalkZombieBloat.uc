/* MEDIUM ZED */

class FalkZombieBloat extends FalkZombieBloatBase
   abstract;

#exec OBJ LOAD FILE=KFPlayerSound.uax
#exec OBJ LOAD FILE=KF_EnemiesFinalSnd.uax
#exec OBJ LOAD FILE=LairBloat_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

//var class<FleshHitEmitter> BileExplosion;
//var class<FleshHitEmitter> BileExplosionHeadless;

// vomit difficulty modifier
simulated function PostBeginPlay()
{
   Super.PostBeginPlay();

   if (Role < ROLE_Authority)
      return;

   // Difficulty Scaling
   if (Level.Game != none)
   {
      if (Level.Game.GameDifficulty < 3.0)
         FUsedVomitLimit  = FNormalVomitLimit;

      else if (Level.Game.GameDifficulty < 5.0)
         FUsedVomitLimit  = FHardVomitLimit;

      else if (Level.Game.GameDifficulty < 6.0)
         FUsedVomitLimit  = FSuicidalVomitLimit;

      else if (Level.Game.GameDifficulty < 8.0)
         FUsedVomitLimit  = FHOEVomitLimit;

      else
         FUsedVomitLimit  = FBBVomitLimit;
   }

   FUsedVomitDelay        = FVomitDuration / float(FUsedVomitLimit);
}

// don't do this
function FStairFixJump()
{
}

// resist to vomit and be immune to your bloat one
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   if (class<DamTypeBloatVomitFalk>(DamageType) == none)
   {
      if (HitIndex != 689 && class<DamTypeVomit>(damageType) != none)
         Super.TakeDamage(Damage * FAcidResistance, InstigatedBy, Hitlocation, Momentum, damageType, HitIndex);

      else
         Super.TakeDamage(Damage, InstigatedBy, Hitlocation, Momentum, damageType, HitIndex);
   }
}

// don't interrupt the vomit or moving attacks
simulated function bool HitCanInterruptAction()
{
   if (FIsVomiting || Level.TimeSeconds < FVomitEndTime || RunAttackTimeout > 0)
      return false;

   return !bShotAnim;
}

// stop vomiting on zap
simulated function SetZappedBehavior()
{
   FIsVomiting = False;
   Super.SetZappedBehavior();
}


// don't vomit on door
function DoorAttack(Actor A)
{
   if (bShotAnim || Physics == PHYS_Swimming)
      return;

   else if (A != None)
   {
      bShotAnim = true;
      SetAnimAction('DoorBash');
      GotoState('DoorBashing');
   }
}

function RangedAttack(Actor A)
{
   local int LastFireTime;
   local float ChargeChance;

   if (bShotAnim || bZapped || fFrozen)
      return;

   if ( Physics == PHYS_Swimming )
   {
      SetAnimAction('Claw');
      bShotAnim = true;
      LastFireTime = Level.TimeSeconds;
   }
   else if ( VSize(A.Location - Location) < MeleeRange + CollisionRadius + A.CollisionRadius )
   {
      bShotAnim = true;
      LastFireTime = Level.TimeSeconds;
      SetAnimAction('Claw');
      Controller.bPreparingMove = true;
      Acceleration = vect(0,0,0);
   }
   else if (KFDoorMover(A) == none && VSize(A.Location-Location) <= 250 && !bDecapitated)
   {
      bShotAnim = true;

      // Decide what chance the bloat has of charging during a puke attack
      if (Level.Game.GameDifficulty < 3.0)
         ChargeChance = 0.2;

      else if (Level.Game.GameDifficulty < 5.0)
         ChargeChance = 0.3;

      else if (Level.Game.GameDifficulty < 6.0)
         ChargeChance = 0.4;

      else if (Level.Game.GameDifficulty < 8.0)
         ChargeChance = 0.5;

      else // Hardest difficulty
         ChargeChance = 0.6;

      // Randomly do a moving attack so the player can't kite the zed
      if (FRand() < ChargeChance)
      {

         FVomitEndTime = Level.TimeSeconds + FVomitAnimDuration;
         SetAnimAction('ZombieBarfMoving');
         RunAttackTimeout = GetAnimDuration('ZombieBarf', 1.0);
         bMovingPukeAttack=true;
      }

      else
      {
         FVomitEndTime = Level.TimeSeconds + FVomitAnimDuration;
         SetAnimAction('ZombieBarf');
         Controller.bPreparingMove = true;
         Acceleration = vect(0,0,0);
      }


      // Randomly send out a message about Bloat Vomit burning(3% chance)
      if ( FRand() < 0.03 && KFHumanPawn(A) != none && PlayerController(KFHumanPawn(A).Controller) != none )
      {
         PlayerController(KFHumanPawn(A).Controller).Speech('AUTO', 7, "");
      }
   }
}

// Overridden to handle playing upper body only attacks when moving
simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;
   local bool bWantsToAttackAndMove;

   if (NewAction == '' || fFrozen)
      Return;

   bWantsToAttackAndMove = NewAction == 'ZombieBarfMoving';

   if( NewAction == 'Claw' )
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }
   else if( NewAction == 'DoorBash' )
   {
      CurrentDamtype = ZombieDamType[Rand(3)];
   }

   if( bWantsToAttackAndMove )
   {
      ExpectingChannel = AttackAndMoveDoAnimAction(NewAction);
   }
   else
   {
      ExpectingChannel = DoAnimAction(NewAction);
   }

   if( !bWantsToAttackAndMove && AnimNeedsWait(NewAction) )
   {
      bWaitForAnim = true;
   }
   else
   {
      bWaitForAnim = false;
   }

   if( Level.NetMode!=NM_Client )
   {
      AnimAction = NewAction;
      bResetAnimAct = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

// Handle playing the anim action on the upper body only if we're attacking and moving
simulated function int AttackAndMoveDoAnimAction(name AnimName)
{
   if (fFrozen)
      return 0;

   if (AnimName=='ZombieBarfMoving')
   {
      fAnimEndTime = Level.TimeSeconds + GetAnimDuration('ZombieBarf', 1.0) + fAnimEndAddDelay;
      AnimBlendParams(1, 1.0, 0.0,, FireRootBone);
      PlayAnim('ZombieBarf',, 0.1, 1);

      return 1;
   }

   return super.DoAnimAction(AnimName);
}


function PlayDyingSound()
{
   if (Level.NetMode != NM_Client && !fFrozen)
   {
      if (bGibbed)
      {
         PlaySound(sound'KF_EnemiesFinalSnd.Bloat_DeathPop', SLOT_Pain,2.0,true,525);
         return;
      }

      if (bDecapitated)
         PlaySound(HeadlessDeathSound, SLOT_Pain,1.30,true,525);

      else
         PlaySound(sound'KF_EnemiesFinalSnd.Bloat_DeathPop', SLOT_Pain,2.0,true,525);
   }
}



// Barf Time.
function SpawnTwoShots()
{
   local vector X;
   local vector FireStart;

   if (bDecapitated || bZapped || fFrozen || FVomitClass == None)
      return;

   //warn("SpawnTwoShots:"@Level.TimeSeconds);

   GetAxes(Rotation, X, FFireY, FFireZ);
   //FireStart = GetBoneCoords(default.FHeadBoneName).Origin + Vect(0, 10, 0);;

   /*if (Controller != none)
      FFireRotation = Controller.AdjustAim(SavedFireProperties, FireStart, 600);

   else
      FFireRotation = Rotation;*/

   FireStart      = GetBoneCoords(default.FHeadBoneName).Origin;

   FireStart.X   += FVomitDist * cos(Rotation.Yaw   * (pi / 32768));
   FireStart.Y   += FVomitDist * sin(Rotation.Yaw   * (pi / 32768));
   FireStart.Z   += FVomitDist * sin(Rotation.Pitch * (pi / 32768));

   if (!SavedFireProperties.bInitialized)
   {
      SavedFireProperties.AmmoClass = Class'SkaarjAmmo';
      SavedFireProperties.ProjectileClass = FVomitClass;
      SavedFireProperties.WarnTargetPct = 1;
      SavedFireProperties.MaxRange = 500;
      SavedFireProperties.bTossed = False;
      SavedFireProperties.bTrySplash = False;
      SavedFireProperties.bLeadTarget = True;
      SavedFireProperties.bInstantHit = True;
      SavedFireProperties.bInitialized = True;
   }

   FIsVomiting = True;
}


simulated function Tick(float DeltaTime)
{
   local vector BileExplosionLoc;
   local FleshHitEmitter GibBileExplosion;
   local rotator FireRotation;
   local vector FireStart, FireStartTwo, FireStartThree;
   local float  FVomitRandom;
   local projectile fVomitProj;

   Super.Tick(deltatime);

   // new vomit implementation
   if (Role == ROLE_Authority && FIsVomiting)
   {
      if (FCurVomitCount < FUsedVomitLimit)
      {
         if (FCurVomitTick < FUsedVomitDelay)
            FCurVomitTick += DeltaTime;

         else
         {
            FCurVomitCount++;
            FCurVomitTick -= FUsedVomitDelay;

            FireRotation   = Rotation;
            
            FVomitRandom   = FRand();

            FireStart      = GetBoneCoords(default.FHeadBoneName).Origin;
            FireStartTwo   = FireStart;
            FireStartThree = FireStart;

            FireStart.X   += FVomitDist * cos(Rotation.Yaw   * (pi / 32768));
            FireStart.Y   += FVomitDist * sin(Rotation.Yaw   * (pi / 32768));
            FireStart.Z   += FVomitDist * sin(Rotation.Pitch * (pi / 32768));

            // randomize yaw
            if (FVomitRandom > 0.5)
               FireRotation.Yaw += Rand(FFireYawVar); 

            else
               FireRotation.Yaw -= Rand(FFireYawVar);

            // randomize pitch
            if (FRand() > 0.5)
               FireRotation.Pitch += Rand(FFirePitchVar);

            else
               FireRotation.Pitch -= Rand(FFirePitchVar);

            VomitBID++;

            if (VomitBID > 100)
               VomitBID = 1;

            ToggleAuxCollision(false);
            fVomitProj = spawn(FVomitClass,,,FireStart, FireRotation);

            if (fVomitProj == None)
            {
               fVomitProj = spawn(FVomitClass,,,FireStartTwo, FireRotation);
               //warn("1");
            }

            if (fVomitProj == None)
            {
               FireStartThree.X   -= FVomitDist * cos(Rotation.Yaw   * (pi / 32768));
               FireStartThree.Y   -= FVomitDist * sin(Rotation.Yaw   * (pi / 32768));
               FireStartThree.Z   -= FVomitDist * sin(Rotation.Pitch * (pi / 32768));

               fVomitProj = spawn(FVomitClass,,,FireStartThree, FireRotation);
               //warn("2");
            }


            if (FBloatVomit(fVomitProj) != None)
               FBloatVomit(fVomitProj).BulletID = VomitBID;

            //else
               //warn("IBloatSonoSoloUominiGrassi");

            ToggleAuxCollision(true);

         }
      }

      else
      {
         FCurVomitCount = 0;
         FIsVomiting    = False;
         FCurVomitTick  = 0;
      }
   }

   if( Role == ROLE_Authority && bMovingPukeAttack )
   {
      // Keep moving toward the target until the timer runs out (anim finishes)
      if( RunAttackTimeout > 0 )
      {
         RunAttackTimeout -= DeltaTime;

         if( RunAttackTimeout <= 0 )
         {
            RunAttackTimeout = 0;
            bMovingPukeAttack=false;
         }
      }

      // Keep the gorefat moving toward its target when attacking
      if( bShotAnim && !bWaitForAnim )
      {
         if( LookTarget!=None )
         {
            Acceleration = AccelRate * Normal(LookTarget.Location - Location);
         }
      }
   }

   // Hack to force animation updates on the server for the bloat if he is relevant to someone
   // He has glitches when some of his animations don't play on the server. If we
   // find some other fix for the glitches take this out - Ramm
   if( Level.NetMode != NM_Client && Level.NetMode != NM_Standalone )
   {
      if( (Level.TimeSeconds-LastSeenOrRelevantTime) < 1.0  )
      {
         bForceSkelUpdate=true;
      }
      else
      {
         bForceSkelUpdate=false;
      }
   }

   if ( Level.NetMode!=NM_DedicatedServer && Health <= 0 && !bPlayBileSplash &&
         class<DamTypeBleedOut>(HitDamageType) == none)
   {
      if (!class'GameInfo'.static.UseLowGore())
      {
         BileExplosionLoc = self.Location;
         BileExplosionLoc.z += (CollisionHeight - (CollisionHeight * 0.5));

         GibBileExplosion = Spawn(class'FalkBileExplosion',self,, BileExplosionLoc);
         bPlayBileSplash = true;
      }
      else
      {
         BileExplosionLoc = self.Location;
         BileExplosionLoc.z += (CollisionHeight - (CollisionHeight * 0.5));

         GibBileExplosion = Spawn(class'LowGoreBileExplosion',self,, BileExplosionLoc);
         bPlayBileSplash = true;
      }
   }
}

function BileBomb()
{
   if (fBileBomb)
      return;

   fBileBomb  = True;
   fIsStunned = False;
   fFrozen    = False;

   ToggleAuxCollision(false);
   BloatJet = spawn(class'InstantAcidNadeFalk', self,,Location);
   ToggleAuxCollision(True);

   if (BloatJet != none)
   {
      BloatJet.BulletID = VomitBID++;
      BloatJet.Instigator = self;
   }
}

function PlayDyingAnimation(class<DamageType> DamageType, vector HitLoc)
{
   //    local bool AttachSucess;

   super.PlayDyingAnimation(DamageType, HitLoc);

   // Don't blow up with bleed out
   if( bDecapitated && class<DamTypeBleedOut>(DamageType) != none )
      return;

   if ( !class'GameInfo'.static.UseLowGore() )
   {
      HideBone(SpineBone2);
   }

   if(Role == ROLE_Authority)
   {
      BileBomb();
   }
}

simulated function HideBone(name boneName)
{
   local int BoneScaleSlot;
   local coords boneCoords;
   local bool bValidBoneToHide;

   if( boneName == LeftThighBone )
   {
      boneScaleSlot = 0;
      bValidBoneToHide = true;
      if( SeveredLeftLeg == none )
      {
         SeveredLeftLeg = Spawn(SeveredLegAttachClass,self);
         SeveredLeftLeg.SetDrawScale(SeveredLegAttachScale);
         boneCoords = GetBoneCoords( 'lleg' );
         AttachEmitterEffect( LimbSpurtEmitterClass, 'lleg', boneCoords.Origin, rot(0,0,0) );
         AttachToBone(SeveredLeftLeg, 'lleg');
      }
   }
   else if ( boneName == RightThighBone )
   {
      boneScaleSlot = 1;
      bValidBoneToHide = true;
      if( SeveredRightLeg == none )
      {
         SeveredRightLeg = Spawn(SeveredLegAttachClass,self);
         SeveredRightLeg.SetDrawScale(SeveredLegAttachScale);
         boneCoords = GetBoneCoords( 'rleg' );
         AttachEmitterEffect( LimbSpurtEmitterClass, 'rleg', boneCoords.Origin, rot(0,0,0) );
         AttachToBone(SeveredRightLeg, 'rleg');
      }
   }
   else if( boneName == RightFArmBone )
   {
      boneScaleSlot = 2;
      bValidBoneToHide = true;
      if( SeveredRightArm == none )
      {
         SeveredRightArm = Spawn(SeveredArmAttachClass,self);
         SeveredRightArm.SetDrawScale(SeveredArmAttachScale);
         boneCoords = GetBoneCoords( 'rarm' );
         AttachEmitterEffect( LimbSpurtEmitterClass, 'rarm', boneCoords.Origin, rot(0,0,0) );
         AttachToBone(SeveredRightArm, 'rarm');
      }
   }
   else if ( boneName == LeftFArmBone )
   {
      boneScaleSlot = 3;
      bValidBoneToHide = true;
      if( SeveredLeftArm == none )
      {
         SeveredLeftArm = Spawn(SeveredArmAttachClass,self);
         SeveredLeftArm.SetDrawScale(SeveredArmAttachScale);
         boneCoords = GetBoneCoords( 'larm' );
         AttachEmitterEffect( LimbSpurtEmitterClass, 'larm', boneCoords.Origin, rot(0,0,0) );
         AttachToBone(SeveredLeftArm, 'larm');
      }
   }
   else if ( boneName == HeadBone )
   {
      // Only scale the bone down once
      if( SeveredHead == none )
      {
         bValidBoneToHide = true;
         boneScaleSlot = 4;
         SeveredHead = Spawn(SeveredHeadAttachClass,self);
         SeveredHead.SetDrawScale(SeveredHeadAttachScale);
         boneCoords = GetBoneCoords( 'neck' );
         AttachEmitterEffect( NeckSpurtEmitterClass, 'neck', boneCoords.Origin, rot(0,0,0) );
         AttachToBone(SeveredHead, 'neck');
      }
      else
      {
         return;
      }
   }
   else if ( boneName == 'spine' )
   {
      bValidBoneToHide = true;
      boneScaleSlot = 5;
   }
   else if ( boneName == SpineBone2 )
   {
      bValidBoneToHide = true;
      boneScaleSlot = 6;
   }

   // Only hide the bone if it is one of the arms, legs, or head, don't hide other misc bones
   if( bValidBoneToHide )
   {
      SetBoneScale(BoneScaleSlot, 0.0, BoneName);
   }
}


State Dying
{
   function tick(float deltaTime)
   {
      FIsVomiting = False;

      if (BloatJet != none)
      {
         BloatJet.SetLocation(location);

         BloatJet.SetRotation(GetBoneRotation(FireRootBone));
      }

      super.tick(deltaTime);
   }
}

function RemoveHead()
{
   FIsVomiting = False;
   Super.RemoveHead();
}


// Added burnt limbs effect
simulated function ProcessHitFX()
{
   local Coords boneCoords;
   local class<xEmitter> HitEffects[4];
   local int i,j;
   local float GibPerterbation;

   if((Level.NetMode == NM_DedicatedServer) || bSkeletized || (Mesh == SkeletonMesh))
   {
      SimHitFxTicker = HitFxTicker;
      return;
   }

   for (SimHitFxTicker = SimHitFxTicker; SimHitFxTicker != HitFxTicker; SimHitFxTicker = (SimHitFxTicker + 1) % ArrayCount(HitFX))
   {
      j++;
      if (j > 30)
      {
         SimHitFxTicker = HitFxTicker;
         return;
      }

      if((HitFX[SimHitFxTicker].damtype == None) || (Level.bDropDetail && (Level.TimeSeconds - LastRenderTime > 3) && !IsHumanControlled()))
         continue;

      //log("Processing effects for damtype "$HitFX[SimHitFxTicker].damtype);

      if(HitFX[SimHitFxTicker].bone == 'obliterate' && !class'GameInfo'.static.UseLowGore())
      {
         SpawnGibs(HitFX[SimHitFxTicker].rotDir, 1);
         bGibbed = true;

         // Wait a tick on a listen server so the obliteration can replicate before the pawn is destroyed
         if(Level.NetMode == NM_ListenServer)
         {
            bDestroyNextTick = true;
            TimeSetDestroyNextTickTime = Level.TimeSeconds;
         }
         else
         {
            Destroy();
         }
         return;
      }

      boneCoords = GetBoneCoords(HitFX[SimHitFxTicker].bone);

      if (!Level.bDropDetail && !class'GameInfo'.static.NoBlood() && !bSkeletized && !class'GameInfo'.static.UseLowGore())
      {
         //AttachEmitterEffect(BleedingEmitterClass, HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir);

         HitFX[SimHitFxTicker].damtype.static.GetHitEffects(HitEffects, Health);

         if(!PhysicsVolume.bWaterVolume) // don't attach effects under water
         {
            for(i = 0; i < ArrayCount(HitEffects); i++)
            {
               if(HitEffects[i] == None)
                  continue;

               AttachEffect(HitEffects[i], HitFX[SimHitFxTicker].bone, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir);
            }
         }
      }

      if (class'GameInfo'.static.UseLowGore())
      {
         HitFX[SimHitFxTicker].bSever = false;

         switch(HitFX[SimHitFxTicker].bone)
         {
            case 'head':
               if(!bHeadGibbed)
               {
                  if (HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if(HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if(HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }
      }

      if(HitFX[SimHitFxTicker].bSever)
      {
         GibPerterbation = HitFX[SimHitFxTicker].damtype.default.GibPerterbation;

         switch(HitFX[SimHitFxTicker].bone)
         {
            case 'obliterate':
               break;

            case LeftThighBone:

               if (!bLeftLegGibbed)
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  bLeftLegGibbed=true;
               }
               break;

            case RightThighBone:
               if (!bRightLegGibbed)
               {
                  if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
                     SpawnSeveredGiblet(BurntDetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  else
                     SpawnSeveredGiblet(DetachedLegClass, boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, GetBoneRotation(HitFX[SimHitFxTicker].bone));

                  KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  KFSpawnGiblet(class 'KFMod.KFGibBrainb',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  KFSpawnGiblet(class 'KFMod.KFGibBrain',boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, GibPerterbation, 250);
                  bRightLegGibbed=true;
               }
               break;

            case 'head':
               if(!bHeadGibbed)
               {
                  if (HitFX[SimHitFxTicker].damtype == class'DamTypeDecapitation')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false);
                  }
                  else if(HitFX[SimHitFxTicker].damtype == class'DamTypeProjectileDecap')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, false, true);
                  }
                  else if(HitFX[SimHitFxTicker].damtype == class'DamTypeMeleeDecapitation')
                  {
                     DecapFX(boneCoords.Origin, HitFX[SimHitFxTicker].rotDir, true);
                  }

                  bHeadGibbed=true;
               }
               break;
         }


         if(HitFX[SimHitFXTicker].bone != 'Spine' && HitFX[SimHitFXTicker].bone != FireRootBone &&
               HitFX[SimHitFXTicker].bone != 'head' && Health <=0)
            HideBone(HitFX[SimHitFxTicker].bone);
      }
   }
}

// Added burnt limbs effect
simulated function SpawnGibs(Rotator HitRotation, float ChunkPerterbation)
{
   bGibbed = true;
   PlayDyingSound();

   if (class'GameInfo'.static.UseLowGore())
      return;

   if (FlamingFXs != none)
   {
      FlamingFXs.Emitters[0].SkeletalMeshActor = none;
      FlamingFXs.Destroy();
   }

   if (ObliteratedEffectClass != none)
      Spawn(ObliteratedEffectClass,,, Location, HitRotation);

   //super.SpawnGibs(HitRotation,ChunkPerterbation);

   if (FRand() < 0.1)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);


      if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
      {
         SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
         SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }

      else
      {
         SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
         SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }


      if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
         SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

      else
         SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

      if (DetachedSpecialArmClass != None)
      {
         if (bCrispified && BurntDetachedSpecialArmClass != Class'KFChar.SeveredArmClot')
            SpawnSeveredGiblet(BurntDetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

         else
            SpawnSeveredGiblet(DetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }

      else
      {
         if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
            SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
         
         else
            SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }
   }

   else if (FRand() < 0.25)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

      if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
      {
         SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
         SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }

      else
      {
         SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
         SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }

      if (FRand() < 0.5)
      {
         KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

         if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
            SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

         else
            SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
      }
   }
   else if (FRand() < 0.35)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
      
      if (bCrispified && BurntDetachedLegClass != Class'KFChar.SeveredLegClot')
         SpawnSeveredGiblet(BurntDetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);

      else
         SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
   }

   else if (FRand() < 0.5)
   {
      KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
      KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

      if (bCrispified && BurntDetachedArmClass != Class'KFChar.SeveredArmClot')
         SpawnSeveredGiblet(BurntDetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

      else
         SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
   }
}


// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieBloat(P) != none || Super.SameSpeciesAs(P));
}

static simulated function PreCacheStaticMeshes(LevelInfo myLevel)
{
   Super.PreCacheStaticMeshes(myLevel);
   myLevel.AddPrecacheStaticMesh(StaticMesh'kf_gore_trip_sm.limbs.bloat_head');
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T.bloat_cmb');
   myLevel.AddPrecacheMaterial(Combiner'KF_Specimens_Trip_T.bloat_env_cmb');
   myLevel.AddPrecacheMaterial(Texture'KF_Specimens_Trip_T.bloat_diffuse');
}

// don't stun while vomiting
function bool FlipOver()
{
   if (!FIsVomiting && Level.TimeSeconds > FVomitEndTime)
   {
      FIsVomiting = False;
      return Super.FlipOver();
   }

   return False;
}

// stop vomiting on freeze (but continue the animation)
function fFreezeFix()
{
   //warn("fFreezeFix:"@Level.TimeSeconds);
   FIsVomiting      = False;

   fClientFreezeFix();
}

simulated function fClientFreezeFix()
{
   //warn("fClientFreezeFix:"@Level.TimeSeconds);
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Bloat;
}

// first attempt to have dead bloats do damage
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   FIsVomiting = False;
   fIsStunned  = False;
   Super.Died(Killer, damageType, HitLocation);
}

defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieBloat_STANDARD"
   ControllerClass=Class'FMonsterController'
}
