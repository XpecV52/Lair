/* MEDIUM ZED */

class FalkZombieSirenBase extends FalkMonster
   abstract;

var () int ScreamRadius; // AOE for scream attack.

var () class <DamageType> ScreamDamageType;
var () int ScreamForce;

var(Shake)  rotator RotMag;            // how far to rot view
var(Shake)  float   RotRate;           // how fast to rot view
var(Shake)  vector  OffsetMag;         // max view offset vertically
var(Shake)  float   OffsetRate;        // how fast to offset view vertically
var(Shake)  float   ShakeTime;         // how long to shake for per scream
var(Shake)  float   ShakeFadeTime;     // how long after starting to shake to start fading out
var(Shake)  float	ShakeEffectScalar; // Overall scale for shake/blur effect
var(Shake)  float	MinShakeEffectScale;// The minimum that the shake effect drops off over distance
var(Shake)  float	ScreamBlurScale;   // How much motion blur to give from screams

var bool bAboutToDie;
var float DeathTimer;


var bool  FIsScreaming;      // are we screaming?


var class<Projectile> FScreamClass;      // what class to spawn for the scream effect
var float             FScreamEndTime;    // when the scream will hopefully stop
var float             FScreamSpawnTime;  // when we should spawn the scream effect
var float             FScreamDelay;      // delay between the start of the screaming animation and the effect
var int               FScreamDist;       // how distant from the face the scream is
var name              FHeadBoneName;     // head bone to spawn the scream around

defaultproperties
{
   ScreamRadius=700
   ScreamDamageType=Class'KFMod.SirenScreamDamage'
   RotMag=(Pitch=150,Yaw=150,Roll=150)
   RotRate=500.000000
   OffsetMag=(Y=5.000000,Z=1.000000)
   OffsetRate=500.000000
   ShakeTime=2.000000
   ShakeFadeTime=0.250000
   ShakeEffectScalar=1.000000
   MinShakeEffectScale=0.600000
   ScreamBlurScale=0.850000
   MeleeAnims(0)="Siren_Bite"
   MeleeAnims(1)="Siren_Bite2"
   MeleeAnims(2)="Siren_Bite"
   HitAnims(0)="HitReactionF"
   HitAnims(1)="HitReactionF"
   HitAnims(2)="HitReactionF"
   ZombieFlag=1
   MeleeDamage=13
   damageForce=5000
   KFRagdollName="Siren_Trip"
   ZombieDamType(0)=Class'KFMod.DamTypeSlashingAttack'
   ZombieDamType(1)=Class'KFMod.DamTypeSlashingAttack'
   ZombieDamType(2)=Class'KFMod.DamTypeSlashingAttack'
   ScreamDamage=8
   CrispUpThreshhold=7
   bCanDistanceAttackDoors=True
   bUseExtendedCollision=True
   ColOffset=(Z=48.000000)
   ColRadius=25.000000
   ColHeight=5.000000
   ExtCollAttachBoneName="Collision_Attach"
   SeveredLegAttachScale=0.700000
   OnlineHeadshotOffset=(X=6.000000,Z=41.000000)
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Challenge'
   SoundGroupClass=Class'KFMod.KFFemaleZombieSounds'
   IdleHeavyAnim="Siren_Idle"
   IdleRifleAnim="Siren_Idle"
   MeleeRange=45.000000
   GroundSpeed=100.000000
   WaterSpeed=100.000000
   HeadHeight=1.000000
   HeadScale=1.000000
   MenuName="Siren"
   MovementAnims(0)="Siren_Walk"
   MovementAnims(1)="Siren_Walk"
   MovementAnims(2)="Siren_Walk"
   MovementAnims(3)="Siren_Walk"
   WalkAnims(0)="Siren_Walk"
   WalkAnims(1)="Siren_Walk"
   WalkAnims(2)="Siren_Walk"
   WalkAnims(3)="Siren_Walk"
   IdleCrouchAnim="Siren_Idle"
   IdleWeaponAnim="Siren_Idle"
   IdleRestAnim="Siren_Idle"
   DrawScale=1.050000
   PrePivot=(Z=3.000000)
   RotationRate=(Yaw=45000,Roll=0)
   Mass=150.000000                // light zeds standard, siren makes for an exception due to her slimness
   MotionDetectorThreat=0.500000  // a single medium zed triggers a pipe, two trigger an ATM
   ScreamForce=0
   Health=300
   HealthMax=300
   fHSKThreshold=0.55             // instakill at 165 damage at 1man normal
   ScoringValue=20                // medium zeds standard
   PlayerCountHealthScale=0.1     // medium zeds standard
   PlayerNumHeadHealthScale=0.1   // medium zeds standard
   fDeathDrama=0.06               // medium zeds standard
   HeadHealth=150
   BleedOutDuration=5.0           // medium zeds standard
   OnlineHeadshotScale=1.9        // to help hitting her head better, was 1.2 in vanilla
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=10.000000         // medium zeds standard
   ZapDuration=3.5                // medium zeds standard
   fShotgunWeakness=True
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   fStunThresh=400                // medium zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   FHeadBoneName="CHR_Head"
   FScreamDist=10
   FScreamDelay=1.6
   FScreamClass=class'InstantSirenScream'
   fShockTime=5.0
   fFleshpoundRageImmune=True
}
