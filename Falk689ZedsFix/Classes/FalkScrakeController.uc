class FalkScrakeController extends FMonsterController;

var bool	bDoneSpottedCheck;

state ZombieHunt
{
	event SeePlayer(Pawn SeenPlayer)
	{
		if (!bDoneSpottedCheck && PlayerController(SeenPlayer.Controller) != none)
		{
			if (!KFGameType(Level.Game).bDidSpottedScrakeMessage && FRand() < 0.25)
			{
				PlayerController(SeenPlayer.Controller).Speech('AUTO', 14, "");
				KFGameType(Level.Game).bDidSpottedScrakeMessage = true;
			}

			bDoneSpottedCheck = true;
		}

		super.SeePlayer(SeenPlayer);
	}
}

function TimedFireWeaponAtEnemy()
{
	if ((Enemy == None) || FireWeaponAt(Enemy))
		SetCombatTimer();

	else SetTimer(0.01, True);
}

state ZombieCharge
{
    function GetOutOfTheWayOfShot(vector ShotDirection, vector ShotOrigin){}

	function bool StrafeFromDamage(float Damage, class<DamageType> DamageType, bool bFindDest)
	{
		return false;
	}

	function bool TryStrafe(vector sideDir)
	{
		return false;
	}

	function Timer()
	{
		Disable('NotifyBump');
		Target = Enemy;
		TimedFireWeaponAtEnemy();
	}

WaitForAnim:

	While (Monster(Pawn).bShotAnim)
		Sleep(0.25);

	if (!FindBestPathToward(Enemy, false,true))
		GotoState('ZombieRestFormation');

Moving:
	MoveToward(Enemy);
	WhatToDoNext(17);

	if (bSoaking)
		SoakStop("STUCK IN CHARGING!");
}

// fixed slowrage mechanics after freezehack was activated
state WaitForAnim
{
   Ignores SeePlayer,HearNoise,Timer,EnemyNotVisible,NotifyBump,Startle;

   // Don't do this in this state
   function GetOutOfTheWayOfShot(vector ShotDirection, vector ShotOrigin){}

   event AnimEnd(int Channel)
   {
      Pawn.AnimEnd(Channel);

      if (!Monster(Pawn).bShotAnim)
         WhatToDoNext(99);
   }

   function BeginState()
   {
      bUseFreezeHack = False;
   }

   function Tick(float Delta)
   {
      Global.Tick(Delta);

      if (bUseFreezeHack)
      {
         MoveTarget = None;
         MoveTimer = -1;
         Pawn.Acceleration = vect(0,0,0);

         if (KFMonster(Pawn) != none)
            KFMonster(Pawn).SetGroundSpeed(1);

         else
            Pawn.GroundSpeed = 1;

         Pawn.AccelRate = 0;
      }
   }

   function EndState()
   {
      if (Pawn != None)
      {
         Pawn.AccelRate = Pawn.Default.AccelRate;

         // try to properly reset ground speed
         if (FalkMonster(Pawn) != none)
         {
            // the zed was decapitated, take it into account
            if (FalkMonster(Pawn).DecapitatedBy != None || FalkMonster(Pawn).bDecapitated)
            {
               //warn("Decapitated, speed:"@Pawn.Default.GroundSpeed * FalkMonster(Pawn).fHeadlessSpeed);
               Pawn.GroundSpeed = Pawn.Default.GroundSpeed * FalkMonster(Pawn).fHeadlessSpeed;
            }

            if (FalkMonster(Pawn).fGroundSpeed > 0)
            {
               // don't set weird results, only decent values
               if (FalkMonster(Pawn).fGroundSpeed <= Pawn.Default.GroundSpeed * FalkMonster(Pawn).Default.fRageSpeed)
               {
                  //warn("Normal, speed :"@FalkMonster(Pawn).Default.fGroundSpeed);
                  Pawn.GroundSpeed = FalkMonster(Pawn).Default.fGroundSpeed;
               }

               else
               {
                  //warn("Weird, speed :"@Pawn.Default.GroundSpeed);
                  FalkMonster(Pawn).fGroundSpeed = Pawn.Default.GroundSpeed;
                  Pawn.GroundSpeed = Pawn.Default.GroundSpeed;
               }
            }

            else
            {
               //warn("Weird 2, speed :"@Pawn.Default.GroundSpeed);
               FalkMonster(Pawn).fGroundSpeed = Pawn.Default.GroundSpeed;
               Pawn.GroundSpeed               = Pawn.Default.GroundSpeed;
            }

            FalkMonster(Pawn).SetGroundSpeed(Pawn.GroundSpeed);
         }

         else
         {
            //warn("Weird 3, speed :"@Pawn.Default.GroundSpeed);
            Pawn.GroundSpeed = Pawn.Default.GroundSpeed;
         }
      }

      bUseFreezeHack = False;
   }

Begin:
   while (KFM.bShotAnim)
   {
      Sleep(0.15);
   }
   WhatToDoNext(99);
}

defaultproperties
{
}
