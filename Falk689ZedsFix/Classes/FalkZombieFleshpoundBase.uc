/* HEAVY ZED */

class FalkZombieFleshpoundBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=LairFleshpound_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool                bChargingPlayer;
var bool                bClientCharge;
var int                 TwoSecondDamageTotal;
var float               LastDamagedTime;
var float               RageEndTime;

var vector              RotMag;						// how far to rot view
var vector              RotRate;				      // how fast to rot view
var float	            RotTime;				      // how much time to rot the instigator's view
var vector              OffsetMag;			      // max view offset vertically
var vector              OffsetRate;				   // how fast to offset view vertically
var float	            OffsetTime;				   // how much time to offset view

var name                ChargingAnim;		      // How he runs when charging the player.

var int                 NrmRDThreshold;         // Rage damage threshold on normal
var int                 HrdRDThreshold;         // Rage damage threshold on hard
var int                 SuiRDThreshold;         // Rage damage threshold on suicidal
var int                 HOERDThreshold;         // Rage damage threshold on hell on earth
var int                 BBRDThreshold;          // Rage damage threshold on bloodbath
var int                 CurRDThreshold;         // currently used threshold for rage damage

var FleshPoundAvoidArea AvoidArea;              // Make the other AI fear this AI

var bool                bFrustrated;            // The fleshpound is tired of being kited and is pissed and ready to attack
var float               fRageDamageMult;        // damage multiplier when pissed off
var float               FMinRageTime;           // minimum rage time
var float               FRandRageTime;          // random additional rage time 
var float               FUnfreezeTime;          // unfreeze fix function call time plus wait, used to block rage for a while
var float               FFreezeRageWait;        // time, in seconds, to wait before freeze to allow rage
var float               FChargeMoveTime;        // time when we'll be allowed to move after a rage state started
var float               FRageAnimDuration;      // duration in seconds of the rage anim

var int                 fFleshpoundID;          // debug id, used to make debug shit easier for us
var float               fFleshpoundAttackDelay; // time between RangedAttack calls

var float               fLastMeleeDamage;       // last time melee damage was called
var float               fMeleeDamageCD;         // melee damage cooldown, used to further prevent double hits

replication
{
   reliable if(Role == ROLE_Authority)
      bChargingPlayer, bFrustrated, CurRDThreshold, FUnfreezeTime;
}

defaultproperties
{
   RotMag=(X=500.000000,Y=500.000000,Z=600.000000)
   RotRate=(X=12500.000000,Y=12500.000000,Z=12500.000000)
   RotTime=6.000000
   OffsetMag=(X=5.000000,Y=10.000000,Z=5.000000)
   OffsetRate=(X=300.000000,Y=300.000000,Z=300.000000)
   OffsetTime=3.500000
   ChargingAnim="PoundRun"
   NrmRDThreshold=400
   HrdRDThreshold=375
   SuiRDThreshold=350
   HOERDThreshold=325
   BBRDThreshold=300
   MeleeAnims(0)="PoundAttack1"
   MeleeAnims(1)="PoundAttack2"
   MeleeAnims(2)="PoundAttack3"
   StunsRemaining=1
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   DamageToMonsterScale=5.000000
   ZombieFlag=3
   damageForce=15000
   bFatAss=True
   KFRagdollName="FleshPound_Trip"
   SpinDamConst=20.000000
   SpinDamRand=20.000000
   Intelligence=BRAINS_Mammal
   bUseExtendedCollision=True
   ColOffset=(Z=52.000000)
   ColRadius=36.000000
   ColHeight=35.000000
   SeveredArmAttachScale=1.300000
   SeveredLegAttachScale=1.200000
   SeveredHeadAttachScale=1.500000
   OnlineHeadshotOffset=(X=22.000000,Z=68.000000)
   OnlineHeadshotScale=1.300000
   bBoss=True
   IdleHeavyAnim="PoundIdle"
   IdleRifleAnim="PoundIdle"
   RagDeathUpKick=100.000000
   MeleeRange=55.000000
   GroundSpeed=128.000000
   WaterSpeed=128.000000
   HeadHeight=2.500000
   HeadScale=1.300000
   MovementAnims(0)="PoundWalk"
   MovementAnims(1)="WalkB"
   WalkAnims(0)="PoundWalk"
   WalkAnims(1)="WalkB"
   WalkAnims(2)="RunL"
   WalkAnims(3)="RunR"
   IdleCrouchAnim="PoundIdle"
   IdleWeaponAnim="PoundIdle"
   IdleRestAnim="PoundIdle"
   PrePivot=(Z=0.000000)
   Skins(1)=Shader'KFCharacters.FPAmberBloomShader'
   RotationRate=(Yaw=45000,Roll=0)
   Mass=500.000000               // heavy zeds standard
   HeadHealth=625.0              
   Health=1250
   HealthMax=1250                // should be 8193 at 12men hoe
   fHSKThreshold=0.72            // instakill at 900 damage at 1man normal
   fDeathDrama=0.08              // heavy zeds standard
   PlayerCountHealthScale=0.25   // heavy zeds standard
   PlayerNumHeadHealthScale=0.25 // heavy zeds standard
   BleedOutDuration=7.0          // heavy zeds standard
   ScoringValue=40               // heavy zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=20.000000         // heavy zeds standard
   ZapDuration=3.0                // heavy zeds standard
   MotionDetectorThreat=1.000000  // a single heavy zed will trigger both a pipe and an ATM
   MenuName="Fleshpound"
   fDamageMultiplier=0.5
   fKaboomMultiplier=1.5
   fSFireMultiplier=0.5
   fWFireMultiplier=0.5
   MeleeDamage=35
   fRageDamageMult=1.5
   FZMultiFix=True
   fStunThresh=600               // heavy zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fShouldRage=True
   fShouldExitRage=True
   FMinRageTime=10.0
   FRandRageTime=3.0
   fAnimEndAddDelay=0.2
   fRageSpeed=2.0
   fNormalRageSpeed=2.0
   fHardRageSpeed=2.0
   fSuicidalRageSpeed=2.0
   fHOERageSpeed=2.0
   fBBRageSpeed=2.0
   fRageSpeedCap=310.0
   FFreezeRageWait=0.5
   FRageAnimDuration=2.4
   fFleshpoundAttackDelay=0.2
   fMeleeDamageCD=0.3
   fFleshpoundRageImmune=True
}
