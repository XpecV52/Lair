/* LIGHT ZED */

class FalkZombieClot extends FalkZombieClotBase
   abstract;

#exec OBJ LOAD FILE=LairClot_A.ukx
#exec OBJ LOAD FILE=KF_Freaks_Trip.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=KF_Specimens_Trip_T.utx

//----------------------------------------------------------------------------
// NOTE: All Variables are declared in the base class to eliminate hitching
//----------------------------------------------------------------------------

function ClawDamageTarget()
{
   local vector PushDir;
   local KFPawn KFP;

   if (fFrozen)
      return;

   // If zombie has latched onto us...
   if (!fFrozen && !bZapped && !fIsStunned && MeleeDamageTarget(MeleeDamage, PushDir))
   {
      KFP = KFPawn(Controller.Target);

      PlaySound(MeleeAttackHitSound, SLOT_Interact, 2.0);

      if( !bDecapitated && KFP != none )
      {
         if ( KFPlayerReplicationInfo(KFP.PlayerReplicationInfo) == none ||
               KFP.GetVeteran().static.CanBeGrabbed(KFPlayerReplicationInfo(KFP.PlayerReplicationInfo), self))
         {
            if( DisabledPawn != none )
            {
               DisabledPawn.bMovementDisabled = false;
            }

            KFP.DisableMovement(GrappleDuration);
            DisabledPawn = KFP;
         }
      }
   }
}

function RangedAttack(Actor A)
{
   if (bShotAnim || Physics == PHYS_Swimming || fFrozen)
      return;

   else if (CanAttack(A))
   {
      bShotAnim = true;
      SetAnimAction('Claw');
      return;
   }
}

simulated event SetAnimAction(name NewAction)
{
   local int meleeAnimIndex;

   if (NewAction == '' || fFrozen)
      Return;

   if (NewAction == 'Claw')
   {
      meleeAnimIndex = Rand(3);
      NewAction = meleeAnims[meleeAnimIndex];
      CurrentDamtype = ZombieDamType[meleeAnimIndex];
   }

   // Do a claw attack on door, not a grapple
   else if (NewAction == 'DoorBash')
      CurrentDamtype = ZombieDamType[Rand(3)];

   ExpectingChannel = DoAnimAction(NewAction);

   if (AnimNeedsWait(NewAction))
      bWaitForAnim = true;

   if (Level.NetMode != NM_Client)
   {
      AnimAction       = NewAction;
      bResetAnimAct    = True;
      ResetAnimActTime = Level.TimeSeconds+0.3;
   }
}

simulated function bool AnimNeedsWait(name TestAnim)
{
   if (TestAnim == 'KnockDown' || TestAnim == 'DoorBash')
      return true;

   return false;
}

simulated function int DoAnimAction(name AnimName)
{
   if (fFrozen)
      return 0; 

   if (AnimName=='ClotGrapple' || AnimName=='ClotGrappleTwo' || AnimName=='ClotGrappleThree')
   {
      AnimBlendParams(1, 1.0, 0.1,, FireRootBone, true);
      PlayAnim(AnimName,, 0.1, 1);

      // Randomly send out a message about Clot grabbing you(10% chance)
      if ( FRand() < 0.10 && LookTarget != none && KFPlayerController(LookTarget.Controller) != none &&
            VSizeSquared(Location - LookTarget.Location) < 2500 &&
            Level.TimeSeconds - KFPlayerController(LookTarget.Controller).LastClotGrabMessageTime > ClotGrabMessageDelay &&
            class<FVeterancyTypes>(KFPlayerController(LookTarget.Controller).SelectedVeterancy).static.CanBeGrabbed(KFPlayerReplicationInfo(LookTarget.PlayerReplicationInfo), self))

      {
         PlayerController(LookTarget.Controller).Speech('AUTO', 11, "");
         KFPlayerController(LookTarget.Controller).LastClotGrabMessageTime = Level.TimeSeconds;
      }

      bGrappling = true;
      GrappleEndTime = Level.TimeSeconds + GrappleDuration;
      fAnimEndTime   = Level.TimeSeconds + GetAnimDuration(AnimName, 1.0) + fAnimEndAddDelay;

      return 1;
   }

   return super.DoAnimAction( AnimName );
}

simulated function Tick(float DeltaTime)
{
   Super.Tick(DeltaTime);

   if( bShotAnim && Role == ROLE_Authority )
   {
      if( LookTarget!=None )
      {
         Acceleration = AccelRate * Normal(LookTarget.Location - Location);
      }
   }

   if( Role == ROLE_Authority && bGrappling )
   {
      if( Level.TimeSeconds > GrappleEndTime )
      {
         bGrappling = false;
      }
   }

   // if we move out of melee range, stop doing the grapple animation
   if(bGrappling && LookTarget != none && !fFrozen)
   {
      if( VSize(LookTarget.Location - Location) > MeleeRange + CollisionRadius + LookTarget.CollisionRadius )
      {
         bGrappling = false;
         AnimEnd(1);
      }
   }
}

function RemoveHead()
{
   Super.RemoveHead();
   MeleeAnims[0] = 'Claw';
   MeleeAnims[1] = 'Claw2';
   MeleeAnims[2] = 'Claw3';

   if (DisabledPawn != none)
   {
      DisabledPawn.bMovementDisabled = false;
      DisabledPawn = none;
   }
}

function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   if (DisabledPawn != none)
   {
      DisabledPawn.bMovementDisabled = false;
      DisabledPawn = none;
   }

   super.Died(Killer, damageType, HitLocation);

}

simulated function Destroyed()
{
   super.Destroyed();

   if (DisabledPawn != none)
   {
      DisabledPawn.bMovementDisabled = false;
      DisabledPawn = none;
   }
}

// Shouldn't fight with our own and boss
function bool SameSpeciesAs(Pawn P)
{
   return (FalkZombieClot(P) != none || Super.SameSpeciesAs(P)); 
}

static simulated function PreCacheStaticMeshes(LevelInfo myLevel)
{
   Super.PreCacheStaticMeshes(myLevel);
}

static simulated function PreCacheMaterials(LevelInfo myLevel)
{
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.ClotCombinerFinal');
   myLevel.AddPrecacheMaterial(Combiner'LairTextures_T.ZedsUpscale.ClotEnvironmentCombiner');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.clot_diffuse');
   myLevel.AddPrecacheMaterial(Texture'LairTextures_T.ZedsUpscale.clot_spec');
}

// release grab on fix
function fFreezeFix()
{
   bGrappling     = false;
   //GrappleEndTime = Level.TimeSeconds - 1;

   if (DisabledPawn != none)
   {
      DisabledPawn.bMovementDisabled = false;
      DisabledPawn = none;
   }

   Super.fFreezeFix();
}

// reset grab state on unfreeze, just to be sure
function fUnfreezeFix()
{
   bGrappling = false;
   GrappleEndTime = Level.TimeSeconds - 1;

   Super.fUnfreezeFix();
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Clot;
}

// release grabbed pawn on zap
simulated function SetZappedBehavior()
{
   if (fFrozen)
      return;

   bGrappling     = false;

   if (DisabledPawn != none)
   {
      DisabledPawn.bMovementDisabled = false;
      DisabledPawn = none;
   }

   Super.SetZappedBehavior();
}



defaultproperties
{
   EventClasses(0)="Falk689ZedsFix.FZombieClot_STANDARD"
   ControllerClass=Class'FMonsterController'
}

