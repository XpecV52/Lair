/* HEAVY ZED */

class FZombieFleshpound_STANDARD extends FalkZombieFleshpound;

#exec OBJ LOAD FILE=LairFleshpound_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmPound'
   DetachedLegClass=Class'KFChar.SeveredLegPound'
   DetachedHeadClass=Class'KFChar.SeveredHeadPound'
   BurntDetachedHeadClass=Class'FleshpoundSeveredBurntHead'
   BurntDetachedArmClass=Class'FleshpoundSeveredBurntArm'
   BurntDetachedLegClass=Class'FleshpoundSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Fleshpound.FP_Challenge'
   AmbientSound=Sound'KF_BaseFleshpound.FP_IdleLoop'
   Mesh=SkeletalMesh'LairFleshpound_A.FleshPound_Freak'
   Skins(0)=Combiner'LairTextures_T.ZedsUpscale.FleshpoundCombinerFinal'
}
