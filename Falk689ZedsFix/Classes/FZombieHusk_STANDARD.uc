/* MEDIUM ZED */

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairHusk_A.ukx

class FZombieHusk_STANDARD extends FalkZombieHusk;

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmHusk'
   DetachedLegClass=Class'HuskSeveredLeg'
   DetachedHeadClass=Class'KFChar.SeveredHeadHusk'
   DetachedSpecialArmClass=Class'KFChar.SeveredArmHuskGun'
   BurntDetachedArmClass=Class'HuskSeveredBurntArm'
   BurntDetachedLegClass=Class'HuskSeveredBurntLeg'
   BurntDetachedHeadClass=Class'HuskSeveredBurntHead'
   BurntDetachedSpecialArmClass=Class'HuskSeveredBurntSpecialArm'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Husk.Husk_Challenge'
   AmbientSound=Sound'KF_BaseHusk.Husk_IdleLoop'
   Mesh=SkeletalMesh'LairHusk_A.Burns_Freak'
   Skins(0)=Texture'KF_Specimens_Trip_T_Two.burns.burns_tatters'
   Skins(1)=Shader'LairTextures_T.ZedsUpscale.BurnsShader'
}
