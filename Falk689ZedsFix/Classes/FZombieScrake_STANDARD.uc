/* HEAVY ZED */

class FZombieScrake_STANDARD extends FalkZombieScrake;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairScrake_A.ukx

defaultproperties
{
   SawAttackLoopSound=Sound'KF_BaseScrake.Chainsaw.Scrake_Chainsaw_Impale'
   ChainSawOffSound=SoundGroup'KF_ChainsawSnd.Chainsaw_Deselect'
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Chainsaw_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmScrake'
   DetachedLegClass=Class'KFChar.SeveredLegScrake'
   DetachedHeadClass=Class'KFChar.SeveredHeadScrake'
   BurntDetachedArmClass=Class'ScrakeSeveredBurntArm'
   BurntDetachedLegClass=Class'ScrakeSeveredBurntLeg'
   BurntDetachedSpecialArmClass=Class'ScrakeSeveredBurntSpecialArm'
   BurntDetachedHeadClass=Class'ScrakeSeveredBurntHead'
   DetachedSpecialArmClass=Class'KFChar.SeveredArmScrakeSaw'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Scrake.Scrake_Challenge'
   AmbientSound=Sound'KF_BaseScrake.Chainsaw.Scrake_Chainsaw_Idle'
   Mesh=SkeletalMesh'LairScrake_A.Scrake_Freak'
   Skins(0)=Shader'LairTextures_T.ZedsUpscale.ScrakeShader'
   Skins(1)=TexPanner'LairTextures_T.ZedsUpscale.ChainsawBladeTexPanner'
}
