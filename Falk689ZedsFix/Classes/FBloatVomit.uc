class FBloatVomit extends FBloatExplosionVomit;

defaultproperties
{
   TouchDetonationDelay=0.000000
   BaseDamage=2
   Speed=300.000000
   Damage=2.000000
   FDamage=2.000000
   MomentumTransfer=0.000000
   MyDamageType=Class'DamTypeBloatVomitFalk'
   ImpactSound=SoundGroup'KF_EnemiesFinalSnd.Bloat.Bloat_AcidSplash'
   DrawType=DT_StaticMesh
   StaticMesh=StaticMesh'LairStaticMeshes_SM.VomitRework.BloatVomit'
   DrawScale=0.6
   bDynamicLight=False
   LifeSpan=8.000000
   Skins(0)=Texture'LairTextures_T.VomitRework.BloatVomit'
   bUseCollisionStaticMesh=True
   bBlockHitPointTraces=False
   CollisionRadius=5.000000
   CollisionHeight=5.000000
}
