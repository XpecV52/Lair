class FStalkersKills extends SRCustomProgressInt;

function NotifyPlayerKill(Pawn Killed, class<DamageType> damageType)
{
   if(FalkZombieStalker(Killed)                        != None &&
      (damageType == class'DamTypeBullpupFalk'                 ||
       damageType == class'DamTypeAK47LLIAssaultRifleFalk'     ||
       damageType == class'DamTypeSCARMK17AssaultRifleFalk'    ||
       damageType == class'DamTypeM4AssaultRifleFalk'          ||
       damageType == class'DamTypeFNFALAssaultRifleFalk'       ||
       damageType == class'DamTypeMKb42AssaultRifleFalk'       ||
       damageType == class'DamTypeThompsonFalk'                ||
       damageType == class'DamTypeThompsonDrumFalk'            ||
       damageType == class'DamTypeSPThompsonFalk'              ||
       damageType == class'DamTypeAAR525Falk'                  ||
       damageType == class'DamTypeAcidNadeFalk'                ||
       damageType == class'DamTypecz805bFalk'                  ||
       damageType == class'DamTypeGlock18SAFalk'               ||
       damageType == class'DamtypeAR15SAFalk'                  ||
       damageType == class'DamTypeAK12SAFalk'                  ||
       damageType == class'DamTypeSlayer_AUGFalk'              ||
       damageType == class'DamTypePDWAssaultRifleFalk'         ||
       damageType == class'DamTypeAK12LLIFalk'                 ||
       damageType == class'DamTypeHKG36CSAAssaultRifleFalk'))
   {
      IncrementProgress(1);
   }
}

defaultproperties 
{
   ProgressName = "Custom stalkers kills"
}
