/* MEDIUM ZED */

class FalkZombieBloatBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=KF_EnemiesFinalSnd.uax
#exec OBJ LOAD FILE=LairBloat_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var InstantAcidNadeFalk BloatJet;
var bool    bMovingPukeAttack;
var bool    fBileBomb;
var float   RunAttackTimeout;

// new vomit implementation vars
var class<Projectile> FVomitClass;           // class of the vomit projectile
var float             FUsedVomitDelay;       // used delay between two vomit projectiles
var float             FCurVomitTick;         // current vomit tick
var float             FVomitDuration;        // how long do we vomit? 
var float             FVomitAnimDuration;    // how long does the vomit anim last?
var float             FVomitEndTime;         // when we'll end vomiting
var byte              FUsedVomitLimit;       // how many projectiles do we spawn?
var byte              FCurVomitCount;        // how many projectiles we spawned so far
var rotator           FFireRotation;         // where are we aiming?
var int               FFireYawVar;           // maximum yaw variation of a projectile trajectory from the aim
var int               FFirePitchVar;         // maximum pitch variation of a projectile trajectory from the aim
var bool              FIsVomiting;           // are we vomiting?
var vector            FFireStart;            // where the vomit is spawned
var vector            FFireY;                // Y variation on the projectile spawn
var vector            FFireZ;                // Z variation on the projectile spawn
var name 			    FHeadBoneName;         // head bone to spawn vomit around
var byte              VomitBID;              // vomit bullet ID
var int               FVomitDist;            // how distant from the face the vomit is
var float             FAcidResistance;       // acid damage resistance

// vomit projectiles difficulty limits
var byte              FNormalVomitLimit;
var byte              FHardVomitLimit;
var byte              FSuicidalVomitLimit;
var byte              FHOEVomitLimit;
var byte              FBBVomitLimit;

replication
{
   reliable if(Role == ROLE_Authority)
      fBileBomb, FVomitEndTime;
}

defaultproperties
{
   MeleeAnims(0)="BloatChop2"
   MeleeAnims(1)="BloatChop2"
   MeleeAnims(2)="BloatChop2"
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   ZombieFlag=1
   MeleeDamage=14
   damageForce=70000
   bFatAss=True
   KFRagdollName="Bloat_Trip"
   PuntAnim="BloatPunt"
   Intelligence=BRAINS_Stupid
   bUseExtendedCollision=True
   ColOffset=(Z=60.000000)
   ColRadius=27.000000
   ColHeight=22.000000
   SeveredArmAttachScale=1.100000
   SeveredLegAttachScale=1.300000
   SeveredHeadAttachScale=1.700000
   OnlineHeadshotOffset=(X=5.000000,Z=70.000000)
   OnlineHeadshotScale=1.500000
   AmmunitionClass=Class'KFMod.BZombieAmmo'
   IdleHeavyAnim="BloatIdle"
   IdleRifleAnim="BloatIdle"
   MeleeRange=30.000000
   GroundSpeed=75.000000
   WaterSpeed=75.000000
   HealthMax=525
   Health=525
   HeadHealth=25               
   fHSKThreshold=0.12                // instakill at 63 damage at 1man normal
   HeadHeight=2.500000
   HeadScale=1.500000
   AmbientSoundScaling=8.000000
   MenuName="Bloat"
   MovementAnims(0)="WalkBloat"
   MovementAnims(1)="WalkBloat"
   WalkAnims(0)="WalkBloat"
   WalkAnims(1)="WalkBloat"
   WalkAnims(2)="WalkBloat"
   WalkAnims(3)="WalkBloat"
   IdleCrouchAnim="BloatIdle"
   IdleWeaponAnim="BloatIdle"
   IdleRestAnim="BloatIdle"
   DrawScale=1.075000
   PrePivot=(Z=5.000000)
   SoundVolume=200
   RotationRate=(Yaw=45000,Roll=0)
   Mass=400.000000                  // medium zeds standard
   ScoringValue=20                  // medium zeds standard
   MotionDetectorThreat=0.500000    // a single medium zed triggers a pipe, two trigger an ATM
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=10.000000           // medium zeds standard
   ZapDuration=3.5                  // medium zeds standard
   fDeathDrama=0.06                 // medium zeds standard
   PlayerCountHealthScale=0.1       // medium zeds standard
   PlayerNumHeadHealthScale=0.1     // medium zeds standard
   BleedOutDuration=5.0             // medium zeds standard
   fShotgunWeakness=True 
   fStunThresh=400                  // medium zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   FVomitClass=Class'FBloatVomit'
   FVomitDuration=0.666
   FVomitAnimDuration=2.4
   FFireYawVar=1000
   FFirePitchVar=300
   FNormalVomitLimit=15
   FHardVomitLimit=20
   FSuicidalVomitLimit=25
   FHOEVomitLimit=30
   FBBVomitLimit=35
   FHeadBoneName="CHR_Head"
   FZMultiFix=True
   FVomitDist=10
   fFleshpoundRageImmune=True
   FAcidResistance=0.25
}
