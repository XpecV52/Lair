/* LIGHT ZED */

class FalkZombieStalkerBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=KFX.utx
#exec OBJ LOAD FILE=KF_BaseStalker.uax
#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairStalker_A.ukx

var float NextCheckTime;
var KFHumanPawn LocalKFHumanPawn;
var float LastUncloakTime;

defaultproperties
{
   MeleeAnims(0)="StalkerSpinAttack"
   MeleeAnims(1)="StalkerAttack1"
   MeleeAnims(2)="JumpAttack"
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   damageForce=5000
   KFRagdollName="Stalker_Trip"
   CrispUpThreshhold=10
   PuntAnim="ClotPunt"
   SeveredArmAttachScale=0.800000
   SeveredLegAttachScale=0.700000
   OnlineHeadshotOffset=(X=18.000000,Z=33.000000)
   OnlineHeadshotScale=1.200000
   SoundGroupClass=Class'KFMod.KFFemaleZombieSounds'
   IdleHeavyAnim="StalkerIdle"
   IdleRifleAnim="StalkerIdle"
   MeleeRange=30.000000
   GroundSpeed=200.000000
   WaterSpeed=200.000000
   JumpZ=350.000000
   HeadHeight=2.500000
   MenuName="Stalker"
   MovementAnims(0)="ZombieRun"
   MovementAnims(1)="ZombieRun"
   MovementAnims(2)="ZombieRun"
   MovementAnims(3)="ZombieRun"
   WalkAnims(0)="ZombieRun"
   WalkAnims(1)="ZombieRun"
   WalkAnims(2)="ZombieRun"
   WalkAnims(3)="ZombieRun"
   IdleCrouchAnim="StalkerIdle"
   IdleWeaponAnim="StalkerIdle"
   IdleRestAnim="StalkerIdle"
   DrawScale=1.100000
   PrePivot=(Z=5.000000)
   RotationRate=(Yaw=45000,Roll=0)
   fShouldSetHitLoc=True
   Mass=150.000000                // light zeds standard
   MotionDetectorThreat=0.250000  // two light zeds trigger a pipe, four trigger an ATM
   ZombieDamType[0]=Class'DamTypeSlashingAttackFalk'
   ZombieDamType[1]=Class'DamTypeSlashingAttackFalk'
   ZombieDamType[2]=Class'DamTypeSlashingAttackFalk'
   Health=100
   HealthMax=100
   HeadHealth=25
   MeleeDamage=9
   fHSKThreshold=0.35             // instakill at 35 damage at 1man normal
   fDeathDrama=0.05               // light zeds standard
   ScoringValue=10                // light zeds standard
   PlayerCountHealthScale=0.0     // light zeds standard
   PlayerNumHeadHealthScale=0.0   // light zeds standard
   BleedOutDuration=3.0           // light zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=5.000000          // light zeds standard
   fStunThresh=200                // light zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
}
