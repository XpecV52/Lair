/* MEDIUM ZED */

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairSiren_A.ukx

class FZombieSiren_STANDARD extends FalkZombieSiren;

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Talk'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Jump'
   DetachedLegClass=Class'KFChar.SeveredLegSiren'
   DetachedHeadClass=Class'KFChar.SeveredHeadSiren'
   BurntDetachedHeadClass=Class'SirenSeveredBurntHead'
   BurntDetachedLegClass=Class'SirenSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.siren.Siren_Death'
   AmbientSound=Sound'KF_BaseSiren.Siren_IdleLoop'
   Mesh=SkeletalMesh'LairSiren_A.Siren_Freak'
   Skins(0)=FinalBlend'KF_Specimens_Trip_T.siren_hair_fb'
   Skins(1)=Combiner'LairTextures_T.ZedsUpscale.SirenCombinerFinal'
}
