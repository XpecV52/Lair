/* SPECIAL ZED */

// This zed is basically a rare bonus for players to grind damage points on for their perks

#exec OBJ LOAD FILE=LairClot_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

class FZombieMetalClot_STANDARD extends FalkZombieClot;

// #exec OBJ LOAD FILE=MetalClot.utx

// Prevent damage from other zeds
function TakeDamage(int Damage, Pawn InstigatedBy, Vector Hitlocation, Vector Momentum, class<DamageType> damageType, optional int HitIndex)
{
   if (KFHumanPawn(InstigatedBy) != none)
      Super.TakeDamage(Damage, InstigatedBy, Hitlocation, Momentum, damageType, HitIndex); 
}


// Metal Clot shouldn't show skin changes from burns 
simulated function ZombieCrispUp()
{
   bAshen = true;
   bCrispified = true;
   SetBurningBehavior();
}


// Shouldn't fight with anything but humans
function bool SameSpeciesAs(Pawn P)
{
   return (KFHumanPawn(P) == none);
}


// give the Metal Clot his own variables
function float DifficultyHealthModifer()
{
   if (Level.Game.GameDifficulty >= 7.0) // hoe
      return 1.75;

   if (Level.Game.GameDifficulty >= 5.0) // suicidal
      return 1.5;

   else if (Level.Game.GameDifficulty >= 4.0) // hard
      return 1.25;

   return 1.0;
}

// this dood no stun
function bool FlipOver()
{
   Return False;
}


// vanilla function since we don't burn and we don't want to introduce bugs
simulated function DecapFX(Vector DecapLocation, Rotator DecapRotation, bool bSpawnDetachedHead, optional bool bNoBrainBits)
{
	local float GibPerterbation;
	local BrainSplash SplatExplosion;
	local int i;

	// Do the cute version of the Decapitation
	if (class'GameInfo'.static.UseLowGore())
	{
		CuteDecapFX();
		return;
	}

	bNoBrainBitEmitter = bNoBrainBits;

	GibPerterbation = 0.060000; // damageType.default.GibPerterbation;

	if (bSpawnDetachedHead)
	   SpecialHideHead();

	else
		HideBone(HeadBone);

	if (bSpawnDetachedHead)
		SpawnSeveredGiblet(DetachedHeadClass, DecapLocation, DecapRotation, GibPerterbation, GetBoneRotation(HeadBone));

	// Plug in headless anims if we have them
	for (i = 0; i < 4; i++)
	{
		if (HeadlessWalkAnims[i] != '' && HasAnim(HeadlessWalkAnims[i]))
		{
			MovementAnims[i] = HeadlessWalkAnims[i];
			WalkAnims[i]     = HeadlessWalkAnims[i];
		}
	}

	if (!bSpawnDetachedHead && !bNoBrainBits && EffectIsRelevant(DecapLocation,false))
	{
		KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',DecapLocation, self.Rotation, GibPerterbation, 250);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',DecapLocation, self.Rotation, GibPerterbation, 250);
	}
   
	SplatExplosion = Spawn(class 'BrainSplash',self,, DecapLocation);
}

// more vanilla stuff to hopefully prevent more bugs
simulated function SpawnGibs(Rotator HitRotation, float ChunkPerterbation)
{
	bGibbed = true;
	PlayDyingSound();

	if (class'GameInfo'.static.UseLowGore())
		return;

	if (FlamingFXs != none)
	{
		FlamingFXs.Emitters[0].SkeletalMeshActor = none;
		FlamingFXs.Destroy();
	}

	if(ObliteratedEffectClass != none)
		Spawn(ObliteratedEffectClass,,, Location, HitRotation);

	//super.SpawnGibs(HitRotation,ChunkPerterbation);

	if (FRand() < 0.1)
	{
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

		SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);

		if (DetachedSpecialArmClass != None)
		{
			SpawnSeveredGiblet(DetachedSpecialArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		}
		else
		{
			SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		}
	}

	else if (FRand() < 0.25)
	{
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);

		SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		if (FRand() < 0.5)
		{
			KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
			SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
		}
	}

	else if (FRand() < 0.35)
	{
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
		SpawnSeveredGiblet(DetachedLegClass, Location, HitRotation, ChunkPerterbation, HitRotation);
	}
	else if (FRand() < 0.5)
	{
		KFSpawnGiblet(class 'KFMod.KFGibBrainb',Location, HitRotation, ChunkPerterbation, 500);
		KFSpawnGiblet(class 'KFMod.KFGibBrain',Location, HitRotation, ChunkPerterbation, 500);
		SpawnSeveredGiblet(DetachedArmClass, Location, HitRotation, ChunkPerterbation, HitRotation);
	}
}

// Remove the head on kill if needed
function Died(Controller Killer, class<DamageType> damageType, vector HitLocation)
{
   local Class<KFWeaponDamageType> KFWDT;

   if (HeadHealth <= 0 && SeveredHead == none)
   {
      KFWDT = Class<KFWeaponDamageType>(damageType);

      if (KFWDT != none)
         DecapFX(HitLocation, Rotation, KFWDT.Default.bIsMeleeDamage);
   }

   Super.Died(Killer, damageType, HitLocation);
}

// don't zap
function SetZapped(float ZapAmount, Pawn Instigator)
{
}

// don't zap
simulated function SetZappedBehavior()
{
}

// return zed class
function FalkZedClass FalkGetZedClass()
{
   return Falk_Metal_Clot;
}

defaultproperties
{
   fDeathDrama=1.0                // Always trigger a zed time on kill
   fDamageMultiplier=8.0
   fKaboomMultiplier=8.0
   fSFireMultiplier=8.0
   fWFireMultiplier=8.0
   ZappedDamageMod=1.000000
   ZapThreshold=999.000000        // immune to plasma
   ZapResistanceScale=999.000000  // immune to plasma
   MeleeDamage=1
   HealthMax=32000.000000
   Health=32000
   HeadHealth=32000
   ScoringValue=100               // metal clot exclusive
   PlayerCountHealthScale=0.25
   PlayerNumHeadHealthScale=0.25
   BleedOutDuration=3.0
   Mass=950.000000               // boss and metal clot exclusive - don't let support specialist guns move them
   MotionDetectorThreat=1.000000  // a single metal clot will trigger both a pipe and an ATM
   MenuName="Metal Clot"
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Jump'
   DetachedArmClass=Class'MetalClotSeveredArm'
   DetachedLegClass=Class'MetalClotSeveredLeg'
   DetachedHeadClass=Class'MetalClotSeveredHead'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.clot.Clot_Challenge'
   AmbientSound=Sound'KF_BaseClot.Clot_Idle1Loop'
   Mesh=SkeletalMesh'LairClot_A.CLOT_Freak'
   Skins(0)=Texture'LairTextures_T.ZedsUpscale.MetalClot'
   fAlreadyStunned=True
   fStunMult=1.0 // no relative stun for you
}
