/* LIGHT ZED */

class FZombieStalker_STANDARD extends FalkZombieStalker;

#exec OBJ LOAD FILE=LairTextures_T.utx
#exec OBJ LOAD FILE=LairStalker_A.ukx

defaultproperties
{
   MoanVoice=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Talk'
   MeleeAttackHitSound=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_HitPlayer'
   JumpSound=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Jump'
   DetachedArmClass=Class'KFChar.SeveredArmStalker'
   DetachedLegClass=Class'KFChar.SeveredLegStalker'
   DetachedHeadClass=Class'KFChar.SeveredHeadStalker'
   BurntDetachedHeadClass=Class'StalkerSeveredBurntHead'
   BurntDetachedArmClass=Class'StalkerSeveredBurntArm'
   BurntDetachedLegClass=Class'StalkerSeveredBurntLeg'
   HitSound(0)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Pain'
   DeathSound(0)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Death'
   ChallengeSound(0)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Challenge'
   ChallengeSound(1)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Challenge'
   ChallengeSound(2)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Challenge'
   ChallengeSound(3)=SoundGroup'KF_EnemiesFinalSnd.Stalker.Stalker_Challenge'
   AmbientSound=Sound'KF_BaseStalker.Stalker_IdleLoop'
   Mesh=SkeletalMesh'LairStalker_A.Stalker_Freak'
   Skins(0)=Shader'KF_Specimens_Trip_T.stalker_invisible'
   Skins(1)=Shader'KF_Specimens_Trip_T.stalker_invisible'
}
