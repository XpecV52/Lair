/* LIGHT ZED */

class FalkZombieGorefastBase extends FalkMonster
   abstract;

#exec OBJ LOAD FILE=LairGorefast_A.ukx
#exec OBJ LOAD FILE=LairTextures_T.utx

var bool bRunning;
var float RunAttackTimeout;

replication
{
   reliable if(Role == ROLE_Authority)
      bRunning;
}

defaultproperties
{
   MeleeAnims(0)="GoreAttack1"
   MeleeAnims(1)="GoreAttack2"
   MeleeAnims(2)="GoreAttack2"
   bCannibal=True
   damageForce=5000
   bHarpoonToBodyStuns=False
   bHarpoonToHeadStuns=False
   KFRagdollName="GoreFast_Trip"
   CrispUpThreshhold=8
   bUseExtendedCollision=True
   ColOffset=(Z=52.000000)
   ColRadius=25.000000
   ColHeight=10.000000
   ExtCollAttachBoneName="Collision_Attach"
   SeveredArmAttachScale=0.900000
   SeveredLegAttachScale=0.900000
   OnlineHeadshotOffset=(X=5.000000,Z=53.000000)
   OnlineHeadshotScale=1.500000
   IdleHeavyAnim="GoreIdle"
   IdleRifleAnim="GoreIdle"
   MeleeRange=30.000000
   GroundSpeed=120.000000
   WaterSpeed=120.000000
   HeadHeight=2.500000
   HeadScale=1.500000
   MenuName="Gorefast"
   MovementAnims(0)="GoreWalk"
   WalkAnims(0)="GoreWalk"
   WalkAnims(1)="GoreWalk"
   WalkAnims(2)="GoreWalk"
   WalkAnims(3)="GoreWalk"
   IdleCrouchAnim="GoreIdle"
   IdleWeaponAnim="GoreIdle"
   IdleRestAnim="GoreIdle"
   DrawScale=1.200000
   PrePivot=(Z=10.000000)
   RotationRate=(Yaw=45000,Roll=0)
   fHSKThreshold=0.35            // instakill at 76 damage at 1man normal
   Mass=150.000000               // light zeds standard
   MeleeDamage=14
   MotionDetectorThreat=0.250000 // two light zeds trigger a pipe, four trigger an ATM
   HealthMax=215
   Health=215
   HeadHealth=22
   fDeathDrama=0.05              // light zeds standard
   ScoringValue=10               // light zeds standard
   PlayerCountHealthScale=0.0    // light zeds standard
   PlayerNumHeadHealthScale=0.0  // light zeds standard
   BleedOutDuration=3.0          // light zeds standard
   ZappedDamageMod=1.000000
   ZapResistanceScale=3.000000
   ZapThreshold=5.000000         // light zeds standard
   fStunThresh=200               // light zeds standard
   bMeleeStunImmune=false
   bStunImmune=false
   fShouldRage=True
   fRageSpeed=1.9
   fNormalRageSpeed=1.9
   fHardRageSpeed=1.9
   fSuicidalRageSpeed=1.9
   fHOERageSpeed=1.9
   fBBRageSpeed=1.9
   fRageSpeedCap=295.0
}
