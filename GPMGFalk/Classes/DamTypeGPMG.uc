class DamTypeGPMG extends DamTypeSW76Falk;
   
defaultproperties
{
   HeadShotDamageMult=1.100000
   bIsPowerWeapon=True
   WeaponClass=Class'GPMGFalk.GPMG'
   DeathString="%k killed %o (GPMG)."
   FemaleSuicide="%o shot herself in the foot."
   MaleSuicide="%o shot himself in the foot."
   KDamageImpulse=5250.000000
   KDeathVel=425.000000
   KDeathUpKick=45.000000
}
