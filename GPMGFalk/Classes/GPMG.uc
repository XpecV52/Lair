class GPMG extends KFWeapon;

#exec OBJ LOAD FILE=KillingFloorWeapons.utx
#exec OBJ LOAD FILE=KillingFloorHUD.utx
#exec OBJ LOAD FILE=Inf_Weapons_Foley.uax

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

var int               FStoredAmmo;          // used to fix the weirdest shit happening with MagAmmoRemaining being basically always wrong

// Use alt fire to switch fire modes
simulated function AltFire(float F)
{
    if (ReadyToFire(0))
        DoToggle();
}

simulated function BringUp(optional Weapon PrevWeapon)
{
   local KFPlayerController Player;

   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super.BringUp(PrevWeapon);

   Player = KFPlayerController(Instigator.Controller);

   if (Player != none && ClientGrenadeState != GN_BringUp)
   {
      Player.WeaponPulloutRemark(23); // LAW
   }
}

// removed weird vanilla shit
simulated function FillToInitialAmmo()
{
   if (bNoAmmoInstances)
   {
      if (AmmoClass[0] != None)
      {
         AmmoCharge[0] = AmmoClass[0].Default.InitialAmount;

         if (MagAmmoRemaining > AmmoCharge[0])
         {
            MagAmmoRemaining = AmmoCharge[0];

            //warn("CAP1:"@MagAmmoRemaining);
         }
      }

      if ((AmmoClass[1] != None) && (AmmoClass[0] != AmmoClass[1]))
         AmmoCharge[1] = AmmoClass[1].Default.InitialAmount;


      return;
   }

   if (Ammo[0] != None)
   {
      Ammo[0].AmmoAmount = Ammo[0].Default.InitialAmount;

      if (MagAmmoRemaining > AmmoCharge[0])
      {
         MagAmmoRemaining = AmmoCharge[0];
         //warn("CAP2:"@MagAmmoRemaining);
      }
   }

   if (Ammo[1] != None)
      Ammo[1].AmmoAmount = Ammo[1].Default.InitialAmount;
}

// removed more vanilla shit
function GiveAmmo(int m, WeaponPickup WP, bool bJustSpawned)
{
   local bool bJustSpawnedAmmo;
   local int addAmount, InitialAmount;
   local KFPawn KFP;
   local KFPlayerReplicationInfo KFPRI;

   KFP = KFPawn(Instigator);

   if(KFP != none)
      KFPRI = KFPlayerReplicationInfo(KFP.PlayerReplicationInfo);

   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   NetUpdateTime = Level.TimeSeconds - 1;

   if (FireMode[m] != None && FireMode[m].AmmoClass != None)
   {
      Ammo[m] = Ammunition(Instigator.FindInventoryType(FireMode[m].AmmoClass));
      bJustSpawnedAmmo = false;

      if (bNoAmmoInstances)
      {
         if ((FireMode[m].AmmoClass == None) || ((m != 0) && (FireMode[m].AmmoClass == FireMode[0].AmmoClass)))
            return;

         InitialAmount = FireMode[m].AmmoClass.Default.InitialAmount;

         if (WP!=none && WP.bThrown==true)
         {
            InitialAmount = WP.AmmoAmount[m];

            if (KFWeaponPickup(WP) != none)
            {
               MagAmmoRemaining = KFWeaponPickup(WP).MagAmmoRemaining;
               FStoredAmmo      = MagAmmoRemaining;

               //warn("RESTORE:"@MagAmmoRemaining);
            }
         }

         else
         {
		      MagAmmoRemaining = AmmoClass[0].Default.InitialAmount;
            //warn("GiveAmmo:"@MagAmmoRemaining);
            
            FStoredAmmo = MagAmmoRemaining;
         }

         if (Ammo[m] != None)
         {
            addamount = InitialAmount + Ammo[m].AmmoAmount;
            Ammo[m].Destroy();
         }
         else
            addAmount = InitialAmount;

         AddAmmo(addAmount,m);
      }

      else
      {
         if ((Ammo[m] == None) && (FireMode[m].AmmoClass != None))
         {
            Ammo[m] = Spawn(FireMode[m].AmmoClass, Instigator);
            Instigator.AddInventory(Ammo[m]);
            bJustSpawnedAmmo = true;
         }

         else if ((m == 0) || (FireMode[m].AmmoClass != FireMode[0].AmmoClass))
            bJustSpawnedAmmo = (bJustSpawned || ((WP != None) && !WP.bWeaponStay));

         if (WP != none && WP.bThrown == true)
            addAmount = WP.AmmoAmount[m];

         else if (bJustSpawnedAmmo)
         {
            if (default.MagCapacity == 0)
               addAmount = 0;  // prevent division by zero.
            else
               addAmount = Ammo[m].InitialAmount;
         }

         if (WP != none && m > 0 && (FireMode[m].AmmoClass == FireMode[0].AmmoClass))
            return;

         if(KFPRI != none && KFPRI.ClientVeteranSkill != none)
            Ammo[m].MaxAmmo = float(Ammo[m].MaxAmmo) * KFPRI.ClientVeteranSkill.Static.AddExtraAmmoFor(KFPRI, Ammo[m].Class);

         Ammo[m].AddAmmo(addAmount);
         Ammo[m].GotoState('');
      }
   }
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
    local int Mode;

    Instigator = Pawn(Owner);

    bPendingSwitch = bPossiblySwitch;

    if( Instigator == None )
    {
        GotoState('PendingClientWeaponSet');
        return;
    }

    for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
    {
        if( FireModeClass[Mode] != None )
        {
			// laurent -- added check for vehicles (ammo not replicated but unlimited)
            if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
            {
                GotoState('PendingClientWeaponSet');
                return;
            }
        }

        FireMode[Mode].Instigator = Instigator;
        FireMode[Mode].Level = Level;
    }

    ClientState = WS_Hidden;
    GotoState('Hidden');

    if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
        return;

    if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
    {
		if (Instigator.PendingWeapon != None)
            Instigator.ChangedWeapon();
        else
            BringUp();
        return;
    }

    if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
        return;

    if ( Instigator.Weapon == None)
    {
        Instigator.PendingWeapon = self;
        Instigator.ChangedWeapon();
    }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// block reload on bringup
simulated function bool AllowReload()
{
   UpdateMagCapacity(Instigator.PlayerReplicationInfo);

   MagAmmoRemaining = FStoredAmmo;

   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
   {
      //warn("BLOCK 1");
      return False;
   }

   if (KFInvasionBot(Instigator.Controller) != none && !bIsReloading && MagAmmoRemaining < MagCapacity && AmmoAmount(0) > MagAmmoRemaining)
      return true;

   if (KFFriendlyAI(Instigator.Controller) != none && !bIsReloading && MagAmmoRemaining < MagCapacity && AmmoAmount(0) > MagAmmoRemaining)
      return true;

   if (FireMode[0].IsFiring() || FireMode[1].IsFiring() || bIsReloading || MagAmmoRemaining >= MagCapacity || ClientState == WS_BringUp ||
       AmmoAmount(0) <= MagAmmoRemaining || (FireMode[0].NextFireTime - Level.TimeSeconds) > 0.1 )
   {
      //warn("BLOCK 2:"@MagAmmoRemaining@"/"@ MagCapacity@"-"@AmmoAmount(0)@" - BringUp:"@ClientState == WS_BringUp);
      return false;
   }

   return true;
}

// removed weird interface bug where initial ammo were wrong
simulated function GetAmmoCount(out float MaxAmmoPrimary, out float CurAmmoPrimary)
{
   if (Instigator == None || Instigator.Controller == None)
      return;

	if (AmmoClass[0] == None)
		return;

	if (bNoAmmoInstances)
	{
      FillToInitialAmmo();

		MaxAmmoPrimary = MaxAmmo(0);
		CurAmmoPrimary = AmmoClass[0].Default.InitialAmount;

      if (MagAmmoRemaining > CurAmmoPrimary)
         MagAmmoRemaining = CurAmmoPrimary;

      //warn("1:"@CurAmmoPrimary);

		if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
		{
			MaxAmmoPrimary *= KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.static.AddExtraAmmoFor(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), AmmoClass[0]);
			MaxAmmoPrimary = int(MaxAmmoPrimary);
		}

		return;
	}

	if (Ammo[0] == None)
	{
		return;
	}

	MaxAmmoPrimary = Ammo[0].default.MaxAmmo;
	CurAmmoPrimary = Ammo[0].AmmoAmount;

   if (MagAmmoRemaining > CurAmmoPrimary)
      MagAmmoRemaining = CurAmmoPrimary;

   //warn("2:"@CurAmmoPrimary);

	if (KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo) != none && KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill != none)
	{
		MaxAmmoPrimary *= KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo).ClientVeteranSkill.static.AddExtraAmmoFor(KFPlayerReplicationInfo(Instigator.PlayerReplicationInfo), Ammo[0].class);
		MaxAmmoPrimary = int(MaxAmmoPrimary);
	}
}

// Storing mag ammo on pickup
function GiveTo( pawn Other, optional Pickup Pickup )
{
	UpdateMagCapacity(Other.PlayerReplicationInfo);

	if (KFWeaponPickup(Pickup) != None && Pickup.bDropped)
	{
      if (KFWeaponPickup(Pickup).MagAmmoRemaining > MagCapacity)
		   MagAmmoRemaining = Clamp(KFWeaponPickup(Pickup).MagAmmoRemaining, 0, MagCapacity);

      else
		   MagAmmoRemaining = KFWeaponPickup(Pickup).MagAmmoRemaining;

      FStoredAmmo         = MagAmmoRemaining;

      //warn("Pickup:"@MagAmmoRemaining);
	}
	else
   {
      MagAmmoRemaining = AmmoClass[0].Default.InitialAmount;

      if (MagAmmoRemaining > MagCapacity)
         MagAmmoRemaining = MagCapacity;
      
      //warn("Init:"@MagAmmoRemaining);
      
      FStoredAmmo = MagAmmoRemaining;
   }

	Super.GiveTo(Other,Pickup);
}

// Add the ammo for this reload
function AddReloadedAmmo()
{
	UpdateMagCapacity(Instigator.PlayerReplicationInfo);

	if (AmmoAmount(0) >= MagCapacity)
	   MagAmmoRemaining = MagCapacity;

	else
		MagAmmoRemaining = AmmoAmount(0);

	// Don't do this on a "Hold to reload" weapon, as it can update too quick actually and cause issues maybe - Ramm
	if (!bHoldToReload)
		ClientForceKFAmmoUpdate(MagAmmoRemaining,AmmoAmount(0));

	if (PlayerController(Instigator.Controller) != none && KFSteamStatsAndAchievements(PlayerController(Instigator.Controller).SteamStatsAndAchievements) != none)
		KFSteamStatsAndAchievements(PlayerController(Instigator.Controller).SteamStatsAndAchievements).OnWeaponReloaded();
}


// Just fire one ammo per bullet, pls
simulated function bool ConsumeAmmo( int Mode, float Load, optional bool bAmountNeededIsMax )
{
	local Inventory Inv;
	local bool bOutOfAmmo;
	local KFWeapon KFWeap;

	if (Super.ConsumeAmmo(Mode, Load, bAmountNeededIsMax))
	{
		if (Load > 0 && (Mode == 0 || bReduceMagAmmoOnSecondaryFire))
      {
         // for some reason we randomly have the server doing weird shit with ammo, let's attempt to fix this
         if (MagAmmoRemaining != FStoredAmmo)
         {
            //warn("BEFORE:"@MagAmmoRemaining@"- TIME:"@Level.TimeSeconds);

            FStoredAmmo--;
            MagAmmoRemaining = FStoredAmmo;

            //warn("FIX:"@MagAmmoRemaining);
         }

         else
         {
			   MagAmmoRemaining--;
            //warn("CONSUME:"@MagAmmoRemaining@"- TIME:"@Level.TimeSeconds);
         }

      }

		NetUpdateTime = Level.TimeSeconds - 1;

		if (FireMode[Mode].AmmoPerFire > 0 && InventoryGroup > 0 && !bMeleeWeapon && bConsumesPhysicalAmmo &&
			 (Ammo[0] == none || FireMode[0] == none || FireMode[0].AmmoPerFire <= 0 || Ammo[0].AmmoAmount < FireMode[0].AmmoPerFire) &&
			 (Ammo[1] == none || FireMode[1] == none || FireMode[1].AmmoPerFire <= 0 || Ammo[1].AmmoAmount < FireMode[1].AmmoPerFire))
		{
			bOutOfAmmo = true;

			for (Inv = Instigator.Inventory; Inv != none; Inv = Inv.Inventory)
			{
				KFWeap = KFWeapon(Inv);

				if (Inv.InventoryGroup > 0 && KFWeap != none && !KFWeap.bMeleeWeapon && KFWeap.bConsumesPhysicalAmmo &&
					 ((KFWeap.Ammo[0] != none && KFWeap.FireMode[0] != none && KFWeap.FireMode[0].AmmoPerFire > 0 &&KFWeap.Ammo[0].AmmoAmount >= KFWeap.FireMode[0].AmmoPerFire) ||
					 (KFWeap.Ammo[1] != none && KFWeap.FireMode[1] != none && KFWeap.FireMode[1].AmmoPerFire > 0 && KFWeap.Ammo[1].AmmoAmount >= KFWeap.FireMode[1].AmmoPerFire)))
				{
					bOutOfAmmo = false;
					break;
				}
			}

			if (bOutOfAmmo)
			{
				PlayerController(Instigator.Controller).Speech('AUTO', 3, "");
			}
		}

		return true;
	}

	return false;
}

// store new ammo count on reload
simulated function ActuallyFinishReloading()
{
   FStoredAmmo = MagAmmoRemaining;

   //warn("RELOAD DONE:"@FStoredAmmo);
   Super.ActuallyFinishReloading();
}

defaultproperties
{
   MagCapacity=125
   ReloadRate=6.000000
   ReloadAnim="Reload"
   ReloadAnimRate=1.2500000
   WeaponReloadAnim="Reload"
   HudImage=Texture'JS_GPMG_T.GPMG_Unselected'
   SelectedHudImage=Texture'JS_GPMG_T.GPMG_selected'
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=70.000000
   bModeZeroCanDryFire=True
   SleeveNum=0
   Weight=13.000000
   TraderInfoTexture=Texture'JS_GPMG_T.GPMG_Trader'
   bIsTier3Weapon=True
   SkinRefs(1)="JS_GPMG_T.GPMG_CMB"
   PlayerIronSightFOV=40.000000
   ZoomedDisplayFOV=65.000000
   FireModeClass(0)=Class'GPMGFalk.GPMGFire'
   FireModeClass(1)=Class'KFMod.NoFire'
   PutDownAnim="PutDown"
   SelectAnimRate=1.667000
   PutDownTime=0.900000
   BringUpTime=1.080000
   SelectSound=Sound'KF_9MMSnd.9mm_Select'
   AIRating=0.250000
   CurrentRating=0.250000
   bShowChargingBar=True
   Description="The GPMG is an air-cooled and fully automatic weapon with an outstanding stopping power. It can be seen as a straight upgrade of the RPK47 in terms of damage output, total ammo, and magazine capacity, but it's heavier and reloads slower."
   DisplayFOV=70.000000
   Priority=85
   InventoryGroup=4
   GroupOffset=1
   PickupClass=Class'GPMGFalk.GPMGPickup'
   PlayerViewOffset=(X=20.000000,Y=25.000000,Z=-10.000000)
   BobDamping=6.000000
   AttachmentClass=Class'GPMGFalk.GPMGAttachment'
   IconCoords=(X1=434,Y1=253,X2=506,Y2=292)
   ItemName="FN General Purpose Machine Gun"
   Mesh=SkeletalMesh'JS_GPMG_A.GPMG_Weapon'
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
