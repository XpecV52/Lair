class GPMGPickup extends KFWeaponPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

// use FStoredAmmo instead of MagAmmoRemaining to have proper ammo amount on drop/pickup
function InitDroppedPickupFor(Inventory Inv)
{
   local KFWeapon W;
   local Inventory InvIt;
   local byte bSaveAmmo[2];
   local int m;

   W = KFWeapon(Inv);

   if (W != None)
   {
      //Check if any other weapon is using the same ammo
      for(InvIt = W.Owner.Inventory; InvIt!=none; InvIt=InvIt.Inventory)
      {
         if(Weapon(InvIt)!=none && InvIt!=W)
         {
            for(m=0; m < 2; m++)
            {
               if(Weapon(InvIt).AmmoClass[m] == W.AmmoClass[m])
                  bSaveAmmo[m] = 1;
            }
         }
      }

      if (bSaveAmmo[0] == 0)
      {
         if (GPMG(W) != none)
            MagAmmoRemaining = GPMG(W).FStoredAmmo;

         else
            MagAmmoRemaining = W.MagAmmoRemaining;

         AmmoAmount[0] = W.AmmoAmount(0);
      }

      if (bSaveAmmo[1] == 0)
         AmmoAmount[1] = W.AmmoAmount(1);

      SellValue = W.SellValue;
   }
   SetPhysics(PHYS_Falling);
   GotoState('FallingPickup');
   Inventory = Inv;
   bAlwaysRelevant = false;
   bOnlyReplicateHidden = false;
   bUpdateSimulatedPosition = true;
   bDropped = true;
   bIgnoreEncroachers = false; // handles case of dropping stuff on lifts etc
   NetUpdateFrequency = 8;
   bNoRespawn = true;

   if ( KFWeapon(Inventory) != none )
   {
      if ( KFWeapon(Inventory).bIsTier2Weapon )
      {
         if ( !KFWeapon(Inventory).bPreviouslyDropped && PlayerController(Pawn(Inventory.Owner).Controller) != none )
         {
            KFWeapon(Inventory).bPreviouslyDropped = true;
            KFSteamStatsAndAchievements(PlayerController(Pawn(Inventory.Owner).Controller).SteamStatsAndAchievements).AddDroppedTier2Weapon();
         }
      }
      else
      {
         bPreviouslyDropped = KFWeapon(Inventory).bPreviouslyDropped;
         DroppedBy = PlayerController(Pawn(W.Owner).Controller);
      }
   }
}

defaultproperties
{
   cost=2500
   AmmoCost=125
   BuyClipSize=125
   PowerValue=66
   SpeedValue=50
   RangeValue=100
   Weight=13.000000
   ItemName="FN General Purpose Machine Gun"
   ItemShortName="GPMG"
   AmmoItemName="7.62 belt ammo"
   AmmoMesh=StaticMesh'KillingFloorStatics.L85Ammo'
   CorrespondingPerkIndex=7
   EquipmentCategoryID=4
   InventoryType=Class'GPMGFalk.GPMG'
   PickupMessage="You got an FN General Purpose Machine Gun"
   PickupSound=Sound'KF_FNFALSnd.FNFAL_Pickup'
   PickupForce="AssaultRiflePickup"
   StaticMesh=StaticMesh'JS_GPMG_M.LMG.GPMGStatic'
   CollisionRadius=25.000000
   CollisionHeight=5.000000
}
