class GPMGAmmoPickup extends KFAmmoPickup;

defaultproperties
{
   AmmoAmount=32
   InventoryType=Class'GPMGFalk.GPMGAmmo'
   RespawnTime=0.000000
   PickupMessage="Box (7.62mm)"
   StaticMesh=StaticMesh'KillingFloorStatics.DualiesAmmo'
}
