class HuskGunProjectileFalk_Weak extends HuskGunProjectileFalk;

defaultproperties
{
   Speed=4500
   MaxSpeed=4500
   ExplosionEmitter=Class'KFMod.FlameImpact_Weak'
   FlameTrailEmitterClass=Class'KFMod.FlameThrowerHusk_Weak'
   ExplosionSoundVolume=1.250000
   ExplosionDecal=Class'BurnMarkSmallFalk'
}
