class HuskGunProjectileFalk_Strong extends HuskGunProjectileFalk;

defaultproperties
{
   Speed=4500
   MaxSpeed=4500
   MyDamageType=Class'DamTypeHuskGunStrongFalk'
   ExplosionEmitter=Class'KFMod.FlameImpact_Strong'
   FlameTrailEmitterClass=Class'KFMod.FlameThrowerHusk_Strong'
   ExplosionSoundVolume=2.000000
   ExplosionDecal=Class'BurnMarkLargeFalk'
}
