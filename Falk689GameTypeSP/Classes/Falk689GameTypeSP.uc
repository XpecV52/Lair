class Falk689GameTypeSP extends Falk689GameType;

var WaveInfo FWaves[16];                 // yep, FalkWaves, bitches.
var array<MClassTypes> FMonsterClasses;  // falkified to prevent other code to mess with it
var array<string>      FMonsterSquad;    // again, don't touch my monstersquads
var bool               fSPZedTimeUsed;   // single player variable
var bool               fAlreadySkipped;  // used to fix trader skipping in solo

var float              FSingleSlayTimer;
var int                FSingleSlayNumber;

replication
{
	reliable if(Role == ROLE_Authority)
      fSPZedTimeUsed, fAlreadySkipped;
}

// game start
/*event InitGame(string Options, out string Error)
{
   Super.InitGame(Options, Error);

   StartingCash = Default.FStartingCash; // force starting cash to 500
}*/

// We're alone, just skip the trader.
function bool VoteSkipTrader(PlayerController PC)
{
   if (fAlreadySkipped)
      return False;

   fAlreadySkipped   = True;
   fShouldSkipTrader = True;

   return True;
}

// Revert this to vanilla
function RestartPlayer(Controller aPlayer)
{
   Super(KFGameType).RestartPlayer(aPlayer);
}

// force game length to custom
event PreBeginPlay()
{
   FinalWave    = 10;
   KFGameLength = GL_Custom;
   Super.PreBeginPlay();
}

// Return true if we can use our zed time skill, simplified to sp mode
function bool CanUseZedTimeSkill(PlayerController P, float ZTECD, float ZTSCD)
{
   if (InZedTime())
   {

      if (!fSPZedTimeUsed                               &&
         Level.TimeSeconds >= fZedTimeEnd + ZTECD       &&
         Level.TimeSeconds >= LastZedTimeEvent + ZTSCD)
         return True;
   }

   return false;
}

// simplified to set single player used state
function ZedTimeSkillUsed(PlayerController P)
{
   fSPZedTimeUsed = True;
}

// simplified reset zed time skill
function ZedTimeEndReset()
{
   fSPZedTimeUsed = False;
}

// only use default zed numbers
function SetupWave()
{
   local int i,j;
   local float NewMaxMonsters;
   local float DifficultyMod;
   local int UsedNumPlayers;

   fAlreadySkipped    = False;
   AmmoSpawnTimer     = 0; // take some time to spawn more ammo
   WaveSpawnedAmmo    = 0; 
   WaveSpawnedWeapons = 0;
   TraderProblemLevel = 0;
   rewardFlag         = false;
   fChangeCurWave     = true;
   ZombiesKilled      = 0;
   WaveMonsters       = 0;
   WaveNumClasses     = 0;
   FCurSlayTimer      = 0;
   NewMaxMonsters     = FWaves[WaveNum].WaveMaxMonsters;
   FAlivePlayers      = GetAlivePlayers(); // set alive players count on wave start
   FAlivePlayersCheck = FAlivePlayers;     // set the check at wave start

   //log("Wave start alive players: "@FAlivePlayers);

   // scale number of zombies by difficulty
   if (GameDifficulty >= 7.0) // Hell on Earth
      DifficultyMod = 1.6;

   else if (GameDifficulty >= 5.0) // Suicidal
      DifficultyMod = 1.4;

   else if (GameDifficulty >= 4.0) // Hard
      DifficultyMod = 1.2;

   else                            // Normal
      DifficultyMod = 1.0;

   UsedNumPlayers = NumPlayers + NumBots;

   TotalMaxMonsters = NewMaxMonsters * DifficultyMod * FAlivePlayers; // scale based only on alive players

   MaxZombiesOnce = 28;
   MaxMonsters    = Clamp(TotalMaxMonsters, 5, MaxZombiesOnce);
   //log("MaxMonsters: "@MaxMonsters);

   KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonsters   = TotalMaxMonsters;
   KFGameReplicationInfo(Level.Game.GameReplicationInfo).MaxMonstersOn = true;
   WaveEndTime                                                         = Level.TimeSeconds + FWaves[WaveNum].WaveDuration;
   AdjustedDifficulty                                                  = GameDifficulty + FWaves[WaveNum].WaveDifficulty;

   j = ZedSpawnList.Length;

   for(i=0; i<j; i++)
      ZedSpawnList[i].Reset();

   j = 1;
   SquadsToUse.Length = 0;

   for (i=0; i<InitSquads.Length; i++)
   {
      if ((j & FWaves[WaveNum].WaveMask) != 0)
      {
         SquadsToUse.Insert(0,1);
         SquadsToUse[0] = i;
      }

      j *= 2;
   }

   // Save this for use elsewhere
   InitialSquadsToUseSize = SquadsToUse.Length;
   bUsedSpecialSquad      = false;
   SpecialListCounter     = 1;

   //Now build the first squad to use
   BuildNextSquad();
}

// use default (our) monsters for waves 
function BuildNextSquad()
{
    local int i, j, RandNum;

    // Reinitialize the SquadsToUse after all the squads have been used up
    if( SquadsToUse.Length == 0 )
    {
        j = 1;

        for ( i=0; i<InitSquads.Length; i++ )
        {
            if ( (j & FWaves[WaveNum].WaveMask) != 0 )
            {
                SquadsToUse.Insert(0,1);
                SquadsToUse[0] = i;
            }

            j *= 2;
        }

        if( SquadsToUse.Length==0 )
        {
            Warn("No squads to initilize with.");
            Return;
        }

        // Save this for use elsewhere
        InitialSquadsToUseSize = SquadsToUse.Length;
        SpecialListCounter++;
        bUsedSpecialSquad=false;
    }

    RandNum        = Rand(SquadsToUse.Length);
    NextSpawnSquad = InitSquads[SquadsToUse[RandNum]].MSquad;

    // Take this squad out of the list so we don't get repeats
    SquadsToUse.Remove(RandNum,1);
}

// again, be default pls
function LoadUpMonsterList()
{
   local int i,j,q,c,n;
   local Class<KFMonster> MC;
   local string S,ID;
   local bool bInitSq;
   local array<IMClassList> InitMList;

   InitMList = LoadUpMonsterListFromGameType();

   //Log("Got"@j@"monsters. Loading up monster squads...",'Init');
   for( i=0; i<FMonsterSquad.Length; i++ )
   {
      S = FMonsterSquad[i];
      if( S=="" )
         Continue;
      bInitSq = False;
      n = 0;
      While( S!="" )
      {
         q = int(Left(S,1));
         ID = Mid(S,1,1);
         S = Mid(S,2);
         MC = None;
         for( j=0; j<InitMList.Length; j++ )
         {
            if( InitMList[j].ID~=ID )
            {
               MC = InitMList[j].MClass;
               Break;
            }
         }
         if( MC==None )
            Continue;
         if( !bInitSq )
         {
            InitSquads.Length = c+1;
            bInitSq = True;
         }
         while( (q--)>0 )
         {
            InitSquads[c].MSquad.Length = n+1;
            InitSquads[c].MSquad[n] = MC;
            n++;
         }
      }
      if( bInitSq )
         c++;
   }
   //Log("Got"@c@"monster squads.",'Init');
   if( FallbackMonster==class'EliteKrall' && InitMList.Length>0 )
      FallbackMonster = InitMList[0].MClass;
}

// guess what, don't customize this either
function array<IMClassList> LoadUpMonsterListFromGameType()
{
    local array<IMClassList> InitMList;
    local int i,j;
    local Class<KFMonster> MC;

    for( i=0; i<FMonsterClasses.Length; i++ )
    {
        if (FMonsterClasses[i].MClassName=="" || FMonsterClasses[i].MID=="" )
            Continue;

        MC = Class<KFMonster>(DynamicLoadObject(FMonsterClasses[i].MClassName,Class'Class'));

        if( MC==None )
            Continue;

        MC.static.PreCacheAssets(Level);

        InitMList.Length = j+1;
        InitMList[j].MClass = MC;
        InitMList[j].ID = FMonsterClasses[i].MID;
        j++;
    }

    return InitMList;
}

// another one bugs the dust, use only default monster stuff
exec function DumpZedSquads(int MyKFGameLength)
{
    local WaveInfo MyWaves[16];
    local int MyFinalWave, m, MyWaveNum, OldMySquadToUse, i, j, q, c, n;
    local array<MSquadsList> MyInitSquads;
    local array<int> MySquadsToUse; // Pointers
    local Class<KFMonster> MC;
    local string S,ID;
    local bool bInitSq;
    local array<IMClassList> InitMList;

    if ( MyKFGameLength == 0 )
    {
        MyFinalWave = 4;
        for ( i = 0; i < MyFinalWave; i++ )
        {
            MyWaves[i] = ShortWaves[i];
        }
    }
    else if ( MyKFGameLength == 1 )
    {
        MyFinalWave = 7;
        for ( i = 0; i < MyFinalWave; i++ )
        {
            MyWaves[i] = NormalWaves[i];
        }
    }
    else if ( MyKFGameLength == 2 )
    {
        MyFinalWave = 10;
        for ( i = 0; i < MyFinalWave; i++ )
        {
            MyWaves[i] = LongWaves[i];
        }
    }

    InitMList = LoadUpMonsterListFromGameType();

    for ( i = 0; i < FMonsterSquad.Length; i++ )
    {
        S = FMonsterSquad[i];
        if ( S == "" )
        {
            Continue;
        }

        bInitSq = False;
        n = 0;

        while ( S != "" )
        {
            q = int(Left(S, 1));
            ID = Mid(S, 1, 1);
            S = Mid(S, 2);
            MC = None;

            for ( j = 0; j < InitMList.Length; j++ )
            {
                if ( InitMList[j].ID ~= ID )
                {
                    MC = InitMList[j].MClass;
                    Break;
                }
            }

            if ( MC == None )
            {
                Continue;
            }

            if ( !bInitSq )
            {
                MyInitSquads.Length = c+1;
                bInitSq = True;
            }

            while ( (q--) > 0 )
            {
                MyInitSquads[c].MSquad.Length = n+1;
                MyInitSquads[c].MSquad[n] = MC;
                n++;
            }
        }

        if ( bInitSq )
        {
            c++;
        }
    }

    for ( MyWaveNum = 0; MyWaveNum < MyFinalWave; MyWaveNum++ )
    {
        j = 1;
        for ( i = 0; i < MyInitSquads.Length; i++ )
        {
            if ( (j & MyWaves[MyWaveNum].WaveMask) != 0 )
            {
                MySquadsToUse.Insert(0,1);
                MySquadsToUse[0] = i;

                for ( m = 0; m < InitSquads[i].MSquad.Length; m++ )
                {
                   log("Wave " $ MyWaveNum + 1 $ " Squad " $ MySquadsToUse.Length $ " Monster " $ m + 1 $ " "$MyInitSquads[i].MSquad[m]);
                }
            }
            if ( OldMySquadToUse != MySquadsToUse.Length )
            {
                log("==================End Squad " @ MySquadsToUse.Length @ "==================");
                OldMySquadToUse = MySquadsToUse.Length;
            }
            j *= 2;
        }
        log(" ");
        log("##################End Wave" @ MyWaveNum + 1 @ "##################");
        log(" ");
    }
}

// Now my kids tend to die if you (a single parent) forget them in your car under the boiling sun, just like irl
State MatchInProgress
{
   function Tick(float Delta)
   {
      local Controller C;
      local array<FalkMonster> FC;
      local int i;

      Global.Tick(Delta);

      if (Level.NetMode == NM_ListenServer && FCurrentWave < FinalWave && NumMonsters <= FSingleSlayNumber)
      {
         if (FCurSlayTimer < FSingleSlayTimer)
            FCurSlayTimer += Delta;

         else
         {
            for (C=Level.ControllerList; C!=None; C=C.NextController)
            {
               if (C != none && C.Pawn                 != none &&
                   FalkMonster(C.Pawn)                 != none &&
                   FalkZombieBoss(C.Pawn)              == none &&
                   FalkZombieFleshpound(C.Pawn)        == none &&
                   FalkZombieScrake(C.Pawn)            == none &&
                   FZombieMetalClot_STANDARD(C.Pawn)   == none)
               {
                  FC[FC.Length] = FalkMonster(C.Pawn);
               }
            }

            for (i=0; i<FC.Length; i++)
            {
               if (FC[i] == none)
                  continue;

               FC[i].FSeppuku();
            }
         }
      }
   }
}

// reset local variables with this
function FalkEnablePerkSelect()
{
   fAlreadySkipped = False;
   Super.FalkEnablePerkSelect();
}

// nope
function FlareSpawned(PlayerController PC)
{
}

// hyper nope
function FlareDestroyed(PlayerController PC)
{
}

defaultproperties
{
   GameName="Lair Single Player"
   Description="Total game balance rework, with more guns, more zeds, custom maps, and with a side of bugfixes. This game mode is meant to be a demo experience to let you test the waters and get you acquainted with the rework itself. All perk levels are set to 9 and there is no progression. You're welcome in our online servers to experience the originally intended mode."
   bEnemyHealthBars=False
   FallbackMonsterClass="Falk689ZedsFix.FZombieStalker_STANDARD"
   InitialWave=0
   FinalWave=10
   FSingleSlayTimer=65.0
   FSingleSlayNumber=3
   fWaves[0]=(wavemask=55,wavemaxmonsters=16,waveduration=255,wavedifficulty=0.000000)
   fWaves[1]=(wavemask=66711,wavemaxmonsters=20,waveduration=255,wavedifficulty=0.100000)
   fWaves[2]=(wavemask=328855,wavemaxmonsters=24,waveduration=255,wavedifficulty=0.100000)
   fWaves[3]=(wavemask=537201015,wavemaxmonsters=28,waveduration=255,wavedifficulty=0.200000)
   fWaves[4]=(wavemask=539310455,wavemaxmonsters=32,waveduration=255,wavedifficulty=0.200000)
   fWaves[5]=(wavemask=539310455,wavemaxmonsters=36,waveduration=255,wavedifficulty=0.200000)
   fWaves[6]=(wavemask=589649399,wavemaxmonsters=40,waveduration=255,wavedifficulty=0.200000)
   fWaves[7]=(wavemask=589657591,wavemaxmonsters=44,waveduration=255,wavedifficulty=0.300000)
   fWaves[8]=(wavemask=572873207,wavemaxmonsters=48,waveduration=255,wavedifficulty=0.300000)
   fWaves[9]=(wavemask=621968247,wavemaxmonsters=52,waveduration=255,wavedifficulty=0.300000)
   fWaves[10]=(wavemask=65026687,wavemaxmonsters=60,waveduration=180,wavedifficulty=1.500000)
   fWaves[11]=(wavemask=63750079,wavemaxmonsters=50,waveduration=180,wavedifficulty=1.500000)
   fWaves[12]=(wavemask=64810679,wavemaxmonsters=50,waveduration=180,wavedifficulty=1.500000)
   fWaves[13]=(wavemask=62578607,wavemaxmonsters=60,waveduration=180,wavedifficulty=2.000000)
   fWaves[14]=(wavemask=100663295,wavemaxmonsters=50,waveduration=180,wavedifficulty=2.000000)
   fWaves[15]=(wavemask=134217727,wavemaxmonsters=15,waveduration=255,wavedifficulty=2.000000)
   bAllowNonTeamChat=True
   FriendlyFireScale=0.000000
   NetWait=5
   bForceRespawn=True
   bAdjustSkill=False
   bAllowTaunts=True
   bAllowTrans=False
   SpawnProtectionTime=0.000000
   LateEntryLives=1
   bAllowPrivateChat=True
   bWeaponStay=True
   bAllowWeaponThrowing=True
   ResetTimeDelay=10
   GoalScore=60
   MaxLives=1
   TimeLimit=0
   FMonsterSquad[0]=2A
   FMonsterSquad[1]=4A
   FMonsterSquad[2]=2C
   FMonsterSquad[3]=4C
   FMonsterSquad[4]=1G
   FMonsterSquad[5]=3A
   FMonsterSquad[6]=1A2C1G
   FMonsterSquad[7]=2B
   FMonsterSquad[8]=4B
   FMonsterSquad[9]=6B
   FMonsterSquad[10]=2D
   FMonsterSquad[11]=4D
   FMonsterSquad[12]=5A
   FMonsterSquad[13]=2B2D
   FMonsterSquad[14]=4B4D
   FMonsterSquad[15]=1I
   FMonsterSquad[16]=1I1A
   FMonsterSquad[17]=1I1C
   FMonsterSquad[18]=1H
   FMonsterSquad[19]=1H1A
   FMonsterSquad[20]=1H1G
   FMonsterSquad[21]=1E
   FMonsterSquad[22]=1E1A
   FMonsterSquad[23]=1E1B
   FMonsterSquad[24]=2D1E
   FMonsterSquad[25]=1F
   FMonsterSquad[26]=1F2A
   FMonsterSquad[27]=1F2B
   FMonsterSquad[28]=1F2C
   FMonsterSquad[29]=1J
   FMonsterSquad[30]=2J
   FMonsterSquad[31]=1K
   FMonsterClasses[0]=(MClassName="Falk689ZedsFix.FZombieClot_STANDARD",Mid="A")
   FMonsterClasses[1]=(MClassName="Falk689ZedsFix.FZombieCrawler_STANDARD",Mid="B")
   FMonsterClasses[2]=(MClassName="Falk689ZedsFix.FZombieGoreFast_STANDARD",Mid="C")
   FMonsterClasses[3]=(MClassName="Falk689ZedsFix.FZombieStalker_STANDARD",Mid="D")
   FMonsterClasses[4]=(MClassName="Falk689ZedsFix.FZombieScrake_STANDARD",Mid="E")
   FMonsterClasses[5]=(MClassName="Falk689ZedsFix.FZombieFleshpound_STANDARD",Mid="F")
   FMonsterClasses[6]=(MClassName="Falk689ZedsFix.FZombieBloat_STANDARD",Mid="G")
   FMonsterClasses[7]=(MClassName="Falk689ZedsFix.FZombieSiren_STANDARD",Mid="H")
   FMonsterClasses[8]=(MClassName="Falk689ZedsFix.FZombieHusk_STANDARD",Mid="I")
   FMonsterClasses[9]=(MClassName="Falk689ZedsFix.FZombieBrute_STANDARD",Mid="J")
   //FMonsterClasses[10]=(MClassName="Falk689ZedsFix.FZombieMetalClot_STANDARD",Mid="K")
   StandardMonsterSquads[0]=2A
   StandardMonsterSquads[1]=4A
   StandardMonsterSquads[2]=2C
   StandardMonsterSquads[3]=4C
   StandardMonsterSquads[4]=1G
   StandardMonsterSquads[5]=3A
   StandardMonsterSquads[6]=1A2C1G
   StandardMonsterSquads[7]=2B
   StandardMonsterSquads[8]=4B
   StandardMonsterSquads[9]=6B
   StandardMonsterSquads[10]=2D
   StandardMonsterSquads[11]=4D
   StandardMonsterSquads[12]=5A
   StandardMonsterSquads[13]=2B2D
   StandardMonsterSquads[14]=4B4D
   StandardMonsterSquads[15]=1I
   StandardMonsterSquads[16]=1I1A
   StandardMonsterSquads[17]=1I1C
   StandardMonsterSquads[18]=1H
   StandardMonsterSquads[19]=1H1A
   StandardMonsterSquads[20]=1H1G
   StandardMonsterSquads[21]=1E
   StandardMonsterSquads[22]=1E1A
   StandardMonsterSquads[23]=1E1B
   StandardMonsterSquads[24]=2D1E
   StandardMonsterSquads[25]=1F
   StandardMonsterSquads[26]=1F2A
   StandardMonsterSquads[27]=1F2B
   StandardMonsterSquads[28]=1F2C
   StandardMonsterSquads[29]=1J
   StandardMonsterSquads[30]=2J
   StandardMonsterSquads[31]=1K
   ScreenShotName="LairTextures_T.Thumbnails.LairTGBSinglePlayer"
   StartingCash=500
   //FStartingCash=500
   GIPropsExtras(0)="2.000000;Normal;4.000000;Hard;5.000000;Suicidal;7.000000;Hell On Earth"
}
