class FArtilleristKills extends SRCustomProgressInt;

function NotifyPlayerKill(Pawn Killed, class<DamageType> damageType)
{   
   local FHumanPawn FP;

   FP = FHumanPawn(KFMonster(Killed).LastDamagedBy);

   if (FP != none && FP.fHealth >= 70)
   {
      if(damageType == class'DamTypeRPK47Falk'                  ||
         damageType == class'DamTypeXMV850Falk'                 ||
         damageType == class'DamTypeM249Falk'                   ||
         damageType == class'DamTypeMercyFalk'                  ||
         damageType == class'DamTypeSW76Falk'                   ||
         damageType == class'DamTypeTHR40DTFalk'                ||
         damageType == class'DamTypeM60Falk'                    ||
         damageType == class'DamTypeHK23EFalk'                  ||
         damageType == class'DamTypeStingerFalk'                ||
         damageType == class'DamTypeGPMG')
      {
         IncrementProgress(1);
      }
   }
}

defaultproperties 
{
   ProgressName = "Artillerist kills"
}
