class FBuyMenuSaleListBox extends SRBuyMenuSaleListBox;

function InitComponent(GUIController MyController, GUIComponent MyOwner)
{
	DefaultListClass = string(Class'FBuyMenuSaleList');
	Super(KFBuyMenuSaleListBox).InitComponent(MyController, MyOwner);
}

function GUIBuyable GetSelectedBuyable()
{
	return SRBuyMenuSaleList(List).GetSelectedBuyable();
}

defaultproperties
{
}
