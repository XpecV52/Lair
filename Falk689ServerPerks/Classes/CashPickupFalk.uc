class CashPickupFalk extends KFMod.CashPickup;

var int FalkSpan;
var float fSafety;

function InitDroppedPickupFor(Inventory Inv)
{
   SetPhysics(PHYS_Falling);
   GotoState('FallingPickup');
   Inventory = Inv;
   bAlwaysRelevant = false;
   bOnlyReplicateHidden = false;
   bUpdateSimulatedPosition = true;
   bDropped = true;
   bDroppedCash = true;
   LifeSpan = 0;
   bIgnoreEncroachers = false; // handles case of dropping stuff on lifts etc
   NetUpdateFrequency = 8;
}

function GiveCashTo(Pawn Other)
{
   if (!bDroppedCash)
		CashAmount = (rand(0.5 * default.CashAmount) + default.CashAmount) * (KFGameReplicationInfo(Level.GRI).GameDiff  * 0.5);

	else if (Other.PlayerReplicationInfo != none && DroppedBy != none && DroppedBy.PlayerReplicationInfo != none &&
			  ((DroppedBy.PlayerReplicationInfo.Score + float(CashAmount)) / Other.PlayerReplicationInfo.Score) >= 0.50 && 
           PlayerController(DroppedBy) != none && KFSteamStatsAndAchievements(PlayerController(DroppedBy).SteamStatsAndAchievements) != none)
	{
		if (Other.PlayerReplicationInfo != DroppedBy.PlayerReplicationInfo)
			KFSteamStatsAndAchievements(PlayerController(DroppedBy).SteamStatsAndAchievements).AddDonatedCash(CashAmount);
	}

	if (Other.Controller != None && Other.Controller.PlayerReplicationInfo != none)
		Other.Controller.PlayerReplicationInfo.Score += CashAmount;

	AnnouncePickup(Other);
	SetRespawn();

   if (FHumanPawn(Other) != none)
      FHumanPawn(Other).FalkSetDosh();
}

auto state Pickup
{
   // When touched by an actor.
   function Touch(actor Other)
   {
      // If touched by a player pawn, let him pick this up.
      if (ValidTouch(Other) && Pawn(Other) != none)
      {
         GiveCashTo(Pawn(Other));
      }
   }

   function bool ValidTouch(Actor Other)
   {
      local Pawn fTouchPawn;

      //warn(Other);

      if (Pawn(Other) != none)
         fTouchPawn = Pawn(Other);

      if (bOnlyOwnerCanPickup && fTouchPawn != none && DroppedBy != none && fTouchPawn.Controller != DroppedBy)
      {
         //warn("FALSE 1");
         return false;
      }

		// make sure its a live player
		if ((fTouchPawn == None) || !fTouchPawn.bCanPickupInventory || (fTouchPawn.DrivenVehicle == None && fTouchPawn.Controller == None))
      {
         //warn("FALSE 2");
			return false;
      }

		// make sure not touching through wall
		if (!FastTrace(Other.Location, Location))
      {
         //warn("FALSE 3");
			return false;
      }

      if (fSafety > 0                       &&
         ((DroppedBy            == none)    || 
         (fTouchPawn            != none     &&
         fTouchPawn.Controller  != none     &&
         fTouchPawn.Controller  == DroppedBy)))
      {
         //warn("FALSE 4");
         return false;
      }

		// make sure game will let player pick me up
		if (Level.Game.PickupQuery(fTouchPawn, self))
		{
         //warn("TRUE:"@DroppedBy@"- Safety:"@fSafety);
			TriggerEvent(Event, self, fTouchPawn);
			return true;
		}

      //warn("FALSE 5");
		return false;
   }

   function Timer()
   {
      if(bDropped && !bPreventFadeOut)
      {
         GotoState('FadeOut');
      }
   }

   function BeginState()
   {
      UntriggerEvent(Event, self, None);
      if ( bDropped )
      {
         AddToNavigation();
         SetTimer(FalkSpan, false);
      }
   }
}



state FallingPickup
{
   // When touched by an actor.
   function Touch(actor Other)
   {
      // If touched by a player pawn, let him pick this up.
      if (ValidTouch(Other) && Pawn(Other) != none)
      {
         GiveCashTo(Pawn(Other));
      }
   }

   function bool ValidTouch(Actor Other)
   {
      local Pawn fTouchPawn;

      //warn(Other);

      if (Pawn(Other) != none)
         fTouchPawn = Pawn(Other);

      if (bOnlyOwnerCanPickup && fTouchPawn != none && DroppedBy != none && fTouchPawn.Controller != DroppedBy)
      {
         //warn("FALSE 1");
         return false;
      }

		// make sure its a live player
		if ((fTouchPawn == None) || !fTouchPawn.bCanPickupInventory || (fTouchPawn.DrivenVehicle == None && fTouchPawn.Controller == None))
      {
         //warn("FALSE 2");
			return false;
      }

		// make sure not touching through wall
		if (!FastTrace(Other.Location, Location))
      {
         //warn("FALSE 3");
			return false;
      }

      if (fSafety > 0                       &&
         ((DroppedBy            == none)    || 
         (fTouchPawn            != none     &&
         fTouchPawn.Controller  != none     &&
         fTouchPawn.Controller  == DroppedBy)))
      {
         //warn("FALSE 4");
         return false;
      }

		// make sure game will let player pick me up
		if (Level.Game.PickupQuery(fTouchPawn, self))
		{
         //warn("TRUE:"@DroppedBy@"- Safety:"@fSafety);
			TriggerEvent(Event, self, fTouchPawn);
			return true;
		}

      //warn("FALSE 5");
		return false;
   }


   function Timer()
   {
      if(!bPreventFadeOut)
      {
         GotoState('FadeOut');
      }
   }

   function BeginState()
   {
   }
}


static function string GetLocalString(optional int Switch, optional PlayerReplicationInfo RelatedPRI_1, optional PlayerReplicationInfo RelatedPRI_2)
{
	return "Got "$Switch$" dosh";
}


// reduce safety time to zero to re-enable the pickup
simulated function Tick(float DeltaTime)
{
   if (fSafety > 0)
      fSafety -= DeltaTime;

   Super.Tick(DeltaTime);
}

defaultproperties
{
   StaticMesh = StaticMesh'LairStaticMeshes_SM.Equipment.Banknote'
   LifeSpan=0
   FalkSpan=69
   bDroppedCash=True
   fSafety=0.2
}
