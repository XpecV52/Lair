class FGUIBuyMenu extends SRGUIBuyMenu;

// open falk buy menu*/
function InitTabs()
{
   local SRKFTab_BuyMenu B;

   B = SRKFTab_BuyMenu(c_Tabs.AddTab(PanelCaption[0], string(Class'FTab_BuyMenu'),, PanelHint[0]));
   c_Tabs.AddTab(PanelCaption[1], string(Class'SRKFTab_Perks'),, PanelHint[1]);

   SRBuyMenuFilter(BuyMenuFilter).SaleListBox = SRBuyMenuSaleList(B.SaleSelect.List);
}

defaultproperties
{
   Begin Object class=FKFQuickPerkSelect Name=FalkQSF
      WinTop=0.011906
      WinLeft=0.008008
      WinWidth=0.316601
      WinHeight=0.082460
   End Object
   QuickPerkSelect=FalkQSF

   Begin Object Class=SRWeightBar Name=FalkWeightB
      WinTop=0.945302
      WinLeft=0.055266
      WinWidth=0.443888
      WinHeight=0.053896
   End Object
   WeightBar=FalkWeightB

   Begin Object class=SRBuyMenuFilter Name=FalkFilter
      WinTop=0.051
      WinLeft=0.67
      WinWidth=0.305
      WinHeight=0.082460
      RenderWeight=0.5
   End Object
   BuyMenuFilter=FalkFilter

   Begin Object Class=GUILabel Name=FalkHBGLL
   Caption=""
   TextAlign=TXTA_Center
   TextColor=(B=6,G=8,R=9)
   TextFont="UT2ServerListFont"
   WinTop=0
   WinLeft=0
   WinWidth=0
   WinHeight=0
   End Object
   HeaderBG_Left_Label=GUILabel'FalkHBGLL'
}
