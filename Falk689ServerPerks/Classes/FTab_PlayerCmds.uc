class FTab_PlayerCmds extends SRTab_Base;

var bool bReceivedGameClass;

var automated GUISectionBackground sb_Background;
var automated GUILabel HintLabel;
var automated GUIButton SkipButton, HiddenButton;

function InitComponent(GUIController MyController, GUIComponent MyOwner)
{
   HintLabel.Caption    = "Cast your vote to skip the current trader";
   HiddenButton.Caption = "Madonna Pallida";
   SkipButton.Caption   = "Vote";
	
	Super.Initcomponent(MyController, MyOwner);
}

function bool ButtonClicked(GUIComponent Sender)
{ 
	if (Sender == SkipButton)
      PlayerOwner().ServerMutate(Class'Falk689VoteSkipTrader'.Default.VoteSkipCmd);

	return true;
}

defaultproperties
{
	Begin Object Class=GUILabel Name=Hint
		TextAlign=TXTA_Center
		TextColor=(B=255,G=255,R=255)
		WinTop=0.300000
		WinLeft=0.302670
		WinWidth=0.363829
		WinHeight=32.000000
		TabOrder=0
	End Object
	HintLabel=Hint

	Begin Object Class=GUIButton Name=Hidden
      WinWidth=0.0
      WinHeight=0.0
      WinLeft=0.0
		WinTop=0.0
		TabOrder=1
      bBoundToParent=True
		OnClick=ButtonClicked
	End Object
	HiddenButton=Hidden

	Begin Object Class=GUIButton Name=Skip
		StyleName="SquareButton"
      WinWidth=0.363829
      WinHeight=0.042757
      WinLeft=0.302670
		WinTop=0.400000
		TabOrder=2
      bBoundToParent=True
		OnClick=ButtonClicked
	End Object
	SkipButton=Skip
   
	//OnPreDraw=FixUp
}
