Class FInvasionLoginMenu extends SRInvasionLoginMenu;

function InitComponent(GUIController MyController, GUIComponent MyOwner)
{
   local int i;
   local string s;
   local eFontScale FS;
	local SRMenuAddition M;

	// Setup panel classes.
	Panels[0].ClassName = string(Class'SRTab_ServerNews');
	Panels[1].ClassName = string(Class'SRTab_MidGamePerks');
	Panels[2].ClassName = string(Class'SRTab_MidGameVoiceChat');
   Panels[3].ClassName = string(Class'FTab_PlayerCmds');
	Panels[4].ClassName = string(Class'SRTab_MidGameStats');

	// Setup localization.
	Panels[1].Caption = Class'KFInvasionLoginMenu'.Default.Panels[1].Caption;
	Panels[2].Caption = Class'KFInvasionLoginMenu'.Default.Panels[2].Caption;
	Panels[3].Caption = "Trader";
	Panels[1].Hint = Class'KFInvasionLoginMenu'.Default.Panels[1].Hint;
	Panels[2].Hint = Class'KFInvasionLoginMenu'.Default.Panels[2].Hint;
	Panels[3].Hint = "Cast your vote to skip the current trader";
	b_Spec.Caption=class'KFTab_MidGamePerks'.default.b_Spec.Caption;
	b_MatchSetup.Caption=class'KFTab_MidGamePerks'.default.b_MatchSetup.Caption;
	b_KickVote.Caption=class'KFTab_MidGamePerks'.default.b_KickVote.Caption;
	b_MapVote.Caption=class'KFTab_MidGamePerks'.default.b_MapVote.Caption;
	b_Quit.Caption=class'KFTab_MidGamePerks'.default.b_Quit.Caption;
	b_Favs.Caption=class'KFTab_MidGamePerks'.default.b_Favs.Caption;
	b_Favs.Hint=class'KFTab_MidGamePerks'.default.b_Favs.Hint;
	b_Settings.Caption=class'KFTab_MidGamePerks'.default.b_Settings.Caption;
	b_Browser.Caption=class'KFTab_MidGamePerks'.default.b_Browser.Caption;

 	Super(UT2K4PlayerLoginMenu).InitComponent(MyController, MyOwner);

	// Mod menus
	foreach MyController.ViewportOwner.Actor.DynamicActors(class'SRMenuAddition',M)
		if( M.bHasInit )
		{
			AddOnList[AddOnList.Length] = M;
			M.NotifyMenuOpen(Self,MyController);
		}

   	s = GetSizingCaption();

	for ( i = 0; i < Controls.Length; i++ )
    {
    	if ( GUIButton(Controls[i]) != None )
        {
            GUIButton(Controls[i]).bAutoSize = true;
            GUIButton(Controls[i]).SizingCaption = s;
            GUIButton(Controls[i]).AutoSizePadding.HorzPerc = 0.04;
            GUIButton(Controls[i]).AutoSizePadding.VertPerc = 0.5;
        }
    }
    s = class'KFTab_MidGamePerks'.default.PlayerStyleName;
    PlayerStyle = MyController.GetStyle(s, fs);
	InitGRI();
}

defaultproperties
{
	Panels(0)=(Caption="News",Hint="View server news")
	Panels(4)=(Caption="Stats",Hint="View your current stats of this server")

	Begin Object Class=GUIButton Name=SettingsButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=SettingsButton.InternalOnKeyEvent
		WinTop=0.9
		WinLeft=0.194420
		WinWidth=0.147268
		WinHeight=0.035
		TabOrder=0
		bBoundToParent=True
		bScaleToParent=True
	End Object
	b_Settings=SettingsButton

	Begin Object Class=GUIButton Name=BrowserButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=BrowserButton.InternalOnKeyEvent
		WinTop=0.850000
		WinLeft=0.375000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=1
		bBoundToParent=True
		bScaleToParent=True
	End Object
	b_Browser=BrowserButton

	Begin Object Class=GUIButton Name=FavoritesButton
		StyleName="SquareButton"
		WinTop=0.870000
		WinLeft=0.025000
		WinWidth=0.200000
		WinHeight=0.050000
		bBoundToParent=True
		bScaleToParent=True
		OnClick=ButtonClicked
		OnKeyEvent=FavoritesButton.InternalOnKeyEvent
		bAutoSize=True
		TabOrder=2
	End Object
	b_Favs=FavoritesButton

	Begin Object Class=GUIButton Name=MapVotingButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=MapVotingButton.InternalOnKeyEvent
		WinTop=0.890000
		WinLeft=0.025000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=3
	End Object
	b_MapVote=MapVotingButton

	Begin Object Class=GUIButton Name=KickVotingButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=KickVotingButton.InternalOnKeyEvent
		WinTop=0.890000
		WinLeft=0.375000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=4
	End Object
	b_KickVote=KickVotingButton

	Begin Object Class=GUIButton Name=MatchSetupButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=MatchSetupButton.InternalOnKeyEvent
		WinTop=0.890000
		WinLeft=0.725000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=5
	End Object
	b_MatchSetup=MatchSetupButton

	Begin Object Class=GUIButton Name=SpectateButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=SpectateButton.InternalOnKeyEvent
		WinTop=0.890000
		WinLeft=0.725000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=6
	End Object
	b_Spec=SpectateButton

	Begin Object Class=GUIButton Name=ProfileButton
		Caption="Profile"
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=ProfileButton.InternalOnKeyEvent
		WinLeft=0.725000
		WinTop=0.890000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=7
	End Object
	b_Profile=ProfileButton
	
	Begin Object Class=GUIButton Name=LeaveMatchButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=LeaveMatchButton.InternalOnKeyEvent
		WinTop=0.870000
		WinLeft=0.725000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=49
		bBoundToParent=True
		bScaleToParent=True
	End Object
	b_Leave=LeaveMatchButton
	
	Begin Object Class=GUIButton Name=QuitGameButton
		StyleName="SquareButton"
		OnClick=ButtonClicked
		OnKeyEvent=QuitGameButton.InternalOnKeyEvent
		WinTop=0.870000
		WinLeft=0.725000
		WinWidth=0.200000
		WinHeight=0.050000
		bAutoSize=True
		TabOrder=50
	End Object
	b_Quit=QuitGameButton
	
	CurTabOrder=8
	OnPreDraw=InternalOnPreDraw
}

