class FKFQuickPerkSelect extends ServerPerks.SRKFQuickPerkSelect;

function ResizeIcons(Canvas C)
{
	PerkBack0.WinWidth = 0;
	PerkBack1.WinWidth = 0;
	PerkBack2.WinWidth = 0; 
	PerkBack3.WinWidth = 0;
	PerkBack4.WinWidth = 0;
	PerkBack5.WinWidth = 0;

	PerkSelectIcon0.WinWidth = 0;
	PerkSelectIcons[0] = PerkSelectIcon0;
	PerkSelectIcon1.WinWidth = 0;
	PerkSelectIcons[1] = PerkSelectIcon1;
	PerkSelectIcon2.WinWidth = 0;
	PerkSelectIcons[2] = PerkSelectIcon2;
	PerkSelectIcon3.WinWidth = 0;
	PerkSelectIcons[3] = PerkSelectIcon3;
	PerkSelectIcon4.WinWidth = 0;
	PerkSelectIcons[4] = PerkSelectIcon4;
	PerkSelectIcon5.WinWidth = 0;
	PerkSelectIcons[5] = PerkSelectIcon5;

	bResized = true;
}

function bool MyOnDraw(Canvas C)
{                                                                                                         
	local ClientPerkRepLink S;

	super(GUIMultiComponent).OnDraw(C);
	
	C.SetDrawColor(255, 255, 255, 255);
	
	// make em disappear
	if ( !bResized )
	{
		ResizeIcons(C);
	}		

	S = Class'ClientPerkRepLink'.Static.FindStats(C.Viewport.Actor);

	if(S != None)
	{
		// check if the current perk has changed recently
		CheckPerksX(S);

		// Draw current perk
		if( CurPerk!=255 )
			DrawCurrentPerkX(S, C, CurPerk);
	}
	
	return false;
}


function bool InternalOnClick(GUIComponent Sender)
{
	return false;	
}

defaultproperties
{
}
