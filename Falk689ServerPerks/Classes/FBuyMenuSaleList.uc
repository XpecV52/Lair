//=============================================================================
// The trader menu's list with items for sale
//=============================================================================
class FBuyMenuSaleList extends SRBuyMenuSaleList;

var Material FDefaultIcon;   // default icon used on shop items when none is found
var Material FFavoritesIcon; // favorites icon

event Opened(GUIComponent Sender)
{
   super(GUIVertList).Opened(Sender);

   // Get localized string
   FavoriteGroupName = Class'KFBuyMenuFilter'.Default.PerkSelectIcon8.Hint;

   // Fixed script warnings.
   UpdateForSaleBuyables();
}

function UpdateList()
{
   local int i;

   Super.UpdateList();

   ListPerkIcons[0] = FFavoritesIcon;

   for (i=1; i<ListPerkIcons.Length; i++)
   {
      if (ListPerkIcons[i] == None)
         ListPerkIcons[i] = FDefaultIcon;
   }
}


// allow partial kevlar to nudist only
function bool IsInInventory(class<Pickup> Item)
{
   if (class<FVestPickup>(Item) != none && FHumanPawn(PlayerOwner().Pawn) != none && FHumanPawn(PlayerOwner().Pawn).fShield > 0)
      return True;

   return Super.IsInInventory(Item);
}


function UpdateForSaleBuyables()
{
   local class<KFVeterancyTypes> PlayerVeterancy;
   local KFPlayerReplicationInfo KFPRI;
   local ClientPerkRepLink CPRL;
   local GUIBuyable ForSaleBuyable;
   local class<KFWeaponPickup> ForSalePickup;
   local int j, DualDivider, i, Num, z, PerkSaleOffset;
   local class<KFWeapon> ForSaleWeapon,SecType;
   local class<SRVeterancyTypes> Blocker;
   local KFShopVolume_Story CurrentShop;
   local byte DLCLocked;

   // Clear the ForSaleBuyables array
   CopyAllBuyables();
   ForSaleBuyables.Length = 0;

   // Grab the items for sale
   CPRL = Class'ClientPerkRepLink'.Static.FindStats(PlayerOwner());
   if( CPRL==None )
      return; // Hmmmm?

   KFPRI = KFPlayerReplicationInfo(PlayerOwner().PlayerReplicationInfo);

   // Grab Players Veterancy for quick reference
   if ( KFPRI!=none )
      PlayerVeterancy = KFPRI.ClientVeteranSkill;
   if( PlayerVeterancy==None )
      PlayerVeterancy = class'KFVeterancyTypes';
   CurrentShop = GetCurrentShop();

   // Grab the weapons!
   if( ActiveCategory>=-1 )
   {
      if( CurrentShop!=None )
         Num = CurrentShop.SaleItems.Length;
      else Num = CPRL.ShopInventory.Length;
      for ( z=0; z<Num; z++ )
      {
         if( CurrentShop!=None )
         {
            // Allow story mode volume limit weapon availability.
            ForSalePickup = class<KFWeaponPickup>(CurrentShop.SaleItems[z]);
            if( ForSalePickup==None )
               continue;
            for ( j=(CPRL.ShopInventory.Length-1); j>=CPRL.ShopInventory.Length; --j )
               if( CPRL.ShopInventory[j].PC==ForSalePickup )
                  break;
            if( j<0 )
               continue;
         }
         else
         {
            ForSalePickup = class<KFWeaponPickup>(CPRL.ShopInventory[z].PC);
            j = z;
         }

         if (ForSalePickup==None || class<KFWeapon>(ForSalePickup.default.InventoryType)==None || class<KFWeapon>(ForSalePickup.default.InventoryType).default.bKFNeverThrow || IsInInventory(ForSalePickup))
            continue;

         if (ActiveCategory == -1)
         {
            if( !Class'SRClientSettings'.Static.IsFavorite(ForSalePickup) )
               continue;
         }
         else if( ActiveCategory!=CPRL.ShopInventory[j].CatNum )
            continue;

         ForSaleWeapon = class<KFWeapon>(ForSalePickup.default.InventoryType);

         // Remove single weld.
         if( class'DualWeaponsManager'.Static.HasDualies(ForSaleWeapon,PlayerOwner().Pawn.Inventory) || (ForSalePickup.Default.VariantClasses.Length>0 && CheckGoldGunAvailable(ForSalePickup)) )
            continue;

         DualDivider = 1;

         // Make cheaper.
         if( ForSaleWeapon!=class'Dualies' && class'DualWeaponsManager'.Static.IsDualWeapon(ForSaleWeapon,SecType) && IsInInventoryWep(SecType) )
            DualDivider = 2;

         Blocker = None;
         for( i=0; i<CPRL.CachePerks.Length; ++i )
            if( !CPRL.CachePerks[i].PerkClass.Static.AllowWeaponInTrader(ForSalePickup,KFPRI,CPRL.CachePerks[i].CurrentLevel) )
            {
               Blocker = CPRL.CachePerks[i].PerkClass;
               break;
            }
         if( Blocker!=None && Blocker.Default.DisableTag=="" )
            continue;

         ForSaleBuyable = AllocateEntry(CPRL);

         ForSaleBuyable.ItemName 		      = ForSalePickup.default.ItemName;
         ForSaleBuyable.ItemDescription 		= ForSalePickup.default.Description;
         ForSaleBuyable.ItemImage            = ForSaleWeapon.default.TraderInfoTexture;
         ForSaleBuyable.ItemWeaponClass		= ForSaleWeapon;

         if (ForSaleWeapon.default.FireModeClass[0] != none)
            ForSaleBuyable.ItemAmmoClass		= ForSaleWeapon.default.FireModeClass[0].default.AmmoClass;

         ForSaleBuyable.ItemPickupClass		= ForSalePickup;
         ForSaleBuyable.ItemCost			      = int((float(ForSalePickup.default.Cost) * PlayerVeterancy.static.GetCostScaling(KFPRI, ForSalePickup)) / DualDivider);
         ForSaleBuyable.ItemAmmoCost		   = 0;
         ForSaleBuyable.ItemFillAmmoCost		= 0;

         ForSaleBuyable.ItemWeight	         = ForSaleWeapon.default.Weight;

         if( DualDivider==2 )
            ForSaleBuyable.ItemWeight       -= SecType.Default.Weight;

         ForSaleBuyable.ItemPower		      = ForSalePickup.default.PowerValue;
         ForSaleBuyable.ItemRange	         = ForSalePickup.default.RangeValue;
         ForSaleBuyable.ItemSpeed		      = ForSalePickup.default.SpeedValue;
         ForSaleBuyable.ItemAmmoMax		      = 0;
         ForSaleBuyable.ItemPerkIndex		   = ForSalePickup.default.CorrespondingPerkIndex;

         // Make sure we mark the list as a sale list
         ForSaleBuyable.bSaleList            = true;

         // Sort same perk weapons in front.
         if( ForSalePickup.default.CorrespondingPerkIndex == PlayerVeterancy.default.PerkIndex )
         {
            ForSaleBuyables.Insert(PerkSaleOffset, 1);
            i = PerkSaleOffset++;
         }
         else
         {
            i = ForSaleBuyables.Length;
            ForSaleBuyables.Length = i+1;
         }
         ForSaleBuyables[i] = ForSaleBuyable;
         DLCLocked = CPRL.ShopInventory[j].bDLCLocked;
         if( DLCLocked==0 && Blocker!=None )
         {
            ForSaleBuyable.ItemCategorie = Blocker.Default.DisableTag$":"$Blocker.Default.DisableDescription;
            DLCLocked = 3;
         }
         ForSaleBuyable.ItemAmmoCurrent = DLCLocked; // DLC info.
      }
   }

   // Now Update the list
   UpdateList();
}

defaultproperties
{
   FFavoritesIcon=Texture'KillingFloor2HUD.PerkIcons.Favorite_Perk_Icon'
   FDefaultIcon=Texture'KillingFloor2HUD.Perk_Icons.No_Perk_Icon'
}
