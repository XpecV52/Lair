class FBuyMenuInvList extends SRKFBuyMenuInvList;

var Material FDefaultIcon;  // default icon used on shop items when none is found

// edited vest price
function UpdateMyBuyables()
{
   local GUIBuyable MyBuyable, KnifeBuyable, FragBuyable;
   local array<GUIBuyable> SecTypes;
   local Inventory CurInv;
   local float CurAmmo, MaxAmmo;
   local class<KFWeaponPickup> MyPickup,MyPrimaryPickup;
   local int DualDivider,i;
   local class<KFVeterancyTypes> KFV;
   local class<FVeterancyTypes> FVet;
   local FHumanPawn             FPawn;
   local ClientPerkRepLink KFLR;
   local KFPlayerReplicationInfo PRI;
   local int                     fDiv;

   PRI = KFPlayerReplicationInfo(PlayerOwner().PlayerReplicationInfo);
   KFLR = Class'ClientPerkRepLink'.Static.FindStats(PlayerOwner());

   if( KFLR==None || PRI==None )
      return; // NANI!? Omae Wa Mou Shindeiru!!!

   // Let's start with our current inventory
   if ( PlayerOwner().Pawn.Inventory == none )
   {
      log("Inventory is none!");
      return;
   }

   DualDivider = 1;
   AutoFillCost = 0.00000;

   // Clear the MyBuyables array
   CopyAllBuyables();
   MyBuyables.Length = 0;

   KFV = PRI.ClientVeteranSkill;

   if (KFV == None)
      KFV = Class'KFVeterancyTypes';

   FVet  = class<FVeterancyTypes>(KFV);
   FPawn = FHumanPawn(PlayerOwner().Pawn);

   // Fill the Buyables
   for (CurInv = PlayerOwner().Pawn.Inventory; CurInv != none; CurInv = CurInv.Inventory )
   {
      if (KFWeapon(CurInv)==None /*|| CurInv.IsA('Welder') || CurInv.IsA('Syringe')*/ || CurInv.IsA('Dummy_JoggingWeapon'))
         continue;

      if (CurInv.IsA('DualDeagle') || CurInv.IsA('Dual44Magnum') || CurInv.IsA('DualMK23Pistol') || KFWeapon(CurInv).DemoReplacement != None)
         DualDivider = 2;

      else DualDivider = 1;

      MyPickup = class<KFWeaponPickup>(CurInv.default.PickupClass);

      KFWeapon(CurInv).GetAmmoCount(MaxAmmo, CurAmmo);

      // let's make this simple, set a fixed alt ammo price
      /*if ( KFWeapon(CurInv).bHasSecondaryAmmo )
         MyPrimaryPickup = MyPickup.default.PrimaryWeaponPickup;
      else MyPrimaryPickup = MyPickup;*/

      MyPrimaryPickup = MyPickup;
      MyBuyable = AllocateEntry(KFLR);

      MyBuyable.ItemName 	   	= MyPickup.default.ItemShortName;
      MyBuyable.ItemDescription 	= KFWeapon(CurInv).default.Description;
      MyBuyable.ItemCategorie		= "Melee"; // More dummy.
      MyBuyable.ItemImage		   = KFWeapon(CurInv).default.TraderInfoTexture;
      MyBuyable.ItemWeaponClass	= KFWeapon(CurInv).class;
      MyBuyable.ItemAmmoClass		= KFWeapon(CurInv).default.FireModeClass[0].default.AmmoClass;
      MyBuyable.ItemPickupClass	= MyPrimaryPickup;
      MyBuyable.ItemCost		   = (float(MyPickup.default.Cost) * KFV.static.GetCostScaling(PRI, MyPickup)) / DualDivider;

      if ((KFWeapon(CurInv).default.MagCapacity == 1 && MyPrimaryPickup.default.BuyClipSize > 1) ||
          (KFWeapon(CurInv).default.MagCapacity != MyPrimaryPickup.default.BuyClipSize))
      {
         if (MaxAmmo - CurAmmo < MyPrimaryPickup.default.BuyClipSize * KFV.static.GetMagCapacityMod(PRI, KFWeapon(CurInv)))
            MyBuyable.ItemAmmoCost	= MyPrimaryPickup.default.AmmoCost * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup) / (MyPrimaryPickup.default.BuyClipSize * KFV.static.GetMagCapacityMod(PRI, KFWeapon(CurInv))) * (MaxAmmo - CurAmmo);

         else
            MyBuyable.ItemAmmoCost	= MyPrimaryPickup.default.AmmoCost * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup) * KFV.static.GetMagCapacityMod(PRI, KFWeapon(CurInv));

         MyBuyable.ItemFillAmmoCost	= (int(((MaxAmmo - CurAmmo) * float(MyPrimaryPickup.default.AmmoCost)) / float(MyPrimaryPickup.default.BuyClipSize))) * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup);
      }

      else if (MyPickup == class'HuskGunPickup')
      {
         MyBuyable.ItemFillAmmoCost	= (int(((MaxAmmo - CurAmmo) * float(MyPrimaryPickup.default.AmmoCost)) / float(MyPrimaryPickup.default.BuyClipSize))) * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup);
      }

      else
      {
         MyBuyable.ItemAmmoCost		   = MyPrimaryPickup.default.AmmoCost * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup) * KFV.static.GetMagCapacityMod(PRI, KFWeapon(CurInv));
         MyBuyable.ItemFillAmmoCost		= (int(((MaxAmmo - CurAmmo) * float(MyPrimaryPickup.default.AmmoCost)) / float(KFWeapon(CurInv).default.MagCapacity))) * KFV.static.GetAmmoCostScaling(PRI, MyPrimaryPickup);
      }

      MyBuyable.ItemWeight		= KFWeapon(CurInv).Weight;
      MyBuyable.ItemPower		= MyPickup.default.PowerValue;
      MyBuyable.ItemRange		= MyPickup.default.RangeValue;
      MyBuyable.ItemSpeed		= MyPickup.default.SpeedValue;
      MyBuyable.ItemAmmoCurrent	= CurAmmo;
      MyBuyable.ItemAmmoMax		= MaxAmmo;
      MyBuyable.bMelee			= (KFMeleeGun(CurInv)!=none || MyBuyable.ItemAmmoClass==None || SparkGunBaseFalk(CurInv) != none);
      MyBuyable.bSaleList		= false;
      MyBuyable.ItemPerkIndex		= MyPickup.default.CorrespondingPerkIndex;

      if ( KFWeapon(CurInv) != none && KFWeapon(CurInv).SellValue != -1 )
         MyBuyable.ItemSellValue = KFWeapon(CurInv).SellValue;
      else MyBuyable.ItemSellValue = MyBuyable.ItemCost * 0.5;

      if ( !MyBuyable.bMelee && int(MaxAmmo)>int(CurAmmo) )
         AutoFillCost += MyBuyable.ItemFillAmmoCost;

      if ( CurInv.IsA('Knife') )
      {
         MyBuyable.bSellable	= false;
         KnifeBuyable = MyBuyable;
      }
      else if ( CurInv.IsA('Frag') )
      {
         MyBuyable.bSellable	= false;
         FragBuyable = MyBuyable;
      }
      else
      {
         MyBuyable.bSellable	= !KFWeapon(CurInv).default.bKFNeverThrow;
         MyBuyables.Insert(0,1);
         MyBuyables[0] = MyBuyable;
      }

      if ( !KFWeapon(CurInv).bHasSecondaryAmmo )
         continue;

      // Add secondary ammo.
      KFWeapon(CurInv).GetSecondaryAmmoCount(MaxAmmo, CurAmmo);

      MyBuyable = AllocateEntry(KFLR);

      MyBuyable.ItemName 		= MyPickup.default.SecondaryAmmoShortName;
      MyBuyable.ItemDescription 	= KFWeapon(CurInv).default.Description;
      MyBuyable.ItemCategorie		= "Melee";
      MyBuyable.ItemImage		= KFWeapon(CurInv).default.TraderInfoTexture;
      MyBuyable.ItemWeaponClass	= KFWeapon(CurInv).class;
      MyBuyable.ItemAmmoClass		= KFWeapon(CurInv).default.FireModeClass[1].default.AmmoClass;
      MyBuyable.ItemPickupClass	= MyPickup;
      MyBuyable.ItemCost		= (float(MyPickup.default.Cost) * KFV.static.GetCostScaling(PRI, MyPickup)) / DualDivider;
      MyBuyable.ItemAmmoCost		= 10.0 * KFV.static.GetAmmoCostScaling(PRI, MyPickup);
      MyBuyable.ItemFillAmmoCost	= (int(((MaxAmmo - CurAmmo) * 10.0))) * KFV.static.GetAmmoCostScaling(PRI, MyPickup);
      MyBuyable.ItemWeight		= KFWeapon(CurInv).Weight;
      MyBuyable.ItemPower		= MyPickup.default.PowerValue;
      MyBuyable.ItemRange		= MyPickup.default.RangeValue;
      MyBuyable.ItemSpeed		= MyPickup.default.SpeedValue;
      MyBuyable.ItemAmmoCurrent	= CurAmmo;
      MyBuyable.ItemAmmoMax		= MaxAmmo;
      MyBuyable.bMelee		= (KFMeleeGun(CurInv) != none || SparkGunBaseFalk(CurInv) != none);
      MyBuyable.bSaleList		= false;
      MyBuyable.ItemPerkIndex		= MyPickup.default.CorrespondingPerkIndex;
      MyBuyable.bSellable		= !KFWeapon(CurInv).default.bKFNeverThrow;

      if ( KFWeapon(CurInv) != none && KFWeapon(CurInv).SellValue != -1 )
         MyBuyable.ItemSellValue = KFWeapon(CurInv).SellValue;
      else MyBuyable.ItemSellValue = MyBuyable.ItemCost * 0.5;

      if ( !MyBuyable.bMelee && int(MaxAmmo) > int(CurAmmo))
         AutoFillCost += MyBuyable.ItemFillAmmoCost;

      SecTypes[SecTypes.Length] = MyBuyable;
   }

   MyBuyable = AllocateEntry(KFLR);

   MyBuyable.ItemName 		   = class'BuyableVest'.default.ItemName;
   MyBuyable.ItemDescription 	= class'BuyableVest'.default.ItemDescription;
   MyBuyable.ItemCategorie		= "";
   MyBuyable.ItemImage		   = class'BuyableVest'.default.ItemImage;
   MyBuyable.ItemAmmoCurrent	= PlayerOwner().Pawn.ShieldStrength;
   MyBuyable.ItemAmmoMax	= 100;

   if (FPawn != none)
      MyBuyable.ItemAmmoCurrent = FPawn.fShield;

   if (FVet != none)
      MyBuyable.ItemAmmoMax	  = FVet.Static.fGetMaxShield(PRI);

   MyBuyable.ItemCost		   = int(class'BuyableVest'.default.ItemCost * KFV.static.GetCostScaling(PRI, class'Vest'));

   if (MyBuyable.ItemAmmoMax != 100)
      MyBuyable.ItemCost		   = int(class'BuyableVest'.default.ItemCost / 100 * MyBuyable.ItemAmmoMax * KFV.static.GetCostScaling(PRI, class'Vest'));

   MyBuyable.ItemAmmoCost		= MyBuyable.ItemCost / MyBuyable.ItemAmmoMax;
   MyBuyable.ItemFillAmmoCost	= int((MyBuyable.ItemAmmoMax - MyBuyable.ItemAmmoCurrent) * MyBuyable.ItemAmmoCost);

   // vest cost
   if (PRI.Score > MyBuyable.ItemAmmoCost && PRI.Score < MyBuyable.ItemFillAmmoCost)
   {
      fDiv = PRI.Score % MyBuyable.ItemAmmoCost;
      MyBuyable.ItemFillAmmoCost = PRI.Score - fDiv;
   }

   MyBuyable.bIsVest			   = true;
   MyBuyable.bMelee			   = false;
   MyBuyable.bSaleList		   = false;
   MyBuyable.bSellable		   = false;
   MyBuyable.ItemPerkIndex		= class'BuyableVest'.default.CorrespondingPerkIndex;

   if( MyBuyables.Length<=(7-SecTypes.Length) )
   {
      MyBuyables.Length = 11;
      for( i=(SecTypes.Length-1); i>=0; --i )
         MyBuyables[7-i] = SecTypes[i];
      MyBuyables[8] = KnifeBuyable;
      MyBuyables[9] = FragBuyable;
      MyBuyables[10] = MyBuyable;
   }
   else
   {
      MyBuyables[MyBuyables.Length] = none;
      for( i=(SecTypes.Length-1); i>=0; --i )
         MyBuyables[MyBuyables.Length] = SecTypes[i];
      MyBuyables[MyBuyables.Length] = KnifeBuyable;
      MyBuyables[MyBuyables.Length] = FragBuyable;
      MyBuyables[MyBuyables.Length] = MyBuyable;
   }

   //Now Update the list
   UpdateList();
}

function DrawInvItem(Canvas Canvas, int CurIndex, float X, float Y, float Width, float Height, bool bSelected, bool bPending)
{
   local float IconBGSize, ItemBGWidth, AmmoBGWidth, ClipButtonWidth, FillButtonWidth;
   local float TempX, TempY;
   local float StringHeight, StringWidth;
   local int fPerkIndex;

   OnClickSound=CS_Click;

   // Initialize the Canvas
   Canvas.Style = 1;
   // Canvas.Font = class'ROHUD'.Static.GetSmallMenuFont(Canvas);
   Canvas.SetDrawColor(255, 255, 255, 255);

   if ( MyBuyables[CurIndex]==None )
   {
      if( MyBuyables.Length==(CurIndex+1) || MyBuyables[CurIndex+1]==None )
         return;

      Canvas.SetPos(X + EquipmentBGXOffset, Y + Height - EquipmentBGYOffset - EquipmentBGHeightScale * Height);
      Canvas.DrawTileStretched(AmmoBackground, EquipmentBGWidthScale * Width, EquipmentBGHeightScale * Height);

      Canvas.SetDrawColor(175, 176, 158, 255);
      Canvas.StrLen(EquipmentString, StringWidth, StringHeight);
      Canvas.SetPos(X + EquipmentBGXOffset + ((EquipmentBGWidthScale * Width - StringWidth) / 2.0), Y + Height - EquipmentBGYOffset - EquipmentBGHeightScale * Height + ((EquipmentBGHeightScale * Height - StringHeight) / 2.0));
      Canvas.DrawText(EquipmentString);
   }
   else
   {
      // Calculate Widths for all components
      IconBGSize = Height;
      ItemBGWidth = (Width * ItemBGWidthScale) - IconBGSize;
      AmmoBGWidth = Width * AmmoBGWidthScale;

      if ( !MyBuyables[CurIndex].bIsVest )
      {
         FillButtonWidth = ((1.0 - ItemBGWidthScale - AmmoBGWidthScale) * Width) - ButtonSpacing;
         ClipButtonWidth = FillButtonWidth * ClipButtonWidthScale;
         FillButtonWidth -= ClipButtonWidth;
      }
      else
      {
         FillButtonWidth = ((1.0 - ItemBGWidthScale - AmmoBGWidthScale) * Width);
      }

      // Offset for the Background
      TempX = X;
      TempY = Y;

      // Draw Item Background
      Canvas.SetPos(TempX, TempY);

      fPerkIndex = min(CurIndex, PerkTextures.Length);

      if ( bSelected )
      {
         Canvas.DrawTileStretched(SelectedItemBackgroundLeft, IconBGSize, IconBGSize);
         Canvas.SetPos(TempX + 4, TempY + 4);

         if (PerkTextures[fPerkIndex] != none)
            Canvas.DrawTile(PerkTextures[fPerkIndex], IconBGSize - 8, IconBGSize - 8, 0, 0, 256, 256);

         else
            Canvas.DrawTile(Texture(FDefaultIcon), IconBGSize - 8, IconBGSize - 8, 0, 0, 256, 256);

         TempX += IconBGSize;
         Canvas.SetPos(TempX, TempY + ItemBGYOffset);
         Canvas.DrawTileStretched(SelectedItemBackgroundRight, ItemBGWidth, IconBGSize - (2.0 * ItemBGYOffset));
      }
      else
      {
         Canvas.DrawTileStretched(ItemBackgroundLeft, IconBGSize, IconBGSize);
         Canvas.SetPos(TempX + 4, TempY + 4);
         if (PerkTextures[fPerkIndex] != none)
            Canvas.DrawTile(PerkTextures[fPerkIndex], IconBGSize - 8, IconBGSize - 8, 0, 0, 256, 256);

         else
            Canvas.DrawTile(Texture(FDefaultIcon), IconBGSize - 8, IconBGSize - 8, 0, 0, 256, 256);

         TempX += IconBGSize;
         Canvas.SetPos(TempX, TempY + ItemBGYOffset);
         Canvas.DrawTileStretched(ItemBackgroundRight, ItemBGWidth, IconBGSize - (2.0 * ItemBGYOffset));
      }

      // Select Text color
      if ( CurIndex == MouseOverIndex && MouseOverXIndex == 0 )
         Canvas.SetDrawColor(255, 255, 255, 255);
      else Canvas.SetDrawColor(0, 0, 0, 255);

      // Draw the item's name
      Canvas.StrLen(NameStrings[CurIndex], StringWidth, StringHeight);
      Canvas.SetPos(TempX + ItemNameSpacing, Y + ((Height - StringHeight) / 2.0));
      Canvas.DrawText(NameStrings[CurIndex]);

      // Draw the item's ammo status if it is not a melee weapon
      if ( !MyBuyables[CurIndex].bMelee )
      {
         TempX += ItemBGWidth + AmmoSpacing;

         Canvas.SetDrawColor(255, 255, 255, 255);
         Canvas.SetPos(TempX, TempY + ((Height - AmmoBGHeightScale * Height) / 2.0));
         Canvas.DrawTileStretched(AmmoBackground, AmmoBGWidth, AmmoBGHeightScale * Height);

         Canvas.SetDrawColor(175, 176, 158, 255);
         Canvas.StrLen(AmmoStrings[CurIndex], StringWidth, StringHeight);
         Canvas.SetPos(TempX + ((AmmoBGWidth - StringWidth) / 2.0), TempY + ((Height - StringHeight) / 2.0));
         Canvas.DrawText(AmmoStrings[CurIndex]);

         TempX += AmmoBGWidth + AmmoSpacing;

         Canvas.SetDrawColor(255, 255, 255, 255);
         Canvas.SetPos(TempX, TempY + ((Height - ButtonBGHeightScale * Height) / 2.0));

         if (!MyBuyables[CurIndex].bIsVest)
         {
            if (MyBuyables[CurIndex].ItemAmmoCurrent >= MyBuyables[CurIndex].ItemAmmoMax || (PlayerOwner().PlayerReplicationInfo.Score < MyBuyables[CurIndex].ItemFillAmmoCost &&
               ((MyBuyables[CurIndex].ItemPickupClass.default.BuyClipSize == 1 && PlayerOwner().PlayerReplicationInfo.Score < MyBuyables[CurIndex].ItemAmmoCost) || (PlayerOwner().PlayerReplicationInfo.Score < (MyBuyables[CurIndex].ItemAmmoCost / float(MyBuyables[CurIndex].ItemPickupClass.default.BuyClipSize))))))
            {
               Canvas.DrawTileStretched(DisabledButtonBackground, ClipButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }
            else if ( CurIndex == MouseOverIndex && MouseOverXIndex == 1 )
            {
               Canvas.DrawTileStretched(HoverButtonBackground, ClipButtonWidth, ButtonBGHeightScale * Height);
            }
            else
            {
               Canvas.DrawTileStretched(ButtonBackground, ClipButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }

            Canvas.StrLen(ClipPriceStrings[CurIndex], StringWidth, StringHeight);
            Canvas.SetPos(TempX + ((ClipButtonWidth - StringWidth) / 2.0), TempY + ((Height - StringHeight) / 2.0));
            Canvas.DrawText(ClipPriceStrings[CurIndex]);

            TempX += ClipButtonWidth + ButtonSpacing;

            Canvas.SetDrawColor(255, 255, 255, 255);
            Canvas.SetPos(TempX, TempY + ((Height - ButtonBGHeightScale * Height) / 2.0));

            if (MyBuyables[CurIndex].ItemAmmoCurrent >= MyBuyables[CurIndex].ItemAmmoMax || (PlayerOwner().PlayerReplicationInfo.Score < MyBuyables[CurIndex].ItemFillAmmoCost &&
               ((MyBuyables[CurIndex].ItemPickupClass.default.BuyClipSize == 1 && PlayerOwner().PlayerReplicationInfo.Score < MyBuyables[CurIndex].ItemAmmoCost) || (PlayerOwner().PlayerReplicationInfo.Score < (MyBuyables[CurIndex].ItemAmmoCost / float(MyBuyables[CurIndex].ItemPickupClass.default.BuyClipSize))))))
            {
               Canvas.DrawTileStretched(DisabledButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }
            else if ( CurIndex == MouseOverIndex && MouseOverXIndex == 2 )
            {
               Canvas.DrawTileStretched(HoverButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
            }
            else
            {
               Canvas.DrawTileStretched(ButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }
         }
         else
         {
            if ( (PlayerOwner().PlayerReplicationInfo.Score < MyBuyables[CurIndex].ItemAmmoCost) ||
                  MyBuyables[CurIndex].ItemAmmoCurrent >= MyBuyables[CurIndex].ItemAmmoMax )
            {
               Canvas.DrawTileStretched(DisabledButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }
            else if ( CurIndex == MouseOverIndex && MouseOverXIndex >= 1 )
            {
               Canvas.DrawTileStretched(HoverButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
            }
            else
            {
               Canvas.DrawTileStretched(ButtonBackground, FillButtonWidth, ButtonBGHeightScale * Height);
               Canvas.SetDrawColor(0, 0, 0, 255);
            }
         }

         Canvas.StrLen(FillPriceStrings[CurIndex], StringWidth, StringHeight);
         Canvas.SetPos(TempX + ((FillButtonWidth - StringWidth) / 2.0), TempY + ((Height - StringHeight) / 2.0));
         Canvas.DrawText(FillPriceStrings[CurIndex]);
      }
      Canvas.SetDrawColor(255, 255, 255, 255);
   }
}

function bool InternalOnClick(GUIComponent Sender)
{
   local int NewIndex;
   local float RelativeMouseX;

   if ( IsInClientBounds() && MyBuyables[NewIndex] != none )
   {
      //  Figure out which Item we're clicking on
      NewIndex = CalculateIndex();
      RelativeMouseX = Controller.MouseX - ClientBounds[0];
      if ( RelativeMouseX < ActualWidth() * ItemBGWidthScale )
      {
         SetIndex(NewIndex);
         MouseOverXIndex = 0;
         return true;
      }
      else
      {
         RelativeMouseX -= ActualWidth() * (ItemBGWidthScale + AmmoBGWidthScale);

         if ( RelativeMouseX > 0 )
         {
            if ( MyBuyables[NewIndex].bIsVest )
            {
               if ( (PlayerOwner().PlayerReplicationInfo.Score >= MyBuyables[NewIndex].ItemAmmoCost) || PlayerOwner().PlayerReplicationInfo.Score >= MyBuyables[NewIndex].ItemCost )
               {
                  OnBuyVestClick();
               }
            }
            else if ( RelativeMouseX < ActualWidth() * (1.0 - ItemBGWidthScale - AmmoBGWidthScale) * ClipButtonWidthScale )
            {
               // Buy Clip
               OnBuyClipClick(MyBuyables[NewIndex]);
            }
            else
            {
               // Fill Ammo
               OnFillAmmoClick(MyBuyables[NewIndex]);
            }
         }
      }
   }

   return false;
}

function UpdateList()
{
   Super.UpdateList();
   PerkTextures[PerkTextures.Length] = Texture(FDefaultIcon);

}

defaultproperties
{
   FDefaultIcon=Texture'KillingFloor2HUD.Perk_Icons.No_Perk_Icon';
}
