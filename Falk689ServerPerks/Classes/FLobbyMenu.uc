class FLobbyMenu extends SRLobbyMenu;

var string FNormalString;
var string FHardString;
var string FSuicidalString;
var string FHellOnEarthString;
var string FBloodbathString;

function bool InternalOnPreDraw(Canvas C)
{
	local int i, j;
	local string StoryString;
	local String SkillString;
	local KFGameReplicationInfo KFGRI;
	local PlayerController PC;

	PC = PlayerOwner();

	if ( PC == none || PC.Level == none ) // Error?
	{
		return false;
	}

	if ( (PC.PlayerReplicationInfo != none && (!PC.PlayerReplicationInfo.bWaitingPlayer || PC.PlayerReplicationInfo.bOnlySpectator)) || PC.Outer.Name=='Entry' )
	{
		bAllowClose = true;
		PC.ClientCloseMenu(True,False);
		return false;
	}

	t_Footer.InternalOnPreDraw(C);

	KFGRI = KFGameReplicationInfo(PC.GameReplicationInfo);

	if ( KFGRI != none )
		WaveLabel.Caption = string(KFGRI.WaveNumber + 1) $ "/" $ string(KFGRI.FinalWave);
	else
	{
		WaveLabel.Caption = "?/?";
		return false;
	}
	C.DrawColor.A = 255;

	// First fill in non-ready players.
	for ( i = 0; i<KFGRI.PRIArray.Length; i++ )
	{
		if ( KFGRI.PRIArray[i] == none || KFGRI.PRIArray[i].bOnlySpectator || KFGRI.PRIArray[i].bReadyToPlay || KFPlayerReplicationInfo(KFGRI.PRIArray[i])==None )
			continue;

		AddPlayer(KFPlayerReplicationInfo(KFGRI.PRIArray[i]),j,C);
		if( ++j>=MaxPlayersOnList )
			GoTo'DoneIt';
	}

	// Then comes rest.
	for ( i = 0; i < KFGRI.PRIArray.Length; i++ )
	{
		if ( KFGRI.PRIArray[i]==none || KFGRI.PRIArray[i].bOnlySpectator || !KFGRI.PRIArray[i].bReadyToPlay || KFPlayerReplicationInfo(KFGRI.PRIArray[i])==None )
			continue;

		if ( KFGRI.PRIArray[i].bReadyToPlay )
		{
			if ( !bTimeoutTimeLogged )
			{
				ActivateTimeoutTime = PC.Level.TimeSeconds;
				bTimeoutTimeLogged = true;
			}
		}
		AddPlayer(KFPlayerReplicationInfo(KFGRI.PRIArray[i]),j,C);
		if( ++j>=MaxPlayersOnList )
			GoTo'DoneIt';
	}

	if( j<MaxPlayersOnList )
		EmptyPlayers(j);

DoneIt:
	StoryString = PC.Level.Description;

	if ( !bStoryBoxFilled )
	{
		l_StoryBox.LoadStoryText();
		bStoryBoxFilled = true;
	}

	CheckBotButtonAccess();

	if (KFGRI.BaseDifficulty <= 1)
		SkillString = BeginnerString;

	else if (KFGRI.BaseDifficulty <= 2)
		SkillString = FNormalString;

	else if (KFGRI.BaseDifficulty <= 4)
		SkillString = FHardString;

	else if (KFGRI.BaseDifficulty <= 5)
		SkillString = FSuicidalString;

	else if (KFGRI.BaseDifficulty <= 7)
      SkillString = FHellOnEarthString;

	else
      SkillString = FBloodbathString;

	CurrentMapLabel.Caption = CurrentMapString @ PC.Level.Title;
	DifficultyLabel.Caption = DifficultyString @ SkillString;

	return false;
}

defaultproperties
{
   Begin Object Class=FLobbyFooter Name=BuyFooter
   RenderWeight=0.300000
   TabOrder=8
   bBoundToParent=False
   bScaleToParent=False
   OnPreDraw=BuyFooter.InternalOnPreDraw
   End Object

   t_Footer=BuyFooter 

   Begin Object Class=GUILabel Name=TimeOutCounter
   Caption="The game will start in: "
   TextAlign=TXTA_Center
   TextColor=(B=158,G=176,R=175)
   WinTop=0.000010
   WinLeft=0.059552
   WinWidth=0.346719
   WinHeight=0.045704
   TabOrder=6
   End Object

   label_TimeOutCounter=GUILabel'FLobbyMenu.TimeOutCounter'
   AutoCommence="The game will start in"

   FNormalString="Normal"
   FHardString="Hard"
   FSuicidalString="Suicidal"
   FHellOnEarthString="Hell On Earth"
   FBloodbathString="Bloodbath"
}
