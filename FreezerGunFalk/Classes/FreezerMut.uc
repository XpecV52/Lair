// Mutator is used only only to spawn game rules and is auto-destroyed after that.
class FreezerMut extends Mutator;

function PostBeginPlay()
{
    Class'FreezeRules'.static.FindOrSpawnMe(Level.Game);
    Destroy();
}

defaultproperties
{
    bAddToServerPackages=True
    GroupName="KF-IJC"
    FriendlyName="Lair - Cryo Mass Driver"
    Description="Adds the Cryo Mass Driver N600 to the game."
}