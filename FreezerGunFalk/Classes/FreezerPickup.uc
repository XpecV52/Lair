class FreezerPickup extends KFWeaponPickup;

function Destroyed()
{
	if (bDropped && Inventory != none && class<Weapon>(Inventory.Class) != none)
	{
		if (KFGameType(Level.Game) != none)
			KFGameType(Level.Game).WeaponDestroyed(class<Weapon>(Inventory.Class));
	}

	super(WeaponPickup).Destroyed();
}

defaultproperties
{
    Weight=6.000000
    cost=800
    AmmoCost=10
    BuyClipSize=40
    PowerValue=5
    SpeedValue=50
    RangeValue=50
    ItemName="Cryo Mass Driver N600"
    ItemShortName="N600"
    AmmoItemName="Liquid nitrogen"
    CorrespondingPerkIndex=8
    EquipmentCategoryID=3
    InventoryType=Class'FreezerGun'
    PickupMessage="You got a Cryo Mass Driver N600"
    PickupSound=Sound'KF_NailShotgun.Handling.KF_NailShotgun_Pickup'
    PickupForce="AssaultRiflePickup"
    StaticMesh=StaticMesh'IJC_Project_Santa_A.FreezerGun_Pickup'
    DrawScale=1.500000
    CollisionRadius=35.000000
    CollisionHeight=5.000000
}
