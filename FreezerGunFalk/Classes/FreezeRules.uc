// FreezeRules controls Zed freezing on server side.
// FreezeReplicationInfo is used to replciate freezing effects to clients.

class FreezeRules extends GameRules
   config(FalkFreezeRulesCfg);

var config FreezeReplicationInfo FreezeRI;

var int   fCurrentIndex;          // currently processed index into the freeze array
var byte  fFrozenCount;           // how many frozen zeds we currently have on the server

//Falk689 stuff.
var config float BloatWarmTime;
var config float BossWarmTime;
var config float BruteWarmTime;
var config float ClotWarmTime;
var config float CrawlerWarmTime;
var config float FleshpoundWarmTime;
var config float GorefastWarmTime;
var config float HuskWarmTime;
var config float ScrakeWarmTime;
var config float SirenWarmTime;
var config float StalkerWarmTime;
var config float FallbackWarmTime;

var config int BloatFreezeThreshold;
var config int BossFreezeThreshold;
var config int BruteFreezeThreshold;
var config int ClotFreezeThreshold;
var config int CrawlerFreezeThreshold;
var config int FleshpoundFreezeThreshold;
var config int GorefastFreezeThreshold;
var config int HuskFreezeThreshold;
var config int ScrakeFreezeThreshold;
var config int SirenFreezeThreshold;
var config int StalkerFreezeThreshold;
var config int FallbackFreezeThreshold;

var config float ResetTime;             // how much time to wait before resetting zeds CurrentFreeze
var config float FreezeResistance;      // multiplier for zeds FreezeThreshold, applied every time they freeze
var config float FreezeDelay;           // standard delay to wait, as a failsafe, after an animation ended to actually freeze the zed
var config float FreezePrecision;       // time to wait between zeds freeze checks, lower means more precise
var config float FreezeMaxLag;          // how much time we're willing to wait between two checks of the same zed
var int          MaxZedsDefault;        // maximum number of zeds we are dealing with using the default precision


struct SFrozen
{
   var FalkMonster M;
   var bool        bFrozen;
   var bool        fShouldBeFrozen; // should this zed be frozen right now?
   var int         CurrentFreeze;
   var int         FreezeThreshold; // when CurrentFreeze reaches this value, zed becomes completely frozen
   var float       WarmTime;        // time after which zed should get warmed up
   var float       FreezeTime;      // when was this zed frozen

   //var float       DeltaTime;       // how much time has passed since the zed was hit by freezing stuff
   var float       LastHitTime;     // when this zed was last hit by freeze

   var vector      FocalPoint;
   var pawn        Enemy;
   var bool        PendingFreeze;   // pending freeze state, used to delay freeze a bit when animating
};

var transient array<SFrozen> Frozen;

final static function FreezeRules FindOrSpawnMe(GameInfo Game)
{
   local GameRules GR;
   local FreezeRules FR;

   FR = FreezeRules(Game.GameRulesModifiers);
   if ( FR == none )
   {
      for ( GR = Game.GameRulesModifiers; GR != none && FR == none; GR = GR.NextGameRules )
         FR = FreezeRules(Game.GameRulesModifiers);

      // Freeze rules not found. Spawn it now
      if ( FR == none )
      {
         FR = Game.Spawn(Class'FreezeRules', Game);
         if( Game.GameRulesModifiers==None )
            Game.GameRulesModifiers = FR;
         else Game.GameRulesModifiers.AddGameRules(FR);        
      }
   }

   return FR;
}


function PostBeginPlay()
{
   FreezeRI = spawn(class'FreezeReplicationInfo', self);

   MaxZedsDefault = Max(FreezeMaxLag / FreezePrecision, 1);

   //warn("MaxZedsDefault:"@MaxZedsDefault);
}

function AddGameRules(GameRules GR)
{
   if ( GR.class != self.class ) //prevent adding same rules more than once
      Super.AddGameRules(GR);
}

function int NetDamage(int OriginalDamage, int Damage, pawn injured, pawn instigatedBy, vector HitLocation, out vector Momentum, class<DamageType> DamageType)
{
   local class<KFWeaponDamageType> KFDamType;
   local class<DamTypeFreezerGun>  FreezeDT;
   local class<DamTypeSW76Falk>    ArtiDT;
   local FalkMonster               ZedVictim;
   local int                       idx;
   local KFPlayerReplicationInfo   KFPRI;
   local class<FVeterancyTypes>    Vet;
   local bool                      fASkill;

   KFDamType = class<KFWeaponDamageType>(damageType);
   ZedVictim = FalkMonster(injured);
   FreezeDT  = class<DamTypeFreezerGun>(damageType);
   ArtiDT    = class<DamTypeSW76Falk>(damageType);

   if (KFDamType != none && ZedVictim != none && ZedVictim.Controller != none && ZedVictim.Health > 0 && !ZedVictim.bZapped)
   {
      // don't freeze the metal clot
      if (ZedVictim.FalkGetZedClass() != Falk_Metal_Clot)
      {
         // artillerist zed time bonus
         if (ArtiDT != none && ZedVictim.Health > 0 && ZedVictim.HeadHealth > 0)
         {
            if (instigatedBy != none)
            {
               KFPRI = KFPlayerReplicationInfo(instigatedBy.PlayerReplicationInfo);

               if (KFPRI != none)
               {
                  Vet = class<FVeterancyTypes>(KFPRI.ClientVeteranSkill);

                  if (Vet != none && Vet.Static.ArtilleristZedTimeBonus(KFPRI))
                     fASkill = True;
               }
            }
         }

         if (FreezeDT != none || fASkill)
         {
            // don't freeze our pat when fleeing / healing
            if (ZedVictim != none && ZedVictim.fBossResist(689, ZedVictim.Health) < 689)
            {
               //warn("STOP");

               if (NextGameRules != None)
                  return NextGameRules.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);

               return 0;
            }

            //log("FreezeDamage");
            //log(Damage);

            ZedVictim.BurnDown   = 0;
            ZedVictim.bBurnified = false;
            ZedVictim.Timer(); // stop burning behavior

            idx = FrozenIndex(ZedVictim, true);
            //Frozen[idx].DeltaTime = 0.0;         
            Frozen[idx].LastHitTime = Level.TimeSeconds;
            //warn("LAST HIT TIME:"@Level.TimeSeconds); 

            // zed time artillerist damage is cooler than yours
            if (fASkill)
               Frozen[idx].CurrentFreeze = Frozen[idx].FreezeThreshold;

            // normal freezing damage
            else
               Frozen[idx].CurrentFreeze += Damage;

            // freeze dat foker
            if (Frozen[idx].CurrentFreeze >= Frozen[idx].FreezeThreshold && !ZedVictim.bZapped)
            {
               Frozen[idx].CurrentFreeze   = 0;
               Frozen[idx].bFrozen         = False;
               Frozen[idx].fShouldBeFrozen = True;

               if (!fASkill)
                  Frozen[idx].FreezeThreshold *= FreezeResistance;

               Frozen[idx].WarmTime = Level.TimeSeconds + GetWarmTime(Frozen[idx].M);

               //warn("Starting Freeze Zed:"@Frozen[idx].M.MenuName@"Index:"@idx@"WarmTime:"@Frozen[idx].WarmTime@"Time:"@Level.TimeSeconds);
            }
         }

         else
         {
            idx = FrozenIndex(ZedVictim, false);

            if (idx != -1)
            {
               if (KFDamType.default.bDealBurningDamage)
               {
                  // fire instantly unfreezes zed
                  if (Frozen[idx].bFrozen)
                  {
                     UnfreezeZed(ZedVictim, idx);

                     // and sets them on fire
                     if (Frozen[idx].M.BurnDown <= 0 && Frozen[idx].M.LastBurnDamage < Damage)
                     {
                        Frozen[idx].M.LastBurnDamage = Damage;
                        Frozen[idx].M.LastDamagedBy  = instigatedBy;
                        Frozen[idx].M.BurnDown       = Frozen[idx].M.Default.fMaxBurnDown;  
                        Frozen[idx].M.SetBurningBehavior();
                     }
                  }
               }
            }
         }
      }
   }

   if (NextGameRules != None)
      return NextGameRules.NetDamage(OriginalDamage, Damage, injured, instigatedBy, HitLocation, Momentum, DamageType);

   return 0;
}

function int FrozenIndex(FalkMonster M, bool bCreate)
{
   local int i;

   for (i=0; i < Frozen.length; ++i)
   {
      if (Frozen[i].M == M)
         return i;
   }

   if (bCreate)
   {
      Frozen.insert(i, 1);
      Frozen[i].M      = M;
      Frozen[i].FreezeThreshold = GetFreezeThreshold(M);
      //Frozen[i].DeltaTime = 0.0;

      //warn("Adding New Zed:"@M.MenuName@"Index:"@i);
      return i;
   }

   return -1;
}


// attempt to compute one zed every tick instead of looping through all of them since that approach looks prone to bugs
function Tick(float DeltaTime)
{
   // this stuff shouldn't happen on the client anyway
   if (Level.NetMode == NM_Client || Frozen.Length == 0)
      return; 

   //warn("FROZEN ZEDS:"@Frozen.Length);

   // update the rep, wait for a given time, then reset the index to zero
   if (fCurrentIndex >= Frozen.length)
   {
      // update the replication right now, so we don't miss the opportunity while waiting
      if (FreezePrecision == Default.FreezePrecision)
      {
         if (fFrozenCount != FreezeRI.FrozenCount)
         {
            FreezeRI.FrozenCount   = fFrozenCount;
            FreezeRI.NetUpdateTime = Level.TimeSeconds - 1;

            //warn("fCurrentIndex:"@fCurrentIndex@"Count:"@fFrozenCount@"Time:"@Level.TimeSeconds);
         }

         // adaptive precision
         if (Frozen.Length > MaxZedsDefault)
         {
            FreezePrecision = MaxZedsDefault / Frozen.Length * Default.FreezePrecision;
            //warn("Adaptive:"@FreezePrecision);
         }
      }

      // wait for a bit after computing the last zed in the list
      if (FreezePrecision > 0)
         FreezePrecision -= DeltaTime;

      // restart from the beginning of the list
      else
      {
         FreezePrecision = Default.FreezePrecision;
         fCurrentIndex   = 0;
         fFrozenCount    = 0;
      }
   }

   // wait for reset, then do your freezing stuff
   else
   {
      FreezePrecision    = Default.FreezePrecision;

      // delayed freeze mechanics and whatever we normally do with frozen zeds
      if (Frozen[fCurrentIndex].M != none && Frozen[fCurrentIndex].M.Health > 0)
      {
         // this zed reached the freezing threshold, start freezing here
         if (Frozen[fCurrentIndex].fShouldBeFrozen)
         {
            //warn("Start Freezing Zed:"@Frozen[fCurrentIndex].M.Menuname@"Idx:"@fCurrentIndex@"Time:"@Level.TimeSeconds);

            Frozen[fCurrentIndex].fShouldBeFrozen = False;
            FreezeZed(Frozen[fCurrentIndex].M, fCurrentIndex);
         }

         // FreezeZed asked to delay actual freeze, do it now.
         else if (Frozen[fCurrentIndex].PendingFreeze && Level.TimeSeconds >= Frozen[fCurrentIndex].M.fAnimEndTime + FreezeDelay)
         {
            //warn("Setting bFrozen to True:"@Frozen[fCurrentIndex].M.Menuname@"Idx:"@fCurrentIndex@"Time:"@Level.TimeSeconds@"AnimEnd:"@Frozen[fCurrentIndex].M.fAnimEndTime);

            Frozen[fCurrentIndex].PendingFreeze = False;
            Frozen[fCurrentIndex].M.FalkFreezeZed();
            Frozen[fCurrentIndex].bFrozen       = True;
            Frozen[fCurrentIndex].M.SetOverlayMaterial(FreezeRI.FrozenMaterialTwo, 999, true);
         }

         // this zed is already frozen
         else if (Frozen[fCurrentIndex].bFrozen)
         {
            if (Frozen[fCurrentIndex].M.Controller != none)
            {
               // prevent rotating
               Frozen[fCurrentIndex].M.Controller.FocalPoint = Frozen[fCurrentIndex].FocalPoint; 

               if (Frozen[fCurrentIndex].M.Controller.Enemy != none)
               {
                  Frozen[fCurrentIndex].Enemy = Frozen[fCurrentIndex].M.Controller.Enemy;
                  Frozen[fCurrentIndex].M.Controller.Enemy = none;
               }

               Frozen[fCurrentIndex].M.Controller.Focus = none;
            }

            if (Level.TimeSeconds >= Frozen[fCurrentIndex].WarmTime)
               UnfreezeZed(Frozen[fCurrentIndex].M, fCurrentIndex);

            else
               fFrozenCount++;
         }

         // reset current freeze after a while
         else if (Frozen[fCurrentIndex].CurrentFreeze > 0 && Frozen[fCurrentIndex].LastHitTime + ResetTime <= Level.TimeSeconds)
         {
            //warn("Reset:"@Frozen[fCurrentIndex].M.Menuname@"Idx:"@fCurrentIndex@"Time:"@Level.TimeSeconds);

            Frozen[fCurrentIndex].CurrentFreeze = 0;
         }

         fCurrentIndex++;
      }

      // actually remove dead zeds
      else
      {
         //warn("Remove:"@Frozen[fCurrentIndex].M.Menuname@"Idx:"@fCurrentIndex@"Time:"@Level.TimeSeconds);

         Frozen.Remove(fCurrentIndex, 1);
      }
   }
}

// this doesn't really freeze zeds but stops their animations, putting them into a pending state
function FreezeZed(FalkMonster M, int FrozenIndex)
{
   M.fFrozen          = True;
   M.fIsStunned       = False;
   M.Velocity         = M.PhysicsVolume.Gravity;

   M.SetGroundSpeed(0);
   M.SetOverlayMaterial(FreezeRI.FrozenMaterial, 999, true);
   Frozen[FrozenIndex].FreezeTime    = Level.TimeSeconds;
   Frozen[FrozenIndex].FocalPoint    = M.Location + 512*vector(M.Rotation);

   // clot shouldn't grab people while frozen
   if (FalkZombieClot(M) != none && FalkZombieClot(M).bGrappling)
   {
      if (FalkZombieClot(M).DisabledPawn != none)
      {
         FalkZombieClot(M).DisabledPawn.bMovementDisabled = false;
         FalkZombieClot(M).DisabledPawn = none;
      }
   }

   if (M.Controller != none)
   {
      M.Controller.FocalPoint = Frozen[FrozenIndex].FocalPoint;

      if (M.Controller.Enemy != none)
      {
         Frozen[FrozenIndex].Enemy = M.Controller.Enemy;
         M.Controller.Enemy = none;
      }

      else if (Pawn(M.Controller.Focus) != none)
         Frozen[FrozenIndex].Enemy = Pawn(M.Controller.Focus);

      M.Controller.Focus = none;

      KFMonsterController(M.Controller).bUseFreezeHack = True;

      if (!M.Controller.IsInState('WaitForAnim'))
         M.Controller.GoToState('WaitForAnim');        
   }

   M.Acceleration = vect(0, 0, 0);

   M.fFreezeFix();

   if (Level.NetMode != NM_Client)
   {
      // the zed isn't attacking, freeze it now
      if (Level.TimeSeconds >= M.fAnimEndTime + FreezeDelay)
      {

         if (Level.NetMode == NM_ListenServer)
         {
            //warn("Starting local instant freeze:"@FrozenIndex);
            Frozen[FrozenIndex].M.fAnimEndTime = Level.TimeSeconds + 0.05;
            Frozen[FrozenIndex].PendingFreeze = True;
         }

         else
         {
            //warn("Freezing zed now:"@FrozenIndex);
            M.FalkFreezeZed();
            Frozen[FrozenIndex].bFrozen = True;
            M.SetOverlayMaterial(FreezeRI.FrozenMaterialTwo, 999, true);
         }
      }

      // set pending freeze state
      else
      {
         //warn("Delayed Freeze start:"@FrozenIndex@"AnimEnd:"@Frozen[FrozenIndex].M.fAnimEndTime);
         Frozen[FrozenIndex].PendingFreeze = True;
      }
   }
}

// warm em' up
function UnfreezeZed(FalkMonster M, int FrozenIndex)
{
   if (M == none)
      M = Frozen[FrozenIndex].M;

   if (M == none || M.Controller == none || M.Health <= 0)
      return;

   //warn("Unfreezing:"@fCurrentIndex@"Time:"@Level.TimeSeconds);

   Frozen[FrozenIndex].bFrozen       = False;
   Frozen[FrozenIndex].PendingFreeze = False;
   Frozen[FrozenIndex].CurrentFreeze = 0;
   //Frozen[FrozenIndex].DeltaTime     = 0.0;

   if (Frozen[FrozenIndex].Enemy != none)
   {
      //warn("Enemy:"@Frozen[FrozenIndex].Enemy);

      M.Controller.FocalPoint = Frozen[FrozenIndex].Enemy.Location; //how about Enemy location?
      M.Controller.Enemy      = Frozen[FrozenIndex].Enemy;
      M.Controller.Focus      = Frozen[FrozenIndex].Enemy;

      if (FMonsterController(M.Controller) != none)
         FMonsterController(M.Controller).fStoredEnemy = Frozen[FrozenIndex].Enemy;
   }

   M.SetOverlayMaterial(none, 0.1, true);

   if (M.HeadRadius < 0)
      M.HeadRadius = -M.HeadRadius; //enable headshots

   M.bShotAnim    = false;
   M.bWaitForAnim = false;
   M.Enable('AnimEnd');
   M.fFrozen      = false;
   M.bStunned     = false;
   M.fIsStunned   = false;
   class'FreezeReplicationInfo'.static.RestoreAnimations(M);
   KFMonsterController(M.Controller).WhatToDoNext(99);
   M.Controller.GotoState('ZombieHunt');

   if (Frozen[FrozenIndex].Enemy == none)
   {
      M.Controller.Enemy = Frozen[FrozenIndex].Enemy;
      M.Controller.Focus = Frozen[FrozenIndex].Enemy;
   }

   M.fUnfreezeFix();
}

// return warm time for different zed, now with boosted metal clot stats
function float GetWarmTime(FalkMonster M)
{
   if (FalkZombieClot(M) != none)
      return ClotWarmTime;

   else if (FalkZombieCrawler(M) != none)
      return CrawlerWarmTime;

   else if (FalkZombieGorefast(M) != none)
      return GorefastWarmTime;

   else if (FalkZombieStalker(M) != none)
      return StalkerWarmTime;

   else if (FalkZombieBloat(M) != none)
      return BloatWarmTime;

   else if (FalkZombieHusk(M) != none)
      return HuskWarmTime;

   else if (FalkZombieSiren(M) != none)
      return SirenWarmTime;

   else if (FalkZombieBrute(M) != none)
      return BruteWarmTime;

   else if (FalkZombieScrake(M) != none)
      return ScrakeWarmTime;

   else if (FalkZombieFleshpound(M) != none)
      return FleshpoundWarmTime;

   else if (FalkZombieBoss(M) != none)
      return BossWarmTime;

   //log("Warning: using fallback WarmTime.");
   return FallbackWarmTime;
}


// return freeze threshold for different zed, now with boosted metal clot resist
function int GetFreezeThreshold(KFMonster M)
{
   if (FalkZombieClot(M) != none)
      return ClotFreezeThreshold;

   else if (FalkZombieCrawler(M) != none)
      return CrawlerFreezeThreshold;

   else if (FalkZombieGorefast(M) != none)
      return GorefastFreezeThreshold;

   else if (FalkZombieStalker(M) != none)
      return StalkerFreezeThreshold;

   else if (FalkZombieBloat(M) != none)
      return BloatFreezeThreshold;

   else if (FalkZombieHusk(M) != none)
      return HuskFreezeThreshold;

   else if (FalkZombieSiren(M) != none)
      return SirenFreezeThreshold;

   else if (FalkZombieBruteBase(M) != none)
      return BruteFreezeThreshold;

   else if (FalkZombieScrake(M) != none)
      return ScrakeFreezeThreshold;

   else if (FalkZombieFleshPound(M) != none)
      return FleshpoundFreezeThreshold;

   else if (FalkZombieBoss(M) != none)
      return BossFreezeThreshold;

   //log("Warning: using fallback FreezeThreshold.");
   return FallbackFreezeThreshold;
}

defaultproperties
{
   //ShatteredIce=Class'IceChunkEmitter'
   // all of the following properties are overridden by "FalkFreezeRulesCfg.ini"
   ClotWarmTime=3.000000
   GorefastWarmTime=3.000000
   CrawlerWarmTime=3.000000
   StalkerWarmTime=3.000000
   BloatWarmTime=2.500000
   HuskWarmTime=2.500000
   SirenWarmTime=2.500000
   BruteWarmTime=2.500000
   ScrakeWarmTime=2.000000
   FleshpoundWarmTime=2.000000
   BossWarmTime=1.500000
   FallbackWarmTime=3.00000
   ResetTime=5.000000
   FreezeResistance=3     
   ClotFreezeThreshold=5
   GorefastFreezeThreshold=5
   CrawlerFreezeThreshold=5
   StalkerFreezeThreshold=5
   BloatFreezeThreshold=10
   HuskFreezeThreshold=10
   SirenFreezeThreshold=10
   BruteFreezeThreshold=10
   ScrakeFreezeThreshold=20
   FleshpoundFreezeThreshold=20
   BossFreezeThreshold=40
   FallbackFreezeThreshold=10
   FreezeDelay=0.2
   FreezePrecision=0.05
   FreezeMaxLag=0.2
}
