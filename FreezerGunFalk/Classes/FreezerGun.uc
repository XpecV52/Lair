class FreezerGun extends KFWeapon;

#exec OBJ LOAD FILE=IJC_Project_Santa_A.ukx

var ScriptedTexture MyScriptedTexture;
var string MyMessage;
var   Font MyFont;
var   Font SmallMyFont;
var  color MyFontColor;

var int OldValue;
var localized   string  ReloadMessage;
var localized   string  EmptyMessage;

var array<Material> GlowSkins; // 0 - glow off, length-1 - all lights on

var byte              fTry;                 // How many times we tried to spawn a pickup
var byte              fMaxTry;              // How many times we should try to spawn a pickup
var vector            fCZRetryLoc;          // Z Correction we apply every retry
var vector            fCXRetryLoc;          // X Correction we apply every retry
var vector            fCYRetryLoc;          // Y Correction we apply every retry
var vector            fACZRetryLoc;         // computed Z correction
var vector            fACXRetryLoc;         // computed X correction
var vector            fACYRetryLoc;         // computed Y correction

var float             FBringUpTime;         // last time bringup was called
var float             FBringUpSafety;       // safety to prevent reload on server before the weapon is ready

simulated final function SetTextColor( byte R, byte G, byte B )
{
   MyFontColor.R = R;
   MyFontColor.G = G;
   MyFontColor.B = B;
   MyFontColor.A = 255;
}


simulated function RenderOverlays( Canvas Canvas )
{
   if( MagAmmoRemaining <= 0 )
   {
      if( OldValue!=-5 )
      {
         OldValue = -5;
         MyFont = SmallMyFont;
         SetTextColor(218,18,18);
         MyMessage = EmptyMessage;
         ++MyScriptedTexture.Revision;
         GlowOff();
      }
   }
   else if( bIsReloading )
   {
      if( OldValue!=-4 )
      {
         OldValue = -4;
         MyFont = SmallMyFont;
         SetTextColor(30,38,43);
         MyMessage = ReloadMessage;
         ++MyScriptedTexture.Revision;
      }
   }
   else if( OldValue!=AmmoAmount(0)*100+MagAmmoRemaining )
   {
      OldValue = AmmoAmount(0)*100+MagAmmoRemaining;
      MyFont = SmallMyFont;

      //if ((MagAmmoRemaining ) <= (MagCapacity/2))
      //	SetTextColor(32,60,77);
      if ( MagAmmoRemaining < 30 )
         SetTextColor(224,44,56);
      else
         SetTextColor(30,38,43);
      MyMessage = MagAmmoRemaining$" / " $ (AmmoAmount(0) - MagAmmoRemaining);
      ++MyScriptedTexture.Revision;
      // glowstage
      GlowStage(MagAmmoRemaining/15 + 1);
   }

   MyScriptedTexture.Client = Self;
   Super.RenderOverlays(Canvas);
   MyScriptedTexture.Client = None;
}

simulated function RenderTexture( ScriptedTexture Tex )
{
   local int w, h;

   // Ammo     - ( w / 2 )    - ( h / 1.2 )
   Tex.TextSize( MyMessage, MyFont, w, h );
   Tex.DrawText( ( Tex.USize / 2.4) - ( w / 2 ), (Tex.VSize / 2 ) - ( h / 2 ),MyMessage, MyFont, MyFontColor );
}


simulated function bool ConsumeAmmo( int Mode, float Load, optional bool bAmountNeededIsMax )
{
   local Inventory Inv;
   local bool bOutOfAmmo;
   local KFWeapon KFWeap;

   if ( Load > 0 )
      Load = FireMode[Mode].AmmoPerFire;
   if ( Super(Weapon).ConsumeAmmo(Mode, Load, bAmountNeededIsMax) )
   {
      if ( Load > 0 && (Mode == 0 || bReduceMagAmmoOnSecondaryFire) ) {
         MagAmmoRemaining -= Load; // Changed from "MagAmmoRemaining--"  -- PooSH
         if ( MagAmmoRemaining < 0 )
            MagAmmoRemaining = 0;
      }
      if ( MagAmmoRemaining == 0 )
         GlowOff();

      NetUpdateTime = Level.TimeSeconds - 1;

      if ( FireMode[Mode].AmmoPerFire > 0 && InventoryGroup > 0 && !bMeleeWeapon && bConsumesPhysicalAmmo
            && (Ammo[0] == none || FireMode[0] == none || FireMode[0].AmmoPerFire <= 0 || Ammo[0].AmmoAmount < FireMode[0].AmmoPerFire)
            && (Ammo[1] == none || FireMode[1] == none || FireMode[1].AmmoPerFire <= 0 || Ammo[1].AmmoAmount < FireMode[1].AmmoPerFire) )
      {
         bOutOfAmmo = true;

         for ( Inv = Instigator.Inventory; Inv != none; Inv = Inv.Inventory )
         {
            KFWeap = KFWeapon(Inv);

            if ( Inv.InventoryGroup > 0 && KFWeap != none && !KFWeap.bMeleeWeapon && KFWeap.bConsumesPhysicalAmmo &&
                  ((KFWeap.Ammo[0] != none && KFWeap.FireMode[0] != none && KFWeap.FireMode[0].AmmoPerFire > 0 &&KFWeap.Ammo[0].AmmoAmount >= KFWeap.FireMode[0].AmmoPerFire) ||
                   (KFWeap.Ammo[1] != none && KFWeap.FireMode[1] != none && KFWeap.FireMode[1].AmmoPerFire > 0 && KFWeap.Ammo[1].AmmoAmount >= KFWeap.FireMode[1].AmmoPerFire)) )
            {
               bOutOfAmmo = false;
               break;
            }
         }

         if ( bOutOfAmmo )
         {
            PlayerController(Instigator.Controller).Speech('AUTO', 3, "");
         }
      }

      return true;
   }
   return false;
}

simulated function bool StartFire(int Mode)
{
   if( Mode == 1 )
      return super.StartFire(Mode);

   if( !super.StartFire(Mode) )  // returns false when mag is empty
      return false;

   if( AmmoAmount(0) <= 0 )
   {
      return false;
   }

   AnimStopLooping();

   if( !FireMode[Mode].IsInState('FireLoop') && (AmmoAmount(0) > 0) )
   {
      FireMode[Mode].StartFiring();
      return true;
   }
   else
   {
      return false;
   }

   return true;
}
simulated function AnimEnd(int channel)
{
   if(!FireMode[1].IsInState('FireLoop'))
   {
      Super.AnimEnd(channel);
   }
}

simulated function Fire(float F)
{
   if( MagAmmoRemaining < FireMode[0].AmmoPerFire && !bIsReloading &&
         FireMode[0].NextFireTime <= Level.TimeSeconds )
   {
      // We're dry, ask the server to autoreload
      ServerRequestAutoReload();

      PlayOwnedSound(FireMode[0].NoAmmoSound,SLOT_None,2.0,,,,false);
   }

   super.Fire(F);
}

simulated function ClientFinishReloading()
{
   bIsReloading = false;

   // Reload animation longs 4.5s while weapon becomes ready in 3.6s
   // Continue animation for last 0.9s if player is not shooting
   // -- PooSH
   //PlayIdle();
   SetTimer(0.9, false);

   if(Instigator.PendingWeapon != none && Instigator.PendingWeapon != self)
      Instigator.Controller.ClientSwitchToBestWeapon();
}

simulated function GlowOn()
{
   GlowStage(255);
   Skins[0] = Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr';
}

simulated function GlowOff()
{
   GlowStage(0);
   Skins[0] = Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr_off';
}

simulated function GlowStage(byte Stage)
{
   Skins[0] = GlowSkins[min(Stage,GlowSkins.length-1)];
}

simulated function Timer()
{
   if ( ClientState == WS_ReadyToFire )
      PlayIdle();
   else
      super.Timer();
}

// don't do weird unwanted switches on pickup
simulated function ClientWeaponSet(bool bPossiblySwitch)
{
   local int Mode;

   Instigator = Pawn(Owner);

   bPendingSwitch = bPossiblySwitch;

   if( Instigator == None )
   {
      GotoState('PendingClientWeaponSet');
      return;
   }

   for( Mode = 0; Mode < NUM_FIRE_MODES; Mode++ )
   {
      if( FireModeClass[Mode] != None )
      {
         // laurent -- added check for vehicles (ammo not replicated but unlimited)
         if( ( FireMode[Mode] == None ) || ( FireMode[Mode].AmmoClass != None ) && !bNoAmmoInstances && Ammo[Mode] == None && FireMode[Mode].AmmoPerFire > 0 )
         {
            GotoState('PendingClientWeaponSet');
            return;
         }
      }

      FireMode[Mode].Instigator = Instigator;
      FireMode[Mode].Level = Level;
   }

   ClientState = WS_Hidden;
   GotoState('Hidden');

   if( Level.NetMode == NM_DedicatedServer || !Instigator.IsHumanControlled() )
      return;

   if( Instigator.Weapon == self || Instigator.PendingWeapon == self ) // this weapon was switched to while waiting for replication, switch to it now
   {
      if (Instigator.PendingWeapon != None)
         Instigator.ChangedWeapon();
      else
         BringUp();
      return;
   }

   if( Instigator.PendingWeapon != None && Instigator.PendingWeapon.bForceSwitch )
      return;

   if ( Instigator.Weapon == None)
   {
      Instigator.PendingWeapon = self;
      Instigator.ChangedWeapon();
   }

   else if (Frag(Instigator.Weapon) != None)
   {
      Instigator.PendingWeapon = self;
      Instigator.Weapon.PutDown();
   }
}

// just don't
simulated function DoAutoSwitch(){}

// use a cluster-like quad to spawn the pickup so it hopefully doesn't get eaten by the void
function DropFrom(vector StartLocation)
{
   local int                  m;
   local Pickup               Pickup;
   local byte                 fAttempt;
   local vector               fTempPos;
   local vector               Direction;

   if (!bCanThrow)
      return;

   for (m = 0; m < NUM_FIRE_MODES; m++)
   {
      // if _RO_
      if( FireMode[m] == none )
         continue;
      // End _RO_

      if (FireMode[m].bIsFiring)
         StopFire(m);
   }

	if (Instigator != None)
		Direction = vector(Instigator.GetViewRotation());

	else if (Pawn(Owner) != none)
		Direction = vector(Pawn(Owner).GetViewRotation());

   Pickup = Spawn(PickupClass,,, StartLocation);

   // Try to spawn remaining clusters
   while (Pickup == None && fTry < fMaxTry)
   {
      fTry++;
      fAttempt = 0;

      while (Pickup == None && fAttempt < 27)
      { 
         fAttempt++; // we don't even test 0 since we're here for a reason

         if (fAttempt >= 27)
         {
            //warn("FAIL"@fTry);
            fACZRetryLoc += fCZRetryLoc;
            fACXRetryLoc += fCXRetryLoc;
            fACYRetryLoc += fCYRetryLoc;
         }

         else if (fAttempt >= 18)
         {
            fTempPos = FClusterQuad(StartLocation - fACZRetryLoc, fAttempt - 18);
            //warn("Third:"@fAttempt-18@"Location:"@fTempPos);
         }

         else if (fAttempt >= 9)
         {
            fTempPos = FClusterQuad(StartLocation + fACZRetryLoc, fAttempt - 9);
            //warn("Second:"@fAttempt-9@"Location:"@fTempPos);
         }

         else
         {
            fTempPos = FClusterQuad(StartLocation, fAttempt);
            //warn("First:"@fAttempt@"Location:"@fTempPos);
         }

         Pickup = Spawn(PickupClass,,, fTempPos);
      }
   }

   fTry         = 0;
   fACZRetryLoc = fCZRetryLoc;
   fACXRetryLoc = fCXRetryLoc;
   fACYRetryLoc = fCYRetryLoc;

   
   if (Pickup != None)
   {
      ClientWeaponThrown();

      if (Instigator != None)
         DetachFromPawn(Instigator);

      Pickup.InitDroppedPickupFor(self);
      Pickup.Velocity = Direction * 450.0f + Vect(0, 0, 300);

      if (Instigator.Health > 0)
         WeaponPickup(Pickup).bThrown = true;

      Destroy();
   }
}

// used to spawn the pickup in a quad
simulated function Vector FClusterQuad(Vector fSLocation, byte fAttempt)
{
   Switch (fAttempt)
   {
      case 0:
         return fSLocation;

      case 1:
         return fSLocation + fACXRetryLoc;

      case 2:
         return fSLocation - fACXRetryLoc;

      case 3:
         return fSLocation + fACYRetryLoc;

      case 4:
         return fSLocation - fACYRetryLoc;

      case 5:
         return fSLocation + fACXRetryLoc + fACYRetryLoc;

      case 6:
         return fSLocation - fACXRetryLoc + fACYRetryLoc;

      case 7:
         return fSLocation + fACXRetryLoc - fACYRetryLoc;

      case 8:
         return fSLocation - fACXRetryLoc - fACYRetryLoc;
   }
}

// setting bringup time
simulated function BringUp(optional Weapon PrevWeapon)
{
   if (Level.NetMode == NM_DedicatedServer)
      FBringUpTime = Level.TimeSeconds;

   Super(KFWeapon).BringUp(PrevWeapon);
}


// block reload on bringup
simulated function bool AllowReload()
{
   if (Level.TimeSeconds <= FBringUpTime + BringUpTime + FBringUpSafety)
      return False;

   return Super.AllowReload();
}

defaultproperties
{
   MyScriptedTexture=ScriptedTexture'IJC_Project_Santa_A.Counter.Counter_Scripted'
   MyFont=Font'IJCFonts.DigitalBig'
   SmallMyFont=Font'IJCFonts.DigitalMed'
   MyFontColor=(B=177,G=148,R=76,A=255)
   ReloadMessage="REL"
   EmptyMessage="---"
   GlowSkins(0)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr_off'
   GlowSkins(1)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr_1'
   GlowSkins(2)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr_2'
   GlowSkins(3)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr_3'
   GlowSkins(4)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr'
   MagCapacity=40
   ReloadRate=3.600000
   ReloadAnim="Reload"
   ReloadAnimRate=0.950000 // higher means faster
   WeaponReloadAnim="Reload_M7A3"
   HudImage=Texture'IJC_Project_Santa_A.HUD.FreezerGun_Unselected'
   SelectedHudImage=Texture'IJC_Project_Santa_A.HUD.FreezerGun'
   Weight=6.000000
   bHasAimingMode=True
   IdleAimAnim="Idle_Iron"
   StandardDisplayFOV=65.000000
   bModeZeroCanDryFire=True
   SleeveNum=3
   TraderInfoTexture=Texture'IJC_Project_Santa_A.HUD.Trader_FreezerGun'
   bIsTier3Weapon=True
   PlayerIronSightFOV=70.000000
   ZoomedDisplayFOV=40.000000
   FireModeClass(0)=Class'FreezerFire'
   FireModeClass(1)=Class'KFMod.NoFire'
   PutDownAnim="PutDown"
   BringUpTime=0.400000
   SelectSound=Sound'KF_NailShotgun.Handling.KF_NailShotgun_Pickup'
   SelectForce="SwitchToAssaultRifle"
   AIRating=0.550000
   CurrentRating=0.550000
   bShowChargingBar=True
   Description="The N600 is a cryogenic device developed by Horzine Tech. It's capable of freezing zeds for a short time, making them weaker to all sources of damage. Can penetrate with a good efficiency."
   EffectOffset=(X=100.000000,Y=25.000000,Z=-10.000000)
   DisplayFOV=65.000000
   Priority=25
   InventoryGroup=4
   GroupOffset=15
   PickupClass=Class'FreezerPickup'
   PlayerViewOffset=(X=25.000000,Y=23.000000,Z=-5.000000)
   BobDamping=4.500000
   AttachmentClass=Class'FreezerAttachment'
   IconCoords=(X1=245,Y1=39,X2=329,Y2=79)
   ItemName="Cryo Mass Driver N600"
   Mesh=SkeletalMesh'IJC_Project_Santa_A.FreezerGun'
   Skins(0)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_shdr'
   Skins(1)=Shader'IJC_Project_Santa_A.Counter.Counter_Shdr'
   Skins(2)=Shader'IJC_Project_Santa_A.FreezerGun.FreezerGun_Sight_shdr'
   TransientSoundVolume=1.250000
   fMaxTry=3
   FBringUpSafety=0.1
   fCZRetryLoc=(Z=25.000000)
   fCXRetryLoc=(X=25.000000)
   fCYRetryLoc=(Y=25.000000)
   fACZRetryLoc=(Z=25.000000)
   fACXRetryLoc=(X=25.000000)
   fACYRetryLoc=(Y=25.000000)
}
