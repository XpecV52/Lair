class Falk689GameTypeBase extends KFMod.KFGameType;

const fNopeHash =  "20b300195d48c2ccc2651885cfea1a2f"; 

var int     FWaveBonusNRM;       // bonus when we survive at normal difficulty
var int     FWaveBonusHRD;       // bonus when we survive at hard difficulty
var int     FWaveBonusSUI;       // bonus when we survive at suicidal difficulty
var int     FWaveBonusHOE;       // bonus when we survive at hell on earth difficulty
var int     FWaveBonusBB;        // bonus when we survive at bloodbath difficulty
var int     FLastWaveBonusNRM;   // last wave bonus, how much to award before the pat wave at normal difficulty
var int     FLastWaveBonusHRD;   // last wave bonus, how much to award before the pat wave at hard difficulty
var int     FLastWaveBonusSUI;   // last wave bonus, how much to award before the pat wave at suicidal difficulty
var int     FLastWaveBonusHOE;   // last wave bonus, how much to award before the pat wave at hell on earth difficulty
var int     FLastWaveBonusBB;    // last wave bonus, how much to award before the pat wave at bloodbath difficulty
var int     FStartingCashNRM;    // starting cash at normal difficulty
var int     FStartingCashHRD;    // starting cash at hard difficulty
var int     FStartingCashSUI;    // starting cash at suicidal difficulty
var int     FStartingCashHOE;    // starting cash at hell on earth difficulty
var int     FStartingCashBB;     // starting cash at bloodbath difficulty
var int     FWaveAddCashNRM;     // how much cash we add every wave for late joiners at normal difficulty
var int     FWaveAddCashHRD;     // how much cash we add every wave for late joiners at hard difficulty
var int     FWaveAddCashSUI;     // how much cash we add every wave for late joiners at suicidal difficulty
var int     FWaveAddCashHOE;     // how much cash we add every wave for late joiners at hell on earth difficulty
var int     FWaveAddCashBB;      // how much cash we add every wave for late joiners at bloodbath difficulty
var int     FLastWaveAddCashNRM; // additional cash we add the last wave for late joiners at normal difficulty
var int     FLastWaveAddCashHRD; // additional cash we add the last wave for late joiners at hard difficulty
var int     FLastWaveAddCashSUI; // additional cash we add the last wave for late joiners at suicidal difficulty
var int     FLastWaveAddCashHOE; // additional cash we add the last wave for late joiners at hell on earth difficulty
var int     FLastWaveAddCashBB;  // additional cash we add the last wave for late joiners at bloodbath difficulty
var int     FAlivePlayers;       // how many players are still breathing
var int     FSkipVoters;         // how many players have voted to skip this trader
var int     FZedDebugID;         // debug ID, used to make log more readable when multiple zeds log at the same time
var float   fZedTimeEnd;         // when the last zedtime ended
var float   fLastFDeleteTime;    // when we deleted the last flare
var float   fLastKilledZedTimer; // timer, restarted after a zed is killed, when zero we should increase fNoZedHeal and fNoZedWeld and eventually stop farming money
var int     fMaxNoZedHeal;       // maximum number of no zed heals before blocking money reward
var int     fMaxNoZedWeld;       // maximum number of no zed welded armor points before blocking money reward
var int     fCurrentFlares;      // how many are currently active in the map
var int     FCurrentWave;        // current wave used to set dosh properly in trader time
var int     fSStalkerSkills;     // amount of alive players sharing the shared unlimited stalker viewing skill
var bool    fChangeCurWave;      // prevents changing current wave multiple times
var bool    fFakeZedTime;        // fake zed time, does the same shit as the other one but doesn't trigger skills or extensions
var bool    fFakeNoSound;        // fake zed time without sounds, only for short ones
var bool    fShouldSkipTrader;   // if true, we delay a skip trader
var bool    fBossWaveStarted;    // if true, we started the boss wave
var float   fSkipTraderDelay;    // amount of delay to skip the trader
var string  fThrowPlayerMsg;     // message when the trader is skipped with only one player alive
var string  fThrowMaxMsg;        // message when the trader is skipped when more than one players are alive

// zed time skills array struct
struct fZedTimeSkillUsed
{
   var PlayerController PC;              // used to identify the player when we can't use SteamID (on disconnect)
   var string           SteamID;         // used to identify the player
   var bool             SkillUsed;       // have we used our zed time skill?
   var bool             Camping;         // are we camping?
   var bool             Disconnected;    // have we disconnected from the server?
   var bool             ShouldRes;       // should we reset this zed time skill?
   var bool             ExitedThisWave;  // we disconnected this wave, used for the dosh system
   var bool             DiedThisWave;    // we died this wave, used for the perk change system
   var bool             CanChangePerk;   // can this dude change perk?
   var bool             WantsTraderSkip; // does this dude want a trader skip?
   var bool             fShareStalkers;  // shared stalker visibility status
   //var bool             fGotDosh;        // have we got at least a dosh already in this game?
   var int              fNoZedHeal;      // if we don't kill a zed for a set time, increase this number to stop people from farming money while healing
   var int              fNoZedWeld;      // if we don't kill a zed for a set time, increase this number to stop people from farming money while welding
   var int              zTExtUsed;       // how many zed time extensions we used
   var int              dWave;           // disconnect wave (corresponds to WaveNum in trader time and WaveNum + 1 during a wave)
   var int              fDWave;          // disconnect wave (corresponds to FCurrentWave)
   var int              LastPlayedWave;  // last wave that we played
   var int              Dosh;            // current dosh
   var int              FPatHit;         // pat hit index
   var int              fSpawnWave;      // spawned from spectator this wave
   var byte             nextWeap;        // next weapon to equip after syringe is used
   var localized string fVetName;        // current veterancy type, used to allow perk change if you died and haven't changed before
};

var transient array<fZedTimeSkillUsed> fZedUsed;

replication
{
	reliable if(Role == ROLE_Authority)
      fZedTimeEnd, FAlivePlayers, fChangeCurWave, fFakeZedTime, fFakeNoSound, fBossWaveStarted, fCurrentFlares;
}

event InitGame(string Options, out string Error)
{
   Super.InitGame(Options, Error);

   if (Level.Game.GameDifficulty >= 8.0)       // BB
      StartingCash = FStartingCashBB;

   else if (Level.Game.GameDifficulty >= 7.0)  // HOE
      StartingCash = FStartingCashHOE;

   else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
      StartingCash = FStartingCashSUI;

   else if (Level.Game.GameDifficulty >= 4.0)  // hard
      StartingCash = FStartingCashHRD;

   else                                        // normal
      StartingCash = FStartingCashNRM;}

// fZedUsed stuff
function int ZedTimeIndex(PlayerController PC, optional byte fStatus)
{
   local int fI;
   local String fID;

   fID = PC.GetPlayerIDHash();

   //warn("current ID: "@fID);

   // don't store anything if we're spectators except on death and don't store stuff with the default hash
   if ((fStatus == 0 && fID == fNopeHash) ||
       ((fStatus == 0 || fStatus == 3) && (PC.Pawn == None  || PC.Pawn.Health < 0 || PC.PlayerReplicationInfo.bOnlySpectator)))
      return -1;

   for (fI=0; fI<fZedUsed.length; fI++) 
   {
      //log("existing ID: "@fZedUsed[i].SteamID);

      if ((fStatus == 1 || fStatus == 2) && fZedUsed[fI].PC == PC) // disconnected or dead
      {
         //log("Disconnected Found");
         return fI;
      }
      
      
      else if (fZedUsed[fI].SteamID == fID)
      {
         //warn("Normal Found");
         //log(fID);
         //log(fZedUsed[fI].SteamID);

         if (fZedUsed[fI].PC == none)
         {
            //warn("setting up new playercontroller");
            fZedUsed[fI].PC = PC;
         }

         if (fStatus == 3 && fZedUsed[fI].ShouldRes) // zed time start
         {
            fZedUsed[fI].ShouldRes = False;
            fZedUsed[fI].SkillUsed = False;
            fZedUsed[fI].zTExtUsed = 0;
         }

         return fI;
      }
   }

   if (fID == fNopeHash)
      return -1;

   fZedUsed.insert(fI, 1);
   fZedUsed[fI].PC             = PC;
   fZedUsed[fI].SteamID        = fID; 
   fZedUsed[fI].Disconnected   = True;
   fZedUsed[fI].fSpawnWave     = -1;
   fZedUsed[fI].LastPlayedWave = -1;
   fZedUsed[fI].CanChangePerk  = True;

   if (Level.Game.GameDifficulty >= 8.0)       // BB
      fZedUsed[fI].Dosh = FStartingCashBB;

   else if (Level.Game.GameDifficulty >= 7.0)  // HOE
      fZedUsed[fI].Dosh = FStartingCashHOE;

   else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
      fZedUsed[fI].Dosh = FStartingCashSUI;

   else if (Level.Game.GameDifficulty >= 4.0)  // hard
      fZedUsed[fI].Dosh = FStartingCashHRD;

   else                                        // normal
      fZedUsed[fI].Dosh = FStartingCashNRM;

   fZedUsed[fI].dWave          = -1;
   fZedUsed[fI].fDWave         = -1;
   fZedUsed[fI].fVetName       = "None";

   if (KFPlayerController(PC) != none && KFPlayerController(PC).SelectedVeterancy != None)
      fZedUsed[fI].fVetName    = KFPlayerController(PC).SelectedVeterancy.Default.VeterancyName;

   //log("Adding new player: "@fID@" - ID: "@fI);

   return fI;
}


// reset zed time skills
function ZedTimeEndReset()
{
   local int i;

   //warn("ZedTimeReset");

   while (i < fZedUsed.length)
   {
      fZedUsed[i].ShouldRes = True;
      i++;
   }

   //warn("ZedTimeReset STOP");
}


// Return true if we can use our zed time skill
function bool CanUseZedTimeSkill(PlayerController P, float ZTECD, float ZTSCD)
{
   local int i;

   if (InZedTime())
   {
      i = ZedTimeIndex(P);

      if (i > -1                                        &&
         !fZedUsed[i].SkillUsed                         &&
         Level.TimeSeconds >= fZedTimeEnd + ZTECD       &&
         Level.TimeSeconds >= LastZedTimeEvent + ZTSCD)
         return True;
   }

   return false;
}


// We used the zed time skill and set it to True
function ZedTimeSkillUsed(PlayerController P)
{
   local int i;

   i = ZedTimeIndex(P);

   if (i > -1)
      fZedUsed[i].SkillUsed = True;
}


// Set Camping state
simulated function SetCamping(PlayerController P, bool fCamp)
{
   local int i;

   i = ZedTimeIndex(P);

   if (i > -1)
      fZedUsed[i].Camping = fCamp;
}


// return True if the pawn is camping
simulated function bool IsCamping(Pawn P)
{
   local int i;
   local PlayerController PC;

   PC = PlayerController(P.Controller);

   if (PC != none)
   {
      i = ZedTimeIndex(PC);

      if (i > -1)
         return fZedUsed[i].Camping;
   }

   return false;
}


// return True if we're in zedtime (or the perk should still be)
function bool InZedTime()
{
   return (bZEDTimeActive && !fFakeZedTime);
}

// return True if we have a zed time extension and increase zTExtUsed
function bool HasZedTimeExtension(PlayerController P, KFPlayerReplicationInfo KFPRI, class<KFVeterancyTypes> Vet)
{
   local int i;

   i = ZedTimeIndex(P);

   if (i > -1 && Vet.static.ZedTimeExtensions(KFPRI) > fZedUsed[i].zTExtUsed)
   {
      fZedUsed[i].zTExtUsed++;
      //log(i @ fZedUsed[i].zTExtUsed);
      return True;
   }

   return false;
}


// return torch battery drain
function int GetTorchBatteryDrain(KFWeapon fWeapon)
{
   return 10;
}


// return dosh amount for this player or -1 if not found
function int FalkGetDosh(string fID)
{
   local int i;
   local int fDosh;
   local int fDWave;

   if (fID == fNopeHash)
      return -1;

   //log(fID);

   for (i=0; i<fZedUsed.length; i++) 
   {
      if (fZedUsed[i].SteamID == fID) 
      {
         //warn("Player found");
         fDosh = fZedUsed[i].Dosh;

         //warn("Stored dosh: "@fDosh);

         if (fZedUsed[i].Disconnected) // add some cash if we reconnect on a later wave
         {
            fZedUsed[i].Disconnected = False;

            fDWave = Max(0, fZedUsed[i].dWave);

            //warn("ID:"@fID@"Disconnected dosh:"@fDosh@"- dwave:"@fDWave@"- cwave:"@WaveNum);

            if (WaveNum > fDWave && !fZedUsed[i].ExitedThisWave)
            {
               fDosh += FalkGetWaveDosh(fDWave);
               //warn("Additional dosh:"@FalkGetWaveDosh(fDWave));
            }

            fZedUsed[i].ExitedThisWave = False;
         }

         //warn("Total dosh: "@fDosh);
         return Max(0, fDosh);
      }
   }

   //log("Player not found");
   return -1;
}

// set dosh amount for this player
function FalkSetDosh(PlayerController P, int fAmount, optional byte fDisconnect)
{
   local int i;//, FCurW;

   i = ZedTimeIndex(P, fDisconnect);

   //log("index: "@i@" - len: "@fZedUsed.Length);

   if (i > -1 && i < fZedUsed.Length)
   {  
      // we killed, healed, welded or got dosh from an assist, store it
      /*if (fDisconnect == 4)
      {
         warn("Idx:"@i@"- got dosh.");
         fZedUsed[i].fGotDosh = true;
      }*/

      // this was intended as a fix for a bug that apparently doesn't exist, keeping it until confirming it really was a ghost
      /*if (fZedUsed[i].LastPlayedWave > -1)
         fAmount = FStartingCash;*/

      //warn("Setting:"@fAmount);

      if (fDisconnect == 1 || fDisconnect == 2) // we died or disconnected
      {
         //warn("WaveNum: "@WaveNum@"Currently Stored Dosh: "@fZedUsed[i].Dosh@" - Current Amount: "@fAmount);

         if (fDisconnect == 1 || !bWaveInProgress) // we disconnected or died in trader time
         {

            if (!bWaveInProgress)
               fZedUsed[i].dWave     = WaveNum;

            else
               fZedUsed[i].dWave     = WaveNum + 1;

            fZedUsed[i].fDWave       = FCurrentWave;

            //warn("Setting"@i@"Disconnect or in trader wave:"@fZedUsed[i].dWave);
            
            fZedUsed[i].Disconnected = True;

            if (!bWaveInProgress)
               fZedUsed[i].ExitedThisWave  = True;
         }

         else  
         {
            fZedUsed[i].DiedThisWave       = True;

            if (KFPlayerController(fZedUsed[i].PC) != none && KFPlayerController(fZedUsed[i].PC).SelectedVeterancy != None)
               fZedUsed[i].fVetName = KFPlayerController(fZedUsed[i].PC).SelectedVeterancy.Default.VeterancyName;

            //warn(P@"Died");
         }
      }

      if (fDisconnect == 4)
      {
         fZedUsed[i].Dosh += Max(0, fAmount);
         //log("Adding: "@fAmount@" to "@i@" dosh, total: "@fZedUsed[i].Dosh@" - Pawn: "@P.Pawn);
      }

      else
      {
         //log("Setting "@i@" Dosh to: "@fAmount@" - Pawn: "@P.Pawn);
         fZedUsed[i].Dosh = Max(0, fAmount);
      }
   }
}


// return additional dosh for a new player based on current wave
function int FalkGetWaveDosh(optional int fStartWave)
{
   local int result;
   local int FUsedWaveAddCash;

   if (Level.Game.GameDifficulty >= 8.0)       // BB
      FUsedWaveAddCash = FWaveAddCashBB;

   else if (Level.Game.GameDifficulty >= 7.0)  // HOE
      FUsedWaveAddCash = FWaveAddCashHOE;

   else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
      FUsedWaveAddCash = FWaveAddCashSUI;

   else if (Level.Game.GameDifficulty >= 4.0)  // hard
      FUsedWaveAddCash = FWaveAddCashHRD;

   else                                        // normal
      FUsedWaveAddCash = FWaveAddCashNRM; 

   //warn("WaveNum:"@WaveNum@"- fStartWave:"@fStartWave);

   result = FUsedWaveAddCash * Max(0, WaveNum - fStartWave);

   if (FCurrentWave >= FinalWave)
   {
      if (Level.Game.GameDifficulty >= 8.0)       // BB
         result += FLastWaveAddCashBB;

      else if (Level.Game.GameDifficulty >= 7.0)  // HOE
         result += FLastWaveAddCashHOE;

      else if (Level.Game.GameDifficulty >= 5.0)  // suicidal
         result += FLastWaveAddCashSUI;

      else if (Level.Game.GameDifficulty >= 4.0)  // hard
         result += FLastWaveAddCashHRD;

      else                                        // normal
         result += FLastWaveAddCashNRM;
   }

   return result;
}

// code resides into the falkgametype
function SetUnlocked(PlayerController PC, byte idx, bool uStatus)
{
}

// This is here so I can call this typecasting Level.Game as Falk689GameTypeBase
function int GetAlivePlayers()
{
   return 1;
}

// return True if we should store dosh on disconnect --- doesn't work for some reason, removing it to see how shit works without it
/*function bool fShouldStoreDoshOnDisconnect(PlayerController PC)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC, 1);

      if (i > -1)
      {
         if (fZedUsed[i].fGotDosh)
         {
            warn("Idx:"@i@"- Should store");
            return True;
         }

         warn("LastPlayedWave:"@fZedUsed[i].LastPlayedWave@"fDWave"@fZedUsed[i].fDWave@"FCurrentWave"@FCurrentWave@"Result:"@fZedUsed[i].fSpawnWave > -1 && !(fZedUsed[i].fSpawnWave == FCurrentWave || fZedUsed[i].fDWave >= FCurrentWave));
         return (fZedUsed[i].LastPlayedWave > -1 && !(fZedUsed[i].fSpawnWave == FCurrentWave || fZedUsed[i].fDWave >= WaveNum));
      }
   }

   return False;
}*/

// return True if we already spawned this wave
function bool fGetSpawnedThisWave(PlayerController PC)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC, 1);

      if (i > -1)
      {
         //warn("SW: " @ fZedUsed[i].fSpawnWave @ " -> CW: " @ FCurrentWave @ " | FDW: " @ fZedUsed[i].fDWave @ " -> FCW: "@FCurrentWave);
         return (fZedUsed[i].fSpawnWave == FCurrentWave || fZedUsed[i].fDWave >= WaveNum);
      }

      return true;
   }

   return false;
}

// set spectator spawn wave
function fSetSpawnedThisWave(PlayerController PC)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC);

      if (i > -1)
      {
         fZedUsed[i].fSpawnWave = FCurrentWave;
      }
   }
}

// prevent exploits on our respawn system
function RestartPlayer(Controller aPlayer)
{
   if (PlayerController(aPlayer) != none && !fGetSpawnedThisWave(PlayerController(aPlayer)))
      Super.RestartPlayer(aPlayer);
}


// delay on voteskip
event Tick(float DeltaTime)
{
   if (Role == ROLE_Authority && fShouldSkipTrader)
   {
      if (fSkipTraderDelay > 0)
         fSkipTraderDelay -= DeltaTime;

      else
      {
         fSkipTraderDelay  = Default.fSkipTraderDelay;
         fShouldSkipTrader = False;
         FalkSkipTrader();
      }
   }
}

// Vote a trader skip
function bool VoteSkipTrader(PlayerController PC)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC);

      if (i > -1)
      {
         // we already voted, there's no need to check
         if (fZedUsed[i].WantsTraderSkip)
            return False;

         fZedUsed[i].WantsTraderSkip = True;
         FSkipVoters++;

         // check if we can skip the trader already
         if (FSkipVoters >= GetAlivePlayers())
            fShouldSkipTrader = True;

         return True;
      }
   }

   return False;
}

// Skip trader time
exec function FalkSkipTrader()
{
   FSkipVoters = -1;

   if (bTradingDoorsOpen && WaveCountDown > 3)
   {
      WaveCountDown = 3;
      InvasionGameReplicationInfo(GameReplicationInfo).WaveNumber = WaveNum;
   }
}

// give everybody a chance to change perk (and reset skip trader voting)
function FalkEnablePerkSelect()
{
   local int i;

   //warn("Enable");
   FSkipVoters = 0;

   while (i < fZedUsed.length)
   {

      if ((fZedUsed[i].PC != none && fZedUsed[i].PC.Pawn != none) || fZedUsed[i].LastPlayedWave == FCurrentWave - 1)
      {
         //warn(fZedUsed[i].LastPlayedWave @ " " @ FCurrentWave);
         fZedUsed[i].CanChangePerk   = True;
         fZedUsed[i].WantsTraderSkip = False;
         fZedUsed[i].fNoZedHeal      = 0;
         fZedUsed[i].fNoZedWeld      = 0;
      }
      i++;
   }

   //warn("Enable STOP");
}

// set last played wave and reset died this wave state
function FalkSetLastPlayedWave()
{
   local int i;

   //warn("SetLastPlayedWave");

   while (i < fZedUsed.length)
   {
      if (fZedUsed[i].ExitedThisWave && fZedUsed[i].LastPlayedWave + 1 < FCurrentWave)
      {
         //warn("RESET ExitedThisWave FOR ID:"@i);
         fZedUsed[i].ExitedThisWave = False;
      }

      if (fZedUsed[i].PC != none && fZedUsed[i].PC.Pawn != none)
         fZedUsed[i].LastPlayedWave = FCurrentWave;

      fZedUsed[i].DiedThisWave   = False;
      //warn(fZedUsed[i].PC@"Reset");

      i++;
   }

   //warn("SetLastPlayedWave STOP");
}


// revoke the chance to change perk
function FalkDisablePerkSelect(optional PlayerController PC)
{
   local int i;
   //warn("Disable");
   if (PC != none)
   {
      //warn(PC);
      i = ZedTimeIndex(PC);

      if (i > -1)
         fZedUsed[i].CanChangePerk = False;
   }

   else
   {
      while (i < fZedUsed.length)
      {
         fZedUsed[i].CanChangePerk = False;
         i++;
      }
   }

   //warn("Disable STOP");
}

// return true if the specified player is allowed to change perk
function bool CanChangePerk(PlayerController PC)
{
   local int i;
   local string fVName;

   //warn("CanChangePerk");

   if (PC != none)
   {
      fVName = "Falk689";
      i      = ZedTimeIndex(PC);

      if (KFPlayerController(PC) != none)
      {
         // if we had no perk, let us select one regardless
         if (KFPlayerReplicationInfo(PC.PlayerReplicationInfo).ClientVeteranSkill == none)
            return True;

         fVName = KFPlayerController(PC).SelectedVeterancy.Default.VeterancyName;
      }
         

      if (i > -1)
      {
         //warn("Normal Check:"@fZedUsed[i].CanChangePerk@"Died This Wave:"@fZedUsed[i].DiedThisWave@"Old Vet Name:"@fZedUsed[i].fVetName@"Current Vet Name:"@fVName);

         if (fZedUsed[i].CanChangePerk || (fZedUsed[i].DiedThisWave && fZedUsed[i].fVetName == fVName))
         {
            if (fVName == "Falk689")
               fVName = "None";

            //if (!fZedUsed[i].CanChangePerk)
               //warn(fZedUsed[i].PC@"New System");

            //warn(fZedUsed[i].PC@"Setting Vet Name to:"@fVName);

            fZedUsed[i].DiedThisWave = False;
            fZedUsed[i].fVetName     = fVName;
            return True;
         }

         return False;
      }

      //warn("Default to True");
      return True;
   }

   //warn("No PlayerController");

   return False;
}

// attempt to remove some log spam
function ShowPathTo(PlayerController P, int TeamNum)
{
    if (KFGameReplicationInfo(GameReplicationInfo).CurrentShop == none)
        return;

    KFGameReplicationInfo(GameReplicationInfo).CurrentShop.InitTeleports();

    if (KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList.Length > 0  &&
        KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList[0] != None  &&
        P.FindPathToward(KFGameReplicationInfo(GameReplicationInfo).CurrentShop.TelList[0], false) != None)
    {
        Spawn(class'RedWhisp', P,, P.Pawn.Location);
    }
}

// this will return if we can change perk once per downtime
function bool FalkAllowOnePerkChange()
{
   return False;
}

// used to update the unlimited stalker visibility from classes located before our veterancy types - code resides in Falk689GameType
function ExtStalkerViewShareUpdate(PlayerController PC, class<KFVeterancyTypes> KFVet)
{
}

// this should update the zed time unlimited stalker visibility on one player, preparing for the skill to actually be triggered
function StalkerViewShareUpdate(PlayerController PC, bool fSharing)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC, 4);

      if (i > -1)
      {
         //warn("Setting Stalker Sharing to:"@fSharing);
         fZedUsed[i].fShareStalkers = fSharing;
      }
   }
}

// called on death to check and eventually decrease how many share the unlimited shared zed time stalker skill
function DecStalkerViewSharers(PlayerController PC)
{
   local int i;

   if (PC != none)
   {
      i = ZedTimeIndex(PC, 4);

      //warn("DECREASE:"@i);

      if (i > -1 && fZedUsed[i].fShareStalkers)
      {
         fSStalkerSkills--;
         //warn("Decreasing counter:"@fSStalkerSkills);
      }
   }
}

// return how many share the unlimited shared zed time stalker skill - code resides in Falk689GameType
function int GetStalkerViewSharers()
{
   return 0;
}


// a flare was spawned, update the counter - code resides in Falk689GameType
function FlareSpawned(PlayerController PC)
{
}

// a flare was destroyed, update the counter - code resides in Falk689GameType
function FlareDestroyed(PlayerController PC)
{
}

// increase the ID value and return it for a new zed
function int FalkGetZedDebugID()
{
   FZedDebugID++;

   return FZedDebugID;
}

// check if we should reward this medic and, eventually, increase its heal count
function bool ShouldRewardMedic(PlayerController PC)
{
   local int i;

   if (fLastKilledZedTimer <= 0)
   {
      if (PC != none)
      {
         i = ZedTimeIndex(PC);

         if (i > -1)
         {
            if (fZedUsed[i].fNoZedHeal >= fMaxNoZedHeal)
            {
               //warn("Idx:"@i@"False:"@Level.TimeSeconds);
               return false;
            }

            fZedUsed[i].fNoZedHeal++;
            //warn("Idx:"@i@"Count:"@fZedUsed[i].fNoZedHeal@"Max:"@fMaxNoZedHeal@"True:"@Level.TimeSeconds);
         }
      }
   }

   /*else
   {
      warn(fLastKilledZedTimer@"Seconds of timer left:"@Level.TimeSeconds);
   }


   warn("Return True"@Level.TimeSeconds);*/

   return true;
}

// tweak the weld reward based on how many points we can cash on, increase the count accordingly
function int GetSupportReward(PlayerController PC, int fWeldPoints)
{
   local int i;
   local int reward;

   reward = fWeldPoints;

   if (fLastKilledZedTimer <= 0)
   {
      if (PC != none)
      {
         i = ZedTimeIndex(PC);

         if (i > -1)
         {
            // no reward
            if (fZedUsed[i].fNoZedWeld >= fMaxNoZedWeld)
            {
               reward = 0;
            }

            // partial reward
            else if (fZedUsed[i].fNoZedWeld + fWeldPoints > fMaxNoZedWeld)
            {
               reward = fMaxNoZedWeld - fZedUsed[i].fNoZedWeld;
               fZedUsed[i].fNoZedWeld = fMaxNoZedWeld;
            }

            // total reward
            else
               fZedUsed[i].fNoZedWeld += fWeldPoints;


            //warn("Idx:"@i@"Reward:"@reward@"Time"@Level.TimeSeconds);
         }
      }
   }

   /*else
   {
      warn(fLastKilledZedTimer@"Seconds of timer left:"@Level.TimeSeconds);
   }

   warn("Return"@reward@"Time:"@Level.TimeSeconds);*/

   return reward;
}

// reset healed and welded points with no zeds kills for everyone
function ResetNoZedVars()
{
   local int i;

   //warn("RESET:"@Level.TimeSeconds);

   while (i < fZedUsed.length)
   {
      fZedUsed[i].fNoZedHeal = 0;
      fZedUsed[i].fNoZedWeld = 0;
      i++;
   }

   //warn("ZedTimeReset STOP");
}

defaultproperties
{
   FLastWaveBonusNRM=500
   FLastWaveBonusHRD=400
   FLastWaveBonusSUI=300
   FLastWaveBonusHOE=200
   FLastWaveBonusBB=100
   FWaveBonusNRM=250
   FWaveBonusHRD=200
   FWaveBonusSUI=150
   FWaveBonusHOE=100
   FWaveBonusBB=50
   FStartingCashNRM=500
   FStartingCashHRD=400
   FStartingCashSUI=300
   FStartingCashHOE=200
   FStartingCashBB=100
   FWaveAddCashNRM=150
   FWaveAddCashHRD=120
   FWaveAddCashSUI=90
   FWaveAddCashHOE=60
   FWaveAddCashBB=30
   FLastWaveAddCashNRM=150
   FLastWaveAddCashHRD=120
   FLastWaveAddCashSUI=90
   FLastWaveAddCashHOE=60
   FLastWaveAddCashBB=30
   fLastKilledZedTimer=30.0
   fMaxNoZedHeal=10
   fMaxNoZedWeld=50
   fSkipTraderDelay=1.5
   MapPrefix="KFL"
   Acronym="KFL"
   MapListType="Falk689GameTypeBase.FMapList"
   GameName="DO NOT SELECT ME"
   Description="Base structure to make 'Lair Single Player' work. Do not select this dud, but the single player game type."
   ScreenShotName="LairTextures_T.Thumbnails.DONOTSELECTME"
}
