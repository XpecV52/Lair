class VeterancyTypesFalk extends Falk689ServerPerks.FVeterancyTypes
   abstract;

// Custom damage fix
static function int ReduceDamage(KFPlayerReplicationInfo KFPRI, KFPawn Injured, Pawn Instigator, int InDamage, class<DamageType> DmgType)
{
   if (class<DamTypeFreezerGun>(DmgType) != none || class<DamTypeAcidClusterFalk>(DmgType) != none)
      return 0;

   return InDamage;
}

// Add NadeFalk
static function class<Grenade> GetNadeType(KFPlayerReplicationInfo KFPRI)
{
	return class'NadeFalk';
}

// HuskGun value fix
static function float GetCostScaling(KFPlayerReplicationInfo KFPRI, class<Pickup> Item)
{
   if (Item == class'HuskGunPickup')
      return Default.huskDiv;

   return 1.0;
}

defaultproperties
{
}
