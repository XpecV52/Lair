class FNeckshots extends SRCustomProgressInt;

function NotifyPlayerKill(Pawn Killed, class<DamageType> damageType)
{	
	/*if((FZombieClot_STANDARD(Killed)          != None && FZombieClot_STANDARD(Killed).DecapitatedBy       == FZombieClot_STANDARD(Killed).LastDamagedBy       && FZombieClot_STANDARD(Killed).DecapitatedByMelee)       ||
	   (FZombieMetalClot_STANDARD(Killed)     != None && FZombieMetalClot_STANDARD(Killed).DecapitatedBy  == FZombieMetalClot_STANDARD(Killed).LastDamagedBy  && FZombieMetalClot_STANDARD(Killed).DecapitatedByMelee)  ||
	   (FZombieGorefast_STANDARD(Killed)      != None && FZombieGorefast_STANDARD(Killed).DecapitatedBy   == FZombieGorefast_STANDARD(Killed).LastDamagedBy   && FZombieGorefast_STANDARD(Killed).DecapitatedByMelee)   ||
      (FZombieCrawler_STANDARD(Killed)       != None && FZombieCrawler_STANDARD(Killed).DecapitatedBy    == FZombieCrawler_STANDARD(Killed).LastDamagedBy    && FZombieCrawler_STANDARD(Killed).DecapitatedByMelee)    ||
      (FZombieStalker_STANDARD(Killed)       != None && FZombieStalker_STANDARD(Killed).DecapitatedBy    == FZombieStalker_STANDARD(Killed).LastDamagedBy    && FZombieStalker_STANDARD(Killed).DecapitatedByMelee)    ||
      (FZombieSiren_STANDARD(Killed)         != None && FZombieSiren_STANDARD(Killed).DecapitatedBy      == FZombieSiren_STANDARD(Killed).LastDamagedBy      && FZombieSiren_STANDARD(Killed).DecapitatedByMelee)      ||
	   (FZombieBloat_STANDARD(Killed)         != None && FZombieBloat_STANDARD(Killed).DecapitatedBy      == FZombieBloat_STANDARD(Killed).LastDamagedBy      && FZombieBloat_STANDARD(Killed).DecapitatedByMelee)      ||
	   (FZombieBrute_STANDARD(Killed)         != None && FZombieBrute_STANDARD(Killed).DecapitatedBy      == FZombieBrute_STANDARD(Killed).LastDamagedBy      && FZombieBrute_STANDARD(Killed).DecapitatedByMelee)      ||
	   (FZombieHusk_STANDARD(Killed)          != None && FZombieHusk_STANDARD(Killed).DecapitatedBy       == FZombieHusk_STANDARD(Killed).LastDamagedBy       && FZombieHusk_STANDARD(Killed).DecapitatedByMelee)       ||
      (FZombieScrake_STANDARD(Killed)        != None && FZombieScrake_STANDARD(Killed).DecapitatedBy     == FZombieScrake_STANDARD(Killed).LastDamagedBy     && FZombieScrake_STANDARD(Killed).DecapitatedByMelee)     ||
      (FZombieFleshpound_STANDARD(Killed)    != None && FZombieFleshpound_STANDARD(Killed).DecapitatedBy == FZombieFleshpound_STANDARD(Killed).LastDamagedBy && FZombieFleshpound_STANDARD(Killed).DecapitatedByMelee) ||
      (FZombieBoss_STANDARD(Killed)          != None && FZombieBoss_STANDARD(Killed).DecapitatedBy       == FZombieBoss_STANDARD(Killed).LastDamagedBy       && FZombieBoss_STANDARD(Killed).DecapitatedByMelee))
      {
         IncrementProgress(1);
      }*/

	if (FalkMonster(Killed) != None && FalkMonster(Killed).DecapitatedBy == FalkMonster(Killed).LastDamagedBy && FalkMonster(Killed).DecapitatedByMelee)
      IncrementProgress(1);
}

defaultproperties 
{
   ProgressName = "Neckshot kills"
}
