class Falk689SpecWaves extends Mutator
   config(Falk689SpecWavesCfg);

var config float TouchDisableTimeOverride;

struct MClassTypes
{
   var config string MClassName, MID;
};

var config array<MClassTypes> MonsterClasses;
var config string EndGameBossClass;

struct SpecialSquad
{
   var array<string> ZedClass;
   var array<int> NumZeds;
};

var config array<SpecialSquad> /*MetalClotClass,*/ FinalSquads; 
//var config int MetalClotChance;

struct ArrayReplacement
{
   var class<Pickup> oldClass;
   var class<Pickup> newClass;
};

var array<ArrayReplacement> pickupReplaceArray;

function PostBeginPlay()
{
   local int             i;
   local Falk689GameType KF; 

   KF  = Falk689GameType(Level.Game);

   if (KF == none)
   {
      Destroy();
      return;
   }

   // Overriding special events.

   // Overwriting patriarch healing waves
   for(i=0; i<3; i++)
   {
      KF.MonsterCollection.default.FinalSquads[i].ZedClass         = FinalSquads[i].ZedClass;
      KF.MonsterCollection.default.FinalSquads[i].NumZeds          = FinalSquads[i].NumZeds;
   }

   // Overwriting monster classes
   for(i=0; i<9; i++)
   {
      KF.MonsterCollection.default.MonsterClasses[i].MClassName    = MonsterClasses[i].MClassName;
      KF.MonsterCollection.default.MonsterClasses[i].Mid           = MonsterClasses[i].Mid;
      KF.default.MonsterClasses[i].MClassName                      = MonsterClasses[i].MClassName;
      KF.default.MonsterClasses[i].Mid                             = MonsterClasses[i].Mid;
   }

   // overwriting the pat
   KF.MonsterCollection.default.EndGameBossClass = default.EndGameBossClass;
   SetTimer(0.1, false);
}

// replace pickups with falk pickups
function bool CheckReplacement(Actor Other, out byte bSuperRelevant)
{
   local int i;
   local byte x;

   local KFGameType KF;

   KF  = KFGameType(Level.Game);

   if (Other.class == class'KFRandomItemSpawn')
   {	
      //log("Random item spawn replaced");
      ReplaceWith(Other, "Falk689SpecWaves.Falk689RandomItemSpawn");
      return false;
   }

   // ammo replacement
   if (Other.class == class'KFAmmoPickup')
   {
      //log("Ammo pack replaced");
      ReplaceWith(Other, "Falk689WeaponsFix.FalkAmmoPickup");
      return false;
   }

   else if (Pickup(Other) != none && KF.IsInState('MatchInProgress'))
   {
      i = FindPickupReplacementIndex(Pickup(Other));

      if ( i != -1 )
      {
         ReplaceWith(Other, string(pickupReplaceArray[i].newClass));
         return false;
      }

      return true; // no need to replace
   }

   // testing stuff
   else if (ZombieVolume(Other) != none)
   {
      //log("ZombieVolume");

      ZombieVolume(Other).TouchDisableTime = TouchDisableTimeOverride;

	   for(i=0; i<ZombieVolume(Other).DisallowedZeds.Length; i++)
      {
         if (ClassIsChildOf(ZombieVolume(Other).DisallowedZeds[i], class'ZombieBossBase'))
         {
            //log("NO BOSS NO SPARTY");
            x++;
            ZombieVolume(Other).DisallowedZeds[i] = class'FalkZombieBoss';
         }
      }

	   for(i=0; i<ZombieVolume(Other).OnlyAllowedZeds.Length; i++)
      {
         if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieCrawlerBase'))
         {
            //log("J CRAWLIN'");
            x++;
            ZombieVolume(Other).OnlyAllowedZeds[i] = class'FalkZombieCrawler';
         }

         else if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieStalkerBase'))
         {
            //log("F.A.L.K.E.R.");
            x++;
            ZombieVolume(Other).OnlyAllowedZeds[i] = class'FalkZombieStalker';
         }

         else if (ClassIsChildOf(ZombieVolume(Other).OnlyAllowedZeds[i], class'ZombieBossBase'))
         {
            //log("LIKE A BOSS");
            x++;
            ZombieVolume(Other).OnlyAllowedZeds[i] = class'FalkZombieBoss';
         }
      }

      if (x > 0)
         return false;

      return true;
   }


   return true;
}

// returns -1, if not found
function int FindPickupReplacementIndex(Pickup item)
{
   local int i;

   for  (i=0; i<pickupReplaceArray.length; ++i) 
   {
      if (pickupReplaceArray[i].oldClass == item.class) 
         return i;
   }
   return -1;
}


function Timer()
{
   Destroy(); // Needed for webadmin fix
}

defaultproperties
{
   GroupName="KFFalk689Mut"
   FriendlyName="Lair - Special Squads"
   Description="Sends custom specimen waves when the Patriarch heals. Inspired by the work of 'FluX', 'King Sumo' and 'max-tweak'."
   bAddToServerPackages=true
   pickupReplaceArray(0)=(oldClass=class'KFMod.DualiesPickup',newClass=class'Falk689WeaponsFix.MAC10PickupFalk')
   TouchDisableTimeOverride=3.0
}
