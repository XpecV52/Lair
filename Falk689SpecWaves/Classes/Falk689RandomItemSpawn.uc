class Falk689RandomItemSpawn extends KFMod.KFRandomItemSpawn;

// setting different random pickup value 0.5 instead of 0.75
function SpawnPickup()
{
   Super(KFRandomSpawn).SpawnPickup();

   if (KFWeaponPickup(myPickup) != none)
   {
      KFWeaponPickup(myPickup).MySpawner = self;
      KFWeaponPickup(myPickup).SellValue = KFWeaponPickup(myPickup).default.cost * 0.5;
   }
}

// attempt to make random spawns really random
function int GetWeightedRandClass()
{
   return rand(10); 
}




defaultproperties
{
   PickupClasses(0)=Class'Falk689WeaponsFix.FlareRevolverPickupFalk'
   PickupClasses(1)=Class'Falk689WeaponsFix.AshotPickupFalk'
   PickupClasses(2)=Class'Falk689WeaponsFix.Glock18SAPickupFalk'
   PickupClasses(3)=Class'Falk689WeaponsFix.MK23PickupFalk'
   PickupClasses(4)=Class'Falk689WeaponsFix.MachetePickupFalk'
   PickupClasses(5)=Class'KFMod.Vest'
   PickupClasses(6)=Class'Falk689WeaponsFix.MercyPickupFalk'
   PickupClasses(7)=Class'Falk689WeaponsFix.MedicBrowningPickupFalk'
   PickupClasses(8)=Class'Falk689WeaponsFix.TrigunPickupFalk'
   PickupClasses(9)=Class'Falk689WeaponsFix.SparkGunPickupFalk'
   PickupWeight(0)=2
   PickupWeight(1)=2
   PickupWeight(2)=2
   PickupWeight(3)=2
   PickupWeight(4)=1
   PickupWeight(5)=0
   PickupWeight(6)=2
   PickupWeight(7)=2
   PickupWeight(8)=2
   PickupWeight(9)=1
}
